# leases

Evaluate *dhcpd* lease files, collect entries and store them for a time in
the database (default: 28 days).

This data is later plotted as charts on a simple webpage.

## Database

Bootstrap database with `mysql -u root -p`:

Create schema:

```sql
CREATE SCHEMA `leases` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
```

Create user:

```sql
CREATE USER `leases`@`localhost` IDENTIFIED BY 'very-secure';
```

Grant rights:

```sql
GRANT ALL PRIVILEGES ON `leases` . * TO `leases`@`localhost`;

FLUSH PRIVILEGES;
```
