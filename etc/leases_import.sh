#!/usr/bin/env sh

CURL="${CURL:="/usr/local/bin/curl"}"
LOGS="${LOGS:="/dev/null"}"
SERVICE="${SERVICE:=""}"
SOURCES="${SOURCES:="$*"}"
CONTENT=""

[ -z "$SERVICE" ] && { echo "SERVICE missing"; exit 1; }
[ -z "$SOURCES" ] && { echo "SOURCES missing"; exit 1; }

for SOURCE in $SOURCES; do
    PART=$($CURL -fksSL "$SOURCE" 2>> $LOGS)
    [ -n "$PART" ] && {
        CONTENT="${CONTENT}\n${PART}"
    }
done

[ -z "$CONTENT" ] && { echo "no CONTENT"; exit 0; }

$CURL \
    -X POST -H 'Content-Type: text/plain' \
    -d "$CONTENT" "$SERVICE/api/import" \
    2>> $LOGS
exit $?
