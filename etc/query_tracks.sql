SELECT tr.stamp_id, tr.state, tr.ether_id, et.mac_value, et.hostname, tr.bound_id, bn.ip_value
FROM leases.track AS tr
LEFT JOIN leases.bound AS bn ON tr.bound_id = bn.id
LEFT JOIN leases.ether AS et ON tr.ether_id = et.id
ORDER BY tr.stamp_id DESC, tr.state ASC, et.hostname ASC
;
