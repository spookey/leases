#!/bin/sh
#
# PROVIDE: leases
# REQUIRE: LOGIN mysql
# KEYWORD: shutdown
#

. /etc/rc.subr
name=leases
rcvar=${name}_enable

load_rc_config ${name}
: ${leases_enable:="NO"}
: ${leases_user:="duke"}
: ${leases_group:="duke"}
: ${leases_base_folder:="/usr/home/${leases_user}/run/${name}"}
: ${leases_application_jar:="${leases_base_folder}/${name}.jar"}
: ${leases_application_conf:="${leases_base_folder}/application.yaml"}
: ${leases_logging_conf:="${leases_base_folder}/logback-spring.xml"}
: "${leases_restart_delay:=1}"

command="/usr/sbin/daemon"
java="/usr/local/openjdk17/bin/java"
logfile="/var/log/${name}_daemon.log"
pidfile="/var/run/${name}_daemon.pid"
pidfile_child="/var/run/${name}.pid"


start_precmd=${name}_prestart
leases_prestart() {

  install  -o ${leases_user} -m 644 -g ${leases_group} -- /dev/null "${logfile}" \
    || return 1
  install  -o ${leases_user} -m 644 -g ${leases_group} -- /dev/null "${pidfile}" \
    || return 1
  install  -o ${leases_user} -m 644 -g ${leases_group} -- /dev/null "${pidfile_child}" \
    || return 1

  CMDS="${java} -server"
  CMDS="${CMDS} -Dlogging.config=file:${leases_logging_conf}"
  CMDS="${CMDS} -Djava.net.preferIPv4Stack=false"
  CMDS="${CMDS} -Djava.net.preferIPv6Addresses=true"
  CMDS="${CMDS} -jar ${leases_application_jar}"
  CMDS="${CMDS} --spring.config.additional-location=file:${leases_application_conf}"

  rc_flags="-f -t ${name} -o ${logfile} -P ${pidfile} -p ${pidfile_child} -R ${leases_restart_delay} ${CMDS}"
}

start_postcmd=${name}_poststart
leases_poststart() {
  sleep 1
  run_rc_command status
}

status_cmd=${name}_status
leases_status() {
  pid_daemon=$(check_pidfile "${pidfile}" "${command}")
  if [ -z "${pid_daemon}" ]; then
    echo "${name} daemon is not running"
    return 1
  fi
  pid_service=$(check_pidfile "${pidfile_child}" "${java}")
  if [ -z "${pid_service}" ]; then
    echo "${name} service is not running"
    return 1
  fi
  echo "${name} is running..."
  echo "daemon:  ${pid_daemon}"
  echo "service: ${pid_service}"
}

stop_postcmd=${name}_postcmd
leases_postcmd() {
    rm -f -- "${pidfile}"
    rm -f -- "${pidfile_child}"
}

run_rc_command "$1"
