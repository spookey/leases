#!/usr/bin/env sh

# run this from root of repository as `etc/release.sh`

set -e

GIT_CMD=${GIT:="git"}
MVN_CMD=${MVN:="mvn"}

check_cmd() {
    cmd="$1";
    if ! command -v "$cmd" > /dev/null 2>&1; then
        echo "no such command:" "$cmd"
        exit 1
    fi
}

check_cmd "$GIT_CMD"
check_cmd "$MVN_CMD"

$GIT_CMD push
$MVN_CMD clean release:prepare
$GIT_CMD push
$GIT_CMD push --tags
$MVN_CMD clean release:clean

exit 0
