package su.toor.leases.components;

import static net.logstash.logback.argument.StructuredArguments.kv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.shared.Linkage;
import su.toor.leases.shared.Require;

@Component
public class Accessor {

    private static final Logger LOG = LoggerFactory.getLogger(Accessor.class);

    static final String FIELD_PATH = "path";

    private final BasicConfig basicConfig;

    @Autowired
    public Accessor(
        final BasicConfig basicConfig
    ) {
        this.basicConfig = basicConfig;
    }

    static List<String> readText(final Path path) {
        final var location = Linkage.verified(path, false, FIELD_PATH);
        try (final var reader = Files.newBufferedReader(location)) {
            return reader.lines()
                .collect(Collectors.toList());
        } catch (final IOException ex) {
            LOG.error("cannot read file [{}]", kv(FIELD_PATH, path), ex);
            return List.of();
        }
    }

    public static List<String> readText(final String string) {
        final var content = Require.notEmpty(string, "string");
        try (final var reader = new BufferedReader(new StringReader(content))) {
            return reader.lines()
                .collect(Collectors.toList());
        } catch (final IOException ex) {
            LOG.error("cannot read string", ex);
            return List.of();
        }
    }

    @NonNull
    List<Path> getLeaseFiles() {
        return basicConfig.leaseFiles()
            .stream()
            .map(path -> Linkage.verified(path, false, "leaseFiles"))
            .collect(Collectors.toList());
    }

    @NonNull
    public List<String> loadLeaseFiles() {
        return getLeaseFiles()
            .stream()
            .map(Accessor::readText)
            .flatMap(List::stream)
            .collect(Collectors.toList());
    }
}
