package su.toor.leases.components;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import su.toor.leases.components.core.Batch;
import su.toor.leases.components.core.Changes;
import su.toor.leases.database.StampEntity;
import su.toor.leases.parse.Lease;
import su.toor.leases.parse.LeaseParser;
import su.toor.leases.services.query.EntityService;

@Component
public class Importer {

    private static final Logger LOG = LoggerFactory.getLogger(Importer.class);

    static final String FIELD_STAMP = "stamp";

    private final EntityService entityService;
    private final Changes changes;

    @Autowired
    public Importer(
        final EntityService entityService,
        final Changes changes
    ) {
        this.entityService = entityService;
        this.changes = changes;
    }

    Batch importLeases(
        final List<Lease> leases
    ) {
        final var batch = new Batch(
            entityService.latestStamp()
                .orElse(null)
        );

        if (leases.isEmpty()) {
            return batch;
        }

        final var currStampEntity = entityService.createStamp();
        LOG.info("importing leases [{}]", keyValue(FIELD_STAMP, currStampEntity));

        final var trackEntities = leases.stream()
            .map(lease -> entityService.createTrack(
                currStampEntity,
                entityService.ensureEther(lease.getMacAddress(), lease.getHostname()),
                entityService.ensureBound(lease.getIpAddress()),
                lease.getState()
            ))
            .collect(Collectors.toSet());

        return batch.populate(
            currStampEntity,
            trackEntities
        );
    }

    public Optional<StampEntity> run(final List<String> lines) {
        final var batch = importLeases(LeaseParser.parse(lines));
        changes.perform(batch);

        return Optional.ofNullable(batch.getCurrStampEntity());
    }
    public Optional<StampEntity> run(final String text) {
        return run(Accessor.readText(text));
    }
}
