package su.toor.leases.components;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import su.toor.leases.components.spring.CounterBean;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.database.StampEntity;
import su.toor.leases.services.query.EntityService;

@Component
public class Scrubber {

    private static final Logger LOG = LoggerFactory.getLogger(Scrubber.class);

    static final String FIELD_AMOUNT = "amount";
    static final String FIELD_STAMP = "stamp";

    private final BasicConfig basicConfig;
    private final CounterBean counterBean;
    private final EntityService entityService;

    @Autowired
    public Scrubber(
        final BasicConfig basicConfig,
        final CounterBean counterBean,
        final EntityService entityService
    ) {
        this.basicConfig = basicConfig;
        this.counterBean = counterBean;
        this.entityService = entityService;
    }

    Pair<Long, Long> removeStampEntities(final Collection<StampEntity> stampEntities) {
        final var total = stampEntities.size();
        final var success = stampEntities.stream()
            .map(stampEntity -> {
                if (entityService.deleteStamp(stampEntity.getUuid())) {
                    return 1L;
                }

                LOG.warn("failed to remove stamp [{}]", keyValue(FIELD_STAMP, stampEntity));
                return 0L;
            })
            .reduce(0L, Long::sum);

        return Pair.of(success, total - success);
    }

    Optional<OffsetDateTime> keepLimit() {
        return Optional.of(basicConfig.getKeepDays())
            .filter(days -> days > 0)
            .map(days -> OffsetDateTime
                .now(ZoneOffset.UTC)
                .minusDays(days)
            );
    }

    Set<StampEntity> locateOutdatedStamps() {
        final var optionalLimit = keepLimit();
        if (optionalLimit.isEmpty()) {
            return Set.of();
        }

        return entityService.findStampBefore(optionalLimit.get());
    }

    public void run() {
        final var outdated = locateOutdatedStamps();
        if (outdated.isEmpty()) {
            return;
        }

        LOG.info("scrubbing outdated stamps [{}]", keyValue(FIELD_AMOUNT, outdated.size()));
        final var counts = removeStampEntities(outdated);
        counterBean.addPurgeOutdatedSuccess(counts.getFirst());
        counterBean.addPurgeOutdatedFailed(counts.getSecond());
    }
}
