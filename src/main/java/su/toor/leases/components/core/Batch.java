package su.toor.leases.components.core;

import java.util.Optional;
import java.util.Set;

import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.TrackEntity;

public class Batch {

    @Nullable
    private final StampEntity prevStampEntity;
    @Nullable
    private final StampEntity currStampEntity;
    @Nullable
    private final Set<TrackEntity> trackEntities;

    Batch(
        final StampEntity prevStampEntity,
        final StampEntity currStampEntity,
        final Set<TrackEntity> trackEntities
    ) {
        this.prevStampEntity = prevStampEntity;
        this.currStampEntity = currStampEntity;
        this.trackEntities = trackEntities;
    }
    public Batch(
        final StampEntity prevStampEntity
    ) {
        this(prevStampEntity, null, null);
    }

    public Batch populate(
        final StampEntity currStampEntity,
        final Set<TrackEntity> trackEntities
    ) {
        return new Batch(
            prevStampEntity,
            currStampEntity,
            trackEntities
        );
    }

    @Nullable
    public StampEntity getPrevStampEntity() {
        return prevStampEntity;
    }

    @Nullable
    public StampEntity getCurrStampEntity() {
        return currStampEntity;
    }

    @NonNull
    public Set<TrackEntity> getTrackEntities() {
        return trackEntities != null
            ? Set.copyOf(trackEntities)
            : Set.of();
    }

    public boolean isComplete() {
        return prevStampEntity != null && currStampEntity != null;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
            .append("prevStampEntity", getPrevStampEntity())
            .append("currStampEntity", getCurrStampEntity())
            .append(
                "size",
                Optional.ofNullable(trackEntities)
                    .map(Set::size)
                    .orElse(0)
            )
            .toString();
    }
}
