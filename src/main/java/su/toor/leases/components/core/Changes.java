package su.toor.leases.components.core;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.services.MessageService;
import su.toor.leases.services.query.AlertEntityService;
import su.toor.leases.shared.State;

@Component
public class Changes {

    private static final Logger LOG = LoggerFactory.getLogger(Changes.class);

    static final String FIELD_BATCH = "batch";

    final AtomicInteger guideCounter = new AtomicInteger(0);

    private final BasicConfig basicConfig;
    private final AlertEntityService alertEntityService;
    private final MessageService messageService;

    @Autowired
    public Changes(
        final BasicConfig basicConfig,
        final AlertEntityService alertEntityService,
        final MessageService messageService
    ) {
        this.basicConfig = basicConfig;
        this.alertEntityService = alertEntityService;
        this.messageService = messageService;
    }

    boolean shouldHandleGuides() {
        final var value = guideCounter.decrementAndGet();
        if (value <= 0) {
            guideCounter.set(basicConfig.getGuidesEvery());
            return true;
        }

        return false;
    }

    void handleEthers(final Batch batch) {
        final var hiddenState = basicConfig.isHiddenState();
        final var prevStampEntity = batch.getPrevStampEntity();

        Extract.mapEthers(
            alertEntityService.findAlertEthersEnabled(),
            batch.getTrackEntities()
        ).forEach((alertEtherEntity, trackEntities) -> trackEntities.forEach(currTrackEntity -> {
                      final var currState = currTrackEntity.getState();

                      alertEntityService.findTrackWithoutState(prevStampEntity, alertEtherEntity.getEtherEntity(), currState)
                          .map(TrackEntity::getState)
                          .map(prevState -> prevState.change(currState, hiddenState))
                          .filter(direction -> !State.Direction.NONE.equals(direction))
                          .ifPresent(direction -> messageService.notify(alertEtherEntity, currState));
                  })
        );
    }

    void handleBounds(final Batch batch) {
        final var hiddenState = basicConfig.isHiddenState();
        final var prevStampEntity = batch.getPrevStampEntity();

        Extract.mapBounds(
            alertEntityService.findAlertBoundsEnabled(),
            batch.getTrackEntities()
        ).forEach((alertBoundEntity, trackEntities) -> trackEntities.forEach(currTrackEntity -> {
            final var currState = currTrackEntity.getState();

            alertEntityService.findTrackWithoutState(prevStampEntity, alertBoundEntity.getBoundEntity(), currState)
                .map(TrackEntity::getState)
                .map(prevState -> prevState.change(currState, hiddenState))
                .filter(direction -> !State.Direction.NONE.equals(direction))
                .ifPresent(direction -> messageService.notify(alertBoundEntity, currState));
        }));
    }

    void handleGuides(final Batch batch) {
        if (!shouldHandleGuides()) {
            return;
        }
        final var hiddenState = basicConfig.isHiddenState();

        Extract.mapGuides(
            alertEntityService.findAlertGuidesEnabled(),
            batch.getTrackEntities()
        ).forEach((alertGuideEntity, trackEntities) -> {
            final var guideEntity = alertGuideEntity.getGuideEntity();
            final var counts = Extract.countTracks(guideEntity, hiddenState, trackEntities);

            final var high = counts.getOrDefault(State.ACTIVE, 0L);
            final var down = (
                counts.getOrDefault(State.BACKUP, 0L)
                + counts.getOrDefault(State.FREE, 0L)
            );
            messageService.notify(alertGuideEntity, high, down);
        });
    }

    public void perform(final Batch batch) {
        Optional.ofNullable(batch)
            .map(Batch::isComplete)
            .filter(complete -> complete)
            .ifPresent(complete -> {
                LOG.info("detecting changes [{}]", keyValue(FIELD_BATCH, batch));
                handleEthers(batch);
                handleBounds(batch);
                handleGuides(batch);
            });
    }
}
