package su.toor.leases.components.core;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.database.alert.AlertGuideEntity;
import su.toor.leases.shared.State;

public final class Extract {
    private Extract() {}

    public static Map<AlertEtherEntity, Set<TrackEntity>> mapEthers(
        @NonNull final Collection<AlertEtherEntity> alertEtherEntities,
        @NonNull final Collection<TrackEntity> trackEntities
    ) {
        if (alertEtherEntities.isEmpty() || trackEntities.isEmpty()) {
            return Map.of();
        }

        return alertEtherEntities.stream()
            .collect(Collectors.toMap(
                alertEtherEntity -> alertEtherEntity,
                alertEtherEntity -> trackEntities.stream()
                    .filter(trackEntity -> {
                        final var etherEntity = alertEtherEntity.getEtherEntity();
                        return etherEntity.equals(trackEntity.getEtherEntity());
                    })
                    .collect(Collectors.toSet())
            ));
    }

    public static Map<AlertBoundEntity, Set<TrackEntity>> mapBounds(
        @NonNull final Collection<AlertBoundEntity> alertBoundEntities,
        @NonNull final Collection<TrackEntity> trackEntities
    ) {
        if (alertBoundEntities.isEmpty() || trackEntities.isEmpty()) {
            return Map.of();
        }

        return alertBoundEntities.stream()
            .collect(Collectors.toMap(
                alertBoundEntity -> alertBoundEntity,
                alertBoundEntity -> trackEntities.stream()
                    .filter(trackEntity -> {
                        final var boundEntity = alertBoundEntity.getBoundEntity();
                        return boundEntity.equals(trackEntity.getBoundEntity());
                    })
                    .collect(Collectors.toSet())
            ));
    }

    public static Map<AlertGuideEntity, Set<TrackEntity>> mapGuides(
        @NonNull final Collection<AlertGuideEntity> alertGuideEntities,
        @NonNull final Collection<TrackEntity> trackEntities
    ) {
        if (alertGuideEntities.isEmpty() || trackEntities.isEmpty()) {
            return Map.of();
        }

        return alertGuideEntities.stream()
            .collect(Collectors.toMap(
                alertGuideEntity -> alertGuideEntity,
                alertGuideEntity -> filterTracks(alertGuideEntity.getGuideEntity(), trackEntities)
            ));
    }

    public static Set<TrackEntity> filterTracks(
        @NonNull final GuideEntity guideEntity,
        @NonNull final Collection<TrackEntity> trackEntities
    ) {
        return trackEntities.stream()
            .filter(trackEntity -> {
                final var boundEntity = trackEntity.getBoundEntity();
                final var mask = guideEntity.getMask();

                return mask.contains(boundEntity.getIp());
            })
            .collect(Collectors.toSet());
    }

    public static Map<State, Long> countTracks(
        @NonNull final GuideEntity guideEntity,
        @Nullable final Boolean hiddenState,
        @NonNull final Collection<TrackEntity> trackEntities
    ) {
        if (trackEntities.isEmpty()) {
            return Map.of();
        }

        return State.items(hiddenState).stream()
            .collect(Collectors.toMap(
                state -> state,
                state -> filterTracks(guideEntity, trackEntities).stream()
                    .filter(trackEntity -> state.equals(trackEntity.getState()))
                    .count()
            ));
    }
}
