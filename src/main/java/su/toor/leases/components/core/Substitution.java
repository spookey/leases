package su.toor.leases.components.core;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.util.PropertyPlaceholderHelper;
import su.toor.leases.shared.Require;

public final class Substitution {
    private Substitution() {}

    private static final Logger LOG = LoggerFactory.getLogger(Substitution.class);

    static final String PREFIX = "{{";
    static final String SUFFIX = "}}";
    static final String SEPARATOR = "|";

    static final String FIELD_MESSAGE = "message";
    static final String FIELD_TEMPLATE = "template";
    static final String FIELD_TEMPLATE_VALUES = "templateValues";

    private static final PropertyPlaceholderHelper HELPER = new PropertyPlaceholderHelper(
        PREFIX, SUFFIX, SEPARATOR, false
    );
    private static final Resolver RESOLVER = new Resolver();

    static class Resolver implements PropertyPlaceholderHelper.PlaceholderResolver {
        private final HashMap<String, String> values = new HashMap<>();

        public Resolver feed(final Map<String, String> templateValues) {
            Require.notNull(templateValues, FIELD_TEMPLATE_VALUES);
            values.clear();
            values.putAll(templateValues);
            return this;
        }

        @Nullable
        @Override
        public String resolvePlaceholder(final String key) {
            return values.get(key);
        }
    }

    static String perform(final String template, final Map<String, String> templateValues) {
        return HELPER.replacePlaceholders(
            template,
            RESOLVER.feed(templateValues)
        );
    }

    @Nullable
    public static String make(
        @Nullable final String template,
        @NonNull final String fallback,
        @NonNull final Map<String, String> templateValues
    ) throws IllegalArgumentException {
        try {
            return perform(
                Optional.ofNullable(template)
                    .filter(tpl -> !tpl.isBlank())
                    .orElseGet(() -> Require.notNull(fallback, "fallback")),
                Require.notNull(templateValues, FIELD_TEMPLATE_VALUES)
            );
        } catch (final IllegalArgumentException ex) {
            LOG.error(
                "substitution failed [{}] [{}] [{}]",
                keyValue(FIELD_TEMPLATE, template),
                keyValue(FIELD_TEMPLATE_VALUES, templateValues),
                keyValue(FIELD_MESSAGE, ex.getMessage())
            );
        }

        return null;
    }
}
