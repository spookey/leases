package su.toor.leases.components.mqtt;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.Map;

import org.eclipse.paho.mqttv5.client.IMqttToken;
import org.eclipse.paho.mqttv5.client.MqttCallback;
import org.eclipse.paho.mqttv5.client.MqttDisconnectResponse;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.packet.MqttProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import su.toor.leases.components.spring.CounterBean;

@Component
public class Callback implements MqttCallback {

    private static final Logger LOG = LoggerFactory.getLogger(Callback.class);

    private final CounterBean counterBean;

    @Autowired
    public Callback(
        final CounterBean counterBean
    ) {
        this.counterBean = counterBean;
    }

    @Override
    public void deliveryComplete(final IMqttToken token) {
        LOG.info(
            "delivery complete [{}]",
            keyValue("token", token)
        );
        counterBean.incrementMqttDelivered();
    }

    @Override
    public void messageArrived(final String topic, final MqttMessage mqttMessage) {
        LOG.info(
            "message arrived [{}] [{}]",
            keyValue("topic", topic),
            keyValue("mqttMessage", mqttMessage)
        );
        counterBean.incrementMqttArrived();
    }

    @Override
    public void connectComplete(final boolean isReconnect, final String serverUri) {
        LOG.info(
            "connect complete [{}] [{}]",
            keyValue("isReconnect", isReconnect),
            keyValue("serverUri", serverUri)
        );
        counterBean.incrementMqttConnects();
    }

    @Override
    public void disconnected(final MqttDisconnectResponse disconnectResponse) {
        LOG.error(
            "disconnected [{}]",
            keyValue("response", disconnectResponse)
        );
        counterBean.incrementMqttDisconnects();
    }

    @Override
    public void mqttErrorOccurred(final MqttException exception) {
        LOG.error(
            "mqtt error occurred [{}]",
            keyValue("message", exception.getMessage()),
            exception
        );
        counterBean.incrementMqttErrors();
    }

    @Override
    public void authPacketArrived(final int reasonCode, final MqttProperties properties) {
        final var reason = Map.of(
            0, "Success",
            24, "Continue authentication",
            25, "Reauthenticate"
        ).getOrDefault(reasonCode, "¯\\_(ツ)_/¯");

        LOG.info(
            "auth packet arrived [{}] [{}] [{}]",
            keyValue("reasonCode", reasonCode),
            keyValue("reason", reason),
            keyValue("properties", properties)
        );
        counterBean.incrementMqttAuthPackages();
    }
}
