package su.toor.leases.components.mqtt;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import su.toor.leases.configs.sinks.StatusBirthConfig;
import su.toor.leases.shared.Require;

@Component(value = "nonInitializedClient")
public class Client {

    private static final Logger LOG = LoggerFactory.getLogger(Client.class);

    private final String applicationName;
    private final MqttAsyncClient mqttAsyncClient;

    static final String FIELD_MESSAGE = "message";
    static final String FIELD_NAME = "name";
    static final String FIELD_TOPIC = "topic";

    Client(
        @Value("${spring.application.name:leases}") final String applicationName,
        final MqttAsyncClient mqttAsyncClient
    ) {
        this.applicationName = applicationName;
        this.mqttAsyncClient = mqttAsyncClient;
    }

    @Bean
    public static Client client(
        @Value("${spring.application.name:leases}") final String applicationName,
        final MqttAsyncClient mqttAsyncClient,
        final StatusBirthConfig statusBirthConfig
    )  {
        final var messagingService = new Client(applicationName, mqttAsyncClient);
        messagingService.sendBirthMessage(statusBirthConfig);

        return messagingService;
    }

    boolean sendBirthMessage(final StatusBirthConfig statusBirthConfig) {
        if (!statusBirthConfig.isEnabled()) {
            return false;
        }

        final var topic = statusBirthConfig.getTopic();
        final var message = Composer.make(
            statusBirthConfig,
            Map.of(
                FIELD_NAME, applicationName,
                FIELD_TOPIC, topic
            )
        );

        LOG.info(
            "sending birth message [{}] [{}]",
            keyValue(FIELD_TOPIC, topic),
            keyValue(FIELD_MESSAGE, message)
        );

        return sendSync(topic, message);
    }

    public boolean sendSync(
        @Nullable final String topic,
        @Nullable final MqttMessage message
    ) {
        if (topic == null || message == null) {
            LOG.warn(
                "empty topic or message passed [{}] [{}]",
                keyValue(FIELD_TOPIC, topic),
                 keyValue(FIELD_MESSAGE, message)
            );
            return false;
        }

        Require.validMessageTopic(topic);

        if (!mqttAsyncClient.isConnected()) {
            LOG.error(
                "client is not connected [{}] [{}]",
                keyValue(FIELD_TOPIC, topic),
                keyValue(FIELD_MESSAGE, message)
            );
            return false;
        }

        LOG.info(
            "publishing message [{}] [{}]",
            keyValue(FIELD_TOPIC, topic),
            keyValue(FIELD_MESSAGE, message)
        );

        try {
            final var token = mqttAsyncClient.publish(topic, message);
            token.waitForCompletion();
        } catch (final MqttException ex) {
            LOG.error(
                "publishing failed [{}] [{}]",
                keyValue(FIELD_TOPIC, topic),
                keyValue(FIELD_MESSAGE, message),
                ex
            );
            return false;
        }

        return true;
    }

    public CompletableFuture<Boolean> send(
        @Nullable final String topic,
        @Nullable final MqttMessage message
    ) {
        return CompletableFuture
            .supplyAsync(() -> sendSync(topic, message));
    }
}
