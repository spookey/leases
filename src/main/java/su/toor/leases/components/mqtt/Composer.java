package su.toor.leases.components.mqtt;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import su.toor.leases.components.core.Substitution;
import su.toor.leases.configs.sinks.AbstractAlertSink;
import su.toor.leases.configs.sinks.AbstractStatusSink;
import su.toor.leases.shared.Qos;
import su.toor.leases.shared.Require;

public final class Composer {
    private Composer() {}

    public static final Qos DEFAULT_QOS = Qos.ZERO;
    public static final boolean DEFAULT_RETAINED = false;

    static MqttMessage make(
        @Nullable final Qos qos,
        @Nullable final Boolean retained,
        final String content
    ) {
        return new MqttMessage(
            Require.notNull(content, "content")
                .getBytes(StandardCharsets.UTF_8),
            (qos == null ? DEFAULT_QOS : qos).ordinal(),
            retained == null ? DEFAULT_RETAINED : retained,
            null
        );
    }

    public static <T extends AbstractStatusSink<T>> MqttMessage make(
        @NonNull final AbstractStatusSink<T> config,
        @NonNull final Map<String, String> templateValues
    ) {
        return make(
            config.getQos(),
            config.isRetained(),
            Substitution.make(
                null,
                config.getTemplate(),
                templateValues
            )
        );
    }

    public static <T extends AbstractAlertSink<T>> MqttMessage make(
        @NonNull final AbstractAlertSink<T> config,
        @Nullable final String template,
        @NonNull final Map<String, String> templateValues
    ) {
        return make(
            config.getQos(),
            config.isRetained(),
            Substitution.make(
                template,
                config.getFallbackTemplate(),
                templateValues
            )
        );
    }
}
