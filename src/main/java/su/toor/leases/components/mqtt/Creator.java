package su.toor.leases.components.mqtt;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.Map;

import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.client.MqttConnectionOptions;
import org.eclipse.paho.mqttv5.client.persist.MemoryPersistence;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanInstantiationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import su.toor.leases.configs.MqttConfig;
import su.toor.leases.configs.sinks.StatusWillConfig;

@Component
public class Creator {

    private static final Logger LOG = LoggerFactory.getLogger(Creator.class);

    static final String FIELD_MESSAGE = "message";
    static final String FIELD_NAME = "name";
    static final String FIELD_PASSWORD = "password";
    static final String FIELD_SERVER_URI = "serverUri";
    static final String FIELD_TOPIC = "topic";
    static final String FIELD_USERNAME = "username";

    @Bean
    public MqttConnectionOptions mqttConnectionOptions(
        @Value("${spring.application.name:leases}") final String applicationName,
        final MqttConfig mqttConfig,
        final StatusWillConfig statusWillConfig
    ) {
        final var options = new MqttConnectionOptions();
        options.setCleanStart(mqttConfig.isCleanStart());
        options.setAutomaticReconnect(mqttConfig.isAutoReconnect());
        options.setUseSubscriptionIdentifiers(true);
        options.setRequestResponseInfo(true);
        options.setRequestProblemInfo(true);

        if (mqttConfig.hasCredentials()) {
            LOG.info(
                "using credentials [{}] [{}]",
                keyValue(FIELD_USERNAME, mqttConfig.getUsername()),
                keyValue(FIELD_PASSWORD, mqttConfig.getPassword())
            );
            options.setUserName(mqttConfig.getUsername());
            options.setPassword(mqttConfig.getPasswordBytes());
        }

        if (statusWillConfig.isEnabled()) {
            final var topic = statusWillConfig.getTopic();
            final var message = Composer.make(
                statusWillConfig,
                Map.of(
                    FIELD_NAME, applicationName,
                    FIELD_TOPIC, topic
                )
            );

            LOG.info(
                "register will message [{}] [{}]",
                keyValue(FIELD_TOPIC, topic),
                keyValue(FIELD_MESSAGE, message)
            );
            options.setWill(topic, message);
        }

        return options;
    }

    @Bean(value = "basicMqttAsyncClient")
    public MqttAsyncClient basicMqttAsyncClient(
        final MqttConfig mqttConfig,
        final Callback callback
    ) {

        try {
            final var mqttAsyncClient = new MqttAsyncClient(
                mqttConfig.getBrokerUri(),
                mqttConfig.getClientId(),
                new MemoryPersistence()
            );
            mqttAsyncClient.setCallback(callback);

            return mqttAsyncClient;
        } catch (final IllegalArgumentException | MqttException ex) {
            throw new BeanInstantiationException(
                Creator.class,
                "client creation failed",
                ex
            );
        }
    }

    @Bean
    @Primary
    public MqttAsyncClient mqttAsyncClient(
        final MqttAsyncClient basicMqttAsyncClient,
        final MqttConnectionOptions mqttConnectionOptions
    ) {
        LOG.info(
            "connecting to mqtt server [{}]",
            keyValue(FIELD_SERVER_URI, basicMqttAsyncClient.getServerURI())
        );

        try {
            final var token = basicMqttAsyncClient.connect(mqttConnectionOptions);
            token.waitForCompletion();

            return basicMqttAsyncClient;
        } catch (final MqttException ex) {
            throw new BeanInstantiationException(
                Creator.class,
                "client connection failed",
                ex
            );
        }
    }
}
