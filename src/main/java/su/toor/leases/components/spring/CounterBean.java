package su.toor.leases.components.spring;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Component;

@Component
public class CounterBean {

    private final AtomicLong mqttDelivered = new AtomicLong(0L);
    private final AtomicLong mqttArrived = new AtomicLong(0L);
    private final AtomicLong mqttConnects = new AtomicLong(0L);
    private final AtomicLong mqttDisconnects = new AtomicLong(0L);
    private final AtomicLong mqttAuthPackages = new AtomicLong(0L);
    private final AtomicLong mqttErrors = new AtomicLong(0L);

    private final AtomicLong purgeOutdatedSuccess = new AtomicLong(0L);
    private final AtomicLong purgeOutdatedFailed = new AtomicLong(0L);

    public Long getMqttDelivered() {
        return mqttDelivered.get();
    }
    public Long incrementMqttDelivered() {
        return mqttDelivered.incrementAndGet();
    }

    public Long getMqttArrived() {
        return mqttArrived.get();
    }
    public Long incrementMqttArrived() {
        return mqttArrived.incrementAndGet();
    }

    public Long getMqttConnects() {
        return mqttConnects.get();
    }
    public Long incrementMqttConnects() {
        return mqttConnects.incrementAndGet();
    }

    public Long getMqttDisconnects() {
        return mqttDisconnects.get();
    }
    public Long incrementMqttDisconnects() {
        return mqttDisconnects.incrementAndGet();
    }

    public Long getMqttAuthPackages() {
        return mqttAuthPackages.get();
    }
    public Long incrementMqttAuthPackages() {
        return mqttAuthPackages.incrementAndGet();
    }

    public Long getMqttErrors() {
        return mqttErrors.get();
    }
    public Long incrementMqttErrors() {
        return mqttErrors.incrementAndGet();
    }

    public Long getPurgeOutdatedSuccess() {
        return purgeOutdatedSuccess.get();
    }

    public Long addPurgeOutdatedSuccess(final Long amount) {
        return purgeOutdatedSuccess.addAndGet(amount);
    }

    public Long getPurgeOutdatedFailed() {
        return purgeOutdatedFailed.get();
    }

    public Long addPurgeOutdatedFailed(final Long amount) {
        return purgeOutdatedFailed.addAndGet(amount);
    }
}
