package su.toor.leases.components.spring;

import java.util.function.ToDoubleFunction;
import java.util.stream.Stream;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import su.toor.leases.services.query.AlertEntityService;

@Component
public class MetricsBean {

    private static final Logger LOG = LoggerFactory.getLogger(MetricsBean.class);

    static final String CATEGORY_DB = "database";
    static final String CATEGORY_MQTT = "mqtt";
    static final String CATEGORY_PURGE = "cleanup";
    static final String DESCRIPTION_DB = "Database table counts";
    static final String DESCRIPTION_MQTT = "MQTT action counters";
    static final String DESCRIPTION_PURGE = "Entity removal";
    static final String NAME_DB = "database_count";
    static final String NAME_MQTT = "mqtt_count";
    static final String NAME_PURGE = "purge_count";

    static final String TAG_TYPE = "type";
    static final String TAG_CATEGORY = "category";

    private final MeterRegistry meterRegistry;
    private final CounterBean counterBean;
    private final AlertEntityService alertEntityService;

    @Autowired
    public MetricsBean(
        final MeterRegistry meterRegistry,
        final CounterBean counterBean,
        final AlertEntityService alertEntityService
    ) {
        this.meterRegistry = meterRegistry;
        this.counterBean = counterBean;
        this.alertEntityService = alertEntityService;
    }

    record Hold<T>(T instance, String name, String category, String description) {
        Gauge.Builder<T> build(final String type, final ToDoubleFunction<T> func) {
            return Gauge.builder(name, instance, func)
                .tag(TAG_TYPE, type)
                .tag(TAG_CATEGORY, category)
                .description(description);
        }
    }

    void mqttCounters() {
        LOG.info("registering mqtt counter gauges");

        final var hold = new Hold<>(
            counterBean, NAME_MQTT, CATEGORY_MQTT, DESCRIPTION_MQTT
        );

        Stream.of(
            hold.build("delivered", CounterBean::getMqttDelivered),
            hold.build("arrived", CounterBean::getMqttArrived),
            hold.build("connect", CounterBean::getMqttConnects),
            hold.build("disconnect", CounterBean::getMqttDisconnects),
            hold.build("auth", CounterBean::getMqttAuthPackages),
            hold.build("error", CounterBean::getMqttErrors)
        ).forEach(gauge -> gauge.register(meterRegistry));
    }
    void purgeCounters() {
        LOG.info("registering purge counter gauges");

        final var hold = new Hold<>(
            counterBean, NAME_PURGE, CATEGORY_PURGE, DESCRIPTION_PURGE
        );

        Stream.of(
            hold.build("outdated", CounterBean::getPurgeOutdatedSuccess),
            hold.build("outdated_failed", CounterBean::getPurgeOutdatedFailed)
        ).forEach(gauge -> gauge.register(meterRegistry));
    }
    void databaseCounts() {
        LOG.info("registering database count gauges");

        final var hold = new Hold<>(
            alertEntityService, NAME_DB, CATEGORY_DB, DESCRIPTION_DB
        );

        Stream.of(
            hold.build("stamp", AlertEntityService::countStamps),
            hold.build("ether", AlertEntityService::countEthers),
            hold.build("bound", AlertEntityService::countBounds),
            hold.build("track", AlertEntityService::countTracks),
            hold.build("guide", AlertEntityService::countGuides),
            hold.build("alert_ether", AlertEntityService::countAlertEthers),
            hold.build("alert_bound", AlertEntityService::countAlertBounds),
            hold.build("alert_guide", AlertEntityService::countAlertGuides)
        ).forEach(gauge -> gauge.register(meterRegistry));
    }

    @EventListener(ApplicationReadyEvent.class)
    public void registerMetrics() {
        mqttCounters();
        purgeCounters();
        databaseCounts();
    }
}
