package su.toor.leases.components.spring;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import su.toor.leases.components.Accessor;
import su.toor.leases.components.Importer;
import su.toor.leases.components.Scrubber;
import su.toor.leases.configs.BasicConfig;

@EnableScheduling
@Component
public class TimerBean {

    private static final Logger LOG = LoggerFactory.getLogger(TimerBean.class);

    static final long INTERVAL_RUN_INITIAL = 1000L * 10L;
    static final long INTERVAL_RUN_REGULAR = 1000L * 5L;

    static final String FIELD_STAMP = "stamp";

    private final BasicConfig basicConfig;
    private final Accessor accessor;
    private final Importer importer;
    private final Scrubber scrubber;

    final AtomicLong lastImport = new AtomicLong(0L);

    @Autowired
    public TimerBean(
        final BasicConfig basicConfig,
        final Accessor accessor,
        final Importer importer,
        final Scrubber scrubber
    ) {
        this.basicConfig = basicConfig;
        this.accessor = accessor;
        this.importer = importer;
        this.scrubber = scrubber;
    }

    boolean shouldImport() {
        final var interval = 1000L * basicConfig.getImportInterval();
        if (interval <= 0) {
            return false;
        }

        final var curr = System.currentTimeMillis();
        final var last = lastImport.get();
        final var diff = curr - last;
        if (diff < interval) {
            return false;
        }

        lastImport.set(curr);
        return true;
    }

    void importLeaseFiles() {
        if (!shouldImport()) {
            return;
        }

        final var lines = accessor.loadLeaseFiles();
        importer.run(lines).ifPresent(
            stampEntity -> LOG.info("imported leases [{}]", keyValue(FIELD_STAMP, stampEntity))
        );
    }

    void purgeInvalid() {
        scrubber.run();
    }

    @Scheduled(
        initialDelay = INTERVAL_RUN_INITIAL,
        fixedRate = INTERVAL_RUN_REGULAR
    )
    public void repeating() {
        purgeInvalid();
        importLeaseFiles();
    }
}
