package su.toor.leases.configs;

import java.nio.file.Path;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;
import su.toor.leases.shared.Linkage;

@Configuration
@ConfigurationProperties(prefix = "basic")
@Validated
public class BasicConfig {

    static final int DEFAULT_KEEP_DAYS = 28;
    static final int DEFAULT_PAGINATION_SIZE = 128;
    static final boolean DEFAULT_HIDDEN_STATE = false;
    static final int DEFAULT_IMPORT_INTERVAL = 300;
    static final int DEFAULT_GUIDES_EVERY = 6;
    static final Path DEFAULT_DUMP_LOCATION = Path.of("/tmp");

    private Integer keepDays;
    private Integer paginationSize;
    private Boolean hiddenState;
    private Integer importInterval;
    private Integer guidesEvery;
    private Path dumpLocation;
    private final Set<Path> leaseFiles = new HashSet<>();

    @NonNull
    public Integer getKeepDays() {
        return keepDays == null
            ? DEFAULT_KEEP_DAYS
            : keepDays;
    }

    public BasicConfig setKeepDays(final Integer keepDays) {
        this.keepDays = keepDays;
        return this;
    }

    @NonNull
    public Integer getPaginationSize() {
        return paginationSize == null
            ? DEFAULT_PAGINATION_SIZE
            : paginationSize;
    }

    public BasicConfig setPaginationSize(final Integer paginationSize) {
        this.paginationSize = paginationSize;
        return this;
    }

    @NonNull
    public Boolean isHiddenState() {
        return hiddenState == null
            ? DEFAULT_HIDDEN_STATE
            : hiddenState;
    }

    public BasicConfig setHiddenState(final Boolean hiddenState) {
        this.hiddenState = hiddenState;
        return this;
    }

    @NonNull
    public Integer getImportInterval() {
        return importInterval == null
            ? DEFAULT_IMPORT_INTERVAL
            : importInterval;
    }

    public BasicConfig setImportInterval(final Integer importInterval) {
        this.importInterval = importInterval;
        return this;
    }

    @NonNull
    public Integer getGuidesEvery() {
        return guidesEvery == null
            ? DEFAULT_GUIDES_EVERY
            : guidesEvery;
    }

    public BasicConfig setGuidesEvery(final Integer guidesEvery) {
        this.guidesEvery = guidesEvery;
        return this;
    }

    @NonNull
    Path getDumpLocation() {
        return dumpLocation == null
            ? DEFAULT_DUMP_LOCATION
            : dumpLocation;
    }

    @NonNull
    public Path dumpLocation() {
        return Linkage.verified(getDumpLocation(), true, "dumpLocation");
    }

    public BasicConfig setDumpLocation(final Path dumpLocation) {
        this.dumpLocation = dumpLocation;
        return this;
    }

    @NonNull
    Set<Path> getLeaseFiles() {
        return leaseFiles;
    }

    @NonNull
    public List<Path> leaseFiles() {
        return getLeaseFiles().stream()
            .map(Linkage::path)
            .filter(Objects::nonNull)
            .sorted(Comparator.comparing(Path::toAbsolutePath))
            .collect(Collectors.toList());
    }

    public BasicConfig setLeaseFiles(final Set<Path> leaseFiles) {
        if (leaseFiles != null) {
            this.leaseFiles.clear();
            this.leaseFiles.addAll(leaseFiles);
        }
        return this;
    }
}
