package su.toor.leases.configs;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Optional;
import java.util.Random;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "mqtt")
@Validated
public class MqttConfig {

    private static final Random RND = new SecureRandom();

    static final String DEFAULT_BROKER_URI = "tcp://localhost:1883";
    static final String DEFAULT_CLIENT_ID_PREFIX = "leases";
    static final boolean DEFAULT_CLEAN_START = true;
    static final boolean DEFAULT_AUTO_RECONNECT = true;

    private String brokerUri;
    private String clientId;
    private Boolean cleanStart;
    private Boolean autoReconnect;
    private String username;
    private String password;

    @NonNull
    public String getBrokerUri() {
        return brokerUri == null || brokerUri.isEmpty() ? DEFAULT_BROKER_URI : brokerUri;
    }

    public MqttConfig setBrokerUri(final String brokerUri) {
        this.brokerUri = brokerUri;
        return this;
    }

    @NonNull
    public String getClientId() {
        if (clientId == null) {
            final var suffix = RND.nextInt(1000, 10000);
            return String.format("%s-%04d", DEFAULT_CLIENT_ID_PREFIX, suffix);
        }
        return clientId;
    }

    public MqttConfig setClientId(final String clientId) {
        this.clientId = clientId;
        return this;
    }

    @NonNull
    public Boolean isCleanStart() {
        return cleanStart == null ? DEFAULT_CLEAN_START : cleanStart;
    }

    public MqttConfig setCleanStart(final Boolean cleanStart) {
        this.cleanStart = cleanStart;
        return this;
    }

    @NonNull
    public Boolean isAutoReconnect() {
        return autoReconnect == null ? DEFAULT_AUTO_RECONNECT : autoReconnect;
    }

    public MqttConfig setAutoReconnect(final Boolean autoReconnect) {
        this.autoReconnect = autoReconnect;
        return this;
    }

    @Nullable
    public String getUsername() {
        return username;
    }

    public MqttConfig setUsername(final String username) {
        this.username = username;
        return this;
    }

    @Nullable
    public String getPassword() {
        return Optional.ofNullable(password)
            .map(pass -> "*".repeat(pass.length()))
            .orElse(null);
    }

    @Nullable
    public byte[] getPasswordBytes() {
        return Optional.ofNullable(password)
            .map(pass -> pass.getBytes(StandardCharsets.UTF_8))
            .orElse(null);
    }

    public MqttConfig setPassword(final String password) {
        this.password = password;
        return this;
    }

    public boolean hasCredentials() {
        return username != null && password != null;
    }
}
