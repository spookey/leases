package su.toor.leases.configs.sinks;

import org.springframework.lang.NonNull;

public abstract class AbstractAlertSink<T extends AbstractAlertSink<T>> extends AbstractSink<T> {

    static final String DEFAULT_FALLBACK_TOPIC_ETHER = "network/leases/devices";
    static final String DEFAULT_FALLBACK_TOPIC_BOUND = "network/leases/addresses";
    static final String DEFAULT_FALLBACK_TOPIC_GUIDE = "network/leases/networks";

    static final String DEFAULT_FALLBACK_TEMPLATE_ETHER = "'{{hostname|Device without hostname}}' is '{{state}}' ({{uuid}})";
    static final String DEFAULT_FALLBACK_TEMPLATE_BOUND = "Address '{{ip}}' is '{{state}}' ({{uuid}})";
    static final String DEFAULT_FALLBACK_TEMPLATE_GUIDE = "Network '{{title}}' ({{mask}}): {{high|?!?}} up {{down|?!?}} down ({{uuid}})";

    private String fallbackTopic;
    private String fallbackTemplate;

    @NonNull
    private final String defaultFallbackTopic;

    @NonNull
    private final String defaultFallbackTemplate;

    protected AbstractAlertSink(
        @NonNull final String defaultFallbackTopic,
        @NonNull final String defaultFallbackTemplate
    ) {
        this.defaultFallbackTopic = validTopic(defaultFallbackTopic);
        this.defaultFallbackTemplate = validTemplate(defaultFallbackTemplate);
    }

    protected abstract T get();

    @NonNull
    public String getFallbackTopic() {
        return fallbackTopic == null || fallbackTopic.isEmpty()
            ? defaultFallbackTopic
            : fallbackTopic;
    }

    public T setFallbackTopic(final String fallbackTopic) {
        this.fallbackTopic = validTopic(fallbackTopic);
        return get();
    }

    @NonNull
    public String getFallbackTemplate() {
        return fallbackTemplate == null || fallbackTemplate.isEmpty()
            ? defaultFallbackTemplate
            : fallbackTemplate;
    }

    public T setFallbackTemplate(final String fallbackTemplate) {
        this.fallbackTemplate = validTemplate(fallbackTemplate);
        return get();
    }
}
