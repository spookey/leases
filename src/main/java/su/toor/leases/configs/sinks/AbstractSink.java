package su.toor.leases.configs.sinks;

import org.springframework.lang.NonNull;
import su.toor.leases.shared.Qos;
import su.toor.leases.shared.Require;

public abstract class AbstractSink<T extends AbstractSink<T>> {

    static final boolean DEFAULT_ENABLED = false;
    static final Qos DEFAULT_QOS = Qos.ZERO;
    static final boolean DEFAULT_RETAINED = false;

    private Boolean enabled;
    private Qos qos;
    private Boolean retained;

    protected static String validTopic(final String topic) {
        return Require.validMessageTopic(topic);
    }
    protected static String validTemplate(final String template) {
        return Require.notBlank(template, "template");
    }

    protected abstract T get();

    @NonNull
    public Boolean isEnabled() {
        return enabled == null ? DEFAULT_ENABLED : enabled;
    }

    public T setEnabled(final Boolean enabled) {
        this.enabled = enabled;
        return get();
    }

    @NonNull
    public Qos getQos() {
        return qos == null ? DEFAULT_QOS : qos;
    }

    public T setQos(final Qos qos) {
        this.qos = qos;
        return get();
    }

    @NonNull
    public Boolean isRetained() {
        return retained == null ? DEFAULT_RETAINED : retained;
    }

    public T setRetained(final Boolean retained) {
        this.retained = retained;
        return get();
    }
}
