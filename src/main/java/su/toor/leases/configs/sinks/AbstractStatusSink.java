package su.toor.leases.configs.sinks;

import org.springframework.lang.NonNull;

public abstract class AbstractStatusSink<T extends AbstractStatusSink<T>> extends AbstractSink<T> {

    static final String DEFAULT_TEMPLATE_BIRTH = "{{name}} alive on topic '{{topic}}'";
    static final String DEFAULT_TEMPLATE_WILL = "{{name}} gone off topic '{{topic}}'";
    static final String DEFAULT_TOPIC_STATUS = "status/leases";

    private String topic;
    private String template;

    @NonNull
    private final String defaultTopic;
    @NonNull
    private final String defaultTemplate;

    protected AbstractStatusSink(
        @NonNull final String defaultTopic,
        @NonNull final String defaultTemplate
    ) {
        this.defaultTopic = validTopic(defaultTopic);
        this.defaultTemplate = validTemplate(defaultTemplate);
    }

    protected abstract T get();

    @NonNull
    public String getTopic() {
        return topic == null || topic.isEmpty() ? defaultTopic : topic;
    }

    public T setTopic(final String topic) {
        this.topic = validTopic(topic);
        return get();
    }

    @NonNull
    public String getTemplate() {
        return template == null || template.isEmpty() ? defaultTemplate : template;
    }

    public T setTemplate(final String template) {
        this.template = validTemplate(template);
        return get();
    }
}
