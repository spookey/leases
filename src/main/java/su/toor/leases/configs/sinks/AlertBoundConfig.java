package su.toor.leases.configs.sinks;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties("mqtt.sinks.alert-bound")
@Validated
public class AlertBoundConfig extends AbstractAlertSink<AlertBoundConfig> {

    public AlertBoundConfig() {
        super(DEFAULT_FALLBACK_TOPIC_BOUND, DEFAULT_FALLBACK_TEMPLATE_BOUND);
    }

    @Override
    protected AlertBoundConfig get() {
        return this;
    }
}
