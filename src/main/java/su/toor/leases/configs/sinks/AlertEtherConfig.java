package su.toor.leases.configs.sinks;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties("mqtt.sinks.alert-ether")
@Validated
public class AlertEtherConfig extends AbstractAlertSink<AlertEtherConfig> {

    public AlertEtherConfig() {
        super(DEFAULT_FALLBACK_TOPIC_ETHER, DEFAULT_FALLBACK_TEMPLATE_ETHER);
    }

    @Override
    protected AlertEtherConfig get() {
        return this;
    }
}
