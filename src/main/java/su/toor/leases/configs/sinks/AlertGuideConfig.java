package su.toor.leases.configs.sinks;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties("mqtt.sinks.alert-guide")
@Validated
public class AlertGuideConfig extends AbstractAlertSink<AlertGuideConfig> {

    public AlertGuideConfig() {
        super(DEFAULT_FALLBACK_TOPIC_GUIDE, DEFAULT_FALLBACK_TEMPLATE_GUIDE);
    }

    @Override
    protected AlertGuideConfig get() {
        return this;
    }
}
