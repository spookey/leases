package su.toor.leases.configs.sinks;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties("mqtt.sinks.birth")
@Validated
public class StatusBirthConfig extends AbstractStatusSink<StatusBirthConfig> {

    public StatusBirthConfig() {
        super(DEFAULT_TOPIC_STATUS, DEFAULT_TEMPLATE_BIRTH);
    }

    @Override
    protected StatusBirthConfig get() {
        return this;
    }
}
