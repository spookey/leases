package su.toor.leases.configs.sinks;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties("mqtt.sinks.will")
@Validated
public class StatusWillConfig extends AbstractStatusSink<StatusWillConfig> {

    public StatusWillConfig() {
        super(DEFAULT_TOPIC_STATUS, DEFAULT_TEMPLATE_WILL);
    }

    @Override
    protected StatusWillConfig get() {
        return this;
    }
}
