package su.toor.leases.configs.spring;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfiguration {

    @Bean
    public OpenAPI openAPI(
        @Value("${spring.application.name:leases}") final String applicationName
    ) {
        return new OpenAPI()
            .info(new Info().title(applicationName));
    }
}
