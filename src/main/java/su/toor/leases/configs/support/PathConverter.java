package su.toor.leases.configs.support;

import java.nio.file.Path;

import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@ConfigurationPropertiesBinding
public class PathConverter implements Converter<String, Path> {

    @Override
    public Path convert(final String source) {
        return Path.of(source);
    }
}
