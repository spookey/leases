package su.toor.leases.controllers;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.UUID;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import su.toor.leases.controllers.dtos.BoundDto;
import su.toor.leases.controllers.dtos.side.PageDto;
import su.toor.leases.parse.LeaseConverter;
import su.toor.leases.services.query.EntityService;

@Timed
@Tag(name = "Bound", description = "Query addresses")
@RestController
@RequestMapping("/api/addresses")
public class BoundApiController {

    private static final Logger LOG = LoggerFactory.getLogger(BoundApiController.class);

    static final String FIELD_ADDR = "address";
    static final String FIELD_PAGE = "page";
    static final String FIELD_UUID = "uuid";

    private final EntityService entityService;

    @Autowired
    public BoundApiController(
        final EntityService entityService
    ) {
        this.entityService = entityService;
    }

    @Timed
    @Operation(summary = "View all addresses")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<BoundDto>> allBounds(
        @Parameter(name = "page", description = "Page number")
        @RequestParam(name = "page", defaultValue = "0")
        final Integer page,
        @Parameter(name = "size", description = "Page size")
        @RequestParam(name = "size", defaultValue = "0")
        final Integer size
    ) {
        final var boundEntities = entityService.findBounds(page, size);
        final var response = PageDto.with(boundEntities.map(BoundDto::from));
        LOG.info("bounds requested [{}]", keyValue(FIELD_PAGE, response));

        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "View one address")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/{uuid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<BoundDto> singleBound(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid
    ) {
        LOG.info("single bound requested [{}]", keyValue(FIELD_UUID, uuid));
        final var optionalBoundEntity = entityService.findBound(uuid);
        if (optionalBoundEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = BoundDto.from(optionalBoundEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "Find one address by IP")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/find/{ip}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<BoundDto> findBound(
        @Parameter(name = "ip", description = "IP address")
        @PathVariable(name = "ip")
        final String ipAddress
    ) {
        LOG.info("single bound requested [{}]", keyValue(FIELD_ADDR, ipAddress));
        final var ip = LeaseConverter.convertIpAddress(ipAddress);
        final var optionalBoundEntity = entityService.findBound(ip);
        if (optionalBoundEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = BoundDto.from(optionalBoundEntity.get());
        return ResponseEntity.ok(response);
    }
}
