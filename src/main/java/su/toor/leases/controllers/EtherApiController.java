package su.toor.leases.controllers;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.UUID;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import su.toor.leases.controllers.dtos.EtherDto;
import su.toor.leases.controllers.dtos.side.PageDto;
import su.toor.leases.parse.LeaseConverter;
import su.toor.leases.services.query.EntityService;

@Timed
@Tag(name = "Ether", description = "Query devices")
@RestController
@RequestMapping("/api/devices")
public class EtherApiController {

    private static final Logger LOG = LoggerFactory.getLogger(EtherApiController.class);

    static final String FIELD_ADDR = "address";
    static final String FIELD_PAGE = "page";
    static final String FIELD_UUID = "uuid";

    private final EntityService entityService;

    @Autowired
    public EtherApiController(
        final EntityService entityService
    ) {
        this.entityService = entityService;
    }

    @Timed
    @Operation(summary = "View all devices")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<EtherDto>> allEthers(
        @Parameter(name = "page", description = "Page number")
        @RequestParam(name = "page", defaultValue = "0")
        final Integer page,
        @Parameter(name = "size", description = "Page size")
        @RequestParam(name = "size", defaultValue = "0")
        final Integer size
    ) {
        final var etherEntities = entityService.findEthers(page, size);
        final var response = PageDto.with(etherEntities.map(EtherDto::from));
        LOG.info("ethers requested [{}]", keyValue(FIELD_PAGE, response));

        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "View one device")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/{uuid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<EtherDto> singleEther(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid
    ) {
        LOG.info("single ether requested [{}]", keyValue(FIELD_UUID, uuid));
        final var optionalEtherEntity = entityService.findEther(uuid);
        if (optionalEtherEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = EtherDto.from(optionalEtherEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "Find one device by MAC")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/find/{mac}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<EtherDto> findEther(
        @Parameter(name = "mac", description = "MAC address")
        @PathVariable(name = "mac")
        final String macAddress
    ) {
        LOG.info("single ether requested [{}]", keyValue(FIELD_ADDR, macAddress));
        final var mac = LeaseConverter.convertMacAddress(macAddress);
        final var optionalEtherEntity = entityService.findEther(mac);
        if (optionalEtherEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = EtherDto.from(optionalEtherEntity.get());
        return ResponseEntity.ok(response);
    }
}
