package su.toor.leases.controllers;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.Map;
import java.util.UUID;

import jakarta.validation.Valid;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.leases.controllers.dtos.GuideDto;
import su.toor.leases.controllers.dtos.GuideRequestDto;
import su.toor.leases.controllers.dtos.side.PageDto;
import su.toor.leases.parse.LeaseConverter;
import su.toor.leases.services.query.EntityService;

@Timed
@Tag(name = "Guide", description = "Manage networks")
@RestController
@RequestMapping("/api/networks")
public class GuideApiController {

    private static final Logger LOG = LoggerFactory.getLogger(GuideApiController.class);

    static final String FIELD_BODY = "body";
    static final String FIELD_PAGE = "page";
    static final String FIELD_UUID = "uuid";

    private final EntityService entityService;

    @Autowired
    public GuideApiController(
        final EntityService entityService
    ) {
        this.entityService = entityService;
    }

    @Timed
    @Operation(summary = "View all networks")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<GuideDto>> allGuides(
        @Parameter(name = "page", description = "Page number")
        @RequestParam(name = "page", defaultValue = "0")
        final Integer page,
        @Parameter(name = "size", description = "Page size")
        @RequestParam(name = "size", defaultValue = "0")
        final Integer size
    ) {
        final var guideEntities = entityService.findGuides(page, size);
        final var response = PageDto.with(guideEntities.map(GuideDto::from));
        LOG.info("guides requested [{}]", keyValue(FIELD_PAGE, response));

        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "View one network")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/{uuid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<GuideDto> singleGuide(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid
    ) {
        LOG.info("single guide requested [{}]", keyValue(FIELD_UUID, uuid));
        final var optionalGuideEntity = entityService.findGuide(uuid);
        if (optionalGuideEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = GuideDto.from(optionalGuideEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "Create new network")
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createGuide(
        @Valid @RequestBody final GuideRequestDto body
    ) {
        LOG.info("creation of guide requested [{}]", keyValue(FIELD_BODY, body));

        final var guideEntity = entityService.createGuide(
            LeaseConverter.convertIpAddress(body.getMask()),
            body.getWeight(),
            body.getTitle(),
            body.getDescription()
        );

        return ResponseEntity.created(
            UriComponentsBuilder
                .fromPath("/api/networks/{uuid}")
                .build(Map.of("uuid", guideEntity.getUuid()))
        ).build();
    }

    @Timed
    @Operation(summary = "Modify existing network")
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PutMapping(value = "/{uuid}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<GuideDto> modifyGuide(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid,
        @Valid @RequestBody final GuideRequestDto body
    ) {
        LOG.info("modifying guide [{}]", keyValue(FIELD_UUID, uuid));

        final var optionalGuideEntity = entityService.modifyGuide(
            uuid,
            LeaseConverter.convertIpAddress(body.getMask()),
            body.getWeight(),
            body.getTitle(),
            body.getDescription()
        );
        if (optionalGuideEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = GuideDto.from(optionalGuideEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "Delete existing network")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @DeleteMapping(value = "/{uuid}")
    public ResponseEntity<Void> deleteGuide(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid
    ) {
        LOG.info("deletion of guide requested [{}]", keyValue(FIELD_UUID, uuid));
        if (!entityService.deleteGuide(uuid)) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }
}
