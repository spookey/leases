package su.toor.leases.controllers;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.UUID;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import su.toor.leases.controllers.dtos.StampDto;
import su.toor.leases.controllers.dtos.side.PageDto;
import su.toor.leases.services.query.EntityService;

@Timed
@Tag(name = "Stamp", description = "Query timestamps")
@RestController
@RequestMapping("/api/stamps")
public class StampApiController {

    private static final Logger LOG = LoggerFactory.getLogger(StampApiController.class);

    static final String FIELD_PAGE = "page";
    static final String FIELD_UUID = "uuid";

    private final EntityService entityService;

    @Autowired
    public StampApiController(
        final EntityService entityService
    ) {
        this.entityService = entityService;
    }

    @Timed
    @Operation(summary = "View all stamps")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<StampDto>> allStamps(
        @Parameter(name = "page", description = "Page number")
        @RequestParam(name = "page", defaultValue = "0")
        final Integer page,
        @Parameter(name = "size", description = "Page size")
        @RequestParam(name = "size", defaultValue = "0")
        final Integer size
    ) {
        final var stampEntities = entityService.findStamps(page, size);
        final var response = PageDto.with(stampEntities.map(StampDto::from));
        LOG.info("stamps requested [{}]", keyValue(FIELD_PAGE, response));

        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "View one stamp")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/{uuid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<StampDto> singleStamp(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid
    ) {
        LOG.info("single stamp requested [{}]", keyValue(FIELD_UUID, uuid));
        final var optionalStampEntity = entityService.findStamp(uuid);
        if (optionalStampEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = StampDto.from(optionalStampEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "View latest stamp")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/latest", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<StampDto> latestStamp() {
        LOG.info("latest stamp requested");
        final var optionalStampEntity = entityService.latestStamp();
        if (optionalStampEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = StampDto.from(optionalStampEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Hidden
    @Operation(summary = "Delete existing stamp")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @DeleteMapping(value = "/{uuid}")
    public ResponseEntity<Void> deleteStamp(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid
    ) {
        LOG.info("deletion of stamp requested [{}]", keyValue(FIELD_UUID, uuid));
        if (!entityService.deleteStamp(uuid)) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }
}
