package su.toor.leases.controllers;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.UUID;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import su.toor.leases.controllers.dtos.TrackDto;
import su.toor.leases.controllers.dtos.side.PageDto;
import su.toor.leases.services.query.EntityService;

@Timed
@Tag(name = "Track", description = "Query tracks")
@RestController
@RequestMapping("/api/tracks")
public class TrackApiController {

    private static final Logger LOG = LoggerFactory.getLogger(TrackApiController.class);

    static final String FIELD_PAGE = "page";
    static final String FIELD_UUID = "uuid";

    private final EntityService entityService;

    @Autowired
    public TrackApiController(
        final EntityService entityService
    ) {
        this.entityService = entityService;
    }

    @Timed
    @Operation(summary = "View all tracks")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<TrackDto>> allTracks(
        @Parameter(name = "page", description = "Page number")
        @RequestParam(name = "page", defaultValue = "0")
        final Integer page,
        @Parameter(name = "size", description = "Page size")
        @RequestParam(name = "size", defaultValue = "0")
        final Integer size
    ) {
        final var trackEntities = entityService.findTracks(page, size);
        final var response = PageDto.with(trackEntities.map(TrackDto::from));
        LOG.info("tracks requested [{}]", keyValue(FIELD_PAGE, response));

        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "View tracks of one stamp")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/{uuid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<TrackDto>> singleTracks(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid,
        @Parameter(name = "page", description = "Page number")
        @RequestParam(name = "page", defaultValue = "0")
        final Integer page,
        @Parameter(name = "size", description = "Page size")
        @RequestParam(name = "size", defaultValue = "0")
        final Integer size
    ) {
        LOG.info("single tracks requested [{}]", keyValue(FIELD_UUID, uuid));
        final var optionalStampEntity = entityService.findStamp(uuid);
        if (optionalStampEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var stampEntity = optionalStampEntity.get();
        final var trackEntities = entityService.findTracks(stampEntity.getUuid(), page, size);
        final var response = PageDto.with(trackEntities.map(TrackDto::from));
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "View tracks of latest stamp")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/latest", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<TrackDto>> latestTracks(
        @Parameter(name = "page", description = "Page number")
        @RequestParam(name = "page", defaultValue = "0")
        final Integer page,
        @Parameter(name = "size", description = "Page size")
        @RequestParam(name = "size", defaultValue = "0")
        final Integer size
    ) {
        LOG.info("latest tracks requested [{}]", keyValue(FIELD_PAGE, page));
        final var trackEntities = entityService.latestTracks(page, size);
        final var response = PageDto.with(trackEntities.map(TrackDto::from));

        return ResponseEntity.ok(response);
    }
}
