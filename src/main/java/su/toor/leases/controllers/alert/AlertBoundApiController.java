package su.toor.leases.controllers.alert;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.Map;
import java.util.UUID;

import jakarta.validation.Valid;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.leases.controllers.dtos.alert.AlertBoundDto;
import su.toor.leases.controllers.dtos.alert.AlertBoundRequestDto;
import su.toor.leases.controllers.dtos.side.PageDto;
import su.toor.leases.services.query.AlertEntityService;

@Timed
@Tag(name = "Alert Bound", description = "Manage address alerts")
@RestController
@RequestMapping("/api/alert/addresses")
public class AlertBoundApiController {

    private static final Logger LOG = LoggerFactory.getLogger(AlertBoundApiController.class);

    static final String FIELD_BODY = "body";
    static final String FIELD_PAGE = "page";
    static final String FIELD_UUID = "uuid";

    private final AlertEntityService alertEntityService;

    @Autowired
    public AlertBoundApiController(
        final AlertEntityService alertEntityService
    ) {
        this.alertEntityService = alertEntityService;
    }

    @Timed
    @Operation(summary = "View all address alerts")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<AlertBoundDto>> allAlertBounds(
        @Parameter(name = "page", description = "Page number")
        @RequestParam(name = "page", defaultValue = "0")
        final Integer page,
        @Parameter(name = "size", description = "Page size")
        @RequestParam(name = "size", defaultValue = "0")
        final Integer size
    ) {
        final var alertBoundEntities = alertEntityService.findAlertBounds(page, size);
        final var response = PageDto.with(alertBoundEntities.map(AlertBoundDto::from));

        LOG.info("alert bounds requested [{}]", keyValue(FIELD_PAGE, response));

        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "View one address alert")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/{uuid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<AlertBoundDto> singleAlertBound(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid
    ) {
        LOG.info("single alert bound requested [{}]", keyValue(FIELD_UUID, uuid));
        final var optionalAlertBoundEntity = alertEntityService.findAlertBound(uuid);
        if (optionalAlertBoundEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = AlertBoundDto.from(optionalAlertBoundEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "Create new address alert")
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createAlertBound(
        @Valid @RequestBody final AlertBoundRequestDto body
    ) {
        LOG.info("creation of alert bound requested [{}]", keyValue(FIELD_BODY, body));

        final var optionalAlertBoundEntity = alertEntityService.createAlertBound(
            body.getBoundUuid(),
            body.isEnabled(),
            body.getTopic(),
            body.getTemplate()
        );
        if (optionalAlertBoundEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var alertBoundEntity = optionalAlertBoundEntity.get();
        return ResponseEntity.created(
            UriComponentsBuilder
                .fromPath("/api/alert/addresses/{uuid}")
                .build(Map.of("uuid", alertBoundEntity.getUuid()))
        ).build();
    }

    @Timed
    @Operation(summary = "Modify existing address alert")
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PutMapping(value = "/{uuid}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<AlertBoundDto> modifyAlertBound(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid,
        @Valid @RequestBody final AlertBoundRequestDto body
    ) {
        LOG.info("modifying alert bound [{}]", keyValue(FIELD_UUID, uuid));

        final var optionalAlertBoundEntity = alertEntityService.modifyAlertBound(
            uuid,
            body.isEnabled(),
            body.getTopic(),
            body.getTemplate()
        );
        if (optionalAlertBoundEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = AlertBoundDto.from(optionalAlertBoundEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "Delete existing address alert")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @DeleteMapping(value = "/{uuid}")
    public ResponseEntity<Void> deleteAlertBound(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid
    ) {
        LOG.info("deletion of alert bound requested [{}]", keyValue(FIELD_UUID, uuid));
        if (!alertEntityService.deleteAlertBound(uuid)) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }
}
