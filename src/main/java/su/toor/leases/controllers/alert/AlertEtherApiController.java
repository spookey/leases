package su.toor.leases.controllers.alert;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.Map;
import java.util.UUID;

import jakarta.validation.Valid;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.leases.controllers.dtos.alert.AlertEtherDto;
import su.toor.leases.controllers.dtos.alert.AlertEtherRequestDto;
import su.toor.leases.controllers.dtos.side.PageDto;
import su.toor.leases.services.query.AlertEntityService;

@Timed
@Tag(name = "Alert Ether", description = "Manage device alerts")
@RestController
@RequestMapping("/api/alert/devices")
public class AlertEtherApiController {

    private static final Logger LOG = LoggerFactory.getLogger(AlertEtherApiController.class);

    static final String FIELD_BODY = "body";
    static final String FIELD_PAGE = "page";
    static final String FIELD_UUID = "uuid";

    private final AlertEntityService alertEntityService;

    @Autowired
    public AlertEtherApiController(
        final AlertEntityService alertEntityService
    ) {
        this.alertEntityService = alertEntityService;
    }

    @Timed
    @Operation(summary = "View all device alerts")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<AlertEtherDto>> allAlertEthers(
        @Parameter(name = "page", description = "Page number")
        @RequestParam(name = "page", defaultValue = "0")
        final Integer page,
        @Parameter(name = "size", description = "Page size")
        @RequestParam(name = "size", defaultValue = "0")
        final Integer size
    ) {
        final var alertEtherEntities = alertEntityService.findAlertEthers(page, size);
        final var response = PageDto.with(alertEtherEntities.map(AlertEtherDto::from));

        LOG.info("alert ethers requested [{}]", keyValue(FIELD_PAGE, response));

        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "View one device alert")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/{uuid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<AlertEtherDto> singleAlertEther(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid
    ) {
        LOG.info("single alert ether requested [{}]", keyValue(FIELD_UUID, uuid));
        final var optionalAlertEtherEntity = alertEntityService.findAlertEther(uuid);
        if (optionalAlertEtherEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = AlertEtherDto.from(optionalAlertEtherEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "Create new device alert")
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createAlertEther(
        @Valid @RequestBody final AlertEtherRequestDto body
    ) {
        LOG.info("creation of alert ether requested [{}]", keyValue(FIELD_BODY, body));

        final var optionalAlertEtherEntity = alertEntityService.createAlertEther(
            body.getEtherUuid(),
            body.isEnabled(),
            body.getTopic(),
            body.getTemplate()
        );
        if (optionalAlertEtherEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var alertEtherEntity = optionalAlertEtherEntity.get();
        return ResponseEntity.created(
            UriComponentsBuilder
                .fromPath("/api/alert/devices/{uuid}")
                .build(Map.of("uuid", alertEtherEntity.getUuid()))
        ).build();
    }

    @Timed
    @Operation(summary = "Modify existing device alert")
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PutMapping(value = "/{uuid}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<AlertEtherDto> modifyAlertEther(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid,
        @Valid @RequestBody final AlertEtherRequestDto body
    ) {
        LOG.info("modifying alert ether [{}]", keyValue(FIELD_UUID, uuid));

        final var optionalAlertEtherEntity = alertEntityService.modifyAlertEther(
            uuid,
            body.isEnabled(),
            body.getTopic(),
            body.getTemplate()
        );
        if (optionalAlertEtherEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = AlertEtherDto.from(optionalAlertEtherEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "Delete existing device alert")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @DeleteMapping(value = "/{uuid}")
    public ResponseEntity<Void> deleteAlertEther(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid
    ) {
        LOG.info("deletion of alert ether requested [{}]", keyValue(FIELD_UUID, uuid));
        if (!alertEntityService.deleteAlertEther(uuid)) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }
}
