package su.toor.leases.controllers.alert;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.Map;
import java.util.UUID;

import jakarta.validation.Valid;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.leases.controllers.dtos.alert.AlertGuideDto;
import su.toor.leases.controllers.dtos.alert.AlertGuideRequestDto;
import su.toor.leases.controllers.dtos.side.PageDto;
import su.toor.leases.services.query.AlertEntityService;

@Timed
@Tag(name = "Alert Guide", description = "Manage network alerts")
@RestController
@RequestMapping("/api/alert/networks")
public class AlertGuideApiController {

    private static final Logger LOG = LoggerFactory.getLogger(AlertGuideApiController.class);

    static final String FIELD_BODY = "body";
    static final String FIELD_PAGE = "page";
    static final String FIELD_UUID = "uuid";

    private final AlertEntityService alertEntityService;

    @Autowired
    public AlertGuideApiController(
        final AlertEntityService alertEntityService
    ) {
        this.alertEntityService = alertEntityService;
    }

    @Timed
    @Operation(summary = "View all network alerts")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<AlertGuideDto>> allAlertGuides(
        @Parameter(name = "page", description = "Page number")
        @RequestParam(name = "page", defaultValue = "0")
        final Integer page,
        @Parameter(name = "size", description = "Page size")
        @RequestParam(name = "size", defaultValue = "0")
        final Integer size
    ) {
        final var alertGuideEntities = alertEntityService.findAlertGuides(page, size);
        final var response = PageDto.with(alertGuideEntities.map(AlertGuideDto::from));

        LOG.info("alert guides requested [{}]", keyValue(FIELD_PAGE, response));

        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "View one network alert")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/{uuid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<AlertGuideDto> singleAlertGuide(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid
    ) {
        LOG.info("single alert guide requested [{}]", keyValue(FIELD_UUID, uuid));
        final var optionalAlertGuideEntity = alertEntityService.findAlertGuide(uuid);
        if (optionalAlertGuideEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = AlertGuideDto.from(optionalAlertGuideEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "Create new network alert")
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createAlertGuide(
        @Valid @RequestBody final AlertGuideRequestDto body
    ) {
        LOG.info("creation of alert guide requested [{}]", keyValue(FIELD_BODY, body));

        final var optionalAlertGuideEntity = alertEntityService.createAlertGuide(
            body.getGuideUuid(),
            body.isEnabled(),
            body.getTopic(),
            body.getTemplate()
        );
        if (optionalAlertGuideEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var alertGuideEntity = optionalAlertGuideEntity.get();
        return ResponseEntity.created(
            UriComponentsBuilder
                .fromPath("/api/alert/networks/{uuid}")
                .build(Map.of("uuid", alertGuideEntity.getUuid()))
        ).build();
    }

    @Timed
    @Operation(summary = "Modify existing network alert")
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PutMapping(value = "/{uuid}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<AlertGuideDto> modifyAlertGuide(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid,
        @Valid @RequestBody final AlertGuideRequestDto body
    ) {
        LOG.info("modifying alert guide [{}]", keyValue(FIELD_UUID, uuid));

        final var optionalAlertGuideEntity = alertEntityService.modifyAlertGuide(
            uuid,
            body.isEnabled(),
            body.getTopic(),
            body.getTemplate()
        );
        if (optionalAlertGuideEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = AlertGuideDto.from(optionalAlertGuideEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "Delete existing network alert")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @DeleteMapping(value = "/{uuid}")
    public ResponseEntity<Void> deleteAlertGuide(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid
    ) {
        LOG.info("deletion of alert guide requested [{}]", keyValue(FIELD_UUID, uuid));
        if (!alertEntityService.deleteAlertGuide(uuid)) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }
}
