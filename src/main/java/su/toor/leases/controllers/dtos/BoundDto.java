package su.toor.leases.controllers.dtos;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

import java.time.OffsetDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.style.ToStringCreator;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.parse.LeaseConverter;
import su.toor.leases.shared.Require;

public class BoundDto {

    @Schema(name = "uuid", description = "Unique identifier", accessMode = READ_ONLY)
    private final UUID uuid;
    @Schema(name = "address", description = "Address", accessMode = READ_ONLY)
    private final String address;
    @Schema(name = "created", description = "Creation timestamp", accessMode = READ_ONLY)
    private final OffsetDateTime created;
    @Schema(name = "updated", description = "Update timestamp", accessMode = READ_ONLY)
    private final OffsetDateTime updated;

    @JsonCreator
    BoundDto(
        @JsonProperty("uuid") final UUID uuid,
        @JsonProperty("address") final String address,
        @JsonProperty("created") final OffsetDateTime created,
        @JsonProperty("updated") final OffsetDateTime updated
    ) {
        this.uuid = Require.notNull(uuid, "uuid");
        this.address = Require.notNull(address, "address");
        this.created = Require.notNull(created, "created");
        this.updated = Require.notNull(updated, "updated");
    }

    @JsonIgnore
    public static BoundDto from(
        final BoundEntity boundEntity
    ) {
        Require.notNull(boundEntity, "boundEntity");
        return new BoundDto(
            boundEntity.getUuid(),
            LeaseConverter.formatAddress(boundEntity.getIp()),
            boundEntity.getCreated(),
            boundEntity.getUpdated()
        );
    }

    @JsonProperty("uuid")
    public UUID getUuid() {
        return uuid;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("created")
    public OffsetDateTime getCreated() {
        return created;
    }

    @JsonProperty("updated")
    public OffsetDateTime getUpdated() {
        return updated;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
            .append("uuid", getUuid())
            .append("address", getAddress())
            .append("created", getCreated())
            .append("updated", getUpdated())
            .toString();
    }
}
