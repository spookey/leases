package su.toor.leases.controllers.dtos;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

import java.time.OffsetDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.Nullable;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.shared.Require;

public class EtherDto {

    @Schema(name = "uuid", description = "Unique identifier", accessMode = READ_ONLY)
    private final UUID uuid;

    @Schema(name = "hostname", description = "Hostname", nullable = true, accessMode = READ_ONLY)
    private final String hostname;

    @Schema(name = "created", description = "Creation timestamp", accessMode = READ_ONLY)
    private final OffsetDateTime created;

    @Schema(name = "updated", description = "Update timestamp", accessMode = READ_ONLY)
    private final OffsetDateTime updated;

    @JsonCreator
    EtherDto(
        @JsonProperty("uuid") final UUID uuid,
        @JsonProperty("hostname") final String hostname,
        @JsonProperty("created") final OffsetDateTime created,
        @JsonProperty("updated") final OffsetDateTime updated
    ) {
        this.uuid = Require.notNull(uuid, "uuid");
        this.hostname = hostname;
        this.created = Require.notNull(created, "created");
        this.updated = Require.notNull(updated, "updated");
    }

    @JsonIgnore
    public static EtherDto from(
        final EtherEntity etherEntity
    ) {
        Require.notNull(etherEntity, "etherEntity");
        return new EtherDto(
            etherEntity.getUuid(),
            etherEntity.getHostname(),
            etherEntity.getCreated(),
            etherEntity.getUpdated()
        );
    }

    @JsonProperty("uuid")
    public UUID getUuid() {
        return uuid;
    }

    @Nullable
    @JsonProperty("hostname")
    public String getHostname() {
        return hostname;
    }

    @JsonProperty("created")
    public OffsetDateTime getCreated() {
        return created;
    }

    @JsonProperty("updated")
    public OffsetDateTime getUpdated() {
        return updated;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
            .append("uuid", getUuid())
            .append("hostname", getHostname())
            .append("created", getCreated())
            .append("updated", getUpdated())
            .toString();
    }
}
