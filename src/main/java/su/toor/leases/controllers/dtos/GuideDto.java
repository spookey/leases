package su.toor.leases.controllers.dtos;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

import java.time.OffsetDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.style.ToStringCreator;
import su.toor.leases.controllers.dtos.side.BaseGuideDto;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.parse.LeaseConverter;
import su.toor.leases.shared.Require;

public class GuideDto extends BaseGuideDto {

    @Schema(name = "weight", description = "Sort weight", accessMode = READ_ONLY)
    private final Integer weight;
    @Schema(name = "created", description = "Creation timestamp", accessMode = READ_ONLY)
    private final OffsetDateTime created;
    @Schema(name = "updated", description = "Update timestamp", accessMode = READ_ONLY)
    private final OffsetDateTime updated;

    @JsonCreator
    GuideDto(
        @JsonProperty("uuid") final UUID uuid,
        @JsonProperty("mask") final String mask,
        @JsonProperty("weight") final Integer weight,
        @JsonProperty("title") final String title,
        @JsonProperty("description") final String description,
        @JsonProperty("created") final OffsetDateTime created,
        @JsonProperty("updated") final OffsetDateTime updated
    ) {
        super(uuid, mask, title, description);
        this.weight = Require.notNull(weight, "weight");
        this.created = Require.notNull(created, "created");
        this.updated = Require.notNull(updated, "updated");
    }

    @JsonIgnore
    public static GuideDto from(
        final GuideEntity guideEntity
    ) {
        Require.notNull(guideEntity, "guideEntity");
        return new GuideDto(
            guideEntity.getUuid(),
            LeaseConverter.formatAddress(guideEntity.getMask()),
            guideEntity.getWeight(),
            guideEntity.getTitle(),
            guideEntity.getDescription(),
            guideEntity.getCreated(),
            guideEntity.getUpdated()
        );
    }

    @JsonProperty("weight")
    public Integer getWeight() {
        return weight;
    }

    @JsonProperty("created")
    public OffsetDateTime getCreated() {
        return created;
    }

    @JsonProperty("updated")
    public OffsetDateTime getUpdated() {
        return updated;
    }

    @Override
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
            .append("weight", getWeight())
            .append("created", getCreated())
            .append("updated", getUpdated());
    }
}
