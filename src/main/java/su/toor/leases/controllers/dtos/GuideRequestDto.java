package su.toor.leases.controllers.dtos;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.WRITE_ONLY;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.Nullable;
import su.toor.leases.shared.Require;

public class GuideRequestDto {

    @Schema(name = "mask", description = "Network range", accessMode = WRITE_ONLY)
    private final String mask;
    @Schema(name = "weight", description = "Sort weight", nullable = true, accessMode = WRITE_ONLY)
    private final Integer weight;
    @Schema(name = "title", description = "Main title", accessMode = WRITE_ONLY)
    private final String title;
    @Schema(name = "description", description = "Brief description", nullable = true, accessMode = READ_ONLY)
    private final String description;

    @JsonCreator
    public GuideRequestDto(
        @JsonProperty("mask") final String mask,
        @JsonProperty("weight") final Integer weight,
        @JsonProperty("title") final String title,
        @JsonProperty("description") final String description
    ) {
        this.mask = Require.notNull(mask, "mask");
        this.weight = weight;
        this.title = Require.notBlank(title, "title");
        this.description = description;
    }

    @JsonProperty("mask")
    public String getMask() {
        return mask;
    }

    @Nullable
    @JsonProperty("weight")
    public Integer getWeight() {
        return weight;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @Nullable
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
            .append("mask", getMask())
            .append("weight", getWeight())
            .append("title", getTitle())
            .append("description", getDescription())
            .toString();
    }
}
