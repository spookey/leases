package su.toor.leases.controllers.dtos;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

import java.time.OffsetDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.style.ToStringCreator;
import su.toor.leases.database.StampEntity;
import su.toor.leases.shared.Require;

public class StampDto {

    @Schema(name = "uuid", description = "Unique identifier", accessMode = READ_ONLY)
    private final UUID uuid;
    @Schema(name = "value", description = "Timestamp value", accessMode = READ_ONLY)
    private final OffsetDateTime value;

    @JsonCreator
    StampDto(
        @JsonProperty("uuid") final UUID uuid,
        @JsonProperty("value") final OffsetDateTime value
    ) {
        this.uuid = Require.notNull(uuid, "uuid");
        this.value = Require.notNull(value, "value");
    }

    @JsonIgnore
    public static StampDto from(
        final StampEntity stampEntity
    ) {
        Require.notNull(stampEntity, "stampEntity");
        return new StampDto(
            stampEntity.getUuid(),
            stampEntity.getValue()
        );
    }

    @JsonProperty("uuid")
    public UUID getUuid() {
        return uuid;
    }

    @JsonProperty("value")
    public OffsetDateTime getValue() {
        return value;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
            .append("uuid", getUuid())
            .append("value", getValue())
            .toString();
    }
}
