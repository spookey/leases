package su.toor.leases.controllers.dtos;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.style.ToStringCreator;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.shared.Require;
import su.toor.leases.shared.State;

public class TrackDto {

    @Schema(name = "uuid", description = "Unique identifier", accessMode = READ_ONLY)
    private final UUID uuid;
    @Schema(name = "stamp", description = "Stamp entity", accessMode = READ_ONLY)
    private final StampDto stamp;
    @Schema(name = "ether", description = "Ether entity", accessMode = READ_ONLY)
    private final EtherDto ether;
    @Schema(name = "bound", description = "Bound entity", accessMode = READ_ONLY)
    private final BoundDto bound;
    @Schema(name = "state", description = "State", accessMode = READ_ONLY)
    private final State state;

    @JsonCreator
    TrackDto(
        @JsonProperty("uuid") final UUID uuid,
        @JsonProperty("stamp") final StampDto stamp,
        @JsonProperty("ether") final EtherDto ether,
        @JsonProperty("bound") final BoundDto bound,
        @JsonProperty("state") final State state
    ) {
        this.uuid = Require.notNull(uuid, "uuid");
        this.stamp = Require.notNull(stamp, "stamp");
        this.ether = Require.notNull(ether, "ether");
        this.bound = Require.notNull(bound, "bound");
        this.state = Require.notNull(state, "state");
    }

    @JsonIgnore
    public static TrackDto from(
        final TrackEntity trackEntity
    ) {
        Require.notNull(trackEntity, "trackEntity");
        return new TrackDto(
            trackEntity.getUuid(),
            StampDto.from(trackEntity.getStampEntity()),
            EtherDto.from(trackEntity.getEtherEntity()),
            BoundDto.from(trackEntity.getBoundEntity()),
            trackEntity.getState()
        );
    }

    @JsonProperty("uuid")
    public UUID getUuid() {
        return uuid;
    }

    @JsonProperty("stamp")
    public StampDto getStamp() {
        return stamp;
    }

    @JsonProperty("ether")
    public EtherDto getEther() {
        return ether;
    }

    @JsonProperty("bound")
    public BoundDto getBound() {
        return bound;
    }

    @JsonProperty("state")
    public State getState() {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
            .append("uuid", getUuid())
            .append("stamp", getStamp())
            .append("ether", getEther())
            .append("bound", getBound())
            .append("state", getState())
            .toString();
    }
}
