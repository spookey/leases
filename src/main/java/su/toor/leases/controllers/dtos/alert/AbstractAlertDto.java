package su.toor.leases.controllers.dtos.alert;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

import java.time.OffsetDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.Nullable;
import su.toor.leases.shared.Require;

abstract class AbstractAlertDto {

    @Schema(name = "uuid", description = "Unique identifier", accessMode = READ_ONLY)
    private final UUID uuid;
    @Schema(name = "enabled", description = "Trigger status", accessMode = READ_ONLY)
    private final Boolean enabled;
    @Schema(name = "topic", description = "MQTT Topic", nullable = true, accessMode = READ_ONLY)
    private final String topic;
    @Schema(name = "template", description = "Template placeholder", nullable = true, accessMode = READ_ONLY)
    private final String template;
    @Schema(name = "created", description = "Creation timestamp", accessMode = READ_ONLY)
    private final OffsetDateTime created;
    @Schema(name = "updated", description = "Update timestamp", accessMode = READ_ONLY)
    private final OffsetDateTime updated;


    AbstractAlertDto(
        final UUID uuid,
        final Boolean enabled,
        final String topic,
        final String template,
        final OffsetDateTime created,
        final OffsetDateTime updated
    ) {
        this.uuid = Require.notNull(uuid, "uuid");
        this.enabled = Require.notNull(enabled, "enabled");
        this.topic = topic;
        this.template = template;
        this.created = Require.notNull(created, "created");
        this.updated = Require.notNull(updated, "updated");
    }

    @JsonProperty("uuid")
    public UUID getUuid() {
        return uuid;
    }

    @JsonProperty("enabled")
    public Boolean isEnabled() {
        return enabled;
    }

    @Nullable
    @JsonProperty
    public String getTopic() {
        return topic;
    }

    @Nullable
    @JsonProperty
    public String getTemplate() {
        return template;
    }

    @JsonProperty("created")
    public OffsetDateTime getCreated() {
        return created;
    }

    @JsonProperty("updated")
    public OffsetDateTime getUpdated() {
        return updated;
    }

    protected ToStringCreator toStringCreator() {
        return new ToStringCreator(this)
            .append("uuid", getUuid())
            .append("enabled", isEnabled())
            .append("topic", getTopic())
            .append("template", getTemplate())
            .append("created", getCreated())
            .append("updated", getUpdated());
    }
}
