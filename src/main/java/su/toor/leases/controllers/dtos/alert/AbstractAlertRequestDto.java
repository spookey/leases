package su.toor.leases.controllers.dtos.alert;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.WRITE_ONLY;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.Nullable;
import su.toor.leases.shared.Require;

abstract class AbstractAlertRequestDto {

    @Schema(name = "enabled", description = "Trigger status", accessMode = WRITE_ONLY)
    private final Boolean enabled;
    @Schema(name = "topic", description = "MQTT Topic", nullable = true, accessMode = WRITE_ONLY)
    private final String topic;
    @Schema(name = "template", description = "Template placeholder", nullable = true, accessMode = WRITE_ONLY)
    private final String template;

    AbstractAlertRequestDto(
        final Boolean enabled,
        final String topic,
        final String template
    ) {
        this.enabled = Require.notNull(enabled, "enabled");
        this.topic = topic;
        this.template = template;
    }

    @JsonProperty("enabled")
    public Boolean isEnabled() {
        return enabled;
    }

    @Nullable
    @JsonProperty
    public String getTopic() {
        return topic;
    }

    @Nullable
    @JsonProperty
    public String getTemplate() {
        return template;
    }

    protected ToStringCreator toStringCreator() {
        return new ToStringCreator(this)
            .append("enabled", isEnabled())
            .append("topic", getTopic())
            .append("template", getTemplate());
    }
}
