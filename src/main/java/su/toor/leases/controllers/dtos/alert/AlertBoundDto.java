package su.toor.leases.controllers.dtos.alert;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

import java.time.OffsetDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.shared.Require;

public class AlertBoundDto extends AbstractAlertDto {

    @Schema(name = "boundUuid", description = "Unique identifier of bound", accessMode = READ_ONLY)
    private final UUID boundUuid;

    @JsonCreator
    AlertBoundDto(
        @JsonProperty("uuid") final UUID uuid,
        @JsonProperty("boundUuid") final UUID boundUuid,
        @JsonProperty("enabled") final Boolean enabled,
        @JsonProperty("topic") final String topic,
        @JsonProperty("template") final String template,
        @JsonProperty("created") final OffsetDateTime created,
        @JsonProperty("updated") final OffsetDateTime updated
    ) {
        super(uuid, enabled, topic, template, created, updated);
        this.boundUuid = Require.notNull(boundUuid, "boundUuid");
    }

    @JsonIgnore
    public static AlertBoundDto from(
        final AlertBoundEntity alertBoundEntity
    ) {
        Require.notNull(alertBoundEntity, "alertBoundEntity");
        final var boundEntity = alertBoundEntity.getBoundEntity();
        return new AlertBoundDto(
            alertBoundEntity.getUuid(),
            boundEntity.getUuid(),
            alertBoundEntity.isEnabled(),
            alertBoundEntity.getTopic(),
            alertBoundEntity.getTemplate(),
            alertBoundEntity.getCreated(),
            alertBoundEntity.getUpdated()
        );
    }

    @JsonProperty("boundUuid")
    public UUID getBoundUuid() {
        return boundUuid;
    }

    @Override
    public String toString() {
        return toStringCreator()
            .append("boundUuid", getBoundUuid())
            .toString();
    }
}
