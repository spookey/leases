package su.toor.leases.controllers.dtos.alert;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.WRITE_ONLY;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import su.toor.leases.shared.Require;

public class AlertBoundRequestDto extends AbstractAlertRequestDto {

    @Schema(name = "boundUuid", description = "Unique identifier of bound", accessMode = WRITE_ONLY)
    private final UUID boundUuid;

    @JsonCreator
    public AlertBoundRequestDto(
        @JsonProperty("boundUuid") final UUID boundUuid,
        @JsonProperty("enabled") final Boolean enabled,
        @JsonProperty("topic") final String topic,
        @JsonProperty("template") final String template
    ) {
        super(enabled, topic, template);
        this.boundUuid = Require.notNull(boundUuid, "boundUuid");
    }

    @JsonProperty("boundUuid")
    public UUID getBoundUuid() {
        return boundUuid;
    }

    @Override
    public String toString() {
        return super.toStringCreator()
            .append("boundUuid", getBoundUuid())
            .toString();
    }

}
