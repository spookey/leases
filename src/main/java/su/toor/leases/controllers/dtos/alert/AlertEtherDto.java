package su.toor.leases.controllers.dtos.alert;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

import java.time.OffsetDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.shared.Require;

public class AlertEtherDto extends AbstractAlertDto {

    @Schema(name = "etherUuid", description = "Unique identifier of ether", accessMode = READ_ONLY)
    private final UUID etherUuid;

    @JsonCreator
    AlertEtherDto(
        @JsonProperty("uuid") final UUID uuid,
        @JsonProperty("etherUuid") final UUID etherUuid,
        @JsonProperty("enabled") final Boolean enabled,
        @JsonProperty("topic") final String topic,
        @JsonProperty("template") final String template,
        @JsonProperty("created") final OffsetDateTime created,
        @JsonProperty("updated") final OffsetDateTime updated
    ) {
        super(uuid, enabled, topic, template, created, updated);
        this.etherUuid = Require.notNull(etherUuid, "etherUuid");
    }

    @JsonIgnore
    public static AlertEtherDto from(
        final AlertEtherEntity alertEtherEntity
    ) {
        Require.notNull(alertEtherEntity, "alertEtherEntity");
        final var etherEntity = alertEtherEntity.getEtherEntity();
        return new AlertEtherDto(
            alertEtherEntity.getUuid(),
            etherEntity.getUuid(),
            alertEtherEntity.isEnabled(),
            alertEtherEntity.getTopic(),
            alertEtherEntity.getTemplate(),
            alertEtherEntity.getCreated(),
            alertEtherEntity.getUpdated()
        );
    }

    @JsonProperty("etherUuid")
    public UUID getEtherUuid() {
        return etherUuid;
    }

    @Override
    public String toString() {
        return toStringCreator()
            .append("etherUuid", getEtherUuid())
            .toString();
    }
}
