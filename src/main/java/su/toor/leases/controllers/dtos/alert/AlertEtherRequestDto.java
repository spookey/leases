package su.toor.leases.controllers.dtos.alert;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.WRITE_ONLY;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import su.toor.leases.shared.Require;

public class AlertEtherRequestDto extends AbstractAlertRequestDto {

    @Schema(name = "etherUuid", description = "Unique identifier of ether", accessMode = WRITE_ONLY)
    private final UUID etherUuid;

    @JsonCreator
    public AlertEtherRequestDto(
        @JsonProperty("etherUuid") final UUID etherUuid,
        @JsonProperty("enabled") final Boolean enabled,
        @JsonProperty("topic") final String topic,
        @JsonProperty("template") final String template
    ) {
        super(enabled, topic, template);
        this.etherUuid = Require.notNull(etherUuid, "etherUuid");
    }

    @JsonProperty("etherUuid")
    public UUID getEtherUuid() {
        return etherUuid;
    }

    @Override
    public String toString() {
        return super.toStringCreator()
            .append("etherUuid", getEtherUuid())
            .toString();
    }

}
