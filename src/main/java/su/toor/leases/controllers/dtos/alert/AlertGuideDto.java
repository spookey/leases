package su.toor.leases.controllers.dtos.alert;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

import java.time.OffsetDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import su.toor.leases.database.alert.AlertGuideEntity;
import su.toor.leases.shared.Require;

public class AlertGuideDto extends AbstractAlertDto {

    @Schema(name = "guideUuid", description = "Unique identifier of guide", accessMode = READ_ONLY)
    private final UUID guideUuid;

    @JsonCreator
    AlertGuideDto(
        @JsonProperty("uuid") final UUID uuid,
        @JsonProperty("guideUuid") final UUID guideUuid,
        @JsonProperty("enabled") final Boolean enabled,
        @JsonProperty("topic") final String topic,
        @JsonProperty("template") final String template,
        @JsonProperty("created") final OffsetDateTime created,
        @JsonProperty("updated") final OffsetDateTime updated
    ) {
        super(uuid, enabled, topic, template, created, updated);
        this.guideUuid = Require.notNull(guideUuid, "guideUuid");
    }

    @JsonIgnore
    public static AlertGuideDto from(
        final AlertGuideEntity alertGuideEntity
    ) {
        Require.notNull(alertGuideEntity, "alertGuideEntity");
        final var guideEntity = alertGuideEntity.getGuideEntity();
        return new AlertGuideDto(
            alertGuideEntity.getUuid(),
            guideEntity.getUuid(),
            alertGuideEntity.isEnabled(),
            alertGuideEntity.getTopic(),
            alertGuideEntity.getTemplate(),
            alertGuideEntity.getCreated(),
            alertGuideEntity.getUpdated()
        );
    }

    @JsonProperty("guideUuid")
    public UUID getGuideUuid() {
        return guideUuid;
    }

    @Override
    public String toString() {
        return toStringCreator()
            .append("guideUuid", getGuideUuid())
            .toString();
    }
}
