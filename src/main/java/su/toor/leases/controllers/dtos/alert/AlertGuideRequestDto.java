package su.toor.leases.controllers.dtos.alert;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.WRITE_ONLY;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import su.toor.leases.shared.Require;

public class AlertGuideRequestDto extends AbstractAlertRequestDto {

    @Schema(name = "guideUuid", description = "Unique identifier of guide", accessMode = WRITE_ONLY)
    private final UUID guideUuid;

    @JsonCreator
    public AlertGuideRequestDto(
        @JsonProperty("guideUuid") final UUID guideUuid,
        @JsonProperty("enabled") final Boolean enabled,
        @JsonProperty("topic") final String topic,
        @JsonProperty("template") final String template
    ) {
        super(enabled, topic, template);
        this.guideUuid = Require.notNull(guideUuid, "guideUuid");
    }

    @JsonProperty("guideUuid")
    public UUID getGuideUuid() {
        return guideUuid;
    }

    @Override
    public String toString() {
        return super.toStringCreator()
            .append("guideUuid", getGuideUuid())
            .toString();
    }
}
