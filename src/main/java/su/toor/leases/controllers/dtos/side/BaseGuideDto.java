package su.toor.leases.controllers.dtos.side;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.Nullable;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.parse.LeaseConverter;
import su.toor.leases.shared.Require;

public class BaseGuideDto {

    @Schema(name = "uuid", description = "Unique identifier", accessMode = READ_ONLY)
    private final UUID uuid;
    @Schema(name = "mask", description = "Network range", accessMode = READ_ONLY)
    private final String mask;
    @Schema(name = "title", description = "Main title", accessMode = READ_ONLY)
    private final String title;
    @Schema(name = "description", description = "Brief description", nullable = true, accessMode = READ_ONLY)
    private final String description;

    @JsonCreator
    protected BaseGuideDto(
        @JsonProperty("uuid") final UUID uuid,
        @JsonProperty("mask") final String mask,
        @JsonProperty("title") final String title,
        @JsonProperty("description") final String description
    ) {
        this.uuid = Require.notNull(uuid, "uuid");
        this.mask = Require.notBlank(mask, "mask");
        this.title = Require.notBlank(title, "title");
        this.description = description;
    }

    @JsonIgnore
    public static BaseGuideDto from(
        final GuideEntity guideEntity
    ) {
        Require.notNull(guideEntity, "guideEntity");
        return new BaseGuideDto(
            guideEntity.getUuid(),
            LeaseConverter.formatAddress(guideEntity.getMask()),
            guideEntity.getTitle(),
            guideEntity.getDescription()
        );
    }

    @JsonProperty("uuid")
    public UUID getUuid() {
        return uuid;
    }

    @JsonProperty("mask")
    public String getMask() {
        return mask;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @Nullable
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonIgnore
    protected ToStringCreator toStringCreator() {
        return new ToStringCreator(this)
            .append("uuid", getUuid())
            .append("mask", getMask())
            .append("title", getTitle())
            .append("description", getDescription());
    }

    @Override
    public String toString() {
        return toStringCreator()
            .toString();
    }
}
