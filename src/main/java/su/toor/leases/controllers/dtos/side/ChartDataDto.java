package su.toor.leases.controllers.dtos.side;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.style.ToStringCreator;
import su.toor.leases.database.StampEntity;
import su.toor.leases.shared.Require;

public class ChartDataDto {

    @Schema(name = "x", description = "X value", accessMode = READ_ONLY)
    private final Long xValue;
    @Schema(name = "y", description = "y value", accessMode = READ_ONLY)
    private final Long yValue;

    @JsonCreator
    ChartDataDto(
        @JsonProperty("x") final Long xValue,
        @JsonProperty("y") final Long yValue
    ) {
        this.xValue = Require.notNull(xValue, "xValue");
        this.yValue = Require.notNull(yValue, "yValue");
    }

    @JsonIgnore
    static ChartDataDto with(
        final StampEntity stampEntity,
        final Long yValue
    ) {
        Require.notNull(stampEntity, "stampEntity");
        final var value = stampEntity.getValue();
        final var instant = value.toInstant();

        return new ChartDataDto(
            instant.toEpochMilli(),
            yValue
        );
    }

    @JsonIgnore
    public static ChartDataDto from(
        final Map.Entry<StampEntity, Long> entry
    ) {
        Require.notNull(entry, "entry");
        return with(entry.getKey(), entry.getValue());
    }

    @JsonProperty("x")
    public Long getX() {
        return xValue;
    }

    @JsonProperty("y")
    public Long getY() {
        return yValue;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
            .append("x", getX())
            .append("y", getY())
            .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(xValue, yValue);
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }

        final var that = (ChartDataDto) other;
        return this.xValue.equals(that.xValue)
            && this.yValue.equals(that.yValue);
    }
}
