package su.toor.leases.controllers.dtos.side;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.style.ToStringCreator;
import su.toor.leases.database.StampEntity;
import su.toor.leases.shared.Require;
import su.toor.leases.shared.State;

public class ChartSeriesDto {

    @Schema(name = "name", description = "Series name", accessMode = READ_ONLY)
    private final String name;
    @Schema(name = "data", description = "Series data", accessMode = READ_ONLY)
    private final List<ChartDataDto> data;

    @JsonCreator
    public ChartSeriesDto(
        @JsonProperty("name") final String name,
        @JsonProperty("data") final List<ChartDataDto> data
    ) {
        this.name = Require.notBlank(name, "name");
        this.data = Require.notNull(data, "data");
    }

    @JsonIgnore
    public static ChartSeriesDto from(
        final State state,
        final Map<StampEntity, Long> counts
    ) {
        Require.notNull(state, "state");
        Require.notNull(counts, "counts");

        return new ChartSeriesDto(
            state.getText(),
            counts.entrySet().stream()
                .map(ChartDataDto::from)
                .sorted(Comparator.comparingLong(ChartDataDto::getX))
                .collect(Collectors.toList())
        );
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("data")
    public List<ChartDataDto> getData() {
        return data;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
            .append("name", getName())
            .append("data", getData())
            .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, data);
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }

        final var that = (ChartSeriesDto) other;
        return this.name.equals(that.name)
            && Objects.equals(this.data, that.data);
    }
}
