package su.toor.leases.controllers.dtos.side;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.core.style.ToStringCreator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import su.toor.leases.shared.Require;

public class PageDto<T> {

    private final Page<T> page;

    PageDto(
        final Page<T> page
    ) {
        this.page = Require.notNull(page, "page");
    }

    @JsonIgnore
    public static <T> PageDto<T> with(
        final Page<T> page
    ) {
        return new PageDto<>(page);
    }

    @JsonCreator
    @JsonIgnoreProperties(ignoreUnknown = true)
    static <T> PageDto<T> with(
        @JsonProperty("total") final Long total,
        @JsonProperty("number") final Integer number,
        @JsonProperty("size") final Integer size,
        @JsonProperty("items") final List<T> items
    ) {
        return with(new PageImpl<>(
            items,
            PageRequest.of(number, size),
            total
        ));
    }

    @JsonProperty("pages")
    public Integer getPages() {
        return page.getTotalPages();
    }

    @JsonProperty("total")
    public Long getTotal() {
        return page.getTotalElements();
    }

    @JsonProperty("number")
    public Integer getNumber() {
        return page.getNumber();
    }

    @JsonProperty("size")
    public Integer getSize() {
        return page.getSize();
    }

    @JsonProperty("first")
    public Boolean isFirst() {
        return page.isFirst();
    }

    @JsonProperty("last")
    public Boolean isLast() {
        return page.isLast();
    }

    @JsonProperty("next")
    public Boolean hasNext() {
        return page.hasNext();
    }

    @JsonProperty("previous")
    public Boolean hasPrevious() {
        return page.hasPrevious();
    }

    @JsonProperty("items")
    public List<T> getItems() {
        return page.getContent();
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
            .append("pages", getPages())
            .append("total", getTotal())
            .append("number", getNumber())
            .append("size", getSize())
            .append("first", isFirst())
            .append("last", isLast())
            .append("next", hasNext())
            .append("previous", hasPrevious())
            .append("items", getItems())
            .toString();
    }
}
