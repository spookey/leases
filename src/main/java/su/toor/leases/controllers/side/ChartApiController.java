package su.toor.leases.controllers.side;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;
import java.util.UUID;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import su.toor.leases.controllers.dtos.side.BaseGuideDto;
import su.toor.leases.controllers.dtos.side.ChartSeriesDto;
import su.toor.leases.services.ChartService;

@Timed
@Tag(name = "Charts", description = "Retrieve chart data")
@RestController
@RequestMapping("/api")
public class ChartApiController {

    private static final Logger LOG = LoggerFactory.getLogger(ChartApiController.class);

    static final String FIELD_UUID = "uuid";

    private final ChartService chartService;

    @Autowired
    public ChartApiController(
        final ChartService chartService
    ) {
        this.chartService = chartService;
    }

    @Timed
    @Operation(summary = "Get networks for charts")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = { "/chart/networks", "/chart-networks.json" }, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<BaseGuideDto>> chartGuides() {
        LOG.info("chart guides requested");

        final var response = chartService.constructBaseGuides();
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(summary = "Get payload for charts")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = {"/chart/{uuid}", "chart-{uuid}.json"}, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ChartSeriesDto>> chartSeries(
        @Parameter(name = "uuid", description = "Unique identifier")
        @PathVariable(name = "uuid")
        final UUID uuid
    ) {
        LOG.info("chart series requested [{}]", keyValue(FIELD_UUID, uuid));

        final var optionalSeries = chartService.constructChartSeries(uuid);
        if (optionalSeries.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = optionalSeries.get();
        return ResponseEntity.ok(response);
    }

    @Timed
    @Hidden
    @Operation(summary = "Dump chart data to disk")
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PostMapping(value = "/chart/dump")
    public ResponseEntity<Void> chartDump() {
        LOG.info("chart dump requested");

        if (!chartService.performChartDump()) {
            return ResponseEntity.internalServerError().build();
        }
        return ResponseEntity.ok().build();
    }
}
