package su.toor.leases.controllers.side;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

import java.util.Map;

import jakarta.validation.Valid;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.leases.components.Importer;

@Timed
@Tag(name = "Import", description = "Trigger imports")
@RestController
@RequestMapping("/api/import")
public class ImportApiController {

    private static final Logger LOG = LoggerFactory.getLogger(ImportApiController.class);

    static final String FIELD_STAMP = "stamp";

    private final Importer importer;

    @Autowired
    public ImportApiController(
        final Importer importer
    ) {
        this.importer = importer;
    }

    @Timed
    @Operation(summary = "Import lease file content")
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "422", description = "Very bad")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PostMapping(consumes = TEXT_PLAIN_VALUE)
    public ResponseEntity<Void> importLeases(
        @Valid @RequestBody final String text
    ) {
        final var optionalStampEntity = importer.run(text);
        if (optionalStampEntity.isEmpty()) {
            return ResponseEntity.unprocessableEntity().build();
        }

        final var stampEntity = optionalStampEntity.get();
        LOG.info("imported leases [{}]", keyValue(FIELD_STAMP, stampEntity));

        return ResponseEntity.created(
            UriComponentsBuilder.fromPath("/api/tracks/{uuid}")
                .build(Map.of("uuid", stampEntity.getUuid()))
        ).build();
    }

}
