package su.toor.leases.controllers.side;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RouteController {

    private static final Logger LOG = LoggerFactory.getLogger(RouteController.class);

    @GetMapping(value = "/{path:[^\\\\.]*}")
    public ModelAndView redirect(
        @PathVariable(value = "path") final String path
    ) {
        LOG.info("page requested [{}]", keyValue("path", path));

        return new ModelAndView("forward:/");
    }
}
