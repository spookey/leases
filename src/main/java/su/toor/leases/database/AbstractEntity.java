package su.toor.leases.database;

import java.util.Objects;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;

import org.hibernate.annotations.JdbcTypeCode;
import org.springframework.core.style.ToStringCreator;

@MappedSuperclass
public abstract class AbstractEntity<T extends AbstractEntity<T>> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false, unique = true)
    private Long id;

    @Column(name = "uuid", nullable = false, updatable = false, unique = true)
    @JdbcTypeCode(java.sql.Types.VARCHAR)
    private UUID uuid = UUID.randomUUID();

    @Transient
    protected abstract T get();

    public Long getId() {
        return id;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Transient
    protected ToStringCreator toStringCreator() {
        return new ToStringCreator(this)
            .append("id", getId())
            .append("uuid", getUuid());
    }

    @Transient
    @Override
    public String toString() {
        return toStringCreator()
            .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }

        final var that = (AbstractEntity<? extends AbstractEntity<T>>) other;
        return Objects.equals(this.uuid, that.uuid);
    }
}
