package su.toor.leases.database;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;

import org.springframework.core.style.ToStringCreator;

@MappedSuperclass
public abstract class AbstractStampEntity<T extends AbstractStampEntity<T>> extends AbstractEntity<T> {

    @Column(name = "created", updatable = false, nullable = false, columnDefinition = "TIMESTAMP")
    private OffsetDateTime created = OffsetDateTime.now(ZoneOffset.UTC);

    @Column(name = "updated", nullable = false, columnDefinition = "TIMESTAMP")
    protected OffsetDateTime updated = OffsetDateTime.now(ZoneOffset.UTC);

    public OffsetDateTime getCreated() {
        return created;
    }

    public OffsetDateTime getUpdated() {
        return updated;
    }

    @Transient
    public T update() {
        this.updated = OffsetDateTime.now(ZoneOffset.UTC);
        return get();
    }

    @Transient
    @Override
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
            .append("created", getCreated())
            .append("updated", getUpdated());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(final Object other) {
        return super.equals(other);
    }
}
