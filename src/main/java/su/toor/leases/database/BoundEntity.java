package su.toor.leases.database;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import inet.ipaddr.IPAddress;
import org.springframework.core.style.ToStringCreator;
import su.toor.leases.parse.LeaseConverter;

@Entity
@Table(
    name = "bound",
    indexes = {
        @Index(name = "idx_uuid", columnList = "uuid", unique = true),
        @Index(name = "idx_ip_value", columnList = "ip_value", unique = true),
    }
)
public class BoundEntity extends AbstractStampEntity<BoundEntity> {

    @Column(name = "ip_value", updatable = false, nullable = false, unique = true)
    private String ipValue;


    public static BoundEntity with(
        final IPAddress ip
    ) {
        final var entity = new BoundEntity();
        entity.ipValue = LeaseConverter.formatAddress(ip);
        return entity
            .update();
    }

    @Transient
    @Override
    protected BoundEntity get() {
        return this;
    }

    String getIpValue() {
        return ipValue;
    }

    public IPAddress getIp() {
        return LeaseConverter.convertIpAddress(getIpValue());
    }

    @Transient
    @Override
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
            .append("ipValue", getIpValue());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(final Object other) {
        return super.equals(other);
    }
}
