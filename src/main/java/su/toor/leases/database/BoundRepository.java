package su.toor.leases.database;

import java.util.Optional;
import java.util.UUID;

import inet.ipaddr.IPAddress;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import su.toor.leases.parse.LeaseConverter;

@Repository
public interface BoundRepository extends JpaRepository<BoundEntity, Long> {
    Optional<BoundEntity> findFirstByUuid(final UUID uuid);

    Optional<BoundEntity> findFirstByIpValue(final String ipValue);
    default Optional<BoundEntity> findFirstByIp(final IPAddress ip) {
        return findFirstByIpValue(ip != null ? LeaseConverter.formatAddress(ip) : null);
    }

    Page<BoundEntity> findAllByOrderByIpValueAsc(final Pageable pageable);
}
