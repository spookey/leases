package su.toor.leases.database;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import inet.ipaddr.mac.MACAddress;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.Nullable;
import su.toor.leases.parse.LeaseConverter;

@Entity
@Table(
    name = "ether",
    indexes = {
        @Index(name = "idx_uuid", columnList = "uuid", unique = true),
        @Index(name = "idx_mac_value", columnList = "mac_value", unique = true),
        @Index(name = "idx_hostname", columnList = "hostname"),
    }
)
public class EtherEntity extends AbstractStampEntity<EtherEntity> {

    @Column(name = "mac_value", updatable = false, nullable = false, unique = true)
    private String macValue;

    @Column(name = "hostname")
    private String hostname;


    public static EtherEntity with(
        final MACAddress mac,
        final String hostname
    ) {
        final var entity = new EtherEntity();
        entity.macValue = LeaseConverter.formatAddress(mac);
        return entity
            .setHostname(hostname)
            .update();
    }

    @Transient
    @Override
    protected EtherEntity get() {
        return this;
    }

    String getMacValue() {
        return macValue;
    }

    public MACAddress getMac() {
        return LeaseConverter.convertMacAddress(getMacValue());
    }

    @Nullable
    public String getHostname() {
        return hostname;
    }

    public EtherEntity setHostname(final String hostname) {
        this.hostname = hostname;
        return this.update();
    }

    @Transient
    @Override
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
            .append("macValue", getMacValue())
            .append("hostname", getHostname());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(final Object other) {
        return super.equals(other);
    }
}
