package su.toor.leases.database;

import java.util.Optional;
import java.util.UUID;

import inet.ipaddr.mac.MACAddress;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import su.toor.leases.parse.LeaseConverter;

@Repository
public interface EtherRepository extends JpaRepository<EtherEntity, Long> {
    Optional<EtherEntity> findFirstByUuid(final UUID uuid);

    Optional<EtherEntity> findFirstByMacValue(final String macValue);
    default Optional<EtherEntity> findFirstByMac(final MACAddress mac) {
        return findFirstByMacValue(mac != null ? LeaseConverter.formatAddress(mac) : null);
    }

    Page<EtherEntity> findAllByOrderByHostnameAscMacValueAsc(final Pageable pageable);
}
