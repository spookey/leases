package su.toor.leases.database;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import inet.ipaddr.IPAddress;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.Nullable;
import su.toor.leases.parse.LeaseConverter;
import su.toor.leases.shared.Require;

@Entity
@Table(
    name = "guide",
    indexes = {
        @Index(name = "idx_uuid", columnList = "uuid", unique = true),
        @Index(name = "idx_weight", columnList = "weight"),
    }
)
public class GuideEntity extends AbstractStampEntity<GuideEntity> {

    @Column(name = "mask_value", nullable = false)
    private String maskValue;

    @Column(name = "weight", nullable = false)
    private Integer weight;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;


    public static GuideEntity with(
        final IPAddress mask,
        final Integer weight,
        final String title,
        final String description
    ) {
        return new GuideEntity()
            .setMask(mask)
            .setWeight(weight)
            .setTitle(title)
            .setDescription(description)
            .update();
    }

    @Transient
    @Override
    protected GuideEntity get() {
        return this;
    }

    String getMaskValue() {
        return maskValue;
    }

    GuideEntity setMaskValue(final String maskValue) {
        this.maskValue = Require.notBlank(maskValue, "maskValue");
        return this.update();
    }

    @Transient
    public IPAddress getMask() {
        return LeaseConverter.convertIpAddress(getMaskValue());
    }

    @Transient
    public GuideEntity setMask(final IPAddress mask) {
        return setMaskValue(LeaseConverter.formatAddress(mask));
    }

    public Integer getWeight() {
        return weight;
    }

    public GuideEntity setWeight(final Integer weight) {
        this.weight = Math.abs(weight == null ? Integer.MAX_VALUE : weight);
        return this.update();
    }

    public String getTitle() {
        return title;
    }

    public GuideEntity setTitle(final String title) {
        this.title = Require.notBlank(title, "title");
        return this.update();
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public GuideEntity setDescription(final String description) {
        this.description = description;
        return this.update();
    }

    @Transient
    @Override
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
            .append("maskValue", getMaskValue())
            .append("weight", getWeight())
            .append("title", getTitle())
            .append("description", getDescription());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(final Object other) {
        return super.equals(other);
    }
}
