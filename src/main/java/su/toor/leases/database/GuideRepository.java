package su.toor.leases.database;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GuideRepository extends JpaRepository<GuideEntity, Long> {
    Optional<GuideEntity> findFirstByUuid(final UUID uuid);

    List<GuideEntity> findAllByOrderByWeightAscTitleAsc();
    Page<GuideEntity> findAllByOrderByWeightAscTitleAsc(final Pageable pageable);
}
