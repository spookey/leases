package su.toor.leases.database;

import java.beans.Transient;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;

import org.springframework.core.style.ToStringCreator;

@Entity
@Table(
    name = "stamp",
    indexes = {
        @Index(name = "idx_uuid", columnList = "uuid", unique = true),
        @Index(name = "idx_value", columnList = "value", unique = true),
    }
)
public class StampEntity extends AbstractEntity<StampEntity> {

    @Column(name = "value", updatable = false, nullable = false, unique = true, columnDefinition = "TIMESTAMP")
    private OffsetDateTime value = OffsetDateTime.now(ZoneOffset.UTC);

    public static StampEntity with() {
        return new StampEntity();
    }

    @Transient
    @Override
    protected StampEntity get() {
        return this;
    }

    public OffsetDateTime getValue() {
        return value;
    }

    @Transient
    @Override
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
            .append("value", getValue());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(final Object other) {
        return super.equals(other);
    }
}
