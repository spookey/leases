package su.toor.leases.database;

import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StampRepository extends JpaRepository<StampEntity, Long> {
    Optional<StampEntity> findFirstByUuid(final UUID uuid);

    Optional<StampEntity> findFirstByOrderByValueDesc();

    Page<StampEntity> findAllByOrderByValueAsc(final Pageable pageable);
    Set<StampEntity> findAllByValueBefore(final OffsetDateTime value);
}
