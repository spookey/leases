package su.toor.leases.database;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.core.style.ToStringCreator;
import su.toor.leases.shared.Require;
import su.toor.leases.shared.State;

@Entity
@Table(
    name = "track",
    indexes = {
        @Index(name = "idx_uuid", columnList = "uuid", unique = true),
        @Index(name = "idx_stamp_id", columnList = "stamp_id"),
        @Index(name = "idx_state", columnList = "state"),
    }
)
public class TrackEntity extends AbstractEntity<TrackEntity> {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(
        name = "stamp_id", nullable = false, updatable = false,
        foreignKey = @ForeignKey(name = "fk_track_stamp")
    )
    @OnDelete(action = OnDeleteAction.CASCADE)
    private StampEntity stampEntity;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(
        name = "ether_id", nullable = false, updatable = false,
        foreignKey = @ForeignKey(name = "fk_track_ether")
    )
    @OnDelete(action = OnDeleteAction.CASCADE)
    private EtherEntity etherEntity;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(
        name = "bound_id", nullable = false, updatable = false,
        foreignKey = @ForeignKey(name = "fk_track_bound")
    )
    @OnDelete(action = OnDeleteAction.CASCADE)
    private BoundEntity boundEntity;

    @Column(name = "state", nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private State state;


    public static TrackEntity with(
        final StampEntity stampEntity,
        final EtherEntity etherEntity,
        final BoundEntity boundEntity,
        final State state
    ) {
        final var entity = new TrackEntity();
        entity.stampEntity = Require.notNull(stampEntity, "stampEntity");
        entity.etherEntity = Require.notNull(etherEntity, "etherEntity");
        entity.boundEntity = Require.notNull(boundEntity, "boundEntity");
        entity.state = Require.notNull(state, "state");
        return entity;
    }

    @Transient
    @Override
    protected TrackEntity get() {
        return this;
    }

    public StampEntity getStampEntity() {
        return stampEntity;
    }

    public EtherEntity getEtherEntity() {
        return etherEntity;
    }

    public BoundEntity getBoundEntity() {
        return boundEntity;
    }

    public State getState() {
        return state;
    }

    @Transient
    @Override
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
            .append("stampEntity", getStampEntity())
            .append("etherEntity", getEtherEntity())
            .append("boundEntity", getBoundEntity())
            .append("state", getState());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(final Object other) {
        return super.equals(other);
    }
}
