package su.toor.leases.database;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import su.toor.leases.shared.State;

@Repository
public interface TrackRepository extends JpaRepository<TrackEntity, Long> {
    Optional<TrackEntity> findFirstByUuid(final UUID uuid);

    Optional<TrackEntity> findFirstByStampEntityAndEtherEntityAndStateIsNot(final StampEntity stampEntity, final EtherEntity etherEntity, final State state);
    Optional<TrackEntity> findFirstByStampEntityAndBoundEntityAndStateIsNot(final StampEntity stampEntity, final BoundEntity boundEntity, final State state);

    Set<TrackEntity> findAllBy();
    Page<TrackEntity> findAllByOrderByStateAsc(final Pageable pageable);
    Page<TrackEntity> findAllByStampEntityOrderByStateAsc(final StampEntity stampEntity, final Pageable pageable);
}
