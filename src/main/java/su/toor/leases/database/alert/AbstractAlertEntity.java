package su.toor.leases.database.alert;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;

import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.Nullable;
import su.toor.leases.database.AbstractStampEntity;
import su.toor.leases.shared.Require;

@MappedSuperclass
public abstract class AbstractAlertEntity<T extends AbstractAlertEntity<T>> extends AbstractStampEntity<T> {

    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @Column(name = "topic")
    private String topic;

    @Column(name = "template")
    private String template;


    public Boolean isEnabled() {
        return enabled;
    }

    public T setEnabled(final Boolean enabled) {
        this.enabled = Require.notNull(enabled, "enabled");
        return update();
    }

    @Nullable
    public String getTopic() {
        return topic;
    }

    public T setTopic(final String topic) {
        this.topic = topic != null
            ? Require.validMessageTopic(topic)
            : null;
        return update();
    }

    @Nullable
    public String getTemplate() {
        return template;
    }

    public T setTemplate(final String template) {
        this.template = template != null
            ? Require.notBlank(template, "template")
            : null;
        return update();
    }

    @Transient
    @Override
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
            .append("enabled", isEnabled())
            .append("topic", getTopic())
            .append("template", getTemplate());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(final Object other) {
        return super.equals(other);
    }
}
