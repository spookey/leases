package su.toor.leases.database.alert;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.core.style.ToStringCreator;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.shared.Require;

@Entity
@Table(
    name = "alert_bound",
    indexes = {
        @Index(name = "idx_uuid", columnList = "uuid", unique = true),
        @Index(name = "idx_bound_id", columnList = "bound_id"),
    }
)
public class AlertBoundEntity extends AbstractAlertEntity<AlertBoundEntity> {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(
        name = "bound_id", nullable = false, updatable = false,
        foreignKey = @ForeignKey(name = "fk_alert_bound_bound")
    )
    @OnDelete(action = OnDeleteAction.CASCADE)
    private BoundEntity boundEntity;


    public static AlertBoundEntity with(
        final BoundEntity boundEntity,
        final Boolean enabled,
        final String topic,
        final String template
    ) {
        final var entity = new AlertBoundEntity();
        entity.boundEntity = Require.notNull(boundEntity, "boundEntity");
        return entity
            .setEnabled(enabled)
            .setTopic(topic)
            .setTemplate(template);
    }

    @Transient
    @Override
    protected AlertBoundEntity get() {
        return this;
    }

    public BoundEntity getBoundEntity() {
        return boundEntity;
    }

    @Transient
    @Override
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
            .append("boundEntity", getBoundEntity());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(final Object other) {
        return super.equals(other);
    }
}
