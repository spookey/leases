package su.toor.leases.database.alert;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlertBoundRepository extends JpaRepository<AlertBoundEntity, Long> {
    Optional<AlertBoundEntity> findFirstByUuid(final UUID uuid);

    Page<AlertBoundEntity> findAllByOrderByBoundEntity(final Pageable pageable);
    Set<AlertBoundEntity> findAllByEnabled(final Boolean enabled);
}
