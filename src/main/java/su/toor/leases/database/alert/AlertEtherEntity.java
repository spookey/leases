package su.toor.leases.database.alert;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.core.style.ToStringCreator;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.shared.Require;

@Entity
@Table(
    name = "alert_ether",
    indexes = {
        @Index(name = "idx_uuid", columnList = "uuid", unique = true),
        @Index(name = "idx_ether_id", columnList = "ether_id"),
    }
)
public class AlertEtherEntity extends AbstractAlertEntity<AlertEtherEntity> {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(
        name = "ether_id", nullable = false, updatable = false,
        foreignKey = @ForeignKey(name = "fk_alert_ether_ether")
    )
    @OnDelete(action = OnDeleteAction.CASCADE)
    private EtherEntity etherEntity;


    public static AlertEtherEntity with(
        final EtherEntity etherEntity,
        final Boolean enabled,
        final String topic,
        final String template
    ) {
        final var entity = new AlertEtherEntity();
        entity.etherEntity = Require.notNull(etherEntity, "etherEntity");
        return entity
            .setEnabled(enabled)
            .setTopic(topic)
            .setTemplate(template);
    }

    @Transient
    @Override
    protected AlertEtherEntity get() {
        return this;
    }

    public EtherEntity getEtherEntity() {
        return etherEntity;
    }

    @Transient
    @Override
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
            .append("etherEntity", getEtherEntity());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(final Object other) {
        return super.equals(other);
    }
}
