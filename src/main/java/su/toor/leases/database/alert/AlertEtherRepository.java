package su.toor.leases.database.alert;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlertEtherRepository extends JpaRepository<AlertEtherEntity, Long> {
    Optional<AlertEtherEntity> findFirstByUuid(final UUID uuid);

    Page<AlertEtherEntity> findAllByOrderByEtherEntity(final Pageable pageable);
    Set<AlertEtherEntity> findAllByEnabled(final Boolean enabled);
}
