package su.toor.leases.database.alert;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.core.style.ToStringCreator;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.shared.Require;

@Entity
@Table(
    name = "alert_guide",
    indexes = {
        @Index(name = "idx_uuid", columnList = "uuid", unique = true),
        @Index(name = "idx_guide_id", columnList = "guide_id"),
    }
)
public class AlertGuideEntity extends AbstractAlertEntity<AlertGuideEntity> {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(
        name = "guide_id", nullable = false, updatable = false,
        foreignKey = @ForeignKey(name = "fk_alert_guide_guide")
    )
    @OnDelete(action = OnDeleteAction.CASCADE)
    private GuideEntity guideEntity;


    public static AlertGuideEntity with(
        final GuideEntity guideEntity,
        final Boolean enabled,
        final String topic,
        final String template
    ) {
        final var entity = new AlertGuideEntity();
        entity.guideEntity = Require.notNull(guideEntity, "guideEntity");
        return entity
            .setEnabled(enabled)
            .setTopic(topic)
            .setTemplate(template);
    }

    @Transient
    @Override
    protected AlertGuideEntity get() {
        return this;
    }

    public GuideEntity getGuideEntity() {
        return guideEntity;
    }

    @Transient
    @Override
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
            .append("guideEntity", getGuideEntity());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(final Object other) {
        return super.equals(other);
    }
}
