package su.toor.leases.database.alert;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlertGuideRepository extends JpaRepository<AlertGuideEntity, Long> {
    Optional<AlertGuideEntity> findFirstByUuid(final UUID uuid);

    Page<AlertGuideEntity> findAllByOrderByGuideEntity(final Pageable pageable);
    Set<AlertGuideEntity> findAllByEnabled(final Boolean enabled);
}
