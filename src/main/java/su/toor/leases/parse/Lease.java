package su.toor.leases.parse;

import java.util.Objects;

import inet.ipaddr.IPAddress;
import inet.ipaddr.mac.MACAddress;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import su.toor.leases.shared.Require;
import su.toor.leases.shared.State;

public class Lease {

    private final IPAddress ipAddress;
    private final MACAddress macAddress;
    private final State state;
    private final String hostname;

    Lease(
        final IPAddress ipAddress,
        final MACAddress macAddress,
        final State state,
        final String hostname
    ) {
        this.ipAddress = Require.notNull(ipAddress, "ipAddress");
        this.macAddress = Require.notNull(macAddress, "macAddress");
        this.state = Require.notNull(state, "state");
        this.hostname = hostname;
    }

    @NonNull
    public IPAddress getIpAddress() {
        return ipAddress;
    }

    @NonNull
    public MACAddress getMacAddress() {
        return macAddress;
    }

    @NonNull
    public State getState() {
        return state;
    }

    @Nullable
    public String getHostname() {
        return hostname;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
            .append("ipAddress", ipAddress)
            .append("macAddress", macAddress)
            .append("state", state)
            .append("hostname", hostname)
            .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(ipAddress, macAddress, state, hostname);
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }

        final var that = (Lease) other;
        return Objects.equals(this.ipAddress, that.ipAddress)
            && Objects.equals(this.macAddress, that.macAddress)
            && this.state == that.state
            && Objects.equals(this.hostname, that.hostname);
    }
}
