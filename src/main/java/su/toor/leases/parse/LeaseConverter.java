package su.toor.leases.parse;

import java.util.Optional;

import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import inet.ipaddr.MACAddressString;
import inet.ipaddr.mac.MACAddress;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import su.toor.leases.shared.Require;
import su.toor.leases.shared.State;

public class LeaseConverter {
    private LeaseConverter() {}

    private IPAddress ipAddress;
    private MACAddress macAddress;
    private State state;
    private String hostname;

    @Nullable
    public static IPAddress convertIpAddress(@Nullable final String ipAddress) {
        return new IPAddressString(ipAddress).getAddress();
    }

    @Nullable
    public static MACAddress convertMacAddress(@Nullable final String macAddress) {
        return new MACAddressString(macAddress).getAddress();
    }

    @NonNull
    public static String formatAddress(final IPAddress ipAddress) {
        return Require.notNull(ipAddress, "ipAddress").toCanonicalString();
    }

    @NonNull
    public static String formatAddress(final MACAddress macAddress) {
        return Require.notNull(macAddress, "macAddress").toColonDelimitedString();
    }

    public static LeaseConverter get() {
        return new LeaseConverter()
            .reset();
    }

    public LeaseConverter reset() {
        ipAddress = null;
        macAddress = null;
        state = null;
        hostname = null;
        return this;
    }

    LeaseConverter consumeIpAddress(@Nullable final IPAddress ipAddress) {
        if (ipAddress != null) {
            this.ipAddress = ipAddress;
        }
        return this;
    }
    public LeaseConverter consumeIpAddress(@Nullable final String ipAddress) {
        return consumeIpAddress(convertIpAddress(ipAddress));
    }

    LeaseConverter consumeMacAddress(@Nullable final MACAddress macAddress) {
        if (macAddress != null) {
            this.macAddress = macAddress;
        }
        return this;
    }
    public LeaseConverter consumeMacAddress(@Nullable final String macAddress) {
        return consumeMacAddress(convertMacAddress(macAddress));
    }

    public LeaseConverter consumeState(@Nullable final String state) {
        if (state != null) {
            this.state = State.parse(state.strip());
        }
        return this;
    }

    public LeaseConverter consumeHostname(@Nullable final String hostname) {
        if (hostname != null) {
            this.hostname = hostname.strip()
                .replaceFirst("^\"", "")
                .replaceFirst("\"$", "")
                .strip();
        }
        return this;
    }

    public boolean hasIpAddress() {
        return ipAddress != null;
    }

    public boolean hasMacAddress() {
        return macAddress != null;
    }

    public boolean hasState() {
        return state != null;
    }

    public boolean hasHostname() {
        return hostname != null;
    }

    public boolean isComplete() {
        return hasIpAddress() && hasMacAddress() && hasState();
    }

    public Optional<Lease> build() {
        if (!isComplete()) {
            return Optional.empty();
        }

        return Optional.of(new Lease(
            ipAddress,
            macAddress,
            state,
            hostname
        ));
    }
}
