package su.toor.leases.parse;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import inet.ipaddr.IPAddress;
import org.springframework.lang.Nullable;

public class LeaseParser {
    private LeaseParser() {}

    static final String BLOCK_BEG = "{";
    static final String BLOCK_END = "}";
    static final String LINE_COMMENT = "#";
    static final String LINE_END = ";";

    static final String FIELD_ETHERNET = "hardware ethernet";
    static final String FIELD_HOSTNAME = "client-hostname";
    static final String FIELD_LEASE = "lease";
    static final String FIELD_STATE = "binding state";

    static boolean lineInclude(final String line) {
        return line != null
            && !line.isBlank()
            && !line.strip().startsWith(LINE_COMMENT);
    }

    @Nullable
    static String extract(final String line, final String beg, final String end) {
        if (!lineInclude(line)) {
            return null;
        }
        if (!lineInclude(beg) || !line.startsWith(beg)) {
            return null;
        }
        if (!lineInclude(end) || !line.endsWith(end)) {
            return null;
        }
        return line.substring(beg.length(), line.length() - end.length()).strip();
    }
    @Nullable
    static String extract(final String line, final String beg) {
        return extract(line, beg, LINE_END);
    }

    public static List<Lease> parse(final List<String> lines) {
        final var bucket = new LinkedHashMap<IPAddress, Lease>(128);
        final var converter = LeaseConverter.get();

        final var prepared = lines.stream()
            .filter(Objects::nonNull)
            .map(String::trim)
            .filter(LeaseParser::lineInclude)
            .toList();

        for (final var line : prepared) {
            if (!converter.hasIpAddress()) {
                converter.consumeIpAddress(
                    extract(line, FIELD_LEASE, BLOCK_BEG)
                );
            } else {
                converter.consumeMacAddress(
                    extract(line, FIELD_ETHERNET)
                );
                converter.consumeState(
                    extract(line, FIELD_STATE)
                );
                converter.consumeHostname(
                    extract(line, FIELD_HOSTNAME)
                );

                if (line.endsWith(BLOCK_END)) {
                    /* man 5 dhcpd.leases ->
                     * So if more than one declaration appears
                     * for a given lease, the last one in the
                     * file is the current one. */
                    converter
                        .build()
                        .ifPresent(lease -> bucket.put(
                            lease.getIpAddress(),
                            lease
                        ));

                    converter.reset();
                }
            }
        }

        return bucket.values()
            .stream()
            .toList();
    }
}
