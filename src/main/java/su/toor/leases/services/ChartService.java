package su.toor.leases.services;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import su.toor.leases.components.core.Extract;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.controllers.dtos.side.BaseGuideDto;
import su.toor.leases.controllers.dtos.side.ChartSeriesDto;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.services.query.EntityService;
import su.toor.leases.shared.State;

@Service
public class ChartService {

    private static final Logger LOG = LoggerFactory.getLogger(ChartService.class);

    static final String FIELD_TARGET = "target";

    private final BasicConfig basicConfig;
    private final EntityService entityService;
    private final JsonMapper jsonMapper;

    @Autowired
    public ChartService(
        final BasicConfig basicConfig,
        final EntityService entityService,
        final JsonMapper jsonMapper
    ) {
        this.basicConfig = basicConfig;
        this.entityService = entityService;
        this.jsonMapper = jsonMapper;
    }

    public List<BaseGuideDto> constructBaseGuides() {
        return entityService.findGuides()
            .stream()
            .map(BaseGuideDto::from)
            .toList();
    }

    public Optional<List<ChartSeriesDto>> constructChartSeries(final UUID uuid) {
        return entityService.findGuide(uuid)
            .map(guideEntity -> Extract.filterTracks(guideEntity, entityService.findTracks()))
            .map(trackEntities -> State.items(basicConfig.isHiddenState()).stream()
                .map(state -> ChartSeriesDto.from(
                    state,
                    trackEntities.stream()
                        .filter(trackEntity -> state.equals(trackEntity.getState()))
                        .collect(Collectors.groupingBy(
                            TrackEntity::getStampEntity,
                            Collectors.counting()
                        ))
                ))
                .collect(Collectors.toList())
            );
    }

    <T> boolean dumpToDisk(final Path target, final T payload) {
        try {
            final var content = jsonMapper.writeValueAsString(payload);
            Files.writeString(
                target,
                content,
                StandardOpenOption.CREATE,
                StandardOpenOption.WRITE,
                StandardOpenOption.TRUNCATE_EXISTING
            );

            return true;
        } catch (final JsonProcessingException ex) {
            LOG.error("json processing failed [{}]", keyValue(FIELD_TARGET, target), ex);
        } catch (final IOException ex) {
            LOG.error("file handling failed [{}]", keyValue(FIELD_TARGET, target), ex);
        }
        return false;
    }

    public boolean performChartDump() {
        final var dumpLocation = basicConfig.dumpLocation();
        final var baseGuides = constructBaseGuides();
        if (baseGuides.isEmpty()) {
            return false;
        }

        final var seriesFailed = baseGuides.stream()
            .map(BaseGuideDto::getUuid)
            .map(uuid -> constructChartSeries(uuid)
                .map(series -> dumpToDisk(
                    dumpLocation.resolve(String.format("chart-%s.json", uuid)),
                    series
                ))
                .orElse(false)
            )
            .filter(success -> !success)
            .findAny();

        if (seriesFailed.isPresent()) {
            return false;
        }
        return dumpToDisk(
            dumpLocation.resolve("chart-networks.json"),
            baseGuides
        );
    }
}
