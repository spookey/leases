package su.toor.leases.services;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import su.toor.leases.components.mqtt.Client;
import su.toor.leases.components.mqtt.Composer;
import su.toor.leases.configs.sinks.AbstractAlertSink;
import su.toor.leases.configs.sinks.AlertBoundConfig;
import su.toor.leases.configs.sinks.AlertEtherConfig;
import su.toor.leases.configs.sinks.AlertGuideConfig;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.alert.AbstractAlertEntity;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.database.alert.AlertGuideEntity;
import su.toor.leases.parse.LeaseConverter;
import su.toor.leases.shared.State;

@Service
public class MessageService {

    private static final Logger LOG = LoggerFactory.getLogger(MessageService.class);

    static final String FIELD_CONFIG = "config";
    static final String FIELD_ENTITY = "entity";
    static final String FIELD_MESSAGE = "message";
    static final String FIELD_TOPIC = "topic";

    private final AlertEtherConfig alertEtherConfig;
    private final AlertBoundConfig alertBoundConfig;
    private final AlertGuideConfig alertGuideConfig;
    private final Client client;

    @Autowired
    public MessageService(
        final AlertEtherConfig alertEtherConfig,
        final AlertBoundConfig alertBoundConfig,
        final AlertGuideConfig alertGuideConfig,
        final Client client
    ) {
        this.alertEtherConfig = alertEtherConfig;
        this.alertBoundConfig = alertBoundConfig;
        this.alertGuideConfig = alertGuideConfig;
        this.client = client;
    }

    private static Map<String, String> values(
        @NonNull Map<String, Optional<String>> content
    ) {
        return content.entrySet().stream()
            .filter(entry -> entry.getValue().isPresent())
            .collect(Collectors.toMap(
                Map.Entry::getKey,
                entry -> entry.getValue().get()
            ));
    }

    static Map<String, String> values(
        @NonNull final EtherEntity etherEntity,
        @NonNull final State state
    ) {
        return values(Map.of(
            "uuid", Optional.of(etherEntity)
                .map(EtherEntity::getUuid)
                .map(UUID::toString),
            "hostname", Optional.ofNullable(etherEntity.getHostname()),
            "state", Optional.of(state)
                .map(State::getText)
        ));
    }

    static Map<String, String> values(
        @NonNull final BoundEntity boundEntity,
        @NonNull final State state
    ) {
        return values(Map.of(
            "uuid", Optional.of(boundEntity)
                .map(BoundEntity::getUuid)
                .map(UUID::toString),
            "ip", Optional.of(boundEntity)
                .map(BoundEntity::getIp)
                .map(LeaseConverter::formatAddress),
            "state", Optional.of(state)
                .map(State::getText)
        ));
    }

    static Map<String, String> values(
        @NonNull final GuideEntity guideEntity,
        @Nullable final Long high,
        @Nullable final Long down
    ) {
        return values(Map.of(
            "uuid", Optional.of(guideEntity)
                .map(GuideEntity::getUuid)
                .map(UUID::toString),
            "title", Optional.of(guideEntity)
                .map(GuideEntity::getTitle),
            "mask", Optional.of(guideEntity)
                .map(GuideEntity::getMask)
                .map(LeaseConverter::formatAddress),
            "high", Optional.ofNullable(high)
                .map(String::valueOf),
            "down", Optional.ofNullable(down)
                .map(String::valueOf)
        ));
    }

    private <C extends AbstractAlertSink<C>, E extends AbstractAlertEntity<E>> void send(
        @NonNull final AbstractAlertSink<C> alertConfig,
        @NonNull final AbstractAlertEntity<E> alertEntity,
        @NonNull final Map<String, String> templateValues
    ) {
        final var topic = Optional.ofNullable(alertEntity.getTopic())
                .orElseGet(alertConfig::getFallbackTopic);
        final var message = Composer.make(
            alertConfig,
            alertEntity.getTemplate(),
            templateValues
        );

        if (!Boolean.TRUE.equals(alertConfig.isEnabled())) {
            LOG.info(
                "ignoring message - config disabled [{}] [{}] [{}]",
                keyValue(FIELD_TOPIC, topic),
                keyValue(FIELD_CONFIG, alertConfig),
                keyValue(FIELD_MESSAGE, message)
            );
            return;
        }
        if (!Boolean.TRUE.equals(alertEntity.isEnabled())) {
            LOG.info(
                "ignoring message - entity disabled [{}] [{}] [{}]",
                keyValue(FIELD_TOPIC, topic),
                keyValue(FIELD_ENTITY, alertEntity),
                keyValue(FIELD_MESSAGE, message)
            );
            return;
        }
        LOG.info(
            "sending message [{}] [{}]",
            keyValue(FIELD_TOPIC, topic),
            keyValue(FIELD_MESSAGE, message)
        );
        client.send(topic, message);
    }

    public void notify(
        @NonNull final AlertEtherEntity alertEtherEntity,
        @NonNull final State state
    ) {
        send(
            alertEtherConfig,
            alertEtherEntity,
            values(
                alertEtherEntity.getEtherEntity(),
                state
            )
        );
    }

    public void notify(
        @NonNull final AlertBoundEntity alertBoundEntity,
        @NonNull final State state
    ) {
        send(
            alertBoundConfig,
            alertBoundEntity,
            values(
                alertBoundEntity.getBoundEntity(),
                state
            )
        );
    }

    public void notify(
        @NonNull final AlertGuideEntity alertGuideEntity,
        @Nullable final Long high,
        @Nullable final Long down
    ) {
        send(
            alertGuideConfig,
            alertGuideEntity,
            values(
                alertGuideEntity.getGuideEntity(),
                high,
                down
            )
        );
    }
}
