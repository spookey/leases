package su.toor.leases.services.query;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.PageRequest;
import org.springframework.lang.Nullable;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.BoundRepository;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.EtherRepository;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.GuideRepository;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.StampRepository;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.database.TrackRepository;

abstract class AbstractEntityService {

    static final int MAX_PAGINATION_SIZE = 512;

    static final String FIELD_ALERT_BOUND = "alertBound";
    static final String FIELD_ALERT_ETHER = "alertEther";
    static final String FIELD_ALERT_GUIDE = "alertGuide";
    static final String FIELD_BOUND = "bound";
    static final String FIELD_ETHER = "ether";
    static final String FIELD_GUIDE = "guide";
    static final String FIELD_STAMP = "stamp";
    static final String FIELD_TRACK = "track";

    protected final BasicConfig basicConfig;
    protected final StampRepository stampRepository;
    protected final EtherRepository etherRepository;
    protected final BoundRepository boundRepository;
    protected final TrackRepository trackRepository;
    protected final GuideRepository guideRepository;

    AbstractEntityService(
        final BasicConfig basicConfig,
        final StampRepository stampRepository,
        final EtherRepository etherRepository,
        final BoundRepository boundRepository,
        final TrackRepository trackRepository,
        final GuideRepository guideRepository
    ) {
        this.basicConfig = basicConfig;
        this.stampRepository = stampRepository;
        this.etherRepository = etherRepository;
        this.boundRepository = boundRepository;
        this.trackRepository = trackRepository;
        this.guideRepository = guideRepository;
    }

    PageRequest pagination(
        @Nullable final Integer page,
        @Nullable final Integer size
    ) {
        final var currPage = page == null || page <= 0
            ? 0
            : page;
        final var pageSize = size == null || size <= 0
            ? Math.max(1, basicConfig.getPaginationSize())
            : Math.min(size, MAX_PAGINATION_SIZE);

        return PageRequest
            .ofSize(pageSize)
            .withPage(currPage);
    }

    public long countStamps() {
        return stampRepository.count();
    }
    public long countEthers() {
        return etherRepository.count();
    }
    public long countBounds() {
        return boundRepository.count();
    }
    public long countTracks() {
        return trackRepository.count();
    }
    public long countGuides() {
        return guideRepository.count();
    }

    public Optional<StampEntity> findStamp(final UUID uuid) {
        return stampRepository.findFirstByUuid(uuid);
    }

    public Optional<EtherEntity> findEther(final UUID uuid) {
        return etherRepository.findFirstByUuid(uuid);
    }

    public Optional<BoundEntity> findBound(final UUID uuid) {
        return boundRepository.findFirstByUuid(uuid);
    }

    public Optional<TrackEntity> findTrack(final UUID uuid) {
        return trackRepository.findFirstByUuid(uuid);
    }

    public Optional<GuideEntity> findGuide(final UUID uuid) {
        return guideRepository.findFirstByUuid(uuid);
    }
}
