package su.toor.leases.services.query;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.BoundRepository;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.EtherRepository;
import su.toor.leases.database.GuideRepository;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.StampRepository;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.database.TrackRepository;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.database.alert.AlertBoundRepository;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.database.alert.AlertEtherRepository;
import su.toor.leases.database.alert.AlertGuideEntity;
import su.toor.leases.database.alert.AlertGuideRepository;
import su.toor.leases.shared.State;

@Service
public class AlertEntityService extends AbstractEntityService {

    private static final Logger LOG = LoggerFactory.getLogger(AlertEntityService.class);

    private final AlertEtherRepository alertEtherRepository;
    private final AlertBoundRepository alertBoundRepository;
    private final AlertGuideRepository alertGuideRepository;

    @Autowired
    public AlertEntityService(
        final BasicConfig basicConfig,
        final StampRepository stampRepository,
        final EtherRepository etherRepository,
        final BoundRepository boundRepository,
        final TrackRepository trackRepository,
        final GuideRepository guideRepository,
        final AlertEtherRepository alertEtherRepository,
        final AlertBoundRepository alertBoundRepository,
        final AlertGuideRepository alertGuideRepository
    ) {
        super(
            basicConfig,
            stampRepository,
            etherRepository,
            boundRepository,
            trackRepository,
            guideRepository
        );
        this.alertEtherRepository = alertEtherRepository;
        this.alertBoundRepository = alertBoundRepository;
        this.alertGuideRepository = alertGuideRepository;
    }

    public long countAlertEthers() {
        return alertEtherRepository.count();
    }
    public long countAlertBounds() {
        return alertBoundRepository.count();
    }
    public long countAlertGuides() {
        return alertGuideRepository.count();
    }


    public Optional<TrackEntity> findTrackWithoutState(final StampEntity stampEntity, final EtherEntity etherEntity, final State state) {
        return trackRepository.findFirstByStampEntityAndEtherEntityAndStateIsNot(stampEntity, etherEntity, state);
    }

    public Optional<TrackEntity> findTrackWithoutState(final StampEntity stampEntity, final BoundEntity boundEntity, final State state) {
        return trackRepository.findFirstByStampEntityAndBoundEntityAndStateIsNot(stampEntity, boundEntity, state);
    }

    public Page<AlertEtherEntity> findAlertEthers(
        @Nullable final Integer page,
        @Nullable final Integer size
    ) {
        return alertEtherRepository.findAllByOrderByEtherEntity(
            pagination(page, size)
        );
    }

    public Set<AlertEtherEntity> findAlertEthersEnabled() {
        return alertEtherRepository.findAllByEnabled(true);
    }

    public Optional<AlertEtherEntity> findAlertEther(final UUID uuid) {
        return alertEtherRepository.findFirstByUuid(uuid);
    }

    public Optional<AlertEtherEntity> modifyAlertEther(
        final UUID uuid,
        final Boolean enabled,
        final String topic,
        final String template
    ) {
        return findAlertEther(uuid)
            .map(alertEtherEntity -> alertEtherEntity
                .setEnabled(enabled)
                .setTopic(topic)
                .setTemplate(template)
            )
            .map(alertEtherEntity -> {
                LOG.info("modifying alert ether [{}]", keyValue(FIELD_ALERT_ETHER, alertEtherEntity));
                return alertEtherRepository.save(alertEtherEntity);
            });
    }

    public Optional<AlertEtherEntity> createAlertEther(
        final UUID etherUuid,
        final Boolean enabled,
        final String topic,
        final String template
    ) {
        return findEther(etherUuid)
            .map(etherEntity -> AlertEtherEntity.with(etherEntity, enabled, topic, template))
            .map(alertEtherEntity -> {
                LOG.info("creating new alert ether [{}]", keyValue(FIELD_ALERT_ETHER, alertEtherEntity));

                return alertEtherRepository.save(alertEtherEntity);
            });
    }

    public boolean deleteAlertEther(final UUID uuid) {
        final var optionalAlertEtherEntity = findAlertEther(uuid);
        if (optionalAlertEtherEntity.isEmpty()) {
            return false;
        }

        final var alertEtherEntity = optionalAlertEtherEntity.get();
        LOG.info("deleting alert ether [{}]", keyValue(FIELD_ALERT_ETHER, alertEtherEntity));
        alertEtherRepository.delete(alertEtherEntity);
        return true;
    }

    public Page<AlertBoundEntity> findAlertBounds(
        @Nullable final Integer page,
        @Nullable final Integer size
    ) {
        return alertBoundRepository.findAllByOrderByBoundEntity(
            pagination(page, size)
        );
    }

    public Set<AlertBoundEntity> findAlertBoundsEnabled() {
        return alertBoundRepository.findAllByEnabled(true);
    }

    public Optional<AlertBoundEntity> findAlertBound(final UUID uuid) {
        return alertBoundRepository.findFirstByUuid(uuid);
    }

    public Optional<AlertBoundEntity> modifyAlertBound(
        final UUID uuid,
        final Boolean enabled,
        final String topic,
        final String template
    ) {
        return findAlertBound(uuid)
            .map(alertBoundEntity -> alertBoundEntity
                .setEnabled(enabled)
                .setTopic(topic)
                .setTemplate(template)
            )
            .map(alertBoundEntity -> {
                LOG.info("modifying alert bound [{}]", keyValue(FIELD_ALERT_BOUND, alertBoundEntity));
                return alertBoundRepository.save(alertBoundEntity);
            });
    }

    public Optional<AlertBoundEntity> createAlertBound(
        final UUID boundUuid,
        final Boolean enabled,
        final String topic,
        final String template
    ) {
        return findBound(boundUuid)
            .map(boundEntity -> AlertBoundEntity.with(boundEntity, enabled, topic, template))
            .map(alertBoundEntity -> {
                LOG.info("creating new alert bound [{}]", keyValue(FIELD_ALERT_BOUND, alertBoundEntity));

                return alertBoundRepository.save(alertBoundEntity);
            });
    }

    public boolean deleteAlertBound(final UUID uuid) {
        final var optionalAlertBoundEntity = findAlertBound(uuid);
        if (optionalAlertBoundEntity.isEmpty()) {
            return false;
        }

        final var alertBoundEntity = optionalAlertBoundEntity.get();
        LOG.info("deleting alert bound [{}]", keyValue(FIELD_ALERT_BOUND, alertBoundEntity));
        alertBoundRepository.delete(alertBoundEntity);
        return true;
    }

    public Page<AlertGuideEntity> findAlertGuides(
        @Nullable final Integer page,
        @Nullable final Integer size
    ) {
        return alertGuideRepository.findAllByOrderByGuideEntity(
            pagination(page, size)
        );
    }

    public Set<AlertGuideEntity> findAlertGuidesEnabled() {
        return alertGuideRepository.findAllByEnabled(true);
    }

    public Optional<AlertGuideEntity> findAlertGuide(final UUID uuid) {
        return alertGuideRepository.findFirstByUuid(uuid);
    }

    public Optional<AlertGuideEntity> modifyAlertGuide(
        final UUID uuid,
        final Boolean enabled,
        final String topic,
        final String template
    ) {
        return findAlertGuide(uuid)
            .map(alertGuideEntity -> alertGuideEntity
                .setEnabled(enabled)
                .setTopic(topic)
                .setTemplate(template)
            )
            .map(alertGuideEntity -> {
                LOG.info("modifying alert guide [{}]", keyValue(FIELD_ALERT_GUIDE, alertGuideEntity));
                return alertGuideRepository.save(alertGuideEntity);
            });
    }

    public Optional<AlertGuideEntity> createAlertGuide(
        final UUID guideUuid,
        final Boolean enabled,
        final String topic,
        final String template
    ) {
        return findGuide(guideUuid)
            .map(guideEntity -> AlertGuideEntity.with(guideEntity, enabled, topic, template))
            .map(alertGuideEntity -> {
                LOG.info("creating new alert guide [{}]", keyValue(FIELD_ALERT_GUIDE, alertGuideEntity));

                return alertGuideRepository.save(alertGuideEntity);
            });
    }

    public boolean deleteAlertGuide(final UUID uuid) {
        final var optionalAlertGuideEntity = findAlertGuide(uuid);
        if (optionalAlertGuideEntity.isEmpty()) {
            return false;
        }

        final var alertGuideEntity = optionalAlertGuideEntity.get();
        LOG.info("deleting alert guide [{}]", keyValue(FIELD_ALERT_GUIDE, alertGuideEntity));
        alertGuideRepository.delete(alertGuideEntity);
        return true;
    }
}
