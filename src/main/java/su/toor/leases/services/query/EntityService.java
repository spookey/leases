package su.toor.leases.services.query;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import inet.ipaddr.IPAddress;
import inet.ipaddr.mac.MACAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.BoundRepository;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.EtherRepository;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.GuideRepository;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.StampRepository;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.database.TrackRepository;
import su.toor.leases.shared.State;

@Service
public class EntityService extends AbstractEntityService {

    private static final Logger LOG = LoggerFactory.getLogger(EntityService.class);


    @Autowired
    public EntityService(
        final BasicConfig basicConfig,
        final StampRepository stampRepository,
        final EtherRepository etherRepository,
        final BoundRepository boundRepository,
        final TrackRepository trackRepository,
        final GuideRepository guideRepository
    ) {
        super(
            basicConfig,
            stampRepository,
            etherRepository,
            boundRepository,
            trackRepository,
            guideRepository
        );
    }

    public Page<StampEntity> findStamps(
        @Nullable final Integer page,
        @Nullable final Integer size
    ) {
        return stampRepository.findAllByOrderByValueAsc(
            pagination(page, size)
        );
    }

    public Set<StampEntity> findStampBefore(final OffsetDateTime value) {
        return stampRepository.findAllByValueBefore(value);
    }

    public Optional<StampEntity> latestStamp() {
        return stampRepository.findFirstByOrderByValueDesc();
    }

    public StampEntity createStamp() {
        final var stampEntity = StampEntity.with();
        LOG.info("creating new stamp [{}]", keyValue(FIELD_STAMP, stampEntity));

        return stampRepository.save(stampEntity);
    }

    public boolean deleteStamp(final UUID uuid) {
        final var optionalStampEntity = findStamp(uuid);
        if (optionalStampEntity.isEmpty()) {
            return false;
        }

        final var stampEntity = optionalStampEntity.get();
        LOG.info("deleting stamp [{}]", keyValue(FIELD_STAMP, stampEntity));
        stampRepository.delete(stampEntity);
        return true;
    }

    public Page<EtherEntity> findEthers(
        @Nullable final Integer page,
        @Nullable final Integer size
    ) {
        return etherRepository.findAllByOrderByHostnameAscMacValueAsc(
            pagination(page, size)
        );
    }

    public Optional<EtherEntity> findEther(final MACAddress mac) {
        return etherRepository.findFirstByMac(mac);
    }

    public EtherEntity ensureEther(final MACAddress mac, final String hostname) {
        final var etherEntity = findEther(mac)
            .orElseGet(() -> {
                final var entity = EtherEntity.with(mac, hostname);
                LOG.info("creating new ether [{}]", keyValue(FIELD_ETHER, entity));
                return entity;
            });

        return etherRepository.save(etherEntity.setHostname(hostname).update());
    }

    public Page<BoundEntity> findBounds(
        @Nullable final Integer page,
        @Nullable final Integer size
    ) {
        return boundRepository.findAllByOrderByIpValueAsc(
            pagination(page, size)
        );
    }

    public Optional<BoundEntity> findBound(final IPAddress ip) {
        return boundRepository.findFirstByIp(ip);
    }

    public BoundEntity ensureBound(final IPAddress ip) {
        final var boundEntity = findBound(ip)
            .orElseGet(() -> {
                final var entity = BoundEntity.with(ip);
                LOG.info("creating new bound [{}]", keyValue(FIELD_BOUND, entity));
                return entity;
            });

        return boundRepository.save(boundEntity.update());
    }

    public Page<TrackEntity> findTracks(
        @Nullable final Integer page,
        @Nullable final Integer size
    ) {
        return trackRepository.findAllByOrderByStateAsc(
            pagination(page, size)
        );
    }

    public Page<TrackEntity> findTracks(
        final UUID uuid,
        @Nullable final Integer page,
        @Nullable final Integer size
    ) {
        return findStamp(uuid)
            .map(stampEntity -> trackRepository.findAllByStampEntityOrderByStateAsc(
                stampEntity,
                pagination(page, size)
            ))
            .orElseGet(Page::empty);
    }

    public Set<TrackEntity> findTracks() {
        return trackRepository.findAllBy();
    }

    public Page<TrackEntity> latestTracks(
        @Nullable final Integer page,
        @Nullable final Integer size
    ) {
        return latestStamp()
            .map(StampEntity::getUuid)
            .map(uuid -> findTracks(uuid, page, size))
            .orElseGet(Page::empty);
    }

    public TrackEntity createTrack(
        final StampEntity stampEntity,
        final EtherEntity etherEntity,
        final BoundEntity boundEntity,
        final State state
    ) {
        final var trackEntity = TrackEntity.with(stampEntity, etherEntity, boundEntity, state);
        LOG.info("creating new track [{}]", keyValue(FIELD_TRACK, trackEntity));
        return trackRepository.save(trackEntity);
    }

    public Page<GuideEntity> findGuides(
        @Nullable final Integer page,
        @Nullable final Integer size
    ) {
        return guideRepository.findAllByOrderByWeightAscTitleAsc(
            pagination(page, size)
        );
    }

    public List<GuideEntity> findGuides() {
        return guideRepository.findAllByOrderByWeightAscTitleAsc();
    }

    public Optional<GuideEntity> modifyGuide(
        final UUID uuid,
        final IPAddress mask,
        final Integer weight,
        final String title,
        final String description
    ) {
        return findGuide(uuid)
            .map(guideEntity -> guideEntity
                .setMask(mask)
                .setWeight(weight)
                .setTitle(title)
                .setDescription(description)
            )
            .map(guideEntity -> {
                LOG.info("modifying guide [{}]", keyValue(FIELD_ETHER, guideEntity));
                return guideRepository.save(guideEntity.update());
            });
    }

    public GuideEntity createGuide(
        final IPAddress mask,
        final Integer weight,
        final String title,
        final String description
    ) {
        final var guideEntity = GuideEntity.with(mask, weight, title, description);

        LOG.info("creating new guide [{}]", keyValue(FIELD_ETHER, guideEntity));
        return guideRepository.save(guideEntity.update());
    }

    public boolean deleteGuide(final UUID uuid) {
        final var optionalGuideEntity = findGuide(uuid);
        if (optionalGuideEntity.isEmpty()) {
            return false;
        }

        final var guideEntity = optionalGuideEntity.get();
        LOG.info("deleting guide [{}]", keyValue(FIELD_GUIDE, guideEntity));
        guideRepository.delete(guideEntity);
        return true;
    }
}
