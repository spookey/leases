package su.toor.leases.shared;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public final class Linkage {
    private Linkage() {}

    @Nullable
    public static Path path(@Nullable final Path path) {
        return Optional.ofNullable(path)
            .map(Path::toAbsolutePath)
            .map(Path::normalize)
            .orElse(null);
    }

    @NonNull
    public static Path safePath(
        final Path path,
        final String fieldName
    ) throws RequireException {
        return Require.notNull(path(path), fieldName);
    }

    @NonNull
    public static Path verified(
        final Path path,
        final boolean mustBeFolder,
        final String fieldName
    ) throws RequireException {
        final var result = Require.condition(
            safePath(path, fieldName),
            Files::exists,
            fieldName + " does not exist"
        );
        if (mustBeFolder) {
            return Require.condition(result, Files::isDirectory, fieldName + " is not a folder");
        }
        return Require.condition(result, Files::isRegularFile, fieldName + " is not a file");
    }
}
