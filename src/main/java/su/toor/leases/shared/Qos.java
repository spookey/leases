package su.toor.leases.shared;

public enum Qos {
    ZERO,
    ONE,
    TWO
}
