package su.toor.leases.shared;

import java.util.Objects;
import java.util.function.Predicate;

import org.eclipse.paho.mqttv5.common.util.MqttTopicValidator;
import org.springframework.lang.NonNull;

public final class Require {
    private Require() {}

    static final String IS_NULL = "is null";
    static final String IS_EMPTY = "is empty";
    static final String IS_BLANK = "is blank";

    static final String IS_INVALID = " is invalid";
    static final String MESSAGE_TOPIC = "message topic";

    static <T> T condition(
        final T elem,
        final Predicate<T> check,
        final String message
    ) throws RequireException {
        if (check == null) {
            throw new RequireException("check", IS_NULL);
        }
        if (message == null || message.isEmpty()) {
            throw new RequireException("message", IS_NULL, "or", IS_EMPTY);
        }
        if (!check.test(elem)) {
            throw new RequireException(message);
        }
        return elem;
    }

    @NonNull
    public static <T> T notNull(
        final T elem,
        final String fieldName
    ) throws RequireException {
        return condition(elem, Objects::nonNull, fieldName + " " + IS_NULL);
    }

    @NonNull
    public static String notEmpty(
        final String elem,
        final String fieldName
    ) throws RequireException {
        return condition(
            notNull(elem, fieldName), el -> !el.isEmpty(), fieldName + " " + IS_EMPTY
        );
    }

    @NonNull
    public static String notBlank(
        final String elem,
        final String fieldName
    ) throws RequireException {
        return condition(
            notEmpty(elem, fieldName), el -> !el.isBlank(), fieldName + " " + IS_BLANK
        );
    }

    @NonNull
    static boolean validTopic(final String name, final boolean listening) {
        try {
            MqttTopicValidator.validate(name, listening, listening);
        } catch (final RuntimeException ex) {
            return false;
        }
        return true;
    }

    @NonNull
    public static String validMessageTopic(final String name) throws RequireException {
        return condition(
            notBlank(name, MESSAGE_TOPIC),
            topic -> validTopic(topic, false),
            MESSAGE_TOPIC + " " + IS_INVALID
        );
    }
}
