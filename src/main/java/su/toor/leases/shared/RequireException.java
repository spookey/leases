package su.toor.leases.shared;

public class RequireException extends RuntimeException {

    public RequireException(final String message) {
        super(message);
    }

    public RequireException(final String... elements) {
        this(String.join(" ", elements));
    }
}
