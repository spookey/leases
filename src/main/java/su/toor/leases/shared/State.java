package su.toor.leases.shared;

import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public enum State {
    FREE("Free", false),
    BACKUP("Backup", true),
    ACTIVE("Active", false);

    public enum Direction {
        HIGH("up"),
        DOWN("down"),
        NONE("unknown");

        private final String text;

        Direction(final String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }

    private final String text;
    private final boolean hide;

    State(final String text, final boolean hide) {
        this.text = text;
        this.hide = hide;
    }

    @Nullable
    public static State parse(final String value) {
        return EnumSet.allOf(State.class)
            .stream()
            .filter(state -> state.name().equalsIgnoreCase(value))
            .findFirst()
            .orElse(null);
    }

    @NonNull
    public static List<State> items(final Boolean hidden) {
        final var flag = Boolean.TRUE.equals(hidden);
        return Stream.of(values())
            .filter(state -> (flag || !state.hide))
            .sorted(Comparator.comparing(State::getText))
            .collect(Collectors.toList());
    }
    /*                          +hidden     -hidden
     * ACTIVE    ->  ACTIVE  =>  NONE   =>  NONE
     * ACTIVE    ->  BACKUP  =>  DOWN   =>  DOWN
     * ACTIVE    ->  FREE    =>  DOWN   =>  DOWN
     *
     * BACKUP    ->  ACTIVE  =>  HIGH   =>  HIGH
     * BACKUP    ->  BACKUP  =>  NONE   =>  NONE
     * BACKUP    ->  FREE    =>  DOWN   =>  NONE
     *
     * FREE      ->  ACTIVE  =>  HIGH   =>  HIGH
     * FREE      ->  BACKUP  =>  HIGH   =>  NONE
     * FREE      ->  FREE    =>  NONE   =>  NONE
     */
    @NonNull
    public State.Direction change(final State that, final Boolean hidden) {
        if (that == null || this.equals(that)) {
            return Direction.NONE;
        }
        if (this.equals(ACTIVE)) {
            return Direction.DOWN;
        }
        if (that.equals(ACTIVE)) {
            return Direction.HIGH;
        }
        if (Boolean.TRUE.equals(hidden)) {
            if (this.equals(BACKUP) && that.equals(FREE)) {
                return Direction.DOWN;
            }
            if (this.equals(FREE) && that.equals(BACKUP)) {
                return Direction.HIGH;
            }
        }
        return Direction.NONE;
    }

    public String getText() {
        return text;
    }

    public boolean isHide() {
        return hide;
    }
}
