package su.toor.leases;

import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mockStatic;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ConfigurableApplicationContext;

class ApplicationTest {

    @MockBean
    private ConfigurableApplicationContext applicationContext;

    private static final String[] ARGS = new String[] { "leases" };

    @Test
    void success() {
        try (final var app = mockStatic(SpringApplication.class)) {
            app.when(() -> SpringApplication.run(eq(Application.class), any()))
                .thenReturn(applicationContext);

            assertThatNoException()
                .isThrownBy(() -> Application.main(ARGS));

            app.verify(() -> SpringApplication.run(eq(Application.class), any()));
        }
    }
}
