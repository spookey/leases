package su.toor.leases;

import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import inet.ipaddr.MACAddressString;
import inet.ipaddr.mac.MACAddress;

public interface  Net {

    String HOSTNAME = "machine-3000";

    String MAC_ONE = "c0:ff:ee:00:be:ef";
    String MAC_TWO = "be:ef:00:c0:ff:ee";
    MACAddress MAC_ADDR_ONE = new MACAddressString(MAC_ONE).getAddress();
    MACAddress MAC_ADDR_TWO = new MACAddressString(MAC_TWO).getAddress();

    String IP_ONE = "fd64::1";
    String IP_TWO = "fd56::1";
    IPAddress IP_ADDR_ONE = new IPAddressString(IP_ONE).getAddress();
    IPAddress IP_ADDR_TWO = new IPAddressString(IP_TWO).getAddress();

    String MASK_ONE = "fd64::/64";
    String MASK_TWO = "fd56::/56";
    IPAddress MASK_ADDR_ONE = new IPAddressString(MASK_ONE).getAddress();
    IPAddress MASK_ADDR_TWO = new IPAddressString(MASK_TWO).getAddress();
}
