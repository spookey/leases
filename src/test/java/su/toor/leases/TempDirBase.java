package su.toor.leases;

import static net.logstash.logback.argument.StructuredArguments.kv;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class TempDirBase {

    private static final Logger LOG = LoggerFactory.getLogger(TempDirBase.class);

    protected Path base = null;

    @BeforeEach
    public void beforeEach() throws IOException {
        base = createTempDir(getClass());
    }

    @AfterEach
    public void afterEach() throws IOException {
        removeRecursive(base);
    }
    static Path createTempDir(final Class<?> clazz) throws IOException {
        final var path = Files.createTempDirectory(clazz.getCanonicalName());
        LOG.info("temp dir created [{}]", kv("path", path));
        return path.toAbsolutePath();
    }

    public static void removeRecursive(final Path path) throws IOException {
        if (path == null || !Files.exists(path)) {
            return;
        }
        try (final var stream = Files.walk(path)) {
            final var failed = stream.sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .map(File::delete)
                .filter(res -> !res)
                .findAny();

            if (failed.isPresent()) {
                LOG.warn("failed to remove [{}]", kv("path", path));
            }
        }
    }
}
