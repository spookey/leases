package su.toor.leases.components;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mockConstructionWithAnswer;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import su.toor.leases.TempDirBase;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.shared.RequireException;

class AccessorTest extends TempDirBase {

    @Test
    void readTextIssues() {
        final var file = base.resolve("empty");

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Accessor.readText((Path) null));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Accessor.readText(file));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Accessor.readText((String) null));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Accessor.readText(""));
    }

    @Test
    void readTextPathIssues() throws IOException {
        final var file = Files.createFile(base.resolve("file"));

        try (final var ignored = mockConstructionWithAnswer(
            BufferedReader.class,
            invocation -> { throw new IOException("test"); }
        )) {

            assertThat(Accessor.readText(file)).isEmpty();
        }
    }

    @Test
    void readTextStringIssues() {
        final var text = "some text";

        try (final var ignored = mockConstructionWithAnswer(
            BufferedReader.class,
            invocation -> { throw new IOException("test"); }
        )) {

            assertThat(Accessor.readText(text)).isEmpty();
        }
    }

    @Test
    void readTextPath() throws IOException {
        final var content = "some\nlong\n\rtext";

        final var file = Files.createFile(base.resolve("file"));
        Files.writeString(file, content);

        final var result = Accessor.readText(file);

        assertThat(result).containsExactly("some", "long", "", "text");
    }

    @Test
    void readTextString() {
        final var content = "some\nlong\n\rtext";

        final var result = Accessor.readText(content);

        assertThat(result).containsExactly("some", "long", "", "text");
    }

    @Test
    void leaseFilesDoNotExist() {
        final var leaseFiles = Set.of(
            base.resolve("one"),
            base.resolve("two")
        );

        final var accessor = new Accessor(new BasicConfig().setLeaseFiles(leaseFiles));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(accessor::getLeaseFiles)
            .withMessageContaining("does not exist");
    }

    @Test
    void leaseFilesAreNotFiles() throws IOException {
        final var leaseFiles = Set.of(
            Files.createDirectory(base.resolve("one")),
            Files.createDirectory(base.resolve("two"))
        );

        final var accessor = new Accessor(new BasicConfig().setLeaseFiles(leaseFiles));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(accessor::getLeaseFiles)
            .withMessageContaining("is not a file");
    }

    @Test
    void leaseFiles() throws IOException {
        final var leaseFiles = Set.of(
            Files.createFile(base.resolve("one")),
            Files.createFile(base.resolve("two"))
        );

        final var accessor = new Accessor(new BasicConfig().setLeaseFiles(leaseFiles));
        assertThat(accessor.getLeaseFiles())
            .containsExactlyInAnyOrderElementsOf(leaseFiles);
    }

    @Test
    void loadLeaseFiles() throws IOException {
        final var one = Files.createFile(base.resolve("one"));
        Files.writeString(one, "one");
        final var two = Files.createFile(base.resolve("two"));
        Files.writeString(two, "two");

        final var config = new BasicConfig()
            .setLeaseFiles(Set.of(one, two));
        final var accessor = new Accessor(config);

        final var result = accessor.loadLeaseFiles();
        assertThat(result).isEqualTo(List.of("one", "two"));
    }
}
