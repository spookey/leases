package su.toor.leases.components;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.IP_ONE;
import static su.toor.leases.Net.MAC_ONE;

import java.util.List;
import java.util.Optional;

import inet.ipaddr.IPAddress;
import inet.ipaddr.mac.MACAddress;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.leases.components.core.Batch;
import su.toor.leases.components.core.Changes;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.parse.LeaseConverter;
import su.toor.leases.services.query.EntityService;
import su.toor.leases.shared.State;

@ExtendWith(SpringExtension.class)
class ImporterTest {

    @MockBean
    private EntityService entityService;
    @MockBean
    private Changes changes;

    private Importer create() {
        return new Importer(entityService, changes);
    }

    @Test
    void importLeasesEmpty() {
        final var stampEntity = StampEntity.with();

        when(entityService.latestStamp())
            .thenReturn(Optional.of(stampEntity));

        final var importer = create();
        final var result = importer.importLeases(List.of());

        assertThat(result.getPrevStampEntity()).isEqualTo(stampEntity);
        assertThat(result.getCurrStampEntity()).isNull();

        verify(entityService, times(1))
            .latestStamp();
        verify(entityService, times(0))
            .createStamp();
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void importLeases() {
        final var prevStampEntity = StampEntity.with();
        when(entityService.latestStamp())
            .thenReturn(Optional.of(prevStampEntity));

        final var currStampEntity = StampEntity.with();
        when(entityService.createStamp())
            .thenReturn(currStampEntity);

        final var etherEntity = mock(EtherEntity.class);
        when(entityService.ensureEther(any(MACAddress.class), anyString()))
            .thenReturn(etherEntity);

        final var boundEntity = mock(BoundEntity.class);
        when(entityService.ensureBound(any(IPAddress.class)))
            .thenReturn(boundEntity);

        final var state = State.ACTIVE;

        final var trackEntity = mock(TrackEntity.class);
        when(entityService.createTrack(currStampEntity, etherEntity, boundEntity, state))
            .thenReturn(trackEntity);

        final var optionalLease = LeaseConverter.get()
            .consumeIpAddress(IP_ONE)
            .consumeMacAddress(MAC_ONE)
            .consumeHostname(HOSTNAME)
            .consumeState(state.name())
            .build();
        assertThat(optionalLease).isPresent();
        final var lease = optionalLease.get();

        final var importer = create();
        final var result = importer.importLeases(List.of(lease));

        assertThat(result.getPrevStampEntity()).isEqualTo(prevStampEntity);
        assertThat(result.getCurrStampEntity()).isEqualTo(currStampEntity);

        verify(entityService, times(1))
            .latestStamp();
        verify(entityService, times(1))
            .createStamp();
        verify(entityService, times(1))
            .ensureEther(any(MACAddress.class), anyString());
        verify(entityService, times(1))
            .ensureBound(any(IPAddress.class));
        verify(entityService, times(1))
            .createTrack(currStampEntity, etherEntity, boundEntity, state);
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void runOnLines() {
        final var lines = List.of("import", "text");

        final var stampEntity = StampEntity.with();
        final var batch = new Batch(null)
            .populate(stampEntity, null);

        final var importer = spy(create());
        doReturn(batch)
            .when(importer)
            .importLeases(anyList());

        assertThat(importer.run(lines))
            .hasValue(stampEntity);

        verify(changes, times(1))
            .perform(batch);
        verifyNoMoreInteractions(changes);

        verify(importer, times(1))
            .run(lines);
        verify(importer, times(1))
            .importLeases(anyList());
        verifyNoMoreInteractions(importer);
    }

    @Test
    void runOnText() {
        final var text = "import text";

        final var importer = mock(Importer.class);
        doCallRealMethod()
            .when(importer)
            .run(text);

        assertThatNoException()
            .isThrownBy(() -> importer.run(text));

        verify(importer, times(1))
            .run(text);
        verify(importer, times(1))
            .run(List.of(text));
        verifyNoMoreInteractions(importer);
    }
}
