package su.toor.leases.components;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.util.Pair;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.leases.components.spring.CounterBean;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.database.StampEntity;
import su.toor.leases.services.query.EntityService;

@ExtendWith(SpringExtension.class)
class ScrubberTest {

    @MockBean
    private CounterBean counterBean;
    @MockBean
    private EntityService entityService;

    private static final int KEEP_DAYS = 7;

    private Scrubber create(final int keepDays) {
        return new Scrubber(
            new BasicConfig().setKeepDays(keepDays),
            counterBean,
            entityService
        );
    }
    private Scrubber create() {
        return create(KEEP_DAYS);
    }

    private StampEntity prepareDeletion(final boolean deletionResponse) {
        final var stampEntity = StampEntity.with();
        when(entityService.deleteStamp(stampEntity.getUuid()))
            .thenReturn(deletionResponse);
        return stampEntity;
    }

    @Test
    void removeStampEntitiesEmpty() {
        final var scrubber = create();
        final var result = scrubber.removeStampEntities(Set.of());

        assertThat(result.getFirst()).isZero();
        assertThat(result.getSecond()).isZero();

        verifyNoInteractions(entityService);
    }

    @Test
    void removeStampEntities() {
        final var failed = IntStream.range(0, 5).boxed()
            .map(num -> prepareDeletion(false))
            .collect(Collectors.toSet());
        final var success = IntStream.range(0, 23).boxed()
            .map(num -> prepareDeletion(true))
            .collect(Collectors.toSet());
        final var total = Stream.of(failed, success)
            .flatMap(Set::stream)
            .collect(Collectors.toSet());

        final var scrubber = create();
        final var result = scrubber.removeStampEntities(total);

        assertThat(result.getFirst()).isEqualTo(success.size());
        assertThat(result.getSecond()).isEqualTo(failed.size());

        verify(entityService, times(total.size()))
            .deleteStamp(any(UUID.class));
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void keepLimitDisabled() {
        final var scrubber = create(0);

        assertThat(scrubber.keepLimit())
            .isEmpty();
    }

    @Test
    void keepLimit() {
        final var start = OffsetDateTime.now(ZoneOffset.UTC);

        final var scrubber = create(KEEP_DAYS);
        assertThat(scrubber.keepLimit())
            .hasValueSatisfying(limit -> assertThat(limit)
                .isEqualToIgnoringSeconds(start.minusDays(KEEP_DAYS))
            );
    }

    @Test
    void locateOutdatedStampsDisabled() {
        final var scrubber = create(-1);

        assertThat(scrubber.locateOutdatedStamps())
            .isEmpty();

        verifyNoInteractions(entityService);
    }

    @Test
    void locateOutdatedStamps() {
        final var stampEntities = Set.of(
            StampEntity.with(), StampEntity.with(), StampEntity.with()
        );

        when(entityService.findStampBefore(any(OffsetDateTime.class)))
            .thenReturn(stampEntities);

        final var scrubber = create(KEEP_DAYS);
        assertThat(scrubber.locateOutdatedStamps())
            .containsExactlyInAnyOrderElementsOf(stampEntities);

        verify(entityService, times(1))
            .findStampBefore(any(OffsetDateTime.class));
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void run() {
        final var outdated = Set.of(StampEntity.with());

        final var scrubber = spy(create());
        doReturn(outdated)
            .when(scrubber)
            .locateOutdatedStamps();

        doReturn(Pair.of(13L, 12L))
            .when(scrubber)
            .removeStampEntities(outdated);

        assertThatNoException()
            .isThrownBy(scrubber::run);

                verify(counterBean, times(1))
            .addPurgeOutdatedSuccess(13L);
        verify(counterBean, times(1))
            .addPurgeOutdatedFailed(12L);
        verifyNoMoreInteractions(counterBean);
    }
}
