package su.toor.leases.components.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.TrackEntity;

class BatchTest {

    private static final StampEntity STAMP_ONE = mock(StampEntity.class);
    private static final StampEntity STAMP_TWO = mock(StampEntity.class);
    private static final Set<TrackEntity> TRACKS = Set.of(
        mock(TrackEntity.class),
        mock(TrackEntity.class),
        mock(TrackEntity.class)
    );

    private static final ValueStyler STYLER = new DefaultValueStyler();

    @Test
    void nullFields() {
        final var prevBatch = new Batch(null);

        assertThat(prevBatch.getPrevStampEntity()).isNull();
        assertThat(prevBatch.getCurrStampEntity()).isNull();
        assertThat(prevBatch.getTrackEntities()).isEmpty();
        assertThat(prevBatch.isComplete()).isFalse();

        final var currBatch = prevBatch.populate(null, null);
        assertThat(currBatch)
            .isNotSameAs(prevBatch);

        assertThat(currBatch.getPrevStampEntity()).isNull();
        assertThat(currBatch.getCurrStampEntity()).isNull();
        assertThat(currBatch.getTrackEntities()).isEmpty();
        assertThat(currBatch.isComplete()).isFalse();

        assertThat(prevBatch.toString())
            .isNotBlank()
            .contains("prevStampEntity")
            .contains("currStampEntity")
            .contains("size")
            .contains(STYLER.style(0));

        assertThat(currBatch.toString())
            .isNotBlank()
            .contains("prevStampEntity")
            .contains("currStampEntity")
            .contains("size")
            .contains(STYLER.style(null))
            .contains(STYLER.style(0));
    }

    @Test
    void fields() {
        final var prevBatch = new Batch(STAMP_ONE);

        assertThat(prevBatch.getPrevStampEntity()).isEqualTo(STAMP_ONE);
        assertThat(prevBatch.getCurrStampEntity()).isNull();
        assertThat(prevBatch.getTrackEntities()).isEmpty();
        assertThat(prevBatch.isComplete()).isFalse();

        final var currBatch = prevBatch.populate(STAMP_TWO, TRACKS);
        assertThat(currBatch)
            .isNotSameAs(prevBatch);

        assertThat(currBatch.getPrevStampEntity()).isEqualTo(STAMP_ONE);
        assertThat(currBatch.getCurrStampEntity()).isEqualTo(STAMP_TWO);
        assertThat(currBatch.getTrackEntities()).isEqualTo(TRACKS);
        assertThat(currBatch.isComplete()).isTrue();

        assertThat(prevBatch.toString())
            .isNotBlank()
            .contains(STYLER.style(STAMP_ONE));

        assertThat(currBatch.toString())
            .isNotBlank()
            .contains(STYLER.style(STAMP_ONE))
            .contains(STYLER.style(STAMP_TWO))
            .contains(STYLER.style(TRACKS.size()));
    }
}
