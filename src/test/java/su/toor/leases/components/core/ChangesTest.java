package su.toor.leases.components.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.Net.MASK_ADDR_ONE;

import java.util.Optional;
import java.util.Set;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.database.alert.AlertGuideEntity;
import su.toor.leases.services.MessageService;
import su.toor.leases.services.query.AlertEntityService;
import su.toor.leases.shared.State;

@ExtendWith(SpringExtension.class)
class ChangesTest {

    @MockBean
    private AlertEntityService alertEntityService;
    @MockBean
    private MessageService messageService;

    private Changes create(final int guidesEvery) {
        return new Changes(
            new BasicConfig()
                .setHiddenState(true)
                .setGuidesEvery(guidesEvery),
            alertEntityService,
            messageService
        );
    }
    private Changes create() {
        return create(1);
    }

    @Test
    void shouldHandleGuides() {
        final var service = create(3);
        assertThat(service.guideCounter.get()).isZero();

        IntStream.range(0, 5).forEach(num -> {
            assertThat(service.shouldHandleGuides()).isTrue();
            assertThat(service.guideCounter.get()).isEqualTo(3);
            assertThat(service.shouldHandleGuides()).isFalse();
            assertThat(service.guideCounter.get()).isEqualTo(2);
            assertThat(service.shouldHandleGuides()).isFalse();
            assertThat(service.guideCounter.get()).isOne();
        });
    }

    @Test
    void handleEthers() {
        final var prevStampEntity = StampEntity.with();
        final var currStampEntity = StampEntity.with();
        final var etherEntity = EtherEntity.with(MAC_ADDR_ONE, HOSTNAME);
        final var boundEntity = BoundEntity.with(IP_ADDR_TWO);

        final var prevTrackEntity = TrackEntity.with(prevStampEntity, etherEntity, boundEntity, State.FREE);
        final var currTrackEntity = TrackEntity.with(currStampEntity, etherEntity, boundEntity, State.ACTIVE);

        final var alertEtherEntity = AlertEtherEntity.with(etherEntity, true, null, null);

        final var batch = new Batch(prevStampEntity)
            .populate(currStampEntity, Set.of(currTrackEntity));

        when(alertEntityService.findAlertEthersEnabled())
            .thenReturn(Set.of(alertEtherEntity));
        when(alertEntityService.findTrackWithoutState(prevStampEntity, etherEntity, currTrackEntity.getState()))
            .thenReturn(Optional.of(prevTrackEntity));

        final var service = create();
        assertThatNoException()
            .isThrownBy(() -> service.handleEthers(batch));

        verify(alertEntityService, times(1))
            .findAlertEthersEnabled();
        verify(alertEntityService, times(1))
            .findTrackWithoutState(prevStampEntity, etherEntity, currTrackEntity.getState());
        verifyNoMoreInteractions(alertEntityService);

        verify(messageService, times(1))
            .notify(alertEtherEntity, currTrackEntity.getState());
        verifyNoMoreInteractions(messageService);
    }

    @Test
    void handleBounds() {
        final var prevStampEntity = StampEntity.with();
        final var currStampEntity = StampEntity.with();
        final var etherEntity = EtherEntity.with(MAC_ADDR_TWO, null);
        final var boundEntity = BoundEntity.with(IP_ADDR_ONE);

        final var prevTrackEntity = TrackEntity.with(prevStampEntity, etherEntity, boundEntity, State.FREE);
        final var currTrackEntity = TrackEntity.with(currStampEntity, etherEntity, boundEntity, State.ACTIVE);

        final var alertBoundEntity = AlertBoundEntity.with(boundEntity, true, null, null);

        final var batch = new Batch(prevStampEntity)
            .populate(currStampEntity, Set.of(currTrackEntity));

        when(alertEntityService.findAlertBoundsEnabled())
            .thenReturn(Set.of(alertBoundEntity));
        when(alertEntityService.findTrackWithoutState(prevStampEntity, boundEntity, currTrackEntity.getState()))
            .thenReturn(Optional.of(prevTrackEntity));

        final var service = create();
        assertThatNoException()
            .isThrownBy(() -> service.handleBounds(batch));

        verify(alertEntityService, times(1))
            .findAlertBoundsEnabled();
        verify(alertEntityService, times(1))
            .findTrackWithoutState(prevStampEntity, boundEntity, currTrackEntity.getState());
        verifyNoMoreInteractions(alertEntityService);

        verify(messageService, times(1))
            .notify(alertBoundEntity, currTrackEntity.getState());
        verifyNoMoreInteractions(messageService);
    }

    @Test
    void handleGuidesCounter() {
        final var batch = new Batch(null);
        when(alertEntityService.findAlertGuidesEnabled())
            .thenReturn(Set.of());

        final var service = create(3);
        assertThatNoException()
            .isThrownBy(() -> service.handleGuides(batch));
        assertThatNoException()
            .isThrownBy(() -> service.handleGuides(batch));
        assertThatNoException()
            .isThrownBy(() -> service.handleGuides(batch));

        verify(alertEntityService, times(1))
            .findAlertGuidesEnabled();
        verifyNoMoreInteractions(alertEntityService);
        verifyNoInteractions(messageService);
    }

    @Test
    void handleGuides() {
        final var stampEntity = StampEntity.with();
        final var etherEntity = EtherEntity.with(MAC_ADDR_TWO, null);
        final var boundEntity = BoundEntity.with(IP_ADDR_ONE);

        final var trackEntityNil = TrackEntity.with(stampEntity, etherEntity, boundEntity, State.FREE);
        final var trackEntityOne = TrackEntity.with(stampEntity, etherEntity, boundEntity, State.BACKUP);
        final var trackEntityTwo = TrackEntity.with(stampEntity, etherEntity, boundEntity, State.ACTIVE);

        final var guideEntity = GuideEntity.with(MASK_ADDR_ONE, null, "Title", null);
        final var alertGuideEntity = AlertGuideEntity.with(guideEntity, true, null, null);

        final var batch = new Batch(null)
            .populate(stampEntity, Set.of(trackEntityNil, trackEntityOne, trackEntityTwo));

        when(alertEntityService.findAlertGuidesEnabled())
            .thenReturn(Set.of(alertGuideEntity));

        final var service = create();
        assertThatNoException()
            .isThrownBy(() -> service.handleGuides(batch));

        verify(alertEntityService, times(1))
            .findAlertGuidesEnabled();
        verifyNoMoreInteractions(alertEntityService);

        verify(messageService, times(1))
            .notify(alertGuideEntity, 1L, 2L);
        verifyNoMoreInteractions(messageService);
    }

    @Test
    void performEmpty() {
        final var changes = mock(Changes.class);
        doCallRealMethod()
            .when(changes)
            .perform(any());

        final var prevEmptyBatch = new Batch(null);
        final var currEmptyBatch = new Batch(StampEntity.with())
            .populate(null, null);

        assertThatNoException()
            .isThrownBy(() -> changes.perform(null));
        assertThatNoException()
            .isThrownBy(() -> changes.perform(prevEmptyBatch));
        assertThatNoException()
            .isThrownBy(() -> changes.perform(currEmptyBatch));

        verify(changes, times(3))
            .perform(any());
        verify(changes, times(0))
            .handleEthers(any());
        verify(changes, times(0))
            .handleBounds(any());
        verify(changes, times(0))
            .handleGuides(any());
        verifyNoMoreInteractions(changes);
    }

    @Test
    void perform() {
        final var changes = mock(Changes.class);
        doCallRealMethod()
            .when(changes)
            .perform(any());

        final var batch = new Batch(StampEntity.with())
            .populate(StampEntity.with(), null);

        assertThatNoException()
            .isThrownBy(() -> changes.perform(batch));

        verify(changes, times(1))
            .perform(batch);
        verify(changes, times(1))
            .handleEthers(batch);
        verify(changes, times(1))
            .handleBounds(batch);
        verify(changes, times(1))
            .handleGuides(batch);
        verifyNoMoreInteractions(changes);
    }
}
