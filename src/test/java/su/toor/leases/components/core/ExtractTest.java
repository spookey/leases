package su.toor.leases.components.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import inet.ipaddr.IPAddress;
import org.junit.jupiter.api.Test;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.database.alert.AlertGuideEntity;
import su.toor.leases.shared.State;

class ExtractTest {

    private static EtherEntity makeEther() {
        return mock(EtherEntity.class);
    }
    private static BoundEntity makeBound() {
        final var ip = mock(IPAddress.class);

        final var boundEntity = mock(BoundEntity.class);
        when(boundEntity.getIp()).thenReturn(ip);
        return boundEntity;
    }
    private static GuideEntity makeGuide(final BoundEntity... boundEntities) {
        final var mask = mock(IPAddress.class);
        when(mask.contains(any(IPAddress.class)))
            .thenReturn(false);
        Stream.of(boundEntities)
            .map(BoundEntity::getIp)
            .forEach(ip -> when(mask.contains(ip)).thenReturn(true));

        final var guideEntity = mock(GuideEntity.class);
        when(guideEntity.getMask()).thenReturn(mask);
        return guideEntity;
    }
    private static TrackEntity makeTrack(final EtherEntity etherEntity, final BoundEntity boundEntity, final State state) {
        final var trackEntity = mock(TrackEntity.class);
        when(trackEntity.getEtherEntity()).thenReturn(etherEntity);
        when(trackEntity.getBoundEntity()).thenReturn(boundEntity);
        when(trackEntity.getState()).thenReturn(state);
        return trackEntity;
    }
    private static TrackEntity makeTrack(final EtherEntity etherEntity, final BoundEntity boundEntity) {
        return makeTrack(etherEntity, boundEntity, State.BACKUP);
    }
    private static TrackEntity makeTrack(final BoundEntity boundEntity, final State state) {
        return makeTrack(makeEther(), boundEntity, state);
    }

    private static AlertEtherEntity makeAlertEther(final EtherEntity etherEntity) {
        final var alertEtherEntity = mock(AlertEtherEntity.class);
        when(alertEtherEntity.getEtherEntity()).thenReturn(etherEntity);
        return alertEtherEntity;
    }
    private static AlertBoundEntity makeAlertBound(final BoundEntity boundEntity) {
        final var alertBoundEntity = mock(AlertBoundEntity.class);
        when(alertBoundEntity.getBoundEntity()).thenReturn(boundEntity);
        return alertBoundEntity;
    }
    private static AlertGuideEntity makeAlertGuide(final GuideEntity guideEntity) {
        final var alertGuideEntity = mock(AlertGuideEntity.class);
        when(alertGuideEntity.getGuideEntity()).thenReturn(guideEntity);
        return alertGuideEntity;
    }

    @Test
    void mapEthers() {
        final var etherOne = makeEther();
        final var etherTwo = makeEther();

        final var trackNil = makeTrack(makeEther(), makeBound());
        final var trackOne = makeTrack(etherOne, makeBound());
        final var trackTwo = makeTrack(etherTwo, makeBound());
        final var trackTtw = makeTrack(etherTwo, makeBound());

        final var alertEtherNil = makeAlertEther(makeEther());
        final var alertEtherOne = makeAlertEther(etherOne);
        final var alertEtherTwo = makeAlertEther(etherTwo);

        final var tracks = List.of(trackNil, trackOne, trackTwo, trackTtw);
        final var alerts = List.of(alertEtherNil, alertEtherOne, alertEtherTwo);

        assertThat(Extract.mapEthers(alerts, List.of()))
            .isEmpty();
        assertThat(Extract.mapEthers(List.of(), tracks))
            .isEmpty();

        final var result = Extract.mapEthers(alerts, tracks);
        assertThat(result)
            .containsExactlyInAnyOrderEntriesOf(Map.of(
                alertEtherNil, Set.of(),
                alertEtherOne, Set.of(trackOne),
                alertEtherTwo, Set.of(trackTwo, trackTtw)
            ));
    }

    @Test
    void mapBound() {
        final var boundOne = makeBound();
        final var boundTwo = makeBound();

        final var trackNil = makeTrack(makeEther(), makeBound());
        final var trackOne = makeTrack(makeEther(), boundOne);
        final var trackTwo = makeTrack(makeEther(), boundTwo);
        final var trackTtw = makeTrack(makeEther(), boundTwo);

        final var alertBoundNil = makeAlertBound(makeBound());
        final var alertBoundOne = makeAlertBound(boundOne);
        final var alertBoundTwo = makeAlertBound(boundTwo);

        final var tracks = List.of(trackNil, trackOne, trackTwo, trackTtw);
        final var alerts = List.of(alertBoundNil, alertBoundOne, alertBoundTwo);

        assertThat(Extract.mapBounds(alerts, List.of()))
            .isEmpty();
        assertThat(Extract.mapBounds(List.of(), tracks))
            .isEmpty();

        final var result = Extract.mapBounds(alerts, tracks);
        assertThat(result)
            .containsExactlyInAnyOrderEntriesOf(Map.of(
                alertBoundNil, Set.of(),
                alertBoundOne, Set.of(trackOne),
                alertBoundTwo, Set.of(trackTwo, trackTtw)
            ));
    }

    @Test
    void mapGuide() {
        final var boundOne = makeBound();
        final var boundTwo = makeBound();

        final var guideOne = makeGuide(boundOne);
        final var guideTwo = makeGuide(boundTwo);

        final var trackNil = makeTrack(makeEther(), makeBound());
        final var trackOne = makeTrack(makeEther(), boundOne);
        final var trackTwo = makeTrack(makeEther(), boundTwo);
        final var trackTtw = makeTrack(makeEther(), boundTwo);

        final var alertGuideNil = makeAlertGuide(makeGuide());
        final var alertGuideOne = makeAlertGuide(guideOne);
        final var alertGuideTwo = makeAlertGuide(guideTwo);

        final var tracks = List.of(trackNil, trackOne, trackTwo, trackTtw);
        final var alerts = List.of(alertGuideNil, alertGuideOne, alertGuideTwo);

        assertThat(Extract.mapGuides(alerts, List.of()))
            .isEmpty();
        assertThat(Extract.mapGuides(List.of(), tracks))
            .isEmpty();

        final var result = Extract.mapGuides(alerts, tracks);
        assertThat(result)
            .containsExactlyInAnyOrderEntriesOf(Map.of(
                alertGuideNil, Set.of(),
                alertGuideOne, Set.of(trackOne),
                alertGuideTwo, Set.of(trackTwo, trackTtw)
            ));
    }

    @Test
    void filterTracks() {
        final var boundOne = makeBound();
        final var boundTwo = makeBound();

        final var guideNil = makeGuide(makeBound());
        final var guideOne = makeGuide(boundOne);
        final var guideTwo = makeGuide(boundOne, boundTwo);

        final var trackNil = makeTrack(makeEther(), makeBound());
        final var trackOne = makeTrack(makeEther(), boundOne);
        final var trackTwo = makeTrack(makeEther(), boundTwo);
        final var trackTtw = makeTrack(makeEther(), boundTwo);

        final var tracks = List.of(trackNil, trackOne, trackTwo, trackTtw);

        assertThat(Extract.filterTracks(guideOne, List.of()))
            .isEmpty();
        assertThat(Extract.filterTracks(guideTwo, List.of()))
            .isEmpty();

        assertThat(Extract.filterTracks(guideNil, tracks))
            .isEmpty();
        assertThat(Extract.filterTracks(guideOne, tracks))
            .containsExactlyInAnyOrder(trackOne);
        assertThat(Extract.filterTracks(guideTwo, tracks))
            .containsExactlyInAnyOrder(trackOne, trackTwo, trackTtw);
    }

    @Test
    void countTracks() {
        final var boundOne = makeBound();
        final var boundTwo = makeBound();

        final var guideNil = makeGuide();
        final var guideOne = makeGuide(boundOne);
        final var guideTwo = makeGuide(boundOne, boundTwo);

        final var trackActNil = makeTrack(makeBound(), State.ACTIVE);
        final var trackActOne = makeTrack(boundOne, State.ACTIVE);
        final var trackBakOne = makeTrack(boundOne, State.BACKUP);
        final var trackBakTwo = makeTrack(boundTwo, State.BACKUP);
        final var trackFreOne = makeTrack(boundOne, State.FREE);
        final var trackFreTwo = makeTrack(boundTwo, State.FREE);
        final var trackFreTtw = makeTrack(boundTwo, State.FREE);

        final var tracks = List.of(
            trackActNil, trackActOne, trackBakOne, trackBakTwo, trackFreOne, trackFreTwo, trackFreTtw
        );

        assertThat(Extract.countTracks(guideNil, null, List.of()))
            .isEmpty();

        assertThat(Extract.countTracks(guideNil, false, tracks))
            .containsExactlyInAnyOrderEntriesOf(Map.of(
                State.FREE, 0L,
                State.ACTIVE, 0L
            ));
        assertThat(Extract.countTracks(guideNil, true, tracks))
            .containsExactlyInAnyOrderEntriesOf(Map.of(
                State.FREE, 0L,
                State.BACKUP, 0L,
                State.ACTIVE, 0L
            ));

        assertThat(Extract.countTracks(guideOne, false, tracks))
            .containsExactlyInAnyOrderEntriesOf(Map.of(
                State.FREE, 1L,
                State.ACTIVE, 1L
            ));
        assertThat(Extract.countTracks(guideOne, true, tracks))
            .containsExactlyInAnyOrderEntriesOf(Map.of(
                State.FREE, 1L,
                State.BACKUP, 1L,
                State.ACTIVE, 1L
            ));

        assertThat(Extract.countTracks(guideTwo, false, tracks))
            .containsExactlyInAnyOrderEntriesOf(Map.of(
                State.FREE, 3L,
                State.ACTIVE, 1L
            ));
        assertThat(Extract.countTracks(guideTwo, true, tracks))
            .containsExactlyInAnyOrderEntriesOf(Map.of(
                State.FREE, 3L,
                State.BACKUP, 2L,
                State.ACTIVE, 1L
            ));
    }
}
