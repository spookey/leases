package su.toor.leases.components.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import su.toor.leases.shared.RequireException;

class SubstitutionTest {

    @Test
    void constants() {
        assertThat(Substitution.PREFIX).isEqualTo("{{");
        assertThat(Substitution.SUFFIX).isEqualTo("}}");
        assertThat(Substitution.SEPARATOR).isEqualTo("|");
    }

    @Test
    void resolver() {
        final var keyOne = "keyOne";
        final var keyTwo = "keyTwo";
        final var valueOne = "valueOne";
        final var valueTwo = "valueTwo";

        final var resolver = new Substitution.Resolver();

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> resolver.feed(null));

        assertThat(resolver.resolvePlaceholder(keyOne)).isNull();
        assertThat(resolver.resolvePlaceholder(keyTwo)).isNull();

        assertThat(resolver.feed(Map.of(keyOne, valueOne)))
            .isSameAs(resolver);

        assertThat(resolver.resolvePlaceholder(keyOne)).isEqualTo(valueOne);
        assertThat(resolver.resolvePlaceholder(keyTwo)).isNull();

        assertThat(resolver.feed(Map.of(keyTwo, valueTwo)))
            .isSameAs(resolver);

        assertThat(resolver.resolvePlaceholder(keyOne)).isNull();
        assertThat(resolver.resolvePlaceholder(keyTwo)).isEqualTo(valueTwo);

        assertThat(resolver.feed(Map.of(keyOne, valueOne, keyTwo, valueTwo)))
            .isSameAs(resolver);

        assertThat(resolver.resolvePlaceholder(keyOne)).isEqualTo(valueOne);
        assertThat(resolver.resolvePlaceholder(keyTwo)).isEqualTo(valueTwo);
    }

    @Test
    void performIssues() {
        assertThatIllegalArgumentException()
            .isThrownBy(() -> Substitution.perform(null, Map.of()));

        assertThatIllegalArgumentException()
            .isThrownBy(() -> Substitution.perform("{{text}}", Map.of()));

        assertThatIllegalArgumentException()
            .isThrownBy(() -> Substitution.perform("{{ text }}", Map.of("text", "test")));
    }

    @Test
    void perform() {
        Stream.of(Substitution.PREFIX, Substitution.PREFIX, Substitution.SEPARATOR)
            .forEach(symbol -> {
                assertThat(Substitution.perform(symbol, Map.of()))
                    .isEqualTo(symbol);

                final var twice = String.join("", symbol, symbol);
                assertThat(Substitution.perform(twice, Map.of()))
                    .isEqualTo(twice);

                final var triple = String.join("", twice, symbol);
                assertThat(Substitution.perform(triple, Map.of()))
                    .isEqualTo(triple);
            });

        Stream.of("", " ", "\n", "\t")
            .forEach(symbol -> assertThat(Substitution.perform(symbol, Map.of()))
                .isEqualTo(symbol)
            );

        assertThat(Substitution.perform("{{text}}", Map.of("text", "test")))
            .isEqualTo("test");

        assertThat(Substitution.perform("{{te xt}}", Map.of("te xt", "test")))
            .isEqualTo("test");

        assertThat(Substitution.perform("{{text space text}}", Map.of("text space text", "test")))
            .isEqualTo("test");

        assertThat(Substitution.perform("{{text|test}}", Map.of()))
            .isEqualTo("test");
    }

    @Test
    void make() {
        assertThat(Substitution.make(
            null,
            "missing {{placeholder}}",
            Map.of())
        )
            .isNull();

        assertThat(Substitution.make(
            "\n\t\n",
            "not blank",
            Map.of()
        ))
            .isEqualTo("not blank");

        assertThat(Substitution.make(
            "template {{text}}",
            "fallback {{error}}",
            Map.of("text", "test")
        ))
            .isEqualTo("template test");

        assertThat(Substitution.make(
            null,
            "fallback {{text}}",
            Map.of("text", "test")
        ))
            .isEqualTo("fallback test");
    }
}
