package su.toor.leases.components.mqtt;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatNoException;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.function.Consumer;

import org.eclipse.paho.mqttv5.client.MqttDisconnectResponse;
import org.eclipse.paho.mqttv5.client.MqttToken;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.packet.MqttProperties;
import org.junit.jupiter.api.Test;
import su.toor.leases.components.spring.CounterBean;

class CallbackTest {

    private final static MqttException MQTT_EXCEPTION = new MqttException(new RuntimeException("test"));

    private void check(
        final Consumer<Callback> action,
        final Consumer<CounterBean> counterVerify
    ) {
        final var counterBean = mock(CounterBean.class);
        final var callback = new Callback(counterBean);
        assertThatNoException()
            .isThrownBy(() -> action.accept(callback));

        counterVerify.accept(
            verify(counterBean, times(1))
        );
        verifyNoMoreInteractions(counterBean);
    }


    @Test
    void deliveryComplete() {
        final var token = new MqttToken();

        check(
            callback -> callback.deliveryComplete(token),
            CounterBean::incrementMqttDelivered
        );
    }

    @Test
    void messageArrived() {
        final var topic = "some/topic";
        final var message = new MqttMessage();

        check(
            callback -> callback.messageArrived(topic, message),
            CounterBean::incrementMqttArrived
        );
    }

    @Test
    void connectComplete() {
        final var serverUri = "localhost";

        check(
            callback -> callback.connectComplete(true, serverUri),
            CounterBean::incrementMqttConnects
        );
    }

    @Test
    void disconnected() {
        final var disconnectResponse = new MqttDisconnectResponse(MQTT_EXCEPTION);

        check(
            callback -> callback.disconnected(disconnectResponse),
            CounterBean::incrementMqttDisconnects
        );
    }

    @Test
    void mqttErrorOccurred() {
        check(
            callback -> callback.mqttErrorOccurred(MQTT_EXCEPTION),
            CounterBean::incrementMqttErrors
        );
    }

    @Test
    void authPacketArrived() {
        final var properties = new MqttProperties();

        check(
            callback -> callback.authPacketArrived(0, properties),
            CounterBean::incrementMqttAuthPackages
        );
    }
}
