package su.toor.leases.components.mqtt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.client.MqttToken;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.leases.configs.sinks.StatusBirthConfig;
import su.toor.leases.shared.RequireException;

@ExtendWith(SpringExtension.class)
class ClientTest {

    @MockBean
    private MqttAsyncClient mqttAsyncClient;

    private static final String APPLICATION_NAME = "test-name";
    private static final String TOPIC = "some/topic";
    private static final MqttMessage MESSAGE = new MqttMessage();

    private Client create() {
        return new Client(APPLICATION_NAME, mqttAsyncClient);
    }

    @Test
    void clientBeanCreator() {
        final var birthConfig = new StatusBirthConfig();

        try (final var clientMock = mockConstruction(Client.class)) {
            final var clientBean = Client.client(APPLICATION_NAME, mqttAsyncClient, birthConfig);

            final var client = clientMock.constructed().get(0);
            assertThat(client)
                .isNotNull()
                .isSameAs(clientBean);

            verify(client, times(1))
                .sendBirthMessage(birthConfig);
            verifyNoMoreInteractions(client);

            verifyNoInteractions(mqttAsyncClient);
        }
    }

    @Test
    void sendBirthMessageDisabled() {
        final var birthConfig = new StatusBirthConfig()
            .setEnabled(false);

        final var service = create();
        assertThat(service.sendBirthMessage(birthConfig))
            .isFalse();

        verifyNoInteractions(mqttAsyncClient);
    }

    @Test
    void sendBirthMessage() {
        final var birthConfig = new StatusBirthConfig()
            .setEnabled(true)
            .setTopic("birth/topic")
            .setTemplate("Birth message");

        final var client = spy(create());
        doReturn(true)
            .when(client)
            .sendSync(eq(birthConfig.getTopic()), any(MqttMessage.class));

        assertThat(client.sendBirthMessage(birthConfig))
            .isTrue();

        verify(client, times(1))
            .sendBirthMessage(birthConfig);
        verify(client, times(1))
            .sendSync(eq(birthConfig.getTopic()), any(MqttMessage.class));
        verify(client, times(1))
            .sendSync(eq(birthConfig.getTopic()), any(MqttMessage.class));
        verifyNoMoreInteractions(client);
    }

    @Test
    void sendSyncNullOrInvalid() {
        final var client = create();

        assertThat(client.sendSync(null, MESSAGE)).isFalse();
        assertThat(client.sendSync(TOPIC, null)).isFalse();

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> client.sendSync("invalid/+/topic/#", MESSAGE));

        verifyNoInteractions(mqttAsyncClient);
    }

    @Test
    void sendSyncNotConnected() {
        when(mqttAsyncClient.isConnected())
            .thenReturn(false);

        final var client = create();

        assertThat(client.sendSync(TOPIC, MESSAGE)).isFalse();

        verify(mqttAsyncClient, times(1))
            .isConnected();
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void sendSyncIssues() throws MqttException {
        when(mqttAsyncClient.isConnected())
            .thenReturn(true);
        when(mqttAsyncClient.publish(TOPIC, MESSAGE))
            .thenThrow(new MqttException(new RuntimeException("test")));

        final var client = create();

        assertThat(client.sendSync(TOPIC, MESSAGE)).isFalse();

        verify(mqttAsyncClient, times(1))
            .isConnected();
        verify(mqttAsyncClient, times(1))
            .publish(TOPIC, MESSAGE);
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void sendSync() throws MqttException {
        final var token = mock(MqttToken.class);
        when(mqttAsyncClient.isConnected())
            .thenReturn(true);
        when(mqttAsyncClient.publish(TOPIC, MESSAGE))
            .thenReturn(token);

        final var client = create();

        assertThat(client.sendSync(TOPIC, MESSAGE)).isTrue();

        verify(token, times(1))
            .waitForCompletion();
        verifyNoMoreInteractions(token);

        verify(mqttAsyncClient, times(1))
            .isConnected();
        verify(mqttAsyncClient, times(1))
            .publish(TOPIC, MESSAGE);
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void sync() {
        final var topic = "some/topic";
        final var message = new MqttMessage();
        final var client = spy(create());
        doReturn(true)
            .when(client)
            .sendSync(topic, message);

        try (final var completableFutureMock = mockStatic(CompletableFuture.class)) {
            completableFutureMock.when(
                () -> CompletableFuture.supplyAsync(any())
            ).thenAnswer(
                invocation -> {
                    final var supplier = invocation.getArgument(0, Supplier.class);
                    assertThat(supplier).isNotNull();

                    return CompletableFuture.completedFuture(supplier.get());
                }
            );

            assertThatNoException()
                .isThrownBy(() -> client.send(topic, message));

            completableFutureMock.verify(
                () -> CompletableFuture.supplyAsync(any()),
                times(1)
            );
        }

        verify(client, times(1))
            .send(topic, message);
        verify(client, times(1))
            .sendSync(topic, message);
        verifyNoMoreInteractions(client);
    }
}
