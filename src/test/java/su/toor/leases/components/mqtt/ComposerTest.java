package su.toor.leases.components.mqtt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.stream.Stream;

import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.junit.jupiter.api.Test;
import su.toor.leases.configs.sinks.AlertBoundConfig;
import su.toor.leases.configs.sinks.AlertEtherConfig;
import su.toor.leases.configs.sinks.AlertGuideConfig;
import su.toor.leases.configs.sinks.StatusBirthConfig;
import su.toor.leases.configs.sinks.StatusWillConfig;
import su.toor.leases.shared.Qos;
import su.toor.leases.shared.RequireException;

class ComposerTest {

    private static void check(
        final MqttMessage message,
        final Qos qos,
        final boolean retained,
        final String text
    ) {
        assertThat(message).isNotNull();
        assertThat(message.getProperties()).isNull();
        assertThat(message.getQos()).isEqualTo(qos.ordinal());
        assertThat(message.isRetained()).isEqualTo(retained);
        assertThat(message.getPayload()).isEqualTo(text.getBytes(StandardCharsets.UTF_8));
    }

    @Test
    void nullMessage() {
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Composer.make(Qos.ZERO, false, null));
    }

    @Test
    void nullMetadata() {
        final var text = "text";

        final var result = Composer.make(null, null, text);

        check(
            result,
            Composer.DEFAULT_QOS,
            Composer.DEFAULT_RETAINED,
            text
        );
    }

    @Test
    void string() {
        final var text = "some nice text";
        final var qos = Qos.ZERO;
        final var retained = false;

        final var result = Composer.make(qos, retained, text);

        check(result, qos, retained, text);
    }

    @Test
    void statusConfig() {
        final var qos = Qos.ONE;
        final var retained = true;
        final var template = "some {{text}} message";
        final var templateValues = Map.of("text", "test");
        final var templateExpect = "some test message";

        Stream.of(
            new StatusBirthConfig(),
            new StatusWillConfig()
        )
            .map(config -> config
                .setQos(qos)
                .setRetained(retained)
                .setTemplate(template)
            )
            .forEach(config -> {
                final var result = Composer.make(config, templateValues);

                check(result, qos, retained, templateExpect);
            });
    }

    @Test
    void alertConfig() {
        final var qos = Qos.TWO;
        final var retained = true;
        final var templateValues = Map.of("text", "test");
        final var message = "some real message";
        final var template = "some {{text}} message";
        final var templateExpect = "some test message";
        final var fallbackMessage = "some fallback message";

        Stream.of(
            new AlertEtherConfig(),
            new AlertBoundConfig(),
            new AlertGuideConfig()
        )
            .map(config -> config
                .setQos(qos)
                .setRetained(retained)
                .setFallbackTemplate(fallbackMessage)
            )
            .forEach(config -> {
                final var templateResult = Composer.make(config, template, templateValues);
                final var normalResult = Composer.make(config, message, templateValues);
                final var blankResult = Composer.make(config, "\t", templateValues);
                final var nullResult = Composer.make(config, null, templateValues);

                check(templateResult, qos, retained, templateExpect);
                check(normalResult, qos, retained, message);
                check(blankResult, qos, retained, fallbackMessage);
                check(nullResult, qos, retained, fallbackMessage);
            });
    }
}
