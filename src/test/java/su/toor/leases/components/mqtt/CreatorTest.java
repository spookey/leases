package su.toor.leases.components.mqtt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;

import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.client.MqttConnectionOptions;
import org.eclipse.paho.mqttv5.client.MqttToken;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanInstantiationException;
import su.toor.leases.configs.MqttConfig;
import su.toor.leases.configs.sinks.StatusWillConfig;

class CreatorTest {

    private static final String APPLICATION_NAME = "test-name";

    @Test
    void mqttConnectionOptions() {
        final var cleanStart = true;
        final var autoReconnect = false;
        final var username = "user-name";
        final var password = "pass-word";

        final var topic = "will/topic";
        final var message = "Will message of {{name}}";
        final var expectedMessage = "Will message of " + APPLICATION_NAME;

        final var statusWillConfig = new StatusWillConfig()
            .setEnabled(true)
            .setTopic(topic)
            .setTemplate(message);

        final var mqttConfig = new MqttConfig()
            .setCleanStart(cleanStart)
            .setAutoReconnect(autoReconnect)
            .setUsername(username)
            .setPassword(password);

        final var creator = new Creator();
        final var options = creator.mqttConnectionOptions(APPLICATION_NAME, mqttConfig, statusWillConfig);

        assertThat(options.isCleanStart()).isEqualTo(cleanStart);
        assertThat(options.isAutomaticReconnect()).isEqualTo(autoReconnect);
        assertThat(options.useSubscriptionIdentifiers()).isTrue();
        assertThat(options.getRequestResponseInfo()).isTrue();
        assertThat(options.getRequestProblemInfo()).isTrue();

        assertThat(options.getUserName()).isEqualTo(username);
        assertThat(options.getPassword()).isEqualTo(password.getBytes(StandardCharsets.UTF_8));

        assertThat(options.getWillDestination()).isEqualTo(topic);
        final var will = options.getWillMessage();
        assertThat(will.getPayload()).isEqualTo(expectedMessage.getBytes(StandardCharsets.UTF_8));
        assertThat(will.getQos()).isEqualTo(statusWillConfig.getQos().ordinal());
        assertThat(will.isRetained()).isEqualTo(statusWillConfig.isRetained());
    }

    @Test
    void basicMqttAsyncClientIssues() {
        final var config = mock(MqttConfig.class);
        when(config.getBrokerUri())
            .thenThrow(new IllegalArgumentException(new RuntimeException("test")));

        final var callback = mock(Callback.class);

        final var creator = new Creator();

        assertThatExceptionOfType(BeanInstantiationException.class)
            .isThrownBy(() -> creator.basicMqttAsyncClient(config, callback))
            .withMessageContaining("creation failed");

        verifyNoInteractions(callback);
    }

    @Test
    void basicMqttAsyncClient() {
        final var callback = mock(Callback.class);

        final var config = new MqttConfig()
            .setBrokerUri("tcp://localhost:1312")
            .setClientId("test-client");

        final var creator = new Creator();
        final var client = creator.basicMqttAsyncClient(config, callback);

        assertThat(client).isNotNull();
        assertThat(client.isConnected()).isFalse();
        assertThat(client.getServerURI()).isEqualTo(config.getBrokerUri());
        assertThat(client.getClientId()).isEqualTo(config.getClientId());

        verifyNoInteractions(callback);
    }

    @Test
    void mqttAsyncClientIssues() throws MqttException {
        final var mqttClient = mock(MqttAsyncClient.class);
        final var mqttOptions = mock(MqttConnectionOptions.class);

        when(mqttClient.connect(mqttOptions))
            .thenThrow(new MqttException(new RuntimeException("test")));

        final var creator = new Creator();

        assertThatExceptionOfType(BeanInstantiationException.class)
            .isThrownBy(() -> creator.mqttAsyncClient(mqttClient, mqttOptions))
            .withMessageContaining("connection failed");

        verify(mqttClient, times(1))
            .getServerURI();
        verify(mqttClient, times(1))
            .connect(mqttOptions);
        verifyNoMoreInteractions(mqttClient);

        verifyNoInteractions(mqttOptions);
    }

    @Test
    void mqttAsyncClient() throws MqttException {
        final var mqttClient = mock(MqttAsyncClient.class);
        final var mqttOptions = mock(MqttConnectionOptions.class);
        final var token = mock(MqttToken.class);

        when(mqttClient.connect(mqttOptions))
            .thenReturn(token);

        final var creator = new Creator();

        final var client = creator.mqttAsyncClient(mqttClient, mqttOptions);
        assertThat(client)
            .isSameAs(mqttClient);

        verify(token, times(1))
            .waitForCompletion();
        verifyNoMoreInteractions(token);

        verify(mqttClient, times(1))
            .getServerURI();
        verify(mqttClient, times(1))
            .connect(mqttOptions);
        verifyNoMoreInteractions(mqttClient);

        verifyNoInteractions(mqttOptions);
    }
}
