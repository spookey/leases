package su.toor.leases.components.spring;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

class CounterBeanTest {

    @Test
    void initial() {
        final var counter = new CounterBean();

        Stream.of(
            counter.getMqttDelivered(),
            counter.getMqttArrived(),
            counter.getMqttConnects(),
            counter.getMqttDisconnects(),
            counter.getMqttAuthPackages(),
            counter.getMqttErrors(),

            counter.getPurgeOutdatedSuccess(),
            counter.getPurgeOutdatedFailed()
        ).forEach(num -> assertThat(num).isZero());
    }

    private void verifyIncrement(
        final Supplier<Long> get,
        final Supplier<Long> inc
    ) {
        assertThat(get.get()).isZero();
        assertThat(get.get()).isZero();

        assertThat(inc.get()).isOne();
        assertThat(get.get()).isOne();
        assertThat(get.get()).isOne();

        assertThat(inc.get()).isEqualTo(2L);
        assertThat(get.get()).isEqualTo(2L);
        assertThat(get.get()).isEqualTo(2L);
    }

    @Test
    void increment() {
        final var counter = new CounterBean();

        verifyIncrement(counter::getMqttDelivered, counter::incrementMqttDelivered);
        verifyIncrement(counter::getMqttArrived, counter::incrementMqttArrived);
        verifyIncrement(counter::getMqttConnects, counter::incrementMqttConnects);
        verifyIncrement(counter::getMqttDisconnects, counter::incrementMqttDisconnects);
        verifyIncrement(counter::getMqttAuthPackages, counter::incrementMqttAuthPackages);
        verifyIncrement(counter::getMqttErrors, counter::incrementMqttErrors);
    }

    private void verifyAdd(
        final Supplier<Long> get,
        final Function<Long, Long> add
    ) {
        assertThat(get.get()).isZero();
        assertThat(get.get()).isZero();

        assertThat(add.apply(1L)).isOne();
        assertThat(get.get()).isOne();
        assertThat(get.get()).isOne();

        assertThat(add.apply(1311L)).isEqualTo(1312L);
        assertThat(get.get()).isEqualTo(1312L);
        assertThat(get.get()).isEqualTo(1312L);
    }

    @Test
    void add() {
        final var counter = new CounterBean();

        verifyAdd(counter::getPurgeOutdatedSuccess, counter::addPurgeOutdatedSuccess);
        verifyAdd(counter::getPurgeOutdatedFailed, counter::addPurgeOutdatedFailed);
    }
}
