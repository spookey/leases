package su.toor.leases.components.spring;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.Set;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.leases.services.query.AlertEntityService;

@ExtendWith(SpringExtension.class)
class MetricsBeanTest {

    @MockBean
    private CounterBean counterBean;
    @MockBean
    private AlertEntityService alertEntityService;

    private MetricsBean create(final MeterRegistry registry) {
        return new MetricsBean(registry, counterBean, alertEntityService);
    }

    void check(
        final MeterRegistry registry,
        final String name,
        final String category,
        final String description,
        final Set<String> types
    ) {
        assertThat(registry.getMeters()).hasSize(types.size());
        registry.forEachMeter(meter -> {
            assertThat(meter).isInstanceOf(Gauge.class);

            final var id = meter.getId();
            assertThat(id.getName()).isEqualTo(name);
            assertThat(id.getDescription()).isEqualTo(description);

            final var categoryTag = id.getTag(MetricsBean.TAG_CATEGORY);
            assertThat(categoryTag).isEqualTo(category);
            final var typeTag = id.getTag(MetricsBean.TAG_TYPE);
            assertThat(typeTag).isNotNull().isIn(types);
        });
    }

    @Test
    void mqttCounters() {
        final var registry = new SimpleMeterRegistry();
        final var metrics = create(registry);

        assertThat(registry.getMeters()).isEmpty();
        assertThatNoException()
            .isThrownBy(metrics::mqttCounters);

        check(
            registry,
            MetricsBean.NAME_MQTT,
            MetricsBean.CATEGORY_MQTT,
            MetricsBean.DESCRIPTION_MQTT,
            Set.of("delivered", "arrived", "connect", "disconnect", "auth", "error")
        );
    }

    @Test
    void purgeCounters() {
        final var registry = new SimpleMeterRegistry();
        final var metrics = create(registry);

        assertThat(registry.getMeters()).isEmpty();
        assertThatNoException()
            .isThrownBy(metrics::purgeCounters);

        check(
            registry,
            MetricsBean.NAME_PURGE,
            MetricsBean.CATEGORY_PURGE,
            MetricsBean.DESCRIPTION_PURGE,
            Set.of("outdated", "outdated_failed")
        );
    }

    @Test
    void databaseCounts() {
        final var registry = new SimpleMeterRegistry();
        final var metrics = create(registry);

        assertThat(registry.getMeters()).isEmpty();
        assertThatNoException()
            .isThrownBy(metrics::databaseCounts);

        check(
            registry,
            MetricsBean.NAME_DB,
            MetricsBean.CATEGORY_DB,
            MetricsBean.DESCRIPTION_DB,
            Set.of(
                "stamp", "ether", "bound", "track", "guide",
                "alert_ether", "alert_bound", "alert_guide"
            )
        );
    }

    @Test
    void registerMetrics() {
        final var metrics = mock(MetricsBean.class);
        doCallRealMethod().when(metrics).registerMetrics();

        assertThatNoException()
            .isThrownBy(metrics::registerMetrics);

        verify(metrics, times(1))
            .registerMetrics();
        verify(metrics, times(1))
            .mqttCounters();
        verify(metrics, times(1))
            .purgeCounters();
        verify(metrics, times(1))
            .databaseCounts();
        verifyNoMoreInteractions(metrics);
    }
}
