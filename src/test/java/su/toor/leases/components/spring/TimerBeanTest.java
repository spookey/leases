package su.toor.leases.components.spring;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.leases.components.Accessor;
import su.toor.leases.components.Importer;
import su.toor.leases.components.Scrubber;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.database.StampEntity;

@ExtendWith(SpringExtension.class)
class TimerBeanTest {

    @MockBean
    private Accessor accessor;
    @MockBean
    private Importer importer;
    @MockBean
    private Scrubber scrubber;

    private static final int IMPORT_INTERVAL = 120;

    private TimerBean create(
        final BasicConfig basicConfig
    ) {
        return new TimerBean(basicConfig, accessor, importer, scrubber);
    }

    private TimerBean create(final int importInterval) {
        return create(
            new BasicConfig().setImportInterval(importInterval)
        );
    }
    private TimerBean create() {
        return create(IMPORT_INTERVAL);
    }

    @Test
    void constants() {
        assertThat(TimerBean.INTERVAL_RUN_INITIAL)
            .isPositive();
        assertThat(TimerBean.INTERVAL_RUN_REGULAR)
            .isPositive()
            .isLessThanOrEqualTo(TimerBean.INTERVAL_RUN_INITIAL);
    }

    @Test
    void shouldImportDisabled() {
        final var timer = create(-1);

        assertThat(timer.lastImport.get()).isZero();
        assertThat(timer.shouldImport()).isFalse();
        assertThat(timer.lastImport.get()).isZero();
    }

    @Test
    void shouldImport() {
        final var timer = create();

        assertThat(timer.lastImport.get()).isZero();
        assertThat(timer.shouldImport()).isTrue();
        final var lastImport = timer.lastImport.get();
        assertThat(lastImport).isNotZero().isPositive();

        assertThat(timer.shouldImport()).isFalse();
        assertThat(timer.lastImport.get()).isEqualTo(lastImport);

        assertThat(timer.shouldImport()).isFalse();
        assertThat(timer.lastImport.get()).isEqualTo(lastImport);
    }

    @Test
    void importLeaseFilesDisabled() {
        final var timer = create(-1);
        timer.importLeaseFiles();
        timer.importLeaseFiles();
        timer.importLeaseFiles();

        verifyNoInteractions(accessor);
        verifyNoInteractions(importer);
    }

    @Test
    void importLeaseFilesNoContent() {
        when(accessor.loadLeaseFiles())
            .thenReturn(List.of());

        final var timer = create();
        timer.importLeaseFiles();
        timer.importLeaseFiles();
        timer.importLeaseFiles();

        verify(accessor, times(1))
            .loadLeaseFiles();
        verifyNoMoreInteractions(accessor);

        verify(importer, times(1))
            .run(List.of());
        verifyNoMoreInteractions(importer);
    }

    @Test
    void importLeaseFiles() {
        final var lines = List.of("some", "lines");
        when(accessor.loadLeaseFiles())
            .thenReturn(lines);

        final var stampEntity = StampEntity.with();
        when(importer.run(lines))
            .thenReturn(Optional.of(stampEntity));

        final var timer = create();
        timer.importLeaseFiles();
        timer.importLeaseFiles();
        timer.importLeaseFiles();

        verify(accessor, times(1))
            .loadLeaseFiles();
        verifyNoMoreInteractions(accessor);
        verify(importer, times(1))
            .run(lines);
        verifyNoMoreInteractions(importer);
    }

    @Test
    void purgeInvalid() {
        final var timer = create();
        timer.purgeInvalid();
        timer.purgeInvalid();
        timer.purgeInvalid();

        verify(scrubber, times(3))
            .run();
        verifyNoMoreInteractions(scrubber);
    }

    @Test
    void repeating() {
        final var timer = mock(TimerBean.class);

        doCallRealMethod()
            .when(timer)
            .repeating();

        assertThatNoException()
            .isThrownBy(timer::repeating);

        verify(timer, times(1))
            .repeating();
        verify(timer, times(1))
            .purgeInvalid();
        verify(timer, times(1))
            .importLeaseFiles();
        verifyNoMoreInteractions(timer);
    }
}
