package su.toor.leases.configs;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.nio.file.Path;
import java.util.Set;

import org.junit.jupiter.api.Test;
import su.toor.leases.shared.RequireException;

class BasicConfigTest {

    private static void check(
        final BasicConfig config,
        final Integer keepDays,
        final Integer paginationSize,
        final Boolean hiddenState,
        final Integer importInterval,
        final Integer guidesEvery,
        final Path dumpLocation,
        final Set<Path> leaseFiles
    ) {
        assertThat(config.getKeepDays()).isEqualTo(keepDays);
        assertThat(config.getPaginationSize()).isEqualTo(paginationSize);
        assertThat(config.isHiddenState()).isEqualTo(hiddenState);
        assertThat(config.getImportInterval()).isEqualTo(importInterval);
        assertThat(config.getGuidesEvery()).isEqualTo(guidesEvery);
        assertThat(config.getDumpLocation()).isEqualTo(dumpLocation);
        assertThat(config.getLeaseFiles()).isEqualTo(leaseFiles);
    }

    @Test
    void empty() {
        final var config = new BasicConfig();

        check(
            config,
            BasicConfig.DEFAULT_KEEP_DAYS,
            BasicConfig.DEFAULT_PAGINATION_SIZE,
            BasicConfig.DEFAULT_HIDDEN_STATE,
            BasicConfig.DEFAULT_IMPORT_INTERVAL,
            BasicConfig.DEFAULT_GUIDES_EVERY,
            BasicConfig.DEFAULT_DUMP_LOCATION,
            Set.of()
        );
    }

    @Test
    void some() {
        final var keepDays= 12;
        final var paginationSize = 1312;
        final var hiddenState = true;
        final var importInterval = 1_200;
        final var guidesEvery = 2;
        final var dumpLocation = Path.of("some", "path").toAbsolutePath();
        final var leaseFiles = Set.of(
            Path.of("var", "dhcp.leases").toAbsolutePath(),
            Path.of("var", "dhcp6.leases").toAbsolutePath()
        );

        final var config = new BasicConfig()
            .setKeepDays(keepDays)
            .setPaginationSize(paginationSize)
            .setHiddenState(hiddenState)
            .setImportInterval(importInterval)
            .setGuidesEvery(guidesEvery)
            .setDumpLocation(dumpLocation)
            .setLeaseFiles(leaseFiles);

        check(
            config,
            keepDays,
            paginationSize,
            hiddenState,
            importInterval,
            guidesEvery,
            dumpLocation,
            leaseFiles
        );
    }

    @Test
    void dumpLocation() {
        final var pwd = Path.of("").toAbsolutePath();

        final var config = new BasicConfig()
            .setDumpLocation(pwd);

        assertThat(config.dumpLocation())
            .isEqualTo(pwd);

        config.setDumpLocation(Path.of("does", "not", "exist"));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(config::dumpLocation);
    }

    @Test
    void leaseFiles() {
        final var nil = Path.of("var", "dhcp", "nil.leases").toAbsolutePath();
        final var one = Path.of("var", "one.leases").toAbsolutePath();
        final var two = Path.of("two.leases").toAbsolutePath();

        final var config = new BasicConfig()
            .setLeaseFiles(Set.of(nil, one, two));

        assertThat(config.leaseFiles())
            .containsExactly(two, nil, one);
    }
}
