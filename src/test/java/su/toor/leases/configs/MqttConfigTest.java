package su.toor.leases.configs;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

class MqttConfigTest {

    private static void checkConn(
        final MqttConfig config,
        final String brokerUri,
        final String clientId,
        final Boolean cleanStart,
        final Boolean autoReconnect
    ) {
        assertThat(config.getBrokerUri()).isEqualTo(brokerUri);
        if (clientId == null) {
            assertThat(config.getClientId())
                .startsWith(MqttConfig.DEFAULT_CLIENT_ID_PREFIX)
                .matches("^.+-[0-9]{4}$");
        } else {
            assertThat(config.getClientId()).isEqualTo(clientId);
        }
        assertThat(config.isCleanStart()).isEqualTo(cleanStart);
        assertThat(config.isAutoReconnect()).isEqualTo(autoReconnect);
    }

    private static void checkAuth(
        final MqttConfig config,
        final String username,
        final String password
    ) {
        assertThat(config.getUsername()).isEqualTo(username);
        if (password == null) {
            assertThat(config.getPassword()).isNull();
            assertThat(config.getPasswordBytes()).isNull();
            assertThat(config.hasCredentials()).isFalse();
        } else {
            assertThat(config.getPassword()).isEqualTo("*".repeat(password.length()));
            assertThat(config.getPasswordBytes()).isEqualTo(password.getBytes(StandardCharsets.UTF_8));
            assertThat(config.hasCredentials()).isEqualTo(username != null);
        }
    }

    @Test
    void empty() {
        final var config = new MqttConfig();

        checkConn(
            config,
            MqttConfig.DEFAULT_BROKER_URI,
            null,
            MqttConfig.DEFAULT_CLEAN_START,
            MqttConfig.DEFAULT_AUTO_RECONNECT
        );
        checkAuth(
            config,
            null,
            null
        );
    }

    @Test
    void some() {
        final var brokerUri = "tcp://mqtt.example.org";
        final var clientId = "awesome-test-client";
        final var cleanStart = true;
        final var autoReconnect = false;
        final var username = "the-user-name";
        final var password = "very-secure-password-1";

        final var config = new MqttConfig()
            .setBrokerUri(brokerUri)
            .setClientId(clientId)
            .setCleanStart(cleanStart)
            .setAutoReconnect(autoReconnect)
            .setUsername(username)
            .setPassword(password);


        checkConn(config, brokerUri, clientId, cleanStart, autoReconnect);
        checkAuth(config, username, password);
    }
}
