package su.toor.leases.configs.sinks;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.data.util.Pair;
import su.toor.leases.shared.Qos;
import su.toor.leases.shared.RequireException;

class AlertConfigTest {

    private static <T extends AbstractAlertSink<T>> void check(
        final AbstractAlertSink<T> config,
        final Boolean enabled,
        final Qos qos,
        final Boolean retained,
        final String fallbackTopic,
        final String fallbackTemplate
    ) {
        assertThat(config.isEnabled()).isEqualTo(enabled);
        assertThat(config.getQos()).isEqualTo(qos);
        assertThat(config.isRetained()).isEqualTo(retained);
        assertThat(config.getFallbackTopic()).isEqualTo(fallbackTopic);
        assertThat(config.getFallbackTemplate()).isEqualTo(fallbackTemplate);
    }

    @Test
    void empty() {
        Map.of(
            new AlertEtherConfig(), Pair.of(AbstractAlertSink.DEFAULT_FALLBACK_TOPIC_ETHER, AbstractAlertSink.DEFAULT_FALLBACK_TEMPLATE_ETHER),
            new AlertBoundConfig(), Pair.of(AbstractAlertSink.DEFAULT_FALLBACK_TOPIC_BOUND, AbstractAlertSink.DEFAULT_FALLBACK_TEMPLATE_BOUND),
            new AlertGuideConfig(), Pair.of(AbstractAlertSink.DEFAULT_FALLBACK_TOPIC_GUIDE, AbstractAlertSink.DEFAULT_FALLBACK_TEMPLATE_GUIDE)
        ).forEach((config, pair) -> check(
            config,
            AbstractSink.DEFAULT_ENABLED,
            AbstractSink.DEFAULT_QOS,
            AbstractSink.DEFAULT_RETAINED,
            pair.getFirst(),
            pair.getSecond()
        ));
    }

    @Test
    void invalid() {
        Stream.of(
            new AlertEtherConfig(),
            new AlertBoundConfig(),
            new AlertGuideConfig()
        ).forEach(config -> {
            assertThatExceptionOfType(RequireException.class)
                .isThrownBy(() -> config.setFallbackTopic("some/+/topic"));

            assertThatExceptionOfType(RequireException.class)
                .isThrownBy(() -> config.setFallbackTemplate("\t"));
        });
    }

    @Test
    void some() {
        final var enabled = false;
        final var qos = Qos.ONE;
        final var retained = true;
        final var fallbackTopic = "fallback/topic";
        final var fallbackTemplate = "Some fallback template";

        Stream.of(
            new AlertEtherConfig(),
            new AlertBoundConfig(),
            new AlertGuideConfig()
        )
            .map(config -> config
                .setEnabled(enabled)
                .setQos(qos)
                .setRetained(retained)
                .setFallbackTopic(fallbackTopic)
                .setFallbackTemplate(fallbackTemplate)
            )
            .forEach(config -> check(
                config,
                enabled,
                qos,
                retained,
                fallbackTopic,
                fallbackTemplate
            ));
    }
}
