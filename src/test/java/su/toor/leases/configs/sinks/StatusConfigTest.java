package su.toor.leases.configs.sinks;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.data.util.Pair;
import su.toor.leases.shared.Qos;
import su.toor.leases.shared.RequireException;

class StatusConfigTest {

    private static <T extends AbstractStatusSink<T>> void check(
        final AbstractStatusSink<T> config,
        final Boolean enabled,
        final Qos qos,
        final Boolean retained,
        final String topic,
        final String template
    ) {
        assertThat(config.isEnabled()).isEqualTo(enabled);
        assertThat(config.getQos()).isEqualTo(qos);
        assertThat(config.isRetained()).isEqualTo(retained);
        assertThat(config.getTopic()).isEqualTo(topic);
        assertThat(config.getTemplate()).isEqualTo(template);
    }

    @Test
    void empty() {
        Map.of(
            new StatusBirthConfig(), Pair.of(AbstractStatusSink.DEFAULT_TOPIC_STATUS, AbstractStatusSink.DEFAULT_TEMPLATE_BIRTH),
            new StatusWillConfig(), Pair.of(AbstractStatusSink.DEFAULT_TOPIC_STATUS, AbstractStatusSink.DEFAULT_TEMPLATE_WILL)
        ).forEach((config, pair) -> check(
            config,
            AbstractStatusSink.DEFAULT_ENABLED,
            AbstractStatusSink.DEFAULT_QOS,
            AbstractStatusSink.DEFAULT_RETAINED,
            pair.getFirst(),
            pair.getSecond()
        ));
    }

    @Test
    void invalid() {
        Stream.of(
            new StatusBirthConfig(),
            new StatusWillConfig()
        ).forEach(config -> {
            assertThatExceptionOfType(RequireException.class)
                .isThrownBy(() -> config.setTopic("global/topic/#"));

            assertThatExceptionOfType(RequireException.class)
                .isThrownBy(() -> config.setTemplate("\n"));
        });
    }

    @Test
    void some() {
        final var enabled = false;
        final var qos = Qos.TWO;
        final var retained = true;
        final var topic = "some/topic";
        final var template = "Some test template";

        Stream.of(
            new StatusBirthConfig(),
            new StatusWillConfig()
        )
            .map(config -> config
                .setEnabled(enabled)
                .setQos(qos)
                .setRetained(retained)
                .setTopic(topic)
                .setTemplate(template)
            )
            .forEach(config -> check(
                config,
                enabled,
                qos,
                retained,
                topic,
                template
            ));
    }
}
