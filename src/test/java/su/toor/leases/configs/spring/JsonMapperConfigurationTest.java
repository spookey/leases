package su.toor.leases.configs.spring;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Test;

class JsonMapperConfigurationTest {

    private final static JsonMapperConfiguration CONFIGURATION = new JsonMapperConfiguration();
    private final static JsonMapper JSON_MAPPER = CONFIGURATION.jsonMapper();

    @Test
    void timeModule() {
        final var module = new JavaTimeModule();

        assertThat(JSON_MAPPER.getRegisteredModuleIds())
            .contains(module.getTypeId());
    }

    @Test
    void configuration() {
        final var config = JSON_MAPPER.getSerializationConfig();
        final var datesAsTimestamps = SerializationFeature.WRITE_DATES_AS_TIMESTAMPS.getMask();
        final var durationsAsTimestamps = SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS.getMask();

        assertThat(config.getTimeZone()).isEqualTo(JsonMapperConfiguration.UTC);
        assertThat(config.hasSerializationFeatures(datesAsTimestamps)).isFalse();
        assertThat(config.hasSerializationFeatures(durationsAsTimestamps)).isFalse();
    }
}
