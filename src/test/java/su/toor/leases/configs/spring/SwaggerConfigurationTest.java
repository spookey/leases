package su.toor.leases.configs.spring;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;


class SwaggerConfigurationTest {

    private static final String APPLICATION_NAME = "some-application";
    private static final SwaggerConfiguration SWAGGER_CONFIGURATION = new SwaggerConfiguration();

    @Test
    void openAPI() {
        final var openAPI = SWAGGER_CONFIGURATION.openAPI(APPLICATION_NAME);
        assertThat(openAPI).isNotNull();

        final var info = openAPI.getInfo();
        assertThat(info.getTitle()).isEqualTo(APPLICATION_NAME);
    }
}
