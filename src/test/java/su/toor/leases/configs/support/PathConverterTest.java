package su.toor.leases.configs.support;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Path;

import org.junit.jupiter.api.Test;

class PathConverterTest {

    private final static PathConverter CONVERTER = new PathConverter();

    @Test
    void success() {
        final var source = "some/where";

        final var result = CONVERTER.convert(source);
        assertThat(result).isEqualByComparingTo(Path.of(source));
    }
}
