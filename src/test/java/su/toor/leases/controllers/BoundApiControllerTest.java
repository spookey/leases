package su.toor.leases.controllers;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.IP_ONE;
import static su.toor.leases.Net.IP_TWO;
import static su.toor.leases.controllers.JsonOffsetDateTimeMatcher.jOdtEqual;
import static su.toor.leases.controllers.JsonUuidMatcher.jUuidEqual;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.services.query.EntityService;

@WebMvcTest(BoundApiController.class)
class BoundApiControllerTest {

    private static final String URL_BASE = "/api/addresses";
    private static final String URL_FIND = URL_BASE + "/find/{ip}";
    private static final String URL_UUID = URL_BASE + "/{uuid}";

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EntityService entityService;

    @Test
    void allBounds() throws Exception {
        final var boundEntityOne = BoundEntity.with(IP_ADDR_ONE);
        final var boundEntityTwo = BoundEntity.with(IP_ADDR_TWO);

        when(entityService.findBounds(anyInt(), anyInt()))
            .thenReturn(new PageImpl<>(List.of(boundEntityOne, boundEntityTwo)));

        mockMvc.perform(get(URL_BASE))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.size", equalTo(2)))
            .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(2))))
            .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(boundEntityOne.getUuid())))
            .andExpect(jsonPath("$.items.[0].address", equalTo(IP_ONE)))
            .andExpect(jsonPath("$.items.[0].created", jOdtEqual(boundEntityOne.getCreated())))
            .andExpect(jsonPath("$.items.[0].updated", jOdtEqual(boundEntityOne.getUpdated())))
            .andExpect(jsonPath("$.items.[1]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[1].uuid", jUuidEqual(boundEntityTwo.getUuid())))
            .andExpect(jsonPath("$.items.[1].address", equalTo(IP_TWO)))
            .andExpect(jsonPath("$.items.[1].created", jOdtEqual(boundEntityTwo.getCreated())))
            .andExpect(jsonPath("$.items.[1].updated", jOdtEqual(boundEntityTwo.getUpdated())))
        ;

        verify(entityService, times(1))
            .findBounds(anyInt(), anyInt());
        verifyNoMoreInteractions(entityService);}

    @Test
    void findSingleBound() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var boundEntity = BoundEntity.with(IP_ADDR_TWO);

        when(entityService.findBound(wrongUuid))
            .thenReturn(Optional.empty());
        when(entityService.findBound(IP_ADDR_ONE))
            .thenReturn(Optional.empty());
        when(entityService.findBound(boundEntity.getUuid()))
            .thenReturn(Optional.of(boundEntity));
        when(entityService.findBound(boundEntity.getIp()))
            .thenReturn(Optional.of(boundEntity));

        for (final var perform : List.of(
            get(URL_UUID, wrongUuid.toString()),
            get(URL_FIND, IP_ONE)
        )) {
            mockMvc.perform(perform)
                .andDo(print())
                .andExpect(status().isNotFound())
            ;
        }

        for (final var perform : List.of(
            get(URL_UUID, boundEntity.getUuid()),
            get(URL_FIND, IP_TWO)
        )) {
            mockMvc.perform(perform)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.uuid", jUuidEqual(boundEntity.getUuid())))
                .andExpect(jsonPath("$.address", equalTo(IP_TWO)))
                .andExpect(jsonPath("$.created", jOdtEqual(boundEntity.getCreated())))
                .andExpect(jsonPath("$.updated", jOdtEqual(boundEntity.getUpdated())))
            ;
        }

        verify(entityService, times(1))
            .findBound(wrongUuid);
        verify(entityService, times(1))
            .findBound(IP_ADDR_ONE);
        verify(entityService, times(1))
            .findBound(boundEntity.getUuid());
        verify(entityService, times(1))
            .findBound(boundEntity.getIp());
        verifyNoMoreInteractions(entityService);
    }
}
