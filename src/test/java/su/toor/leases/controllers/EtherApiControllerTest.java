package su.toor.leases.controllers;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.Net.MAC_ONE;
import static su.toor.leases.Net.MAC_TWO;
import static su.toor.leases.controllers.JsonOffsetDateTimeMatcher.jOdtEqual;
import static su.toor.leases.controllers.JsonUuidMatcher.jUuidEqual;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.services.query.EntityService;

@WebMvcTest(EtherApiController.class)
class EtherApiControllerTest {

    private static final String URL_BASE = "/api/devices";
    private static final String URL_FIND = URL_BASE + "/find/{mac}";
    private static final String URL_UUID = URL_BASE + "/{uuid}";

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EntityService entityService;

    @Test
    void allEthers() throws Exception {
        final var etherEntityOne = EtherEntity.with(MAC_ADDR_ONE, null);
        final var etherEntityTwo = EtherEntity.with(MAC_ADDR_TWO, HOSTNAME);

        when(entityService.findEthers(anyInt(), anyInt()))
            .thenReturn(new PageImpl<>(List.of(etherEntityOne, etherEntityTwo)));

        mockMvc.perform(get(URL_BASE))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.size", equalTo(2)))
            .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(2))))
            .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(etherEntityOne.getUuid())))
            .andExpect(jsonPath("$.items.[0].hostname", equalTo(etherEntityOne.getHostname())))
            .andExpect(jsonPath("$.items.[0].created", jOdtEqual(etherEntityOne.getCreated())))
            .andExpect(jsonPath("$.items.[0].updated", jOdtEqual(etherEntityOne.getUpdated())))
            .andExpect(jsonPath("$.items.[1]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[1].uuid", jUuidEqual(etherEntityTwo.getUuid())))
            .andExpect(jsonPath("$.items.[1].hostname", equalTo(etherEntityTwo.getHostname())))
            .andExpect(jsonPath("$.items.[1].created", jOdtEqual(etherEntityTwo.getCreated())))
            .andExpect(jsonPath("$.items.[1].updated", jOdtEqual(etherEntityTwo.getUpdated())))
        ;

        verify(entityService, times(1))
            .findEthers(anyInt(), anyInt());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void findSingleEther() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var etherEntity = EtherEntity.with(MAC_ADDR_TWO, HOSTNAME);

        when(entityService.findEther(wrongUuid))
            .thenReturn(Optional.empty());
        when(entityService.findEther(MAC_ADDR_ONE))
            .thenReturn(Optional.empty());
        when(entityService.findEther(etherEntity.getUuid()))
            .thenReturn(Optional.of(etherEntity));
        when(entityService.findEther(etherEntity.getMac()))
            .thenReturn(Optional.of(etherEntity));

        for (final var perform : List.of(
            get(URL_UUID, wrongUuid.toString()),
            get(URL_FIND, MAC_ONE)
        )) {
            mockMvc.perform(perform)
                .andDo(print())
                .andExpect(status().isNotFound())
            ;
        }

        for (final var perform : List.of(
            get(URL_UUID, etherEntity.getUuid()),
            get(URL_FIND, MAC_TWO)
        )) {
            mockMvc.perform(perform)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.uuid", jUuidEqual(etherEntity.getUuid())))
                .andExpect(jsonPath("$.hostname", equalTo(etherEntity.getHostname())))
                .andExpect(jsonPath("$.created", jOdtEqual(etherEntity.getCreated())))
                .andExpect(jsonPath("$.updated", jOdtEqual(etherEntity.getUpdated())))
            ;
        }

        verify(entityService, times(1))
            .findEther(wrongUuid);
        verify(entityService, times(1))
            .findEther(MAC_ADDR_ONE);
        verify(entityService, times(1))
            .findEther(etherEntity.getUuid());
        verify(entityService, times(1))
            .findEther(etherEntity.getMac());
        verifyNoMoreInteractions(entityService);


    }
}
