package su.toor.leases.controllers;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static su.toor.leases.Net.MASK_ADDR_ONE;
import static su.toor.leases.Net.MASK_ADDR_TWO;
import static su.toor.leases.Net.MASK_ONE;
import static su.toor.leases.Net.MASK_TWO;
import static su.toor.leases.controllers.JsonOffsetDateTimeMatcher.jOdtEqual;
import static su.toor.leases.controllers.JsonUuidMatcher.jUuidEqual;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import com.fasterxml.jackson.databind.json.JsonMapper;
import inet.ipaddr.IPAddress;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.leases.configs.spring.JsonMapperConfiguration;
import su.toor.leases.controllers.dtos.GuideRequestDto;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.services.query.EntityService;

@WebMvcTest(GuideApiController.class)
@Import(JsonMapperConfiguration.class)
class GuideApiControllerTest {

    private static final String URL_BASE = "/api/networks";
    private static final String URL_UUID = URL_BASE + "/{uuid}";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JsonMapper jsonMapper;
    @MockBean
    private EntityService entityService;


    @Test
    void allGuides() throws Exception {
        final var guideEntityOne = GuideEntity.with(MASK_ADDR_ONE, null, "one", null);
        final var guideEntityTwo = GuideEntity.with(MASK_ADDR_TWO, null, "two", null);

        when(entityService.findGuides(anyInt(), anyInt()))
            .thenReturn(new PageImpl<>(List.of(guideEntityOne, guideEntityTwo)));

        mockMvc.perform(get(URL_BASE))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.size", equalTo(2)))
            .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(2))))
            .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(guideEntityOne.getUuid())))
            .andExpect(jsonPath("$.items.[0].mask", equalTo(MASK_ONE)))
            .andExpect(jsonPath("$.items.[0].weight", equalTo(guideEntityOne.getWeight())))
            .andExpect(jsonPath("$.items.[0].title", equalTo(guideEntityOne.getTitle())))
            .andExpect(jsonPath("$.items.[0].description", equalTo(guideEntityOne.getDescription())))
            .andExpect(jsonPath("$.items.[0].created", jOdtEqual(guideEntityOne.getCreated())))
            .andExpect(jsonPath("$.items.[0].updated", jOdtEqual(guideEntityOne.getUpdated())))
            .andExpect(jsonPath("$.items.[1]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[1].uuid", jUuidEqual(guideEntityTwo.getUuid())))
            .andExpect(jsonPath("$.items.[1].mask", equalTo(MASK_TWO)))
            .andExpect(jsonPath("$.items.[1].weight", equalTo(guideEntityTwo.getWeight())))
            .andExpect(jsonPath("$.items.[1].title", equalTo(guideEntityTwo.getTitle())))
            .andExpect(jsonPath("$.items.[1].description", equalTo(guideEntityTwo.getDescription())))
            .andExpect(jsonPath("$.items.[1].created", jOdtEqual(guideEntityTwo.getCreated())))
            .andExpect(jsonPath("$.items.[1].updated", jOdtEqual(guideEntityTwo.getUpdated())))
        ;

        verify(entityService, times(1))
            .findGuides(anyInt(), anyInt());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void singleGuide() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var guideEntity = GuideEntity.with(MASK_ADDR_ONE, null, "some", null);

        when(entityService.findGuide(wrongUuid))
            .thenReturn(Optional.empty());
        when(entityService.findGuide(guideEntity.getUuid()))
            .thenReturn(Optional.of(guideEntity));

        mockMvc.perform(get(URL_UUID, wrongUuid.toString()))
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(get(URL_UUID, guideEntity.getUuid().toString()))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.uuid", jUuidEqual(guideEntity.getUuid())))
            .andExpect(jsonPath("$.mask", equalTo(MASK_ONE)))
            .andExpect(jsonPath("$.weight", equalTo(guideEntity.getWeight())))
            .andExpect(jsonPath("$.title", equalTo(guideEntity.getTitle())))
            .andExpect(jsonPath("$.description", equalTo(guideEntity.getDescription())))
            .andExpect(jsonPath("$.created", jOdtEqual(guideEntity.getCreated())))
            .andExpect(jsonPath("$.updated", jOdtEqual(guideEntity.getUpdated())))
        ;

        verify(entityService, times(1))
            .findGuide(wrongUuid);
        verify(entityService, times(1))
            .findGuide(guideEntity.getUuid());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void createGuide() throws Exception {
        final var uuid = UUID.randomUUID();
        final var weight = 42;
        final var title = "Title";
        final var description = "Description";

        final var guideEntity = mock(GuideEntity.class);
        when(guideEntity.getUuid()).thenReturn(uuid);
        when(guideEntity.getMask()).thenReturn(MASK_ADDR_TWO);
        when(guideEntity.getWeight()).thenReturn(weight);
        when(guideEntity.getTitle()).thenReturn(title);
        when(guideEntity.getDescription()).thenReturn(description);

        when(entityService.createGuide(any(IPAddress.class), eq(weight), eq(title), eq(description)))
            .thenReturn(guideEntity);

        final var requestDto = new GuideRequestDto(MASK_ONE, weight, title, description);
        final var requestPayload = jsonMapper.writeValueAsString(requestDto);

        final var redirection = UriComponentsBuilder
            .fromPath(URL_UUID)
            .build(Map.of("uuid", guideEntity.getUuid()));

        mockMvc.perform(
            post(URL_BASE)
                .contentType(APPLICATION_JSON)
                .content(requestPayload)
        )
            .andDo(print())
            .andExpect(status().isCreated())
            .andExpect(redirectedUrl(redirection.toString()))
        ;

        verify(entityService, times(1))
            .createGuide(any(IPAddress.class), eq(weight), eq(title), eq(description));
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void modifyGuide() throws Exception {
        final var uuid = UUID.randomUUID();
        final var wrongUuid = UUID.randomUUID();
        final var weight = 23;
        final var title = "Title";
        final var description = "Description";
        final var updated = OffsetDateTime.now();
        final var created = updated.minusHours(23);

        final var guideEntity = mock(GuideEntity.class);
        when(guideEntity.getUuid()).thenReturn(uuid);
        when(guideEntity.getMask()).thenReturn(MASK_ADDR_TWO);
        when(guideEntity.getWeight()).thenReturn(weight);
        when(guideEntity.getTitle()).thenReturn(title);
        when(guideEntity.getDescription()).thenReturn(description);
        when(guideEntity.getCreated()).thenReturn(created);
        when(guideEntity.getUpdated()).thenReturn(updated);

        when(entityService.modifyGuide(eq(wrongUuid), any(IPAddress.class), eq(weight), eq(title), eq(description)))
            .thenReturn(Optional.empty());
        when(entityService.modifyGuide(eq(uuid), any(IPAddress.class), eq(weight), eq(title), eq(description)))
            .thenReturn(Optional.of(guideEntity));

        final var requestDto = new GuideRequestDto(MASK_ONE, weight, title, description);
        final var requestPayload = jsonMapper.writeValueAsString(requestDto);

        mockMvc.perform(
            put(URL_UUID, wrongUuid.toString())
                .contentType(APPLICATION_JSON)
                .content(requestPayload)
        )
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(
            put(URL_UUID, uuid.toString())
                .contentType(APPLICATION_JSON)
                .content(requestPayload)
        )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.uuid", jUuidEqual(guideEntity.getUuid())))
            .andExpect(jsonPath("$.mask", equalTo(MASK_TWO)))
            .andExpect(jsonPath("$.weight", equalTo(guideEntity.getWeight())))
            .andExpect(jsonPath("$.title", equalTo(guideEntity.getTitle())))
            .andExpect(jsonPath("$.description", equalTo(guideEntity.getDescription())))
            .andExpect(jsonPath("$.created", jOdtEqual(created)))
            .andExpect(jsonPath("$.updated", jOdtEqual(updated)))
        ;

        verify(entityService, times(1))
            .modifyGuide(eq(wrongUuid), any(IPAddress.class), eq(weight), eq(title), eq(description));
        verify(entityService, times(1))
            .modifyGuide(eq(uuid), any(IPAddress.class), eq(weight), eq(title), eq(description));
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void deleteGuide() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var correctUuid = UUID.randomUUID();

        when(entityService.deleteGuide(wrongUuid))
            .thenReturn(false);
        when(entityService.deleteGuide(correctUuid))
            .thenReturn(true);

        mockMvc.perform(delete(URL_UUID, wrongUuid.toString()))
            .andDo(print())
            .andExpect(status().isNotFound())
        ;
        mockMvc.perform(delete(URL_UUID, correctUuid.toString()))
            .andDo(print())
            .andExpect(status().isOk())
        ;

        verify(entityService, times(1))
            .deleteGuide(wrongUuid);
        verify(entityService, times(1))
            .deleteGuide(correctUuid);
        verifyNoMoreInteractions(entityService);
    }
}
