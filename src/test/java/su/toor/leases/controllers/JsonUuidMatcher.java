package su.toor.leases.controllers;

import java.util.UUID;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class JsonUuidMatcher extends BaseMatcher<UUID> {

    private final UUID expect;

    JsonUuidMatcher(final UUID expect) {
        this.expect = expect;
    }

    public static JsonUuidMatcher jUuidEqual(final UUID expect) {
        return new JsonUuidMatcher(expect);
    }

    @Override
    public boolean matches(final Object item) {
        final var value = UUID.fromString(item.toString());

        return value.equals(expect);
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText(String.format("<%s>", expect));
    }
}
