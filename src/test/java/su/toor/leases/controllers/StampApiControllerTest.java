package su.toor.leases.controllers;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static su.toor.leases.controllers.JsonOffsetDateTimeMatcher.jOdtEqual;
import static su.toor.leases.controllers.JsonUuidMatcher.jUuidEqual;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import su.toor.leases.database.StampEntity;
import su.toor.leases.services.query.EntityService;

@WebMvcTest(StampApiController.class)
class StampApiControllerTest {

    private static final String URL_BASE = "/api/stamps";
    private static final String URL_LATE = URL_BASE + "/latest";
    private static final String URL_UUID = URL_BASE + "/{uuid}";

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EntityService entityService;

    @Test
    void allStamps() throws Exception {
        final var stampEntityNil = StampEntity.with();
        final var stampEntityOne = StampEntity.with();
        final var stampEntityTwo = StampEntity.with();

        when(entityService.findStamps(anyInt(), anyInt()))
            .thenReturn(new PageImpl<>(List.of(stampEntityNil, stampEntityOne, stampEntityTwo)));

        mockMvc.perform(get(URL_BASE))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.size", equalTo(3)))
            .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(3))))
            .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(stampEntityNil.getUuid())))
            .andExpect(jsonPath("$.items.[0].value", jOdtEqual(stampEntityNil.getValue())))
            .andExpect(jsonPath("$.items.[1]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[1].uuid", jUuidEqual(stampEntityOne.getUuid())))
            .andExpect(jsonPath("$.items.[1].value", jOdtEqual(stampEntityOne.getValue())))
            .andExpect(jsonPath("$.items.[2]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[2].uuid", jUuidEqual(stampEntityTwo.getUuid())))
            .andExpect(jsonPath("$.items.[2].value", jOdtEqual(stampEntityTwo.getValue())))
        ;

        verify(entityService, times(1))
            .findStamps(anyInt(), anyInt());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void singleStamp() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var stampEntity = StampEntity.with();

        when(entityService.findStamp(wrongUuid))
            .thenReturn(Optional.empty());
        when(entityService.findStamp(stampEntity.getUuid()))
            .thenReturn(Optional.of(stampEntity));

        mockMvc.perform(get(URL_UUID, wrongUuid.toString()))
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(get(URL_UUID, stampEntity.getUuid().toString()))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.uuid", jUuidEqual(stampEntity.getUuid())))
            .andExpect(jsonPath("$.value", jOdtEqual(stampEntity.getValue())))
        ;

        verify(entityService, times(1))
            .findStamp(wrongUuid);
        verify(entityService, times(1))
            .findStamp(stampEntity.getUuid());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void latestStamp() throws Exception {
        final var stampEntity = StampEntity.with();

        when(entityService.latestStamp())
            .thenReturn(Optional.empty())
            .thenReturn(Optional.of(stampEntity));

        mockMvc.perform(get(URL_LATE))
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(get(URL_LATE))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.uuid", jUuidEqual(stampEntity.getUuid())))
            .andExpect(jsonPath("$.value", jOdtEqual(stampEntity.getValue())))
        ;

        verify(entityService, times(2))
            .latestStamp();
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void deleteStamp() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var correctUuid = UUID.randomUUID();

        when(entityService.deleteStamp(wrongUuid))
            .thenReturn(false);
        when(entityService.deleteStamp(correctUuid))
            .thenReturn(true);

        mockMvc.perform(delete(URL_UUID, wrongUuid.toString()))
            .andDo(print())
            .andExpect(status().isNotFound())
        ;
        mockMvc.perform(delete(URL_UUID, correctUuid.toString()))
            .andDo(print())
            .andExpect(status().isOk())
        ;

        verify(entityService, times(1))
            .deleteStamp(wrongUuid);
        verify(entityService, times(1))
            .deleteStamp(correctUuid);
        verifyNoMoreInteractions(entityService);
    }
}
