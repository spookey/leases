package su.toor.leases.controllers;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.controllers.JsonUuidMatcher.jUuidEqual;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.services.query.EntityService;
import su.toor.leases.shared.State;

@WebMvcTest(TrackApiController.class)
class TrackApiControllerTest {

    private static final String URL_BASE = "/api/tracks";
    private static final String URL_LATE = URL_BASE + "/latest";
    private static final String URL_UUID = URL_BASE + "/{uuid}";

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EntityService entityService;

    @Test
    void allTracks() throws Exception {
        final var stampEntity = StampEntity.with();
        final var etherEntity = EtherEntity.with(MAC_ADDR_TWO, HOSTNAME);
        final var boundEntity = BoundEntity.with(IP_ADDR_TWO);
        final var trackEntityNil = TrackEntity.with(
            stampEntity, etherEntity, boundEntity, State.FREE
        );
        final var trackEntityOne = TrackEntity.with(
            stampEntity, etherEntity, boundEntity, State.ACTIVE
        );

        when(entityService.findTracks(anyInt(), anyInt()))
            .thenReturn(new PageImpl<>(List.of(trackEntityNil, trackEntityOne)));

        mockMvc.perform(get(URL_BASE))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.size", equalTo(2)))
            .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(2))))
            .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(trackEntityNil.getUuid())))
            .andExpect(jsonPath("$.items.[0].stamp", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].stamp.uuid", jUuidEqual(stampEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].ether", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].ether.uuid", jUuidEqual(etherEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].bound", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].bound.uuid", jUuidEqual(boundEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].state", equalTo(trackEntityNil.getState().name())))
            .andExpect(jsonPath("$.items.[1]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[1].uuid", jUuidEqual(trackEntityOne.getUuid())))
            .andExpect(jsonPath("$.items.[1].stamp", isA(Map.class)))
            .andExpect(jsonPath("$.items.[1].stamp.uuid", jUuidEqual(stampEntity.getUuid())))
            .andExpect(jsonPath("$.items.[1].ether", isA(Map.class)))
            .andExpect(jsonPath("$.items.[1].ether.uuid", jUuidEqual(etherEntity.getUuid())))
            .andExpect(jsonPath("$.items.[1].bound", isA(Map.class)))
            .andExpect(jsonPath("$.items.[1].bound.uuid", jUuidEqual(boundEntity.getUuid())))
            .andExpect(jsonPath("$.items.[1].state", equalTo(trackEntityOne.getState().name())))
        ;

        verify(entityService, times(1))
            .findTracks(anyInt(), anyInt());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void singleTracks() throws Exception {
        final var wrongUuid = UUID.randomUUID();

        final var stampEntity = StampEntity.with();
        final var etherEntity = EtherEntity.with(MAC_ADDR_ONE, HOSTNAME);
        final var boundEntity = BoundEntity.with(IP_ADDR_ONE);
        final var trackEntity = TrackEntity.with(
            stampEntity, etherEntity, boundEntity, State.FREE
        );

        when(entityService.findStamp(wrongUuid))
            .thenReturn(Optional.empty());
        when(entityService.findTracks(eq(wrongUuid), anyInt(), anyInt()))
            .thenThrow(new RuntimeException("should not be called"));

        when(entityService.findStamp(stampEntity.getUuid()))
            .thenReturn(Optional.of(stampEntity));
        when(entityService.findTracks(eq(stampEntity.getUuid()), anyInt(), anyInt()))
            .thenReturn(new PageImpl<>(List.of(trackEntity)));

        mockMvc.perform(get(URL_UUID, wrongUuid.toString()))
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(get(URL_UUID, stampEntity.getUuid().toString()))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.size", equalTo(1)))
            .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(1))))
            .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(trackEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].stamp", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].stamp.uuid", jUuidEqual(stampEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].ether", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].ether.uuid", jUuidEqual(etherEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].bound", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].bound.uuid", jUuidEqual(boundEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].state", equalTo(trackEntity.getState().name())))
        ;

        verify(entityService, times(1))
            .findStamp(wrongUuid);
        verify(entityService, times(1))
            .findStamp(stampEntity.getUuid());
        verify(entityService, times(1))
            .findTracks(eq(stampEntity.getUuid()), anyInt(), anyInt());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void latestTracks() throws Exception {
        final var stampEntity = StampEntity.with();
        final var etherEntity = EtherEntity.with(MAC_ADDR_TWO, HOSTNAME);
        final var boundEntity = BoundEntity.with(IP_ADDR_TWO);
        final var trackEntity = TrackEntity.with(
            stampEntity, etherEntity, boundEntity, State.ACTIVE
        );

        when(entityService.latestTracks(anyInt(), anyInt()))
            .thenReturn(new PageImpl<>(List.of()))
            .thenReturn(new PageImpl<>(List.of(trackEntity)));

        mockMvc.perform(get(URL_LATE))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.size", equalTo(0)))
            .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(0))))
        ;

        mockMvc.perform(get(URL_LATE))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.size", equalTo(1)))
            .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(1))))
            .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(trackEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].stamp", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].stamp.uuid", jUuidEqual(stampEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].ether", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].ether.uuid", jUuidEqual(etherEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].bound", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].bound.uuid", jUuidEqual(boundEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].state", equalTo(trackEntity.getState().name())))
        ;

        verify(entityService, times(2))
            .latestTracks(anyInt(), anyInt());
        verifyNoMoreInteractions(entityService);
    }
}
