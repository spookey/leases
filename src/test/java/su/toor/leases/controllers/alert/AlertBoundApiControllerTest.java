package su.toor.leases.controllers.alert;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.controllers.JsonOffsetDateTimeMatcher.jOdtEqual;
import static su.toor.leases.controllers.JsonUuidMatcher.jUuidEqual;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.leases.configs.spring.JsonMapperConfiguration;
import su.toor.leases.controllers.dtos.alert.AlertBoundRequestDto;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.services.query.AlertEntityService;

@WebMvcTest(AlertBoundApiController.class)
@Import(JsonMapperConfiguration.class)
class AlertBoundApiControllerTest {

    private static final String URL_BASE = "/api/alert/addresses";
    private static final String URL_UUID = URL_BASE + "/{uuid}";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JsonMapper jsonMapper;
    @MockBean
    private AlertEntityService alertEntityService;

    @Test
    void allAlertBounds() throws Exception {
        final var boundEntity = BoundEntity.with(IP_ADDR_ONE);
        final var alertBoundEntityOne = AlertBoundEntity.with(boundEntity, true, "some/topic", null);
        final var alertBoundEntityTwo = AlertBoundEntity.with(boundEntity, false, null, "template");

        when(alertEntityService.findAlertBounds(anyInt(), anyInt()))
            .thenReturn(new PageImpl<>(List.of(alertBoundEntityOne, alertBoundEntityTwo)));

        mockMvc.perform(get(URL_BASE))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.size", equalTo(2)))
            .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(2))))
            .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(alertBoundEntityOne.getUuid())))
            .andExpect(jsonPath("$.items.[0].boundUuid", jUuidEqual(boundEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].enabled", equalTo(alertBoundEntityOne.isEnabled())))
            .andExpect(jsonPath("$.items.[0].topic", equalTo(alertBoundEntityOne.getTopic())))
            .andExpect(jsonPath("$.items.[0].template", equalTo(alertBoundEntityOne.getTemplate())))
            .andExpect(jsonPath("$.items.[0].created", jOdtEqual(alertBoundEntityOne.getCreated())))
            .andExpect(jsonPath("$.items.[0].updated", jOdtEqual(alertBoundEntityOne.getUpdated())))
            .andExpect(jsonPath("$.items.[1]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[1].uuid", jUuidEqual(alertBoundEntityTwo.getUuid())))
            .andExpect(jsonPath("$.items.[1].boundUuid", jUuidEqual(boundEntity.getUuid())))
            .andExpect(jsonPath("$.items.[1].enabled", equalTo(alertBoundEntityTwo.isEnabled())))
            .andExpect(jsonPath("$.items.[1].topic", equalTo(alertBoundEntityTwo.getTopic())))
            .andExpect(jsonPath("$.items.[1].template", equalTo(alertBoundEntityTwo.getTemplate())))
            .andExpect(jsonPath("$.items.[1].created", jOdtEqual(alertBoundEntityTwo.getCreated())))
            .andExpect(jsonPath("$.items.[1].updated", jOdtEqual(alertBoundEntityTwo.getUpdated())))
        ;

        verify(alertEntityService, times(1))
            .findAlertBounds(anyInt(), anyInt());
        verifyNoMoreInteractions(alertEntityService);
    }

    @Test
    void singleAlertBound() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var boundEntity = BoundEntity.with(IP_ADDR_TWO);
        final var alertBoundEntity = AlertBoundEntity.with(boundEntity, true, "some/topic", "template");

        when(alertEntityService.findAlertBound(wrongUuid))
            .thenReturn(Optional.empty());
        when(alertEntityService.findAlertBound(alertBoundEntity.getUuid()))
            .thenReturn(Optional.of(alertBoundEntity));

        mockMvc.perform(get(URL_UUID, wrongUuid.toString()))
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(get(URL_UUID, alertBoundEntity.getUuid().toString()))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.uuid", jUuidEqual(alertBoundEntity.getUuid())))
            .andExpect(jsonPath("$.boundUuid", jUuidEqual(boundEntity.getUuid())))
            .andExpect(jsonPath("$.enabled", equalTo(alertBoundEntity.isEnabled())))
            .andExpect(jsonPath("$.topic", equalTo(alertBoundEntity.getTopic())))
            .andExpect(jsonPath("$.template", equalTo(alertBoundEntity.getTemplate())))
            .andExpect(jsonPath("$.created", jOdtEqual(alertBoundEntity.getCreated())))
            .andExpect(jsonPath("$.updated", jOdtEqual(alertBoundEntity.getUpdated())))
        ;

        verify(alertEntityService, times(1))
            .findAlertBound(wrongUuid);
        verify(alertEntityService, times(1))
            .findAlertBound(alertBoundEntity.getUuid());
        verifyNoMoreInteractions(alertEntityService);
    }

    @Test
    void createAlertBound() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var boundUuid = UUID.randomUUID();
        final var uuid = UUID.randomUUID();
        final var enabled = true;
        final var topic = "some/topic";
        final var template = "template";

        final var alertBoundEntity = mock(AlertBoundEntity.class);
        when(alertBoundEntity.getUuid()).thenReturn(uuid);
        when(alertBoundEntity.isEnabled()).thenReturn(enabled);
        when(alertBoundEntity.getTopic()).thenReturn(topic);
        when(alertBoundEntity.getTemplate()).thenReturn(template);

        when(alertEntityService.createAlertBound(wrongUuid, enabled, topic, template))
            .thenReturn(Optional.empty());
        when(alertEntityService.createAlertBound(boundUuid, enabled, topic, template))
            .thenReturn(Optional.of(alertBoundEntity));

        final var wrongRequestDto = new AlertBoundRequestDto(wrongUuid, enabled, topic, template);
        final var wrongRequestPayload = jsonMapper.writeValueAsString(wrongRequestDto);

        final var requestDto = new AlertBoundRequestDto(boundUuid, enabled, topic, template);
        final var requestPayload = jsonMapper.writeValueAsString(requestDto);

        final var redirection = UriComponentsBuilder
            .fromPath(URL_UUID)
            .build(Map.of("uuid", alertBoundEntity.getUuid()));

        mockMvc.perform(
                post(URL_BASE)
                    .contentType(APPLICATION_JSON)
                    .content(wrongRequestPayload)
            )
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(
                post(URL_BASE)
                    .contentType(APPLICATION_JSON)
                    .content(requestPayload)
            )
            .andDo(print())
            .andExpect(status().isCreated())
            .andExpect(redirectedUrl(redirection.toString()))
        ;

        verify(alertEntityService, times(1))
            .createAlertBound(wrongUuid, enabled, topic, template);
        verify(alertEntityService, times(1))
            .createAlertBound(boundUuid, enabled, topic, template);
        verifyNoMoreInteractions(alertEntityService);
    }

    @Test
    void modifyAlertBound() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var boundUuid = UUID.randomUUID();
        final var uuid = UUID.randomUUID();
        final var enabled = true;
        final var topic = "some/topic";
        final var template = "template";
        final var updated = OffsetDateTime.now();
        final var created = updated.minusHours(23);

        final var boundEntity = mock(BoundEntity.class);
        when(boundEntity.getUuid()).thenReturn(boundUuid);

        final var alertBoundEntity = mock(AlertBoundEntity.class);
        when(alertBoundEntity.getBoundEntity()).thenReturn(boundEntity);
        when(alertBoundEntity.getUuid()).thenReturn(uuid);
        when(alertBoundEntity.isEnabled()).thenReturn(enabled);
        when(alertBoundEntity.getTopic()).thenReturn(topic);
        when(alertBoundEntity.getTemplate()).thenReturn(template);
        when(alertBoundEntity.getCreated()).thenReturn(created);
        when(alertBoundEntity.getUpdated()).thenReturn(updated);

        when(alertEntityService.modifyAlertBound(wrongUuid, enabled, topic, template))
            .thenReturn(Optional.empty());
        when(alertEntityService.modifyAlertBound(uuid, enabled, topic, template))
            .thenReturn(Optional.of(alertBoundEntity));

        final var requestDto = new AlertBoundRequestDto(UUID.randomUUID(), enabled, topic, template);
        final var requestPayload = jsonMapper.writeValueAsString(requestDto);

        mockMvc.perform(
                put(URL_UUID, wrongUuid.toString())
                    .contentType(APPLICATION_JSON)
                    .content(requestPayload)
            )
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(
                put(URL_UUID, uuid.toString())
                    .contentType(APPLICATION_JSON)
                    .content(requestPayload)
            )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.uuid", jUuidEqual(alertBoundEntity.getUuid())))
            .andExpect(jsonPath("$.boundUuid", jUuidEqual(boundEntity.getUuid())))
            .andExpect(jsonPath("$.enabled", equalTo(alertBoundEntity.isEnabled())))
            .andExpect(jsonPath("$.topic", equalTo(alertBoundEntity.getTopic())))
            .andExpect(jsonPath("$.template", equalTo(alertBoundEntity.getTemplate())))
            .andExpect(jsonPath("$.created", jOdtEqual(alertBoundEntity.getCreated())))
            .andExpect(jsonPath("$.updated", jOdtEqual(alertBoundEntity.getUpdated())))
        ;

        verify(alertEntityService, times(1))
            .modifyAlertBound(wrongUuid, enabled, topic, template);
        verify(alertEntityService, times(1))
            .modifyAlertBound(uuid, enabled, topic, template);
        verifyNoMoreInteractions(alertEntityService);
    }

    @Test
    void deleteAlertBound() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var correctUuid = UUID.randomUUID();

        when(alertEntityService.deleteAlertBound(wrongUuid))
            .thenReturn(false);
        when(alertEntityService.deleteAlertBound(correctUuid))
            .thenReturn(true);

        mockMvc.perform(delete(URL_UUID, wrongUuid.toString()))
            .andDo(print())
            .andExpect(status().isNotFound())
        ;
        mockMvc.perform(delete(URL_UUID, correctUuid.toString()))
            .andDo(print())
            .andExpect(status().isOk())
        ;

        verify(alertEntityService, times(1))
            .deleteAlertBound(wrongUuid);
        verify(alertEntityService, times(1))
            .deleteAlertBound(correctUuid);
        verifyNoMoreInteractions(alertEntityService);
    }
}
