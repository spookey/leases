package su.toor.leases.controllers.alert;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.controllers.JsonOffsetDateTimeMatcher.jOdtEqual;
import static su.toor.leases.controllers.JsonUuidMatcher.jUuidEqual;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.leases.configs.spring.JsonMapperConfiguration;
import su.toor.leases.controllers.dtos.alert.AlertEtherRequestDto;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.services.query.AlertEntityService;

@WebMvcTest(AlertEtherApiController.class)
@Import(JsonMapperConfiguration.class)
class AlertEtherApiControllerTest {

    private static final String URL_BASE = "/api/alert/devices";
    private static final String URL_UUID = URL_BASE + "/{uuid}";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JsonMapper jsonMapper;
    @MockBean
    private AlertEntityService alertEntityService;

    @Test
    void allAlertEthers() throws Exception {
        final var etherEntity = EtherEntity.with(MAC_ADDR_ONE, null);
        final var alertEtherEntityOne = AlertEtherEntity.with(etherEntity, true, "some/topic", null);
        final var alertEtherEntityTwo = AlertEtherEntity.with(etherEntity, false, null, "template");

        when(alertEntityService.findAlertEthers(anyInt(), anyInt()))
            .thenReturn(new PageImpl<>(List.of(alertEtherEntityOne, alertEtherEntityTwo)));

        mockMvc.perform(get(URL_BASE))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.size", equalTo(2)))
            .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(2))))
            .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(alertEtherEntityOne.getUuid())))
            .andExpect(jsonPath("$.items.[0].etherUuid", jUuidEqual(etherEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].enabled", equalTo(alertEtherEntityOne.isEnabled())))
            .andExpect(jsonPath("$.items.[0].topic", equalTo(alertEtherEntityOne.getTopic())))
            .andExpect(jsonPath("$.items.[0].template", equalTo(alertEtherEntityOne.getTemplate())))
            .andExpect(jsonPath("$.items.[0].created", jOdtEqual(alertEtherEntityOne.getCreated())))
            .andExpect(jsonPath("$.items.[0].updated", jOdtEqual(alertEtherEntityOne.getUpdated())))
            .andExpect(jsonPath("$.items.[1]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[1].uuid", jUuidEqual(alertEtherEntityTwo.getUuid())))
            .andExpect(jsonPath("$.items.[1].etherUuid", jUuidEqual(etherEntity.getUuid())))
            .andExpect(jsonPath("$.items.[1].enabled", equalTo(alertEtherEntityTwo.isEnabled())))
            .andExpect(jsonPath("$.items.[1].topic", equalTo(alertEtherEntityTwo.getTopic())))
            .andExpect(jsonPath("$.items.[1].template", equalTo(alertEtherEntityTwo.getTemplate())))
            .andExpect(jsonPath("$.items.[1].created", jOdtEqual(alertEtherEntityTwo.getCreated())))
            .andExpect(jsonPath("$.items.[1].updated", jOdtEqual(alertEtherEntityTwo.getUpdated())))
        ;

        verify(alertEntityService, times(1))
            .findAlertEthers(anyInt(), anyInt());
        verifyNoMoreInteractions(alertEntityService);
    }

    @Test
    void singleAlertEther() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var etherEntity = EtherEntity.with(MAC_ADDR_TWO, null);
        final var alertEtherEntity = AlertEtherEntity.with(etherEntity, true, "some/topic", "template");

        when(alertEntityService.findAlertEther(wrongUuid))
            .thenReturn(Optional.empty());
        when(alertEntityService.findAlertEther(alertEtherEntity.getUuid()))
            .thenReturn(Optional.of(alertEtherEntity));

        mockMvc.perform(get(URL_UUID, wrongUuid.toString()))
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(get(URL_UUID, alertEtherEntity.getUuid().toString()))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.uuid", jUuidEqual(alertEtherEntity.getUuid())))
            .andExpect(jsonPath("$.etherUuid", jUuidEqual(etherEntity.getUuid())))
            .andExpect(jsonPath("$.enabled", equalTo(alertEtherEntity.isEnabled())))
            .andExpect(jsonPath("$.topic", equalTo(alertEtherEntity.getTopic())))
            .andExpect(jsonPath("$.template", equalTo(alertEtherEntity.getTemplate())))
            .andExpect(jsonPath("$.created", jOdtEqual(alertEtherEntity.getCreated())))
            .andExpect(jsonPath("$.updated", jOdtEqual(alertEtherEntity.getUpdated())))
        ;

        verify(alertEntityService, times(1))
            .findAlertEther(wrongUuid);
        verify(alertEntityService, times(1))
            .findAlertEther(alertEtherEntity.getUuid());
        verifyNoMoreInteractions(alertEntityService);
    }

    @Test
    void createAlertEther() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var etherUuid = UUID.randomUUID();
        final var uuid = UUID.randomUUID();
        final var enabled = true;
        final var topic = "some/topic";
        final var template = "template";

        final var alertEtherEntity = mock(AlertEtherEntity.class);
        when(alertEtherEntity.getUuid()).thenReturn(uuid);
        when(alertEtherEntity.isEnabled()).thenReturn(enabled);
        when(alertEtherEntity.getTopic()).thenReturn(topic);
        when(alertEtherEntity.getTemplate()).thenReturn(template);

        when(alertEntityService.createAlertEther(wrongUuid, enabled, topic, template))
            .thenReturn(Optional.empty());
        when(alertEntityService.createAlertEther(etherUuid, enabled, topic, template))
            .thenReturn(Optional.of(alertEtherEntity));

        final var wrongRequestDto = new AlertEtherRequestDto(wrongUuid, enabled, topic, template);
        final var wrongRequestPayload = jsonMapper.writeValueAsString(wrongRequestDto);

        final var requestDto = new AlertEtherRequestDto(etherUuid, enabled, topic, template);
        final var requestPayload = jsonMapper.writeValueAsString(requestDto);

        final var redirection = UriComponentsBuilder
            .fromPath(URL_UUID)
            .build(Map.of("uuid", alertEtherEntity.getUuid()));

        mockMvc.perform(
                post(URL_BASE)
                    .contentType(APPLICATION_JSON)
                    .content(wrongRequestPayload)
            )
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(
                post(URL_BASE)
                    .contentType(APPLICATION_JSON)
                    .content(requestPayload)
            )
            .andDo(print())
            .andExpect(status().isCreated())
            .andExpect(redirectedUrl(redirection.toString()))
        ;

        verify(alertEntityService, times(1))
            .createAlertEther(wrongUuid, enabled, topic, template);
        verify(alertEntityService, times(1))
            .createAlertEther(etherUuid, enabled, topic, template);
        verifyNoMoreInteractions(alertEntityService);
    }

    @Test
    void modifyAlertEther() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var etherUuid = UUID.randomUUID();
        final var uuid = UUID.randomUUID();
        final var enabled = true;
        final var topic = "some/topic";
        final var template = "template";
        final var updated = OffsetDateTime.now();
        final var created = updated.minusHours(23);

        final var etherEntity = mock(EtherEntity.class);
        when(etherEntity.getUuid()).thenReturn(etherUuid);

        final var alertEtherEntity = mock(AlertEtherEntity.class);
        when(alertEtherEntity.getEtherEntity()).thenReturn(etherEntity);
        when(alertEtherEntity.getUuid()).thenReturn(uuid);
        when(alertEtherEntity.isEnabled()).thenReturn(enabled);
        when(alertEtherEntity.getTopic()).thenReturn(topic);
        when(alertEtherEntity.getTemplate()).thenReturn(template);
        when(alertEtherEntity.getCreated()).thenReturn(created);
        when(alertEtherEntity.getUpdated()).thenReturn(updated);

        when(alertEntityService.modifyAlertEther(wrongUuid, enabled, topic, template))
            .thenReturn(Optional.empty());
        when(alertEntityService.modifyAlertEther(uuid, enabled, topic, template))
            .thenReturn(Optional.of(alertEtherEntity));

        final var requestDto = new AlertEtherRequestDto(UUID.randomUUID(), enabled, topic, template);
        final var requestPayload = jsonMapper.writeValueAsString(requestDto);

        mockMvc.perform(
                put(URL_UUID, wrongUuid.toString())
                    .contentType(APPLICATION_JSON)
                    .content(requestPayload)
            )
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(
                put(URL_UUID, uuid.toString())
                    .contentType(APPLICATION_JSON)
                    .content(requestPayload)
            )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.uuid", jUuidEqual(alertEtherEntity.getUuid())))
            .andExpect(jsonPath("$.etherUuid", jUuidEqual(etherEntity.getUuid())))
            .andExpect(jsonPath("$.enabled", equalTo(alertEtherEntity.isEnabled())))
            .andExpect(jsonPath("$.topic", equalTo(alertEtherEntity.getTopic())))
            .andExpect(jsonPath("$.template", equalTo(alertEtherEntity.getTemplate())))
            .andExpect(jsonPath("$.created", jOdtEqual(alertEtherEntity.getCreated())))
            .andExpect(jsonPath("$.updated", jOdtEqual(alertEtherEntity.getUpdated())))
        ;

        verify(alertEntityService, times(1))
            .modifyAlertEther(wrongUuid, enabled, topic, template);
        verify(alertEntityService, times(1))
            .modifyAlertEther(uuid, enabled, topic, template);
        verifyNoMoreInteractions(alertEntityService);
    }

    @Test
    void deleteAlertEther() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var correctUuid = UUID.randomUUID();

        when(alertEntityService.deleteAlertEther(wrongUuid))
            .thenReturn(false);
        when(alertEntityService.deleteAlertEther(correctUuid))
            .thenReturn(true);

        mockMvc.perform(delete(URL_UUID, wrongUuid.toString()))
            .andDo(print())
            .andExpect(status().isNotFound())
        ;
        mockMvc.perform(delete(URL_UUID, correctUuid.toString()))
            .andDo(print())
            .andExpect(status().isOk())
        ;

        verify(alertEntityService, times(1))
            .deleteAlertEther(wrongUuid);
        verify(alertEntityService, times(1))
            .deleteAlertEther(correctUuid);
        verifyNoMoreInteractions(alertEntityService);
    }
}
