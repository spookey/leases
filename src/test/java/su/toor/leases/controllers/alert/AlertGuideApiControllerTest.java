package su.toor.leases.controllers.alert;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static su.toor.leases.Net.MASK_ADDR_ONE;
import static su.toor.leases.Net.MASK_ADDR_TWO;
import static su.toor.leases.controllers.JsonOffsetDateTimeMatcher.jOdtEqual;
import static su.toor.leases.controllers.JsonUuidMatcher.jUuidEqual;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.leases.configs.spring.JsonMapperConfiguration;
import su.toor.leases.controllers.dtos.alert.AlertGuideRequestDto;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.alert.AlertGuideEntity;
import su.toor.leases.services.query.AlertEntityService;

@WebMvcTest(AlertGuideApiController.class)
@Import(JsonMapperConfiguration.class)
class AlertGuideApiControllerTest {

    private static final String URL_BASE = "/api/alert/networks";
    private static final String URL_UUID = URL_BASE + "/{uuid}";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JsonMapper jsonMapper;
    @MockBean
    private AlertEntityService alertEntityService;

    @Test
    void allAlertGuides() throws Exception {
        final var guideEntity = GuideEntity.with(MASK_ADDR_ONE, null, ".", null);
        final var alertGuideEntityOne = AlertGuideEntity.with(guideEntity, true, "some/topic", null);
        final var alertGuideEntityTwo = AlertGuideEntity.with(guideEntity, false, null, "template");

        when(alertEntityService.findAlertGuides(anyInt(), anyInt()))
            .thenReturn(new PageImpl<>(List.of(alertGuideEntityOne, alertGuideEntityTwo)));

        mockMvc.perform(get(URL_BASE))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.size", equalTo(2)))
            .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(2))))
            .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(alertGuideEntityOne.getUuid())))
            .andExpect(jsonPath("$.items.[0].guideUuid", jUuidEqual(guideEntity.getUuid())))
            .andExpect(jsonPath("$.items.[0].enabled", equalTo(alertGuideEntityOne.isEnabled())))
            .andExpect(jsonPath("$.items.[0].topic", equalTo(alertGuideEntityOne.getTopic())))
            .andExpect(jsonPath("$.items.[0].template", equalTo(alertGuideEntityOne.getTemplate())))
            .andExpect(jsonPath("$.items.[0].created", jOdtEqual(alertGuideEntityOne.getCreated())))
            .andExpect(jsonPath("$.items.[0].updated", jOdtEqual(alertGuideEntityOne.getUpdated())))
            .andExpect(jsonPath("$.items.[1]", isA(Map.class)))
            .andExpect(jsonPath("$.items.[1].uuid", jUuidEqual(alertGuideEntityTwo.getUuid())))
            .andExpect(jsonPath("$.items.[1].guideUuid", jUuidEqual(guideEntity.getUuid())))
            .andExpect(jsonPath("$.items.[1].enabled", equalTo(alertGuideEntityTwo.isEnabled())))
            .andExpect(jsonPath("$.items.[1].topic", equalTo(alertGuideEntityTwo.getTopic())))
            .andExpect(jsonPath("$.items.[1].template", equalTo(alertGuideEntityTwo.getTemplate())))
            .andExpect(jsonPath("$.items.[1].created", jOdtEqual(alertGuideEntityTwo.getCreated())))
            .andExpect(jsonPath("$.items.[1].updated", jOdtEqual(alertGuideEntityTwo.getUpdated())))
        ;

        verify(alertEntityService, times(1))
            .findAlertGuides(anyInt(), anyInt());
        verifyNoMoreInteractions(alertEntityService);
    }

    @Test
    void singleAlertGuide() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var guideEntity = GuideEntity.with(MASK_ADDR_TWO, null, ".", null);
        final var alertGuideEntity = AlertGuideEntity.with(guideEntity, true, "some/topic", "template");

        when(alertEntityService.findAlertGuide(wrongUuid))
            .thenReturn(Optional.empty());
        when(alertEntityService.findAlertGuide(alertGuideEntity.getUuid()))
            .thenReturn(Optional.of(alertGuideEntity));

        mockMvc.perform(get(URL_UUID, wrongUuid.toString()))
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(get(URL_UUID, alertGuideEntity.getUuid().toString()))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.uuid", jUuidEqual(alertGuideEntity.getUuid())))
            .andExpect(jsonPath("$.guideUuid", jUuidEqual(guideEntity.getUuid())))
            .andExpect(jsonPath("$.enabled", equalTo(alertGuideEntity.isEnabled())))
            .andExpect(jsonPath("$.topic", equalTo(alertGuideEntity.getTopic())))
            .andExpect(jsonPath("$.template", equalTo(alertGuideEntity.getTemplate())))
            .andExpect(jsonPath("$.created", jOdtEqual(alertGuideEntity.getCreated())))
            .andExpect(jsonPath("$.updated", jOdtEqual(alertGuideEntity.getUpdated())))
        ;

        verify(alertEntityService, times(1))
            .findAlertGuide(wrongUuid);
        verify(alertEntityService, times(1))
            .findAlertGuide(alertGuideEntity.getUuid());
        verifyNoMoreInteractions(alertEntityService);
    }

    @Test
    void createAlertGuide() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var guideUuid = UUID.randomUUID();
        final var uuid = UUID.randomUUID();
        final var enabled = true;
        final var topic = "some/topic";
        final var template = "template";

        final var alertGuideEntity = mock(AlertGuideEntity.class);
        when(alertGuideEntity.getUuid()).thenReturn(uuid);
        when(alertGuideEntity.isEnabled()).thenReturn(enabled);
        when(alertGuideEntity.getTopic()).thenReturn(topic);
        when(alertGuideEntity.getTemplate()).thenReturn(template);

        when(alertEntityService.createAlertGuide(wrongUuid, enabled, topic, template))
            .thenReturn(Optional.empty());
        when(alertEntityService.createAlertGuide(guideUuid, enabled, topic, template))
            .thenReturn(Optional.of(alertGuideEntity));

        final var wrongRequestDto = new AlertGuideRequestDto(wrongUuid, enabled, topic, template);
        final var wrongRequestPayload = jsonMapper.writeValueAsString(wrongRequestDto);

        final var requestDto = new AlertGuideRequestDto(guideUuid, enabled, topic, template);
        final var requestPayload = jsonMapper.writeValueAsString(requestDto);

        final var redirection = UriComponentsBuilder
            .fromPath(URL_UUID)
            .build(Map.of("uuid", alertGuideEntity.getUuid()));

        mockMvc.perform(
            post(URL_BASE)
                .contentType(APPLICATION_JSON)
                .content(wrongRequestPayload)
        )
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(
            post(URL_BASE)
                .contentType(APPLICATION_JSON)
                .content(requestPayload)
        )
            .andDo(print())
            .andExpect(status().isCreated())
            .andExpect(redirectedUrl(redirection.toString()))
        ;

        verify(alertEntityService, times(1))
            .createAlertGuide(wrongUuid, enabled, topic, template);
        verify(alertEntityService, times(1))
            .createAlertGuide(guideUuid, enabled, topic, template);
        verifyNoMoreInteractions(alertEntityService);
    }

    @Test
    void modifyAlertGuide() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var guideUuid = UUID.randomUUID();
        final var uuid = UUID.randomUUID();
        final var enabled = true;
        final var topic = "some/topic";
        final var template = "template";
        final var updated = OffsetDateTime.now();
        final var created = updated.minusHours(23);

        final var guideEntity = mock(GuideEntity.class);
        when(guideEntity.getUuid()).thenReturn(guideUuid);

        final var alertGuideEntity = mock(AlertGuideEntity.class);
        when(alertGuideEntity.getGuideEntity()).thenReturn(guideEntity);
        when(alertGuideEntity.getUuid()).thenReturn(uuid);
        when(alertGuideEntity.isEnabled()).thenReturn(enabled);
        when(alertGuideEntity.getTopic()).thenReturn(topic);
        when(alertGuideEntity.getTemplate()).thenReturn(template);
        when(alertGuideEntity.getCreated()).thenReturn(created);
        when(alertGuideEntity.getUpdated()).thenReturn(updated);

        when(alertEntityService.modifyAlertGuide(wrongUuid, enabled, topic, template))
            .thenReturn(Optional.empty());
        when(alertEntityService.modifyAlertGuide(uuid, enabled, topic, template))
            .thenReturn(Optional.of(alertGuideEntity));

        final var requestDto = new AlertGuideRequestDto(UUID.randomUUID(), enabled, topic, template);
        final var requestPayload = jsonMapper.writeValueAsString(requestDto);

        mockMvc.perform(
            put(URL_UUID, wrongUuid.toString())
                .contentType(APPLICATION_JSON)
                .content(requestPayload)
        )
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(
            put(URL_UUID, uuid.toString())
                .contentType(APPLICATION_JSON)
                .content(requestPayload)
        )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", isA(Map.class)))
            .andExpect(jsonPath("$.uuid", jUuidEqual(alertGuideEntity.getUuid())))
            .andExpect(jsonPath("$.guideUuid", jUuidEqual(guideEntity.getUuid())))
            .andExpect(jsonPath("$.enabled", equalTo(alertGuideEntity.isEnabled())))
            .andExpect(jsonPath("$.topic", equalTo(alertGuideEntity.getTopic())))
            .andExpect(jsonPath("$.template", equalTo(alertGuideEntity.getTemplate())))
            .andExpect(jsonPath("$.created", jOdtEqual(alertGuideEntity.getCreated())))
            .andExpect(jsonPath("$.updated", jOdtEqual(alertGuideEntity.getUpdated())))
        ;

        verify(alertEntityService, times(1))
            .modifyAlertGuide(wrongUuid, enabled, topic, template);
        verify(alertEntityService, times(1))
            .modifyAlertGuide(uuid, enabled, topic, template);
        verifyNoMoreInteractions(alertEntityService);
    }

    @Test
    void deleteAlertGuide() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var correctUuid = UUID.randomUUID();

        when(alertEntityService.deleteAlertGuide(wrongUuid))
            .thenReturn(false);
        when(alertEntityService.deleteAlertGuide(correctUuid))
            .thenReturn(true);

        mockMvc.perform(delete(URL_UUID, wrongUuid.toString()))
            .andDo(print())
            .andExpect(status().isNotFound())
        ;
        mockMvc.perform(delete(URL_UUID, correctUuid.toString()))
            .andDo(print())
            .andExpect(status().isOk())
        ;

        verify(alertEntityService, times(1))
            .deleteAlertGuide(wrongUuid);
        verify(alertEntityService, times(1))
            .deleteAlertGuide(correctUuid);
        verifyNoMoreInteractions(alertEntityService);
    }
}
