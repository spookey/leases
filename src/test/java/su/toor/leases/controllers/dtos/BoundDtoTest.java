package su.toor.leases.controllers.dtos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ONE;
import static su.toor.leases.Net.IP_TWO;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.shared.RequireException;

class BoundDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final OffsetDateTime UPDATED = OffsetDateTime.now();
    private static final OffsetDateTime CREATED = UPDATED.minusDays(1);

    @Test
    void createIssues() {
        final var uuid = UUID.randomUUID();

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new BoundDto(null, IP_ONE, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new BoundDto(uuid, null, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new BoundDto(uuid, IP_ONE, null, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new BoundDto(uuid, IP_ONE, CREATED, null));

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> BoundDto.from(null));
    }

    @Test
    void fields() {
        final var uuid = UUID.randomUUID();

        final var dto = new BoundDto(uuid, IP_TWO, CREATED, UPDATED);

        assertThat(dto.getUuid()).isEqualTo(uuid);
        assertThat(dto.getAddress()).isEqualTo(IP_TWO);
        assertThat(dto.getCreated()).isEqualTo(CREATED);
        assertThat(dto.getUpdated()).isEqualTo(UPDATED);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(STYLER.style(uuid))
            .contains(IP_TWO)
            .contains(STYLER.style(CREATED))
            .contains(STYLER.style(UPDATED));
    }

    @Test
    void from() {
        final var boundEntity = BoundEntity.with(IP_ADDR_ONE);

        final var dto = BoundDto.from(boundEntity);

        assertThat(dto.getUuid()).isEqualTo(boundEntity.getUuid());
        assertThat(dto.getAddress()).isEqualTo(IP_ONE);
        assertThat(dto.getCreated()).isEqualTo(boundEntity.getCreated());
        assertThat(dto.getUpdated()).isEqualTo(boundEntity.getUpdated());
    }
}
