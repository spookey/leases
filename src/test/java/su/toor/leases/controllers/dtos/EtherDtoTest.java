package su.toor.leases.controllers.dtos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.MAC_ADDR_ONE;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.shared.RequireException;

class EtherDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final OffsetDateTime UPDATED = OffsetDateTime.now();
    private static final OffsetDateTime CREATED = UPDATED.minusDays(1);


    @Test
    void createIssues() {
        final var uuid = UUID.randomUUID();

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new  EtherDto(null, HOSTNAME, CREATED, UPDATED));
        assertThatNoException()
            .isThrownBy(() -> new  EtherDto(uuid, null, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new  EtherDto(uuid, HOSTNAME, null, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new  EtherDto(uuid, HOSTNAME, CREATED, null));

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> EtherDto.from(null));
    }

    @Test
    void fields() {
        final var uuid = UUID.randomUUID();

        final var dto = new EtherDto(uuid, HOSTNAME, CREATED, UPDATED);

        assertThat(dto.getUuid()).isEqualTo(uuid);
        assertThat(dto.getHostname()).isEqualTo(HOSTNAME);
        assertThat(dto.getCreated()).isEqualTo(CREATED);
        assertThat(dto.getUpdated()).isEqualTo(UPDATED);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(STYLER.style(uuid))
            .contains(HOSTNAME)
            .contains(STYLER.style(CREATED))
            .contains(STYLER.style(UPDATED));
    }

    @Test
    void from() {
        final var etherEntity = EtherEntity.with(MAC_ADDR_ONE, HOSTNAME);

        final var dto = EtherDto.from(etherEntity);

        assertThat(dto.getUuid()).isEqualTo(etherEntity.getUuid());
        assertThat(dto.getHostname()).isEqualTo(HOSTNAME);
        assertThat(dto.getCreated()).isEqualTo(etherEntity.getCreated());
        assertThat(dto.getUpdated()).isEqualTo(etherEntity.getUpdated());
    }
}
