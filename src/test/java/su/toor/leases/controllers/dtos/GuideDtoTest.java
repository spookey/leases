package su.toor.leases.controllers.dtos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.leases.Net.MASK_ADDR_ONE;
import static su.toor.leases.Net.MASK_ONE;
import static su.toor.leases.Net.MASK_TWO;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.shared.RequireException;

class GuideDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final int WEIGHT = 1312;
    private static final String TITLE = "Title";
    private static final String DESCRIPTION = "Some description";
    private static final OffsetDateTime UPDATED = OffsetDateTime.now();
    private static final OffsetDateTime CREATED = UPDATED.minusDays(1);

    @Test
    void createIssues() {
        final var uuid = UUID.randomUUID();

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new GuideDto(null, MASK_ONE, WEIGHT, TITLE, DESCRIPTION, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new GuideDto(uuid, "\n", WEIGHT, TITLE, DESCRIPTION, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new GuideDto(uuid, MASK_ONE, null, TITLE, DESCRIPTION, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new GuideDto(uuid, MASK_ONE, WEIGHT, "\t", DESCRIPTION, CREATED, UPDATED));
        assertThatNoException()
            .isThrownBy(() -> new GuideDto(uuid, MASK_ONE, WEIGHT, TITLE, null, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new GuideDto(uuid, MASK_ONE, WEIGHT, TITLE, DESCRIPTION, null, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new GuideDto(uuid, MASK_ONE, WEIGHT, TITLE, DESCRIPTION, CREATED, null));

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> GuideDto.from(null));
    }

    @Test
    void fields() {
        final var uuid = UUID.randomUUID();

        final var dto = new GuideDto(uuid, MASK_TWO, WEIGHT, TITLE, DESCRIPTION, CREATED, UPDATED);

        assertThat(dto.getUuid()).isEqualTo(uuid);
        assertThat(dto.getMask()).isEqualTo(MASK_TWO);
        assertThat(dto.getWeight()).isEqualTo(WEIGHT);
        assertThat(dto.getTitle()).isEqualTo(TITLE);
        assertThat(dto.getDescription()).isEqualTo(DESCRIPTION);
        assertThat(dto.getCreated()).isEqualTo(CREATED);
        assertThat(dto.getUpdated()).isEqualTo(UPDATED);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(STYLER.style(uuid))
            .contains(MASK_TWO)
            .contains(STYLER.style(WEIGHT))
            .contains(TITLE)
            .contains(DESCRIPTION)
            .contains(STYLER.style(CREATED))
            .contains(STYLER.style(UPDATED));
    }

    @Test
    void from() {
        final var guideEntity = GuideEntity.with(MASK_ADDR_ONE, null, TITLE, DESCRIPTION);

        final var dto = GuideDto.from(guideEntity);

        assertThat(dto.getUuid()).isEqualTo(guideEntity.getUuid());
        assertThat(dto.getMask()).isEqualTo(MASK_ONE);
        assertThat(dto.getWeight()).isEqualTo(guideEntity.getWeight());
        assertThat(dto.getTitle()).isEqualTo(TITLE);
        assertThat(dto.getDescription()).isEqualTo(DESCRIPTION);
        assertThat(dto.getCreated()).isEqualTo(guideEntity.getCreated());
        assertThat(dto.getUpdated()).isEqualTo(guideEntity.getUpdated());
    }
}
