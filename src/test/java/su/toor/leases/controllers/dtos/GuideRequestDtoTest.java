package su.toor.leases.controllers.dtos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.leases.Net.MASK_ONE;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.shared.RequireException;

class GuideRequestDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final int WEIGHT = 1312;
    private static final String TITLE = "Title";
    private static final String DESCRIPTION = "Some description";

    @Test
    void createIssues() {
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new GuideRequestDto(null, WEIGHT, TITLE, DESCRIPTION));
        assertThatNoException()
            .isThrownBy(() -> new GuideRequestDto(MASK_ONE, null, TITLE, DESCRIPTION));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new GuideRequestDto(MASK_ONE, WEIGHT, "\t", DESCRIPTION));
        assertThatNoException()
            .isThrownBy(() -> new GuideRequestDto(MASK_ONE, WEIGHT, TITLE, null));
    }

    @Test
    void fields() {
        final var dto = new GuideRequestDto(MASK_ONE, WEIGHT, TITLE, DESCRIPTION);

        assertThat(dto.getMask()).isEqualTo(MASK_ONE);
        assertThat(dto.getWeight()).isEqualTo(WEIGHT);
        assertThat(dto.getTitle()).isEqualTo(TITLE);
        assertThat(dto.getDescription()).isEqualTo(DESCRIPTION);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(MASK_ONE)
            .contains(STYLER.style(WEIGHT))
            .contains(TITLE)
            .contains(DESCRIPTION);
    }
}
