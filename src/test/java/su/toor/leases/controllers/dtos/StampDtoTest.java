package su.toor.leases.controllers.dtos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.StampEntity;
import su.toor.leases.shared.RequireException;

class StampDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final OffsetDateTime VALUE = OffsetDateTime.now();

    @Test
    void createIssues() {
        final var uuid = UUID.randomUUID();

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new StampDto(null, VALUE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new StampDto(uuid, null));

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> StampDto.from(null));
    }

    @Test
    void fields() {
        final var uuid = UUID.randomUUID();

        final var dto = new StampDto(uuid, VALUE);

        assertThat(dto.getUuid()).isEqualTo(uuid);
        assertThat(dto.getValue()).isEqualTo(VALUE);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(STYLER.style(uuid))
            .contains(STYLER.style(VALUE));
    }

    @Test
    void from() {
        final var stampEntity = StampEntity.with();

        final var dto = StampDto.from(stampEntity);

        assertThat(dto.getUuid()).isEqualTo(stampEntity.getUuid());
        assertThat(dto.getValue()).isEqualTo(stampEntity.getValue());
    }
}
