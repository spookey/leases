package su.toor.leases.controllers.dtos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_ONE;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.shared.RequireException;
import su.toor.leases.shared.State;

class TrackDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final StampEntity STAMP_ENTITY = StampEntity.with();
    private static final StampDto STAMP_DTO = StampDto.from(STAMP_ENTITY);

    private static final EtherEntity ETHER_ENTITY = EtherEntity.with(MAC_ADDR_ONE, HOSTNAME);
    private static final EtherDto ETHER_DTO = EtherDto.from(ETHER_ENTITY);

    private static final BoundEntity BOUND_ENTITY = BoundEntity.with(IP_ADDR_ONE);
    private static final BoundDto BOUND_DTO = BoundDto.from(BOUND_ENTITY);

    private static final State STATE = State.ACTIVE;

    @Test
    void createIssues() {
        final var uuid = UUID.randomUUID();

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new TrackDto(null, STAMP_DTO, ETHER_DTO, BOUND_DTO, STATE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new TrackDto(uuid, null, ETHER_DTO, BOUND_DTO, STATE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new TrackDto(uuid, STAMP_DTO, null, BOUND_DTO, STATE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new TrackDto(uuid, STAMP_DTO, ETHER_DTO, null, STATE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new TrackDto(uuid, STAMP_DTO, ETHER_DTO, BOUND_DTO, null));

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> TrackDto.from(null));
    }

    @Test
    void fields() {
        final var uuid = UUID.randomUUID();

        final var dto = new TrackDto(uuid, STAMP_DTO, ETHER_DTO, BOUND_DTO, STATE);

        assertThat(dto.getUuid()).isEqualTo(uuid);
        assertThat(dto.getStamp()).isEqualTo(STAMP_DTO);
        assertThat(dto.getEther()).isEqualTo(ETHER_DTO);
        assertThat(dto.getBound()).isEqualTo(BOUND_DTO);
        assertThat(dto.getState()).isEqualTo(STATE);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(STYLER.style(uuid))
            .contains(STYLER.style(STAMP_DTO))
            .contains(STYLER.style(ETHER_DTO))
            .contains(STYLER.style(BOUND_DTO))
            .contains(STYLER.style(STATE));
    }

    @Test
    void from() {
        final var trackEntity = TrackEntity.with(
            STAMP_ENTITY, ETHER_ENTITY, BOUND_ENTITY, STATE
        );

        final var dto = TrackDto.from(trackEntity);

        assertThat(dto.getUuid()).isEqualTo(trackEntity.getUuid());
        assertThat(dto.getStamp())
            .usingRecursiveComparison()
            .isEqualTo(STAMP_DTO);
        assertThat(dto.getEther())
            .usingRecursiveComparison()
            .isEqualTo(ETHER_DTO);
        assertThat(dto.getBound())
            .usingRecursiveComparison()
            .isEqualTo(BOUND_DTO);
        assertThat(dto.getState())
            .usingRecursiveComparison()
            .isEqualTo(STATE);
    }
}
