package su.toor.leases.controllers.dtos.alert;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.leases.Net.IP_ADDR_TWO;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.shared.RequireException;

class AlertBoundDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final Boolean ENABLED = true;
    private static final String TOPIC = "some/topic";
    private static final String TEMPLATE = "Template";
    private static final OffsetDateTime UPDATED = OffsetDateTime.now();
    private static final OffsetDateTime CREATED = UPDATED.minusDays(1);

    @Test
    void createIssues() {
        final var uuid = UUID.randomUUID();
        final var boundUuid = UUID.randomUUID();

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertBoundDto(null, boundUuid, ENABLED, TOPIC, TEMPLATE, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertBoundDto(uuid, null, ENABLED, TOPIC, TEMPLATE, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertBoundDto(uuid, boundUuid, null, TOPIC, TEMPLATE, CREATED, UPDATED));
        assertThatNoException()
            .isThrownBy(() -> new AlertBoundDto(uuid, boundUuid, ENABLED, null, TEMPLATE, CREATED, UPDATED));
        assertThatNoException()
            .isThrownBy(() -> new AlertBoundDto(uuid, boundUuid, ENABLED, TOPIC, null, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertBoundDto(uuid, boundUuid, ENABLED, TOPIC, TEMPLATE, null, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertBoundDto(uuid, boundUuid, ENABLED, TOPIC, TEMPLATE, CREATED, null));

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> AlertBoundDto.from(null));
    }

    @Test
    void fields() {
        final var uuid = UUID.randomUUID();
        final var boundUuid = UUID.randomUUID();

        final var dto = new AlertBoundDto(uuid, boundUuid, ENABLED, TOPIC, TEMPLATE, CREATED, UPDATED);

        assertThat(dto.getUuid()).isEqualTo(uuid);
        assertThat(dto.getBoundUuid()).isEqualTo(boundUuid);
        assertThat(dto.isEnabled()).isEqualTo(ENABLED);
        assertThat(dto.getTopic()).isEqualTo(TOPIC);
        assertThat(dto.getTemplate()).isEqualTo(TEMPLATE);
        assertThat(dto.getCreated()).isEqualTo(CREATED);
        assertThat(dto.getUpdated()).isEqualTo(UPDATED);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(STYLER.style(uuid))
            .contains(STYLER.style(boundUuid))
            .contains(STYLER.style(ENABLED))
            .contains(TOPIC)
            .contains(TEMPLATE)
            .contains(STYLER.style(CREATED))
            .contains(STYLER.style(UPDATED));
    }

    @Test
    void from() {
        final var boundEntity = BoundEntity.with(IP_ADDR_TWO);
        final var alertBoundEntity = AlertBoundEntity.with(boundEntity, ENABLED, TOPIC, TEMPLATE);

        final var dto = AlertBoundDto.from(alertBoundEntity);

        assertThat(dto.getUuid()).isEqualTo(alertBoundEntity.getUuid());
        assertThat(dto.getBoundUuid()).isEqualTo(boundEntity.getUuid());
        assertThat(dto.isEnabled()).isEqualTo(ENABLED);
        assertThat(dto.getTopic()).isEqualTo(TOPIC);
        assertThat(dto.getTemplate()).isEqualTo(TEMPLATE);
        assertThat(dto.getCreated()).isEqualTo(alertBoundEntity.getCreated());
        assertThat(dto.getUpdated()).isEqualTo(alertBoundEntity.getUpdated());
    }
}
