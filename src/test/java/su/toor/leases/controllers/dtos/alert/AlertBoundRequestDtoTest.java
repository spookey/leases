package su.toor.leases.controllers.dtos.alert;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.shared.RequireException;

class AlertBoundRequestDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final Boolean ENABLED = false;
    private static final String TOPIC = "some/topic";
    private static final String TEMPLATE = "Template";

    @Test
    void createIssues() {
        final var boundUuid = UUID.randomUUID();

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertBoundRequestDto(null, ENABLED, TOPIC, TEMPLATE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertBoundRequestDto(boundUuid, null, TOPIC, TEMPLATE));
        assertThatNoException()
            .isThrownBy(() -> new AlertBoundRequestDto(boundUuid, ENABLED, null, TEMPLATE));
        assertThatNoException()
            .isThrownBy(() -> new AlertBoundRequestDto(boundUuid, ENABLED, TOPIC, null));
    }

    @Test
    void fields() {
        final var boundUuid = UUID.randomUUID();

        final var dto = new AlertBoundRequestDto(boundUuid, ENABLED, TOPIC, TEMPLATE);

        assertThat(dto.getBoundUuid()).isEqualTo(boundUuid);
        assertThat(dto.isEnabled()).isEqualTo(ENABLED);
        assertThat(dto.getTopic()).isEqualTo(TOPIC);
        assertThat(dto.getTemplate()).isEqualTo(TEMPLATE);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(STYLER.style(boundUuid))
            .contains(STYLER.style(ENABLED))
            .contains(TOPIC)
            .contains(TEMPLATE);
    }
}
