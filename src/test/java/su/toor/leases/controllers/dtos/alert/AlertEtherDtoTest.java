package su.toor.leases.controllers.dtos.alert;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.leases.Net.MAC_ADDR_TWO;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.shared.RequireException;

class AlertEtherDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final Boolean ENABLED = true;
    private static final String TOPIC = "some/topic";
    private static final String TEMPLATE = "Template";
    private static final OffsetDateTime UPDATED = OffsetDateTime.now();
    private static final OffsetDateTime CREATED = UPDATED.minusDays(1);

    @Test
    void createIssues() {
        final var uuid = UUID.randomUUID();
        final var etherUuid = UUID.randomUUID();

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertEtherDto(null, etherUuid, ENABLED, TOPIC, TEMPLATE, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertEtherDto(uuid, null, ENABLED, TOPIC, TEMPLATE, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertEtherDto(uuid, etherUuid, null, TOPIC, TEMPLATE, CREATED, UPDATED));
        assertThatNoException()
            .isThrownBy(() -> new AlertEtherDto(uuid, etherUuid, ENABLED, null, TEMPLATE, CREATED, UPDATED));
        assertThatNoException()
            .isThrownBy(() -> new AlertEtherDto(uuid, etherUuid, ENABLED, TOPIC, null, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertEtherDto(uuid, etherUuid, ENABLED, TOPIC, TEMPLATE, null, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertEtherDto(uuid, etherUuid, ENABLED, TOPIC, TEMPLATE, CREATED, null));

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> AlertEtherDto.from(null));
    }

    @Test
    void fields() {
        final var uuid = UUID.randomUUID();
        final var etherUuid = UUID.randomUUID();

        final var dto = new AlertEtherDto(uuid, etherUuid, ENABLED, TOPIC, TEMPLATE, CREATED, UPDATED);

        assertThat(dto.getUuid()).isEqualTo(uuid);
        assertThat(dto.getEtherUuid()).isEqualTo(etherUuid);
        assertThat(dto.isEnabled()).isEqualTo(ENABLED);
        assertThat(dto.getTopic()).isEqualTo(TOPIC);
        assertThat(dto.getTemplate()).isEqualTo(TEMPLATE);
        assertThat(dto.getCreated()).isEqualTo(CREATED);
        assertThat(dto.getUpdated()).isEqualTo(UPDATED);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(STYLER.style(uuid))
            .contains(STYLER.style(etherUuid))
            .contains(STYLER.style(ENABLED))
            .contains(TOPIC)
            .contains(TEMPLATE)
            .contains(STYLER.style(CREATED))
            .contains(STYLER.style(UPDATED));
    }

    @Test
    void from() {
        final var etherEntity = EtherEntity.with(MAC_ADDR_TWO, null);
        final var alertEtherEntity = AlertEtherEntity.with(etherEntity, ENABLED, TOPIC, TEMPLATE);

        final var dto = AlertEtherDto.from(alertEtherEntity);

        assertThat(dto.getUuid()).isEqualTo(alertEtherEntity.getUuid());
        assertThat(dto.getEtherUuid()).isEqualTo(etherEntity.getUuid());
        assertThat(dto.isEnabled()).isEqualTo(ENABLED);
        assertThat(dto.getTopic()).isEqualTo(TOPIC);
        assertThat(dto.getTemplate()).isEqualTo(TEMPLATE);
        assertThat(dto.getCreated()).isEqualTo(alertEtherEntity.getCreated());
        assertThat(dto.getUpdated()).isEqualTo(alertEtherEntity.getUpdated());
    }
}
