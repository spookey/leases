package su.toor.leases.controllers.dtos.alert;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.leases.Net.MASK_ADDR_TWO;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.alert.AlertGuideEntity;
import su.toor.leases.shared.RequireException;

class AlertGuideDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final Boolean ENABLED = true;
    private static final String TOPIC = "some/topic";
    private static final String TEMPLATE = "Template";
    private static final OffsetDateTime UPDATED = OffsetDateTime.now();
    private static final OffsetDateTime CREATED = UPDATED.minusDays(1);

    @Test
    void createIssues() {
        final var uuid = UUID.randomUUID();
        final var guideUuid = UUID.randomUUID();

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertGuideDto(null, guideUuid, ENABLED, TOPIC, TEMPLATE, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertGuideDto(uuid, null, ENABLED, TOPIC, TEMPLATE, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertGuideDto(uuid, guideUuid, null, TOPIC, TEMPLATE, CREATED, UPDATED));
        assertThatNoException()
            .isThrownBy(() -> new AlertGuideDto(uuid, guideUuid, ENABLED, null, TEMPLATE, CREATED, UPDATED));
        assertThatNoException()
            .isThrownBy(() -> new AlertGuideDto(uuid, guideUuid, ENABLED, TOPIC, null, CREATED, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertGuideDto(uuid, guideUuid, ENABLED, TOPIC, TEMPLATE, null, UPDATED));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new AlertGuideDto(uuid, guideUuid, ENABLED, TOPIC, TEMPLATE, CREATED, null));

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> AlertGuideDto.from(null));
    }

    @Test
    void fields() {
        final var uuid = UUID.randomUUID();
        final var guideUuid = UUID.randomUUID();

        final var dto = new AlertGuideDto(uuid, guideUuid, ENABLED, TOPIC, TEMPLATE, CREATED, UPDATED);

        assertThat(dto.getUuid()).isEqualTo(uuid);
        assertThat(dto.getGuideUuid()).isEqualTo(guideUuid);
        assertThat(dto.isEnabled()).isEqualTo(ENABLED);
        assertThat(dto.getTopic()).isEqualTo(TOPIC);
        assertThat(dto.getTemplate()).isEqualTo(TEMPLATE);
        assertThat(dto.getCreated()).isEqualTo(CREATED);
        assertThat(dto.getUpdated()).isEqualTo(UPDATED);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(STYLER.style(uuid))
            .contains(STYLER.style(guideUuid))
            .contains(STYLER.style(ENABLED))
            .contains(TOPIC)
            .contains(TEMPLATE)
            .contains(STYLER.style(CREATED))
            .contains(STYLER.style(UPDATED));
    }

    @Test
    void from() {
        final var guideEntity = GuideEntity.with(MASK_ADDR_TWO, null, ".", null);
        final var alertGuideEntity = AlertGuideEntity.with(guideEntity, ENABLED, TOPIC, TEMPLATE);

        final var dto = AlertGuideDto.from(alertGuideEntity);

        assertThat(dto.getUuid()).isEqualTo(alertGuideEntity.getUuid());
        assertThat(dto.getGuideUuid()).isEqualTo(guideEntity.getUuid());
        assertThat(dto.isEnabled()).isEqualTo(ENABLED);
        assertThat(dto.getTopic()).isEqualTo(TOPIC);
        assertThat(dto.getTemplate()).isEqualTo(TEMPLATE);
        assertThat(dto.getCreated()).isEqualTo(alertGuideEntity.getCreated());
        assertThat(dto.getUpdated()).isEqualTo(alertGuideEntity.getUpdated());
    }
}
