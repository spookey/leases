package su.toor.leases.controllers.dtos.side;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.leases.Net.MASK_ADDR_ONE;
import static su.toor.leases.Net.MASK_ONE;
import static su.toor.leases.Net.MASK_TWO;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.shared.RequireException;

class BaseGuideDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final String TITLE = "Title";
    private static final String DESCRIPTION = "Some description";

    @Test
    void createIssues() {
        final var uuid = UUID.randomUUID();

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new BaseGuideDto(null, MASK_ONE, TITLE, DESCRIPTION));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new BaseGuideDto(uuid, "\n", TITLE, DESCRIPTION));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new BaseGuideDto(uuid, MASK_ONE, "\t", DESCRIPTION));
        assertThatNoException()
            .isThrownBy(() -> new BaseGuideDto(uuid, MASK_ONE, TITLE, null));

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> BaseGuideDto.from(null));
    }

    @Test
    void fields() {
        final var uuid = UUID.randomUUID();

        final var dto = new BaseGuideDto(uuid, MASK_TWO, TITLE, DESCRIPTION);

        assertThat(dto.getUuid()).isEqualTo(uuid);
        assertThat(dto.getMask()).isEqualTo(MASK_TWO);
        assertThat(dto.getTitle()).isEqualTo(TITLE);
        assertThat(dto.getDescription()).isEqualTo(DESCRIPTION);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(STYLER.style(uuid))
            .contains(MASK_TWO)
            .contains(TITLE)
            .contains(DESCRIPTION);
    }

    @Test
    void from() {
        final var guideEntity = GuideEntity.with(MASK_ADDR_ONE, null, TITLE, DESCRIPTION);

        final var dto = BaseGuideDto.from(guideEntity);

        assertThat(dto.getUuid()).isEqualTo(guideEntity.getUuid());
        assertThat(dto.getMask()).isEqualTo(MASK_ONE);
        assertThat(dto.getTitle()).isEqualTo(TITLE);
        assertThat(dto.getDescription()).isEqualTo(DESCRIPTION);
    }
}
