package su.toor.leases.controllers.dtos.side;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.StampEntity;
import su.toor.leases.shared.RequireException;

class ChartDataDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final StampEntity STAMP_ENTITY = StampEntity.with();
    private static final long X_VALUE = STAMP_ENTITY.getValue().toInstant().toEpochMilli();
    private static final long Y_VALUE = 1312;


    @Test
    void createIssues() {
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new ChartDataDto(null, Y_VALUE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new ChartDataDto(X_VALUE, null));

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> ChartDataDto.with(null, Y_VALUE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> ChartDataDto.with(STAMP_ENTITY, null));

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> ChartDataDto.from(null));
    }

    @Test
    void fields() {
        final var dto = new ChartDataDto(X_VALUE, Y_VALUE);

        assertThat(dto.getX()).isEqualTo(X_VALUE);
        assertThat(dto.getY()).isEqualTo(Y_VALUE);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(STYLER.style(X_VALUE))
            .contains(STYLER.style(Y_VALUE));
    }

    @Test
    void with() {
        final var dto = ChartDataDto.with(STAMP_ENTITY, Y_VALUE);

        assertThat(dto.getX()).isEqualTo(X_VALUE);
        assertThat(dto.getY()).isEqualTo(Y_VALUE);
    }

    @Test
    void from() {
        final var entry = Map.entry(STAMP_ENTITY, Y_VALUE);
        final var dto = ChartDataDto.from(entry);

        assertThat(dto.getX()).isEqualTo(X_VALUE);
        assertThat(dto.getY()).isEqualTo(Y_VALUE);
    }

    @Test
    void equals() {
        final var dto = new ChartDataDto(X_VALUE, Y_VALUE);

        final var recreated = new ChartDataDto(X_VALUE, Y_VALUE);
        final var different = new ChartDataDto(2 * X_VALUE, 2 * Y_VALUE);
        assertThat(dto)
            .hasSameHashCodeAs(recreated)
            .isEqualTo(dto)
            .isEqualTo(recreated)
            .isNotEqualTo(new Object())
            .isNotEqualTo(different);
    }
}
