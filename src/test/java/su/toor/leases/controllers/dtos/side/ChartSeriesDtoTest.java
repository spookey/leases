package su.toor.leases.controllers.dtos.side;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.StampEntity;
import su.toor.leases.shared.RequireException;
import su.toor.leases.shared.State;

class ChartSeriesDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final State STATE = State.FREE;
    private static final String NAME = STATE.getText();

    private static final Map<StampEntity, Long> CHART_COUNTS = Map.of(
        StampEntity.with(), 23L,
        StampEntity.with(), 42L
    );
    private static final List<ChartDataDto> CHART_DATA = CHART_COUNTS.entrySet()
        .stream()
        .map(ChartDataDto::from)
        .collect(Collectors.toList());

    @Test
    void createIssues() {
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new ChartSeriesDto("\n", CHART_DATA));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new ChartSeriesDto(NAME, null));

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> ChartSeriesDto.from(null, CHART_COUNTS));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> ChartSeriesDto.from(STATE, null));
    }

    @Test
    void fields() {
        final var dto = new ChartSeriesDto(NAME, CHART_DATA);

        assertThat(dto.getName()).isEqualTo(NAME);
        assertThat(dto.getData()).isEqualTo(CHART_DATA);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(NAME)
            .contains(STYLER.style(CHART_DATA));
    }

    @Test
    void from() {
        final var dto = ChartSeriesDto.from(STATE, CHART_COUNTS);

        assertThat(dto.getName()).isEqualTo(NAME);
        assertThat(dto.getData())
            .usingRecursiveComparison()
            .ignoringCollectionOrder()
            .isEqualTo(CHART_DATA);
    }

    @Test
    void equals() {
        final var dto = new ChartSeriesDto(NAME, CHART_DATA);

        final var recreated = new ChartSeriesDto(NAME, CHART_DATA);
        final var different = new ChartSeriesDto(NAME + NAME, List.of());
        assertThat(dto)
            .hasSameHashCodeAs(recreated)
            .isEqualTo(dto)
            .isEqualTo(recreated)
            .isNotEqualTo(new Object())
            .isNotEqualTo(different);
    }
}
