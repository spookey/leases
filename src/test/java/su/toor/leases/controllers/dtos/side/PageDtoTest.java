package su.toor.leases.controllers.dtos.side;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.shared.RequireException;

class PageDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    @Test
    void createInvalid() {
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new PageDto<>(null));
    }

    @Test
    void withInvalid() {
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> PageDto.with(null));

        final var items = List.of(1, 3, 1, 2);
        assertThatIllegalArgumentException()
            .isThrownBy(() -> PageDto.with(42L, -1, 1, items));
        assertThatIllegalArgumentException()
            .isThrownBy(() -> PageDto.with(42L, 1, 0, items));
        assertThatIllegalArgumentException()
            .isThrownBy(() -> PageDto.with(42L, 1, 1, null));
    }

    @Test
    void with() {
        final var pages = 4;
        final var size = 8;
        final var total = size * pages;
        final var number = pages - 1;

        final var items = IntStream.rangeClosed(1, total)
            .boxed()
            .skip(size * number)
            .limit(size)
            .collect(Collectors.toList());

        final var dto = PageDto.with(
            (long) total, number, size, items
        );

        assertThat(dto.getPages()).isEqualTo(pages);
        assertThat(dto.getTotal()).isEqualTo(total);
        assertThat(dto.getNumber()).isEqualTo(number);
        assertThat(dto.getSize()).isEqualTo(size);
        assertThat(dto.isFirst()).isFalse();
        assertThat(dto.isLast()).isTrue();
        assertThat(dto.hasNext()).isFalse();
        assertThat(dto.hasPrevious()).isTrue();
        assertThat(dto.getItems()).isEqualTo(items);

        assertThat(dto.toString())
            .isNotBlank()
            .contains(STYLER.style(pages))
            .contains(STYLER.style(total))
            .contains(STYLER.style(number))
            .contains(STYLER.style(size))
            .contains(STYLER.style(true))
            .contains(STYLER.style(false))
            .contains(STYLER.style(items));
    }
}
