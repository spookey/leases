package su.toor.leases.controllers.side;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static su.toor.leases.Net.MASK_ADDR_ONE;
import static su.toor.leases.Net.MASK_ADDR_TWO;
import static su.toor.leases.Net.MASK_ONE;
import static su.toor.leases.Net.MASK_TWO;
import static su.toor.leases.controllers.JsonUuidMatcher.jUuidEqual;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import su.toor.leases.controllers.dtos.side.BaseGuideDto;
import su.toor.leases.controllers.dtos.side.ChartSeriesDto;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.StampEntity;
import su.toor.leases.services.ChartService;
import su.toor.leases.shared.State;

@WebMvcTest(ChartApiController.class)
class ChartApiControllerTest {

    private static final String URL_BASE = "/api/chart";
    private static final String URL_DUMP = URL_BASE + "/dump";
    private static final String URL_NETW = URL_BASE + "/networks";
    private static final String URL_UUID = URL_BASE + "/{uuid}";

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ChartService chartService;

    @Test
    void chartGuides() throws Exception {
        final var guideEntityOne = GuideEntity.with(MASK_ADDR_ONE, 1, "One", "Guide 1");
        final var guideEntityTwo = GuideEntity.with(MASK_ADDR_TWO, 2, "Two", "Guide 2");

        when(chartService.constructBaseGuides())
            .thenReturn(List.of(
                BaseGuideDto.from(guideEntityOne),
                BaseGuideDto.from(guideEntityTwo)
            ));

        mockMvc.perform(
            get(URL_NETW)
        )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", allOf(isA(List.class), hasSize(2))))
            .andExpect(jsonPath("$.[0]", isA(Map.class)))
            .andExpect(jsonPath("$.[0].uuid", jUuidEqual(guideEntityOne.getUuid())))
            .andExpect(jsonPath("$.[0].mask", equalTo(MASK_ONE)))
            .andExpect(jsonPath("$.[0].title", equalTo(guideEntityOne.getTitle())))
            .andExpect(jsonPath("$.[0].description", equalTo(guideEntityOne.getDescription())))
            .andExpect(jsonPath("$.[1]", isA(Map.class)))
            .andExpect(jsonPath("$.[1].uuid", jUuidEqual(guideEntityTwo.getUuid())))
            .andExpect(jsonPath("$.[1].mask", equalTo(MASK_TWO)))
            .andExpect(jsonPath("$.[1].title", equalTo(guideEntityTwo.getTitle())))
            .andExpect(jsonPath("$.[1].description", equalTo(guideEntityTwo.getDescription())))
        ;

        verify(chartService, times(1))
            .constructBaseGuides();
        verifyNoMoreInteractions(chartService);
    }

    @Test
    void chartSeries() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var uuid = UUID.randomUUID();

        final var amount = 1312;
        final var stampEntity = StampEntity.with();

        final var seriesDto = ChartSeriesDto.from(
            State.ACTIVE, Map.of(stampEntity, (long) amount)
        );

        when(chartService.constructChartSeries(wrongUuid))
            .thenReturn(Optional.empty());
        when(chartService.constructChartSeries(uuid))
            .thenReturn(Optional.of(List.of(seriesDto)));

        mockMvc.perform(
            get(URL_UUID, wrongUuid.toString())
        )
            .andDo(print())
            .andExpect(status().isNotFound())
        ;

        mockMvc.perform(
            get(URL_UUID, uuid.toString())
        )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", allOf(isA(List.class), hasSize(1))))
            .andExpect(jsonPath("$.[0]", isA(Map.class)))
            .andExpect(jsonPath("$.[0].name", equalTo(State.ACTIVE.getText())))
            .andExpect(jsonPath("$.[0].data", allOf(isA(List.class), hasSize(1))))
            .andExpect(jsonPath("$.[0].data.[0]", isA(Map.class)))
            .andExpect(jsonPath("$.[0].data.[0].x", equalTo(stampEntity.getValue().toInstant().toEpochMilli())))
            .andExpect(jsonPath("$.[0].data.[0].y", equalTo(amount)))
        ;

        verify(chartService, times(1))
            .constructChartSeries(wrongUuid);
        verify(chartService, times(1))
            .constructChartSeries(uuid);
        verifyNoMoreInteractions(chartService);
    }

    @Test
    void chartDump() throws Exception {
        when(chartService.performChartDump())
            .thenReturn(false)
            .thenReturn(true);

        mockMvc.perform(
                post(URL_DUMP)
            )
            .andDo(print())
            .andExpect(status().isInternalServerError())
        ;

        mockMvc.perform(
                post(URL_DUMP)
            )
            .andDo(print())
            .andExpect(status().isOk())
        ;

        verify(chartService, times(2))
            .performChartDump();
        verifyNoMoreInteractions(chartService);
    }
}
