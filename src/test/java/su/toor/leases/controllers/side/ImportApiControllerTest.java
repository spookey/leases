package su.toor.leases.controllers.side;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.TEXT_PLAIN;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.leases.components.Importer;
import su.toor.leases.database.StampEntity;

@WebMvcTest(ImportApiController.class)
class ImportApiControllerTest {

    private static final String URL_BASE = "/api/import";
    private static final String URL_REDR = "/api/tracks/{uuid}";

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private Importer importer;

    @Test
    void importLeases() throws Exception {
        final var text = "lease\nfile";
        final var stampEntity = StampEntity.with();

        when(importer.run(text))
            .thenReturn(Optional.empty())
            .thenReturn(Optional.of(stampEntity));

        final var redirection = UriComponentsBuilder
            .fromPath(URL_REDR)
            .build(Map.of("uuid", stampEntity.getUuid()));

        mockMvc.perform(
            post(URL_BASE)
                .contentType(TEXT_PLAIN)
                .content(text)
        )
            .andDo(print())
            .andExpect(status().isUnprocessableEntity())
        ;

        mockMvc.perform(
            post(URL_BASE)
                .contentType(TEXT_PLAIN)
                .content(text)
        )
            .andDo(print())
            .andExpect(status().isCreated())
            .andExpect(redirectedUrl(redirection.toString()))
        ;

        verify(importer, times(2))
            .run(text);
        verifyNoMoreInteractions(importer);
    }
}
