package su.toor.leases.controllers.side;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(RouteController.class)
class RouteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void routing() throws Exception {
        mockMvc.perform(
            get("/swagger")
        )
            .andDo(print())
            .andExpect(status().isOk())
        ;

        mockMvc.perform(
            get("/some/page")
        )
            .andDo(print())
            .andExpect(status().isNotFound())
        ;
    }

    @Test
    void redirect() {
        final var controller = new RouteController();
        final var model = controller.redirect("some");

        assertThat(model.hasView()).isTrue();
        assertThat(model.getViewName()).isEqualTo("forward:/");
        assertThat(model.getModel()).isEmpty();
    }
}
