package su.toor.leases.database;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mockStatic;

import java.util.UUID;

import jakarta.persistence.Transient;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;

class AbstractEntityTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final class PhonyEntity extends AbstractEntity<PhonyEntity> {
        @Transient
        @Override
        protected PhonyEntity get() {
            return this;
        }
    }

    @Test
    void fields() {
        final var entity = new PhonyEntity();

        assertThat(entity.getId()).isNull();
        final var uuid = entity.getUuid();
        assertThat(uuid).isNotNull();

        assertThat(entity.toString())
            .isNotBlank()
            .contains("id")
            .contains("uuid")
            .contains(STYLER.style(uuid));
    }

    @Test
    void get() {
        final var entity = new PhonyEntity();

        assertThat(entity.get())
            .isSameAs(entity);
    }

    @Test
    void equals() {
        final var uuidOne = UUID.randomUUID();
        final var uuidTwo = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuidOne)
                .thenReturn(uuidOne)
                .thenReturn(uuidTwo);

            final var entity = new PhonyEntity();

            final var recreated = new PhonyEntity();
            final var different = new PhonyEntity();
            assertThat(entity)
                .hasSameHashCodeAs(recreated)
                .isEqualTo(entity)
                .isEqualTo(recreated)
                .isNotEqualTo(new Object())
                .isNotEqualTo(different);
        }
    }
}
