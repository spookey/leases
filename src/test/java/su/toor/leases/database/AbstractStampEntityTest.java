package su.toor.leases.database;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mockStatic;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

import jakarta.persistence.Transient;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;

class AbstractStampEntityTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final class PhonyEntity extends AbstractStampEntity<PhonyEntity> {
        @Transient
        @Override
        protected PhonyEntity get() {
            return this;
        }
    }

    @Test
    void fields() {
        final var start = OffsetDateTime.now();

        final var entity = new PhonyEntity();

        assertThat(entity.getId()).isNull();
        assertThat(entity.getUuid()).isNotNull();
        assertThat(entity.getCreated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now());
        assertThat(entity.getUpdated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now())
            .isAfterOrEqualTo(entity.getCreated());

        assertThat(entity.toString())
            .isNotBlank()
            .contains("id")
            .contains("uuid")
            .contains("created")
            .contains(STYLER.style(entity.getCreated()))
            .contains("created")
            .contains(STYLER.style(entity.getUpdated()));
    }

    @Test
    void get() {
        final var entity = new PhonyEntity();

        assertThat(entity.get())
            .isSameAs(entity);
    }

    @Test
    void changeTimestamps() {
        final var start = OffsetDateTime.now();

        final var entity = new PhonyEntity();

        final var created = entity.getCreated();
        final var updated = entity.getUpdated();

        assertThat(created)
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now())
            .satisfies(cr -> assertThat(cr.getOffset()).isEqualTo(ZoneOffset.UTC));
        assertThat(updated)
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now())
            .isAfterOrEqualTo(entity.getCreated())
            .satisfies(up -> assertThat(up.getOffset()).isEqualTo(ZoneOffset.UTC));

        assertThat(entity.update())
            .isSameAs(entity);

        assertThat(entity.getCreated())
            .isSameAs(created)
            .satisfies(cr -> assertThat(cr.getOffset()).isEqualTo(ZoneOffset.UTC));

        assertThat(entity.getUpdated())
            .isNotSameAs(updated)
            .isAfter(updated)
            .satisfies(up -> assertThat(up.getOffset()).isEqualTo(ZoneOffset.UTC));
    }

    @Test
    void equals() {
        final var uuidOne = UUID.randomUUID();
        final var uuidTwo = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuidOne)
                .thenReturn(uuidOne)
                .thenReturn(uuidTwo);

            final var entity = new PhonyEntity();

            final var recreated = new PhonyEntity();
            final var different = new PhonyEntity();
            assertThat(entity)
                .hasSameHashCodeAs(recreated)
                .isEqualTo(entity)
                .isEqualTo(recreated)
                .isNotEqualTo(new Object())
                .isNotEqualTo(different);
        }
    }
}
