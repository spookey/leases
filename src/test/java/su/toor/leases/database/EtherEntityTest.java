package su.toor.leases.database;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.Mockito.mockStatic;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.Net.MAC_ONE;

import java.time.OffsetDateTime;
import java.util.UUID;

import inet.ipaddr.mac.MACAddress;
import org.junit.jupiter.api.Test;
import su.toor.leases.shared.RequireException;

class EtherEntityTest {

    @Test
    void createIssues() {
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> EtherEntity.with(null, HOSTNAME));
        assertThatNoException()
            .isThrownBy(() -> EtherEntity.with(MAC_ADDR_TWO, null));
    }

    @Test
    void fields() {
        final var start = OffsetDateTime.now();

        final var entity = EtherEntity.with(MAC_ADDR_ONE, HOSTNAME);

        assertThat(entity.getId()).isNull();
        assertThat(entity.getUuid()).isNotNull();
        assertThat(entity.getCreated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now());
        assertThat(entity.getUpdated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now())
            .isAfterOrEqualTo(entity.getCreated());
        assertThat(entity.getMacValue()).isEqualTo(MAC_ONE);
        assertThat((Iterable<? extends MACAddress>) entity.getMac()).isEqualTo(MAC_ADDR_ONE);
        assertThat(entity.getHostname()).isEqualTo(HOSTNAME);

        assertThat(entity.get())
            .isSameAs(entity);

        assertThat(entity.toString())
            .isNotBlank()
            .contains(MAC_ONE)
            .contains(HOSTNAME);
    }

    @Test
    void changeHostname() {
        final var entity = EtherEntity.with(MAC_ADDR_TWO, HOSTNAME);

        assertThat(entity.getHostname()).isEqualTo(HOSTNAME);

        assertThat(entity.setHostname(null))
            .isSameAs(entity);
        assertThat(entity.getHostname()).isNull();
    }

    @Test
    void equals() {
        final var uuidOne = UUID.randomUUID();
        final var uuidTwo = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuidOne)
                .thenReturn(uuidOne)
                .thenReturn(uuidTwo);

            final var entity = EtherEntity.with(MAC_ADDR_ONE, HOSTNAME);

            final var recreated = EtherEntity.with(MAC_ADDR_ONE, HOSTNAME);
            final var different = EtherEntity.with(MAC_ADDR_TWO, null);
            assertThat(entity)
                .hasSameHashCodeAs(recreated)
                .isEqualTo(entity)
                .isEqualTo(recreated)
                .isNotEqualTo(new Object())
                .isNotEqualTo(different);
        }
    }
}
