package su.toor.leases.database;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.Mockito.mockStatic;
import static su.toor.leases.Net.MASK_ADDR_ONE;
import static su.toor.leases.Net.MASK_ADDR_TWO;
import static su.toor.leases.Net.MASK_ONE;
import static su.toor.leases.Net.MASK_TWO;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.shared.RequireException;

class GuideEntityTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final int WEIGHT = 42;
    private static final String TITLE = "Some title";
    private static final String DESCRIPTION = "Some lengthy description";

    @Test
    void createIssues() {

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> GuideEntity.with(null, WEIGHT, TITLE, DESCRIPTION));
        assertThatNoException()
            .isThrownBy(() -> GuideEntity.with(MASK_ADDR_ONE, null, TITLE, DESCRIPTION));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> GuideEntity.with(MASK_ADDR_ONE, WEIGHT, "\t", DESCRIPTION));
        assertThatNoException()
            .isThrownBy(() -> GuideEntity.with(MASK_ADDR_ONE, WEIGHT, TITLE, null));
    }

    @Test
    void fields() {
        final var start = OffsetDateTime.now();

        final var entity = GuideEntity.with(MASK_ADDR_TWO, WEIGHT, TITLE, DESCRIPTION);

        assertThat(entity.getId()).isNull();
        assertThat(entity.getUuid()).isNotNull();
        assertThat(entity.getCreated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now());
        assertThat(entity.getUpdated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now())
            .isAfterOrEqualTo(entity.getCreated());
        assertThat(entity.getMaskValue()).isEqualTo(MASK_TWO);
        assertThat(entity.getMask()).isEqualTo(MASK_ADDR_TWO);
        assertThat(entity.getWeight()).isEqualTo(WEIGHT);
        assertThat(entity.getTitle()).isEqualTo(TITLE);
        assertThat(entity.getDescription()).isEqualTo(DESCRIPTION);

        assertThat(entity.get())
            .isSameAs(entity);

        assertThat(entity.toString())
            .isNotBlank()
            .contains(MASK_TWO)
            .contains(STYLER.style(WEIGHT))
            .contains(TITLE)
            .contains(DESCRIPTION);
    }

    @Test
    void changeMask() {
        final var entity = GuideEntity.with(MASK_ADDR_ONE, WEIGHT, TITLE, DESCRIPTION);

        assertThat(entity.getMaskValue()).isEqualTo(MASK_ONE);
        assertThat(entity.getMask()).isEqualTo(MASK_ADDR_ONE);

        assertThat(entity.setMask(MASK_ADDR_TWO))
            .isSameAs(entity);
        assertThat(entity.getMaskValue()).isEqualTo(MASK_TWO);
        assertThat(entity.getMask()).isEqualTo(MASK_ADDR_TWO);

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> entity.setMaskValue("\n"));
    }

    @Test
    void changeWeight() {
        final var entity = GuideEntity.with(MASK_ADDR_ONE, null, TITLE, DESCRIPTION);

        assertThat(entity.getWeight()).isEqualTo(Integer.MAX_VALUE);

        assertThat(entity.setWeight(WEIGHT))
            .isSameAs(entity);
        assertThat(entity.getWeight()).isEqualTo(WEIGHT);
    }

    @Test
    void changeTitle() {
        final var title = "Some different title";

        final var entity = GuideEntity.with(MASK_ADDR_TWO, WEIGHT, title, DESCRIPTION);

        assertThat(entity.getTitle()).isEqualTo(title);

        assertThat(entity.setTitle(TITLE))
            .isSameAs(entity);
        assertThat(entity.getTitle()).isEqualTo(TITLE);

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> entity.setTitle("\t"));
    }

    @Test
    void changeDescription() {
        final var entity = GuideEntity.with(MASK_ADDR_ONE, WEIGHT, TITLE, null);

        assertThat(entity.getDescription()).isNull();

        assertThat(entity.setDescription(DESCRIPTION))
            .isSameAs(entity);
        assertThat(entity.getDescription()).isEqualTo(DESCRIPTION);
    }

    @Test
    void equals() {
        final var uuidOne = UUID.randomUUID();
        final var uuidTwo = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuidOne)
                .thenReturn(uuidOne)
                .thenReturn(uuidTwo);

            final var entity = GuideEntity.with(MASK_ADDR_ONE, WEIGHT, TITLE, DESCRIPTION);

            final var recreated = GuideEntity.with(MASK_ADDR_ONE, WEIGHT, TITLE, DESCRIPTION);
            final var different = GuideEntity.with(MASK_ADDR_TWO, null, TITLE, null);
            assertThat(entity)
                .hasSameHashCodeAs(recreated)
                .isEqualTo(entity)
                .isEqualTo(recreated)
                .isNotEqualTo(new Object())
                .isNotEqualTo(different);
        }
    }
}
