package su.toor.leases.database;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mockStatic;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;

class StampEntityTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    @Test
    void fields() {
        final var start = OffsetDateTime.now();

        final var entity = StampEntity.with();

        assertThat(entity.getId()).isNull();
        assertThat(entity.getUuid()).isNotNull();
        final var value = entity.getValue();
        assertThat(value)
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now());

        assertThat(entity.get())
            .isSameAs(entity);

        assertThat(entity.toString())
            .isNotBlank()
            .contains(STYLER.style(value));
    }

    @Test
    void equals() {
        final var uuidOne = UUID.randomUUID();
        final var uuidTwo = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuidOne)
                .thenReturn(uuidOne)
                .thenReturn(uuidTwo);

            final var entity = StampEntity.with();

            final var recreated = StampEntity.with();
            final var different = StampEntity.with();
            assertThat(entity)
                .hasSameHashCodeAs(recreated)
                .isEqualTo(entity)
                .isEqualTo(recreated)
                .isNotEqualTo(new Object())
                .isNotEqualTo(different);
        }
    }
}
