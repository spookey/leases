package su.toor.leases.database;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mockStatic;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.shared.RequireException;
import su.toor.leases.shared.State;

class TrackEntityTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final StampEntity STAMP = StampEntity.with();
    private static final EtherEntity ETHER = EtherEntity.with(MAC_ADDR_TWO, null);
    private static final BoundEntity BOUND = BoundEntity.with(IP_ADDR_ONE);
    private static final State STATE = State.ACTIVE;

    @Test
    void createIssues() {
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> TrackEntity.with(null, ETHER, BOUND, STATE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> TrackEntity.with(STAMP, null, BOUND, STATE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> TrackEntity.with(STAMP, ETHER, null, STATE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> TrackEntity.with(STAMP, ETHER, BOUND, null));
    }

    @Test
    void fields() {
        final var entity = TrackEntity.with(STAMP, ETHER, BOUND, STATE);

        assertThat(entity.getId()).isNull();
        assertThat(entity.getUuid()).isNotNull();
        assertThat(entity.getStampEntity()).isEqualTo(STAMP);
        assertThat(entity.getEtherEntity()).isEqualTo(ETHER);
        assertThat(entity.getBoundEntity()).isEqualTo(BOUND);
        assertThat(entity.getState()).isEqualTo(STATE);

        assertThat(entity.get())
            .isSameAs(entity);

        assertThat(entity.toString())
            .isNotBlank()
            .contains(STYLER.style(STAMP))
            .contains(STYLER.style(ETHER))
            .contains(STYLER.style(BOUND))
            .contains(STYLER.style(STATE));
    }

    @Test
    void equals() {
        final var uuidOne = UUID.randomUUID();
        final var uuidTwo = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuidOne)
                .thenReturn(uuidOne)
                .thenReturn(uuidTwo);

            final var entity = TrackEntity.with(STAMP, ETHER, BOUND, State.ACTIVE);

            final var recreated = TrackEntity.with(STAMP, ETHER, BOUND, State.ACTIVE);
            final var different = TrackEntity.with(STAMP, ETHER, BOUND, State.FREE);
            assertThat(entity)
                .hasSameHashCodeAs(recreated)
                .isEqualTo(entity)
                .isEqualTo(recreated)
                .isNotEqualTo(new Object())
                .isNotEqualTo(different);
        }
    }
}
