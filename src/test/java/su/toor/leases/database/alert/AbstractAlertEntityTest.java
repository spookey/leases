package su.toor.leases.database.alert;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.Mockito.mockStatic;

import java.beans.Transient;
import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import su.toor.leases.shared.RequireException;

class AbstractAlertEntityTest {

    private static final class PhonyEntity extends AbstractAlertEntity<PhonyEntity> {
        @Transient
        @Override
        protected PhonyEntity get() {
            return this;
        }
    }

    @Test
    void fields() {
        final var start = OffsetDateTime.now();

        final var entity = new PhonyEntity();

        assertThat(entity.getId()).isNull();
        assertThat(entity.getUuid()).isNotNull();
        assertThat(entity.getCreated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now());
        assertThat(entity.getUpdated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now())
            .isAfterOrEqualTo(entity.getCreated());
        assertThat(entity.isEnabled())
            .isNull();
        assertThat(entity.getTopic())
            .isNull();
        assertThat(entity.getTemplate())
            .isNull();

        assertThat(entity.toString())
            .isNotBlank()
            .contains("id")
            .contains("uuid")
            .contains("created")
            .contains("updated")
            .contains("enabled")
            .contains("topic")
            .contains("template");
    }

    @Test
    void get() {
        final var entity = new PhonyEntity();

        assertThat(entity.get())
            .isSameAs(entity);
    }

    @Test
    void changeEnabled() {
        final var entity = new PhonyEntity();

        assertThat(entity.isEnabled()).isNull();
        assertThat(entity.setEnabled(false))
            .isSameAs(entity);

        assertThat(entity.isEnabled()).isFalse();

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> entity.setEnabled(null));
    }

    @Test
    void changeTopic() {
        final var topic = "some/topic";
        final var entity = new PhonyEntity();

        assertThat(entity.getTopic()).isNull();
        assertThat(entity.setTopic(topic))
            .isSameAs(entity);

        assertThat(entity.getTopic()).isEqualTo(topic);

        assertThatNoException()
            .isThrownBy(() -> entity.setTopic(null));
    }

    @Test
    void changeTemplate() {
        final var template = "Some template";
        final var entity = new PhonyEntity();

        assertThat(entity.getTemplate()).isNull();
        assertThat(entity.setTemplate(template))
            .isSameAs(entity);

        assertThat(entity.getTemplate()).isEqualTo(template);

        assertThatNoException()
            .isThrownBy(() -> entity.setTemplate(null));
    }

    @Test
    void equals() {
        final var uuidOne = UUID.randomUUID();
        final var uuidTwo = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuidOne)
                .thenReturn(uuidOne)
                .thenReturn(uuidTwo);

            final var entity = new PhonyEntity();

            final var recreated = new PhonyEntity();
            final var different = new PhonyEntity();
            assertThat(entity)
                .hasSameHashCodeAs(recreated)
                .isEqualTo(entity)
                .isEqualTo(recreated)
                .isNotEqualTo(new Object())
                .isNotEqualTo(different);
        }
    }
}
