package su.toor.leases.database.alert;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.Mockito.mockStatic;
import static su.toor.leases.Net.IP_ADDR_ONE;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.shared.RequireException;

class AlertBoundEntityTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final BoundEntity BOUND = BoundEntity.with(IP_ADDR_ONE);
    private static final boolean ENABLED = false;
    private static final String TOPIC = "some/topic";
    private static final String TEMPLATE = "Bound template";

    @Test
    void createIssues() {
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> AlertBoundEntity.with(null, ENABLED, TOPIC, TEMPLATE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> AlertBoundEntity.with(BOUND, null, TOPIC, TEMPLATE));
        assertThatNoException()
            .isThrownBy(() -> AlertBoundEntity.with(BOUND, ENABLED, null, TEMPLATE));
        assertThatNoException()
            .isThrownBy(() -> AlertBoundEntity.with(BOUND, ENABLED, TOPIC, null));
    }

    @Test
    void fields() {
        final var start = OffsetDateTime.now();

        final var entity = AlertBoundEntity.with(BOUND, ENABLED, TOPIC, TEMPLATE);

        assertThat(entity.getId()).isNull();
        assertThat(entity.getUuid()).isNotNull();
        assertThat(entity.getCreated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now());
        assertThat(entity.getUpdated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now())
            .isAfterOrEqualTo(entity.getCreated());
        assertThat(entity.getBoundEntity()).isEqualTo(BOUND);
        assertThat(entity.isEnabled()).isEqualTo(ENABLED);
        assertThat(entity.getTopic()).isEqualTo(TOPIC);
        assertThat(entity.getTemplate()).isEqualTo(TEMPLATE);

        assertThat(entity.get())
            .isSameAs(entity);

        assertThat(entity.toString())
            .isNotBlank()
            .contains(STYLER.style(BOUND))
            .contains(STYLER.style(ENABLED))
            .contains(TOPIC)
            .contains(TEMPLATE);
    }

    @Test
    void changeEnabled() {
        final var entity = AlertBoundEntity.with(BOUND, !ENABLED, TOPIC, TEMPLATE);

        assertThat(entity.isEnabled()).isEqualTo(!ENABLED);

        assertThat(entity.setEnabled(ENABLED))
            .isSameAs(entity);
        assertThat(entity.isEnabled()).isEqualTo(ENABLED);
    }

    @Test
    void changeTopic() {
        final var entity = AlertBoundEntity.with(BOUND, ENABLED, null, TEMPLATE);

        assertThat(entity.getTopic()).isNull();

        assertThat(entity.setTopic(TOPIC))
            .isSameAs(entity);
        assertThat(entity.getTopic()).isEqualTo(TOPIC);

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> entity.setTopic("topic/#"));
    }

    @Test
    void changeTemplate() {
        final var entity = AlertBoundEntity.with(BOUND, ENABLED, TOPIC, null);

        assertThat(entity.getTemplate()).isNull();

        assertThat(entity.setTemplate(TEMPLATE))
            .isSameAs(entity);
        assertThat(entity.getTemplate()).isEqualTo(TEMPLATE);

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> entity.setTemplate("\n"));
    }

    @Test
    void equals() {
        final var uuidOne = UUID.randomUUID();
        final var uuidTwo = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuidOne)
                .thenReturn(uuidOne)
                .thenReturn(uuidTwo);

            final var entity = AlertBoundEntity.with(BOUND, ENABLED, TOPIC, TEMPLATE);

            final var recreated = AlertBoundEntity.with(BOUND, ENABLED, TOPIC, TEMPLATE);
            final var different = AlertBoundEntity.with(BOUND, !ENABLED, TOPIC, TEMPLATE);
            assertThat(entity)
                .hasSameHashCodeAs(recreated)
                .isEqualTo(entity)
                .isEqualTo(recreated)
                .isNotEqualTo(new Object())
                .isNotEqualTo(different);
        }
    }
}
