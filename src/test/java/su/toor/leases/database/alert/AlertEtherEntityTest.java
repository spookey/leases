package su.toor.leases.database.alert;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.Mockito.mockStatic;
import static su.toor.leases.Net.MAC_ADDR_ONE;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.shared.RequireException;

class AlertEtherEntityTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final EtherEntity ETHER = EtherEntity.with(
        MAC_ADDR_ONE, null
    );
    private static final boolean ENABLED = false;
    private static final String TOPIC = "some/topic";
    private static final String TEMPLATE = "Ether template";

    @Test
    void createIssues() {
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> AlertEtherEntity.with(null, ENABLED, TOPIC, TEMPLATE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> AlertEtherEntity.with(ETHER, null, TOPIC, TEMPLATE));
        assertThatNoException()
            .isThrownBy(() -> AlertEtherEntity.with(ETHER, ENABLED, null, TEMPLATE));
        assertThatNoException()
            .isThrownBy(() -> AlertEtherEntity.with(ETHER, ENABLED, TOPIC, null));
    }

    @Test
    void fields() {
        final var start = OffsetDateTime.now();

        final var entity = AlertEtherEntity.with(ETHER, ENABLED, TOPIC, TEMPLATE);

        assertThat(entity.getId()).isNull();
        assertThat(entity.getUuid()).isNotNull();
        assertThat(entity.getCreated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now());
        assertThat(entity.getUpdated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now())
            .isAfterOrEqualTo(entity.getCreated());
        assertThat(entity.getEtherEntity()).isEqualTo(ETHER);
        assertThat(entity.isEnabled()).isEqualTo(ENABLED);
        assertThat(entity.getTopic()).isEqualTo(TOPIC);
        assertThat(entity.getTemplate()).isEqualTo(TEMPLATE);

        assertThat(entity.get())
            .isSameAs(entity);

        assertThat(entity.toString())
            .isNotBlank()
            .contains(STYLER.style(ETHER))
            .contains(STYLER.style(ENABLED))
            .contains(TOPIC)
            .contains(TEMPLATE);
    }

    @Test
    void changeEnabled() {
        final var entity = AlertEtherEntity.with(ETHER, !ENABLED, TOPIC, TEMPLATE);

        assertThat(entity.isEnabled()).isEqualTo(!ENABLED);

        assertThat(entity.setEnabled(ENABLED))
            .isSameAs(entity);
        assertThat(entity.isEnabled()).isEqualTo(ENABLED);
    }

    @Test
    void changeTopic() {
        final var entity = AlertEtherEntity.with(ETHER, ENABLED, null, TEMPLATE);

        assertThat(entity.getTopic()).isNull();

        assertThat(entity.setTopic(TOPIC))
            .isSameAs(entity);
        assertThat(entity.getTopic()).isEqualTo(TOPIC);

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> entity.setTopic("topic/#"));
    }

    @Test
    void changeTemplate() {
        final var entity = AlertEtherEntity.with(ETHER, ENABLED, TOPIC, null);

        assertThat(entity.getTemplate()).isNull();

        assertThat(entity.setTemplate(TEMPLATE))
            .isSameAs(entity);
        assertThat(entity.getTemplate()).isEqualTo(TEMPLATE);

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> entity.setTemplate("\n"));
    }

    @Test
    void equals() {
        final var uuidOne = UUID.randomUUID();
        final var uuidTwo = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuidOne)
                .thenReturn(uuidOne)
                .thenReturn(uuidTwo);

            final var entity = AlertEtherEntity.with(ETHER, ENABLED, TOPIC, TEMPLATE);

            final var recreated = AlertEtherEntity.with(ETHER, ENABLED, TOPIC, TEMPLATE);
            final var different = AlertEtherEntity.with(ETHER, !ENABLED, TOPIC, TEMPLATE);
            assertThat(entity)
                .hasSameHashCodeAs(recreated)
                .isEqualTo(entity)
                .isEqualTo(recreated)
                .isNotEqualTo(new Object())
                .isNotEqualTo(different);
        }
    }
}
