package su.toor.leases.database.alert;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.Mockito.mockStatic;
import static su.toor.leases.Net.MASK_ADDR_ONE;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.shared.RequireException;

class AlertGuideEntityTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final GuideEntity GUIDE = GuideEntity.with(MASK_ADDR_ONE, null, "test", null);
    private static final boolean ENABLED = false;
    private static final String TOPIC = "some/topic";
    private static final String TEMPLATE = "Guide template";

    @Test
    void createIssues() {
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> AlertGuideEntity.with(null, ENABLED, TOPIC, TEMPLATE));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> AlertGuideEntity.with(GUIDE, null, TOPIC, TEMPLATE));
        assertThatNoException()
            .isThrownBy(() -> AlertGuideEntity.with(GUIDE, ENABLED, null, TEMPLATE));
        assertThatNoException()
            .isThrownBy(() -> AlertGuideEntity.with(GUIDE, ENABLED, TOPIC, null));
    }

    @Test
    void fields() {
        final var start = OffsetDateTime.now();

        final var entity = AlertGuideEntity.with(GUIDE, ENABLED, TOPIC, TEMPLATE);

        assertThat(entity.getId()).isNull();
        assertThat(entity.getUuid()).isNotNull();
        assertThat(entity.getCreated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now());
        assertThat(entity.getUpdated())
            .isAfterOrEqualTo(start)
            .isBeforeOrEqualTo(OffsetDateTime.now())
            .isAfterOrEqualTo(entity.getCreated());
        assertThat(entity.getGuideEntity()).isEqualTo(GUIDE);
        assertThat(entity.isEnabled()).isEqualTo(ENABLED);
        assertThat(entity.getTopic()).isEqualTo(TOPIC);
        assertThat(entity.getTemplate()).isEqualTo(TEMPLATE);

        assertThat(entity.get())
            .isSameAs(entity);

        assertThat(entity.toString())
            .isNotBlank()
            .contains(STYLER.style(GUIDE))
            .contains(STYLER.style(ENABLED))
            .contains(TOPIC)
            .contains(TEMPLATE);
    }

    @Test
    void changeEnabled() {
        final var entity = AlertGuideEntity.with(GUIDE, !ENABLED, TOPIC, TEMPLATE);

        assertThat(entity.isEnabled()).isEqualTo(!ENABLED);

        assertThat(entity.setEnabled(ENABLED))
            .isSameAs(entity);
        assertThat(entity.isEnabled()).isEqualTo(ENABLED);
    }

    @Test
    void changeTopic() {
        final var entity = AlertGuideEntity.with(GUIDE, ENABLED, null, TEMPLATE);

        assertThat(entity.getTopic()).isNull();

        assertThat(entity.setTopic(TOPIC))
            .isSameAs(entity);
        assertThat(entity.getTopic()).isEqualTo(TOPIC);

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> entity.setTopic("topic/#"));
    }

    @Test
    void changeTemplate() {
        final var entity = AlertGuideEntity.with(GUIDE, ENABLED, TOPIC, null);

        assertThat(entity.getTemplate()).isNull();

        assertThat(entity.setTemplate(TEMPLATE))
            .isSameAs(entity);
        assertThat(entity.getTemplate()).isEqualTo(TEMPLATE);

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> entity.setTemplate("\n"));
    }

    @Test
    void equals() {
        final var uuidOne = UUID.randomUUID();
        final var uuidTwo = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuidOne)
                .thenReturn(uuidOne)
                .thenReturn(uuidTwo);

            final var entity = AlertGuideEntity.with(GUIDE, ENABLED, TOPIC, TEMPLATE);

            final var recreated = AlertGuideEntity.with(GUIDE, ENABLED, TOPIC, TEMPLATE);
            final var different = AlertGuideEntity.with(GUIDE, !ENABLED, TOPIC, TEMPLATE);
            assertThat(entity)
                .hasSameHashCodeAs(recreated)
                .isEqualTo(entity)
                .isEqualTo(recreated)
                .isNotEqualTo(new Object())
                .isNotEqualTo(different);
        }
    }
}
