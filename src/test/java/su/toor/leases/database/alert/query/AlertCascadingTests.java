package su.toor.leases.database.alert.query;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.Net.MASK_ADDR_ONE;
import static su.toor.leases.Net.MASK_ADDR_TWO;

import org.junit.jupiter.api.Test;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.database.alert.AlertGuideEntity;

class AlertCascadingTests extends AlertReposBase {

    @Test
    void etherAlertEther() {
        final var etherEntityOne = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var etherEntityTwo = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, null));
        final var alertGuideEntityOne = alertEtherRepository.save(AlertEtherEntity.with(etherEntityOne, true, null, null));
        final var alertGuideEntityTwo = alertEtherRepository.save(AlertEtherEntity.with(etherEntityTwo, true, null, null));

        assertThat(etherRepository.findAll())
            .containsExactlyInAnyOrder(etherEntityOne, etherEntityTwo);
        assertThat(alertEtherRepository.findAll())
            .containsExactlyInAnyOrder(alertGuideEntityOne, alertGuideEntityTwo);

        etherRepository.delete(etherEntityTwo);

        assertThat(etherRepository.findAll())
            .containsExactlyInAnyOrder(etherEntityOne);
        assertThat(alertEtherRepository.findAll())
            .containsExactlyInAnyOrder(alertGuideEntityOne);

        alertEtherRepository.delete(alertGuideEntityOne);

        assertThat(etherRepository.findAll())
            .containsExactlyInAnyOrder(etherEntityOne);
        assertThat(alertEtherRepository.findAll())
            .isEmpty();
    }

    @Test
    void boundAlertBound() {
        final var boundEntityOne = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var boundEntityTwo = boundRepository.save(BoundEntity.with(IP_ADDR_TWO));
        final var alertBoundEntityOne = alertBoundRepository.save(AlertBoundEntity.with(boundEntityOne, true, null, null));
        final var alertBoundEntityTwo = alertBoundRepository.save(AlertBoundEntity.with(boundEntityTwo, true, null, null));

        assertThat(boundRepository.findAll())
            .containsExactlyInAnyOrder(boundEntityOne, boundEntityTwo);
        assertThat(alertBoundRepository.findAll())
            .containsExactlyInAnyOrder(alertBoundEntityOne, alertBoundEntityTwo);

        boundRepository.delete(boundEntityTwo);

        assertThat(boundRepository.findAll())
            .containsExactlyInAnyOrder(boundEntityOne);
        assertThat(alertBoundRepository.findAll())
            .containsExactlyInAnyOrder(alertBoundEntityOne);

        alertBoundRepository.delete(alertBoundEntityOne);

        assertThat(boundRepository.findAll())
            .containsExactlyInAnyOrder(boundEntityOne);
        assertThat(alertBoundRepository.findAll())
            .isEmpty();
    }

    @Test
    void guideAlertGuide() {
        final var guideEntityOne = guideRepository.save(GuideEntity.with(MASK_ADDR_ONE, null, "one", null));
        final var guideEntityTwo = guideRepository.save(GuideEntity.with(MASK_ADDR_TWO, null, "two", null));
        final var alertGuideEntityOne = alertGuideRepository.save(AlertGuideEntity.with(guideEntityOne, true, null, null));
        final var alertGuideEntityTwo = alertGuideRepository.save(AlertGuideEntity.with(guideEntityTwo, true, null, null));

        assertThat(guideRepository.findAll())
            .containsExactlyInAnyOrder(guideEntityOne, guideEntityTwo);
        assertThat(alertGuideRepository.findAll())
            .containsExactlyInAnyOrder(alertGuideEntityOne, alertGuideEntityTwo);

        guideRepository.delete(guideEntityTwo);

        assertThat(guideRepository.findAll())
            .containsExactlyInAnyOrder(guideEntityOne);
        assertThat(alertGuideRepository.findAll())
            .containsExactlyInAnyOrder(alertGuideEntityOne);

        alertGuideRepository.delete(alertGuideEntityOne);

        assertThat(guideRepository.findAll())
            .containsExactlyInAnyOrder(guideEntityOne);
        assertThat(alertGuideRepository.findAll())
            .isEmpty();
    }
}
