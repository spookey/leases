package su.toor.leases.database.alert.query;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.Net.MASK_ADDR_ONE;
import static su.toor.leases.Net.MASK_ADDR_TWO;

import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Pageable;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.database.alert.AlertGuideEntity;

class AlertFindManyTest extends AlertReposBase {

    @Test
    void alertEtherFindMany() {
        final var etherEntityOne = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var etherEntityTwo = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, null));
        final var enabled = true;
        final var disabled = false;
        final var alertEtherEntityOne = alertEtherRepository.save(AlertEtherEntity.with(etherEntityOne, enabled, null, null));
        final var alertEtherEntityTwo = alertEtherRepository.save(AlertEtherEntity.with(etherEntityTwo, disabled, null, null));

        assertThat(alertEtherRepository.findAllByOrderByEtherEntity(Pageable.unpaged()))
            .containsExactly(alertEtherEntityOne, alertEtherEntityTwo);

        assertThat(alertEtherRepository.findAllByEnabled(enabled))
            .containsExactlyInAnyOrder(alertEtherEntityOne);
        assertThat(alertEtherRepository.findAllByEnabled(disabled))
            .containsExactlyInAnyOrder(alertEtherEntityTwo);
    }

    @Test
    void alertBoundFindMany() {
        final var boundEntityOne = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var boundEntityTwo = boundRepository.save(BoundEntity.with(IP_ADDR_TWO));
        final var enabled = true;
        final var disabled = false;
        final var alertBoundEntityOne = alertBoundRepository.save(AlertBoundEntity.with(boundEntityOne, enabled, null, null));
        final var alertBoundEntityTwo = alertBoundRepository.save(AlertBoundEntity.with(boundEntityTwo, disabled, null, null));

        assertThat(alertBoundRepository.findAllByOrderByBoundEntity(Pageable.unpaged()))
            .containsExactly(alertBoundEntityOne, alertBoundEntityTwo);

        assertThat(alertBoundRepository.findAllByEnabled(enabled))
            .containsExactlyInAnyOrder(alertBoundEntityOne);
        assertThat(alertBoundRepository.findAllByEnabled(disabled))
            .containsExactlyInAnyOrder(alertBoundEntityTwo);
    }

    @Test
    void alertGuideFindMany() {
        final var guideEntityOne = guideRepository.save(GuideEntity.with(MASK_ADDR_ONE, null, "one", null));
        final var guideEntityTwo = guideRepository.save(GuideEntity.with(MASK_ADDR_TWO, null, "two", null));
        final var enabled = true;
        final var disabled = false;
        final var alertGuideEntityOne = alertGuideRepository.save(AlertGuideEntity.with(guideEntityOne, enabled, null, null));
        final var alertGuideEntityTwo = alertGuideRepository.save(AlertGuideEntity.with(guideEntityTwo, disabled, null, null));

        assertThat(alertGuideRepository.findAllByOrderByGuideEntity(Pageable.unpaged()))
            .containsExactly(alertGuideEntityOne, alertGuideEntityTwo);


        assertThat(alertGuideRepository.findAllByEnabled(enabled))
            .containsExactlyInAnyOrder(alertGuideEntityOne);
        assertThat(alertGuideRepository.findAllByEnabled(disabled))
            .containsExactlyInAnyOrder(alertGuideEntityTwo);
    }
}
