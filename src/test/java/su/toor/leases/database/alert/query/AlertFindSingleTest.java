package su.toor.leases.database.alert.query;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MASK_ADDR_ONE;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.database.alert.AlertGuideEntity;

class AlertFindSingleTest extends AlertReposBase {

    @Test
    void alertEtherFindSingle() {
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var enabled = true;
        final var alertEtherEntity = alertEtherRepository.save(AlertEtherEntity.with(etherEntity, enabled, null, null));

        assertThat(alertEtherRepository.findFirstByUuid(UUID.randomUUID()))
            .isEmpty();
        assertThat(alertEtherRepository.findFirstByUuid(alertEtherEntity.getUuid()))
            .hasValue(alertEtherEntity);
    }

    @Test
    void alertBoundFindSingle() {
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var enabled = true;
        final var alertBoundEntity = alertBoundRepository.save(AlertBoundEntity.with(boundEntity, enabled, null, null));

        assertThat(alertBoundRepository.findFirstByUuid(UUID.randomUUID()))
            .isEmpty();
        assertThat(alertBoundRepository.findFirstByUuid(alertBoundEntity.getUuid()))
            .hasValue(alertBoundEntity);
    }

    @Test
    void alertGuideFindSingle() {
        final var guideEntity = guideRepository.save(GuideEntity.with(MASK_ADDR_ONE, null ,"some", null));
        final var enabled = true;
        final var alertGuideEntity = alertGuideRepository.save(AlertGuideEntity.with(guideEntity, enabled, null, null));

        assertThat(alertGuideRepository.findFirstByUuid(UUID.randomUUID()))
            .isEmpty();
        assertThat(alertGuideRepository.findFirstByUuid(alertGuideEntity.getUuid()))
            .hasValue(alertGuideEntity);
    }
}
