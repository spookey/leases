package su.toor.leases.database.alert.query;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import su.toor.leases.database.alert.AlertBoundRepository;
import su.toor.leases.database.alert.AlertEtherRepository;
import su.toor.leases.database.alert.AlertGuideRepository;
import su.toor.leases.database.query.ReposBase;

abstract class AlertReposBase extends ReposBase {

    @Autowired
    protected AlertEtherRepository alertEtherRepository;
    @Autowired
    protected AlertBoundRepository alertBoundRepository;
    @Autowired
    protected AlertGuideRepository alertGuideRepository;

    @BeforeEach
    public void setup() {
        super.setup();
        alertEtherRepository.deleteAllInBatch();
        alertBoundRepository.deleteAllInBatch();
        alertGuideRepository.deleteAllInBatch();
    }
}
