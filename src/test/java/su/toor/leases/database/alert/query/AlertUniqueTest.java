package su.toor.leases.database.alert.query;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mockStatic;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.Net.MASK_ADDR_ONE;
import static su.toor.leases.Net.MASK_ADDR_TWO;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.dao.DataIntegrityViolationException;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.database.alert.AlertGuideEntity;

class AlertUniqueTest extends AlertReposBase {

    @Test
    void alertEtherUniqueUuid() {
        final var etherEntityOne = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var etherEntityTwo = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, null));

        final var uuid = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuid);

            alertEtherRepository.save(AlertEtherEntity.with(etherEntityOne, true, null, null));

            final var alertEtherEntity = AlertEtherEntity.with(etherEntityTwo, false, null, null);
            assertThatExceptionOfType(DataIntegrityViolationException.class)
                .isThrownBy(() -> alertEtherRepository.save(alertEtherEntity));
        }
    }

    @Test
    void alertBoundUniqueUuid() {
        final var boundEntityOne = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var boundEntityTwo = boundRepository.save(BoundEntity.with(IP_ADDR_TWO));

        final var uuid = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuid);

            alertBoundRepository.save(AlertBoundEntity.with(boundEntityOne, true, null, null));

            final var alertBoundEntity = AlertBoundEntity.with(boundEntityTwo, false, null, null);
            assertThatExceptionOfType(DataIntegrityViolationException.class)
                .isThrownBy(() -> alertBoundRepository.save(alertBoundEntity));
        }
    }

    @Test
    void alertGuideUniqueUuid() {
        final var guideEntityOne = guideRepository.save(GuideEntity.with(MASK_ADDR_ONE, null, "one", null));
        final var guideEntityTwo = guideRepository.save(GuideEntity.with(MASK_ADDR_TWO, null, "two", null));

        final var uuid = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuid);

            alertGuideRepository.save(AlertGuideEntity.with(guideEntityOne, true, null, null));

            final var alertGuideEntity = AlertGuideEntity.with(guideEntityTwo, false, null, null);
            assertThatExceptionOfType(DataIntegrityViolationException.class)
                .isThrownBy(() -> alertGuideRepository.save(alertGuideEntity));
        }
    }
}
