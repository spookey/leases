package su.toor.leases.database.query;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;

import org.junit.jupiter.api.Test;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.shared.State;

class CascadingTests extends ReposBase {

    @Test
    void trackBound() {
        final var stampEntityOne = stampRepository.save(StampEntity.with());
        final var stampEntityTwo = stampRepository.save(StampEntity.with());
        final var etherEntityOne = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var etherEntityTwo = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, null));
        final var boundEntityOne = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var boundEntityTwo = boundRepository.save(BoundEntity.with(IP_ADDR_TWO));
        final var trackEntityOne = trackRepository.save(TrackEntity.with(
            stampEntityOne, etherEntityOne, boundEntityOne, State.FREE
        ));
        final var trackEntityTwo = trackRepository.save(TrackEntity.with(
            stampEntityTwo, etherEntityTwo, boundEntityTwo, State.FREE
        ));

        assertThat(stampRepository.findAll())
            .containsExactlyInAnyOrder(stampEntityOne, stampEntityTwo);
        assertThat(boundRepository.findAll())
            .containsExactlyInAnyOrder(boundEntityOne, boundEntityTwo);
        assertThat(etherRepository.findAll())
            .containsExactlyInAnyOrder(etherEntityOne, etherEntityTwo);
        assertThat(trackRepository.findAll())
            .containsExactlyInAnyOrder(trackEntityOne, trackEntityTwo);

        trackRepository.delete(trackEntityOne);

        assertThat(stampRepository.findAll())
            .containsExactlyInAnyOrder(stampEntityOne, stampEntityTwo);
        assertThat(boundRepository.findAll())
            .containsExactlyInAnyOrder(boundEntityOne, boundEntityTwo);
        assertThat(etherRepository.findAll())
            .containsExactlyInAnyOrder(etherEntityOne, etherEntityTwo);
        assertThat(trackRepository.findAll())
            .containsExactlyInAnyOrder(trackEntityTwo);

        boundRepository.delete(boundEntityTwo);

        assertThat(stampRepository.findAll())
            .containsExactlyInAnyOrder(stampEntityOne, stampEntityTwo);
        assertThat(boundRepository.findAll())
            .containsExactlyInAnyOrder(boundEntityOne);
        assertThat(etherRepository.findAll())
            .containsExactlyInAnyOrder(etherEntityOne, etherEntityTwo);
        assertThat(trackRepository.findAll())
            .isEmpty();
    }

    @Test
    void trackEther() {
        final var stampEntityOne = stampRepository.save(StampEntity.with());
        final var stampEntityTwo = stampRepository.save(StampEntity.with());
        final var etherEntityOne = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var etherEntityTwo = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, null));
        final var boundEntityOne = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var boundEntityTwo = boundRepository.save(BoundEntity.with(IP_ADDR_TWO));
        final var trackEntityOne = trackRepository.save(TrackEntity.with(
            stampEntityOne, etherEntityOne, boundEntityOne, State.FREE
        ));
        final var trackEntityTwo = trackRepository.save(TrackEntity.with(
            stampEntityTwo, etherEntityTwo, boundEntityTwo, State.FREE
        ));

        assertThat(stampRepository.findAll())
            .containsExactlyInAnyOrder(stampEntityOne, stampEntityTwo);
        assertThat(boundRepository.findAll())
            .containsExactlyInAnyOrder(boundEntityOne, boundEntityTwo);
        assertThat(etherRepository.findAll())
            .containsExactlyInAnyOrder(etherEntityOne, etherEntityTwo);
        assertThat(trackRepository.findAll())
            .containsExactlyInAnyOrder(trackEntityOne, trackEntityTwo);

        trackRepository.delete(trackEntityOne);

        assertThat(stampRepository.findAll())
            .containsExactlyInAnyOrder(stampEntityOne, stampEntityTwo);
        assertThat(boundRepository.findAll())
            .containsExactlyInAnyOrder(boundEntityOne, boundEntityTwo);
        assertThat(etherRepository.findAll())
            .containsExactlyInAnyOrder(etherEntityOne, etherEntityTwo);
        assertThat(trackRepository.findAll())
            .containsExactlyInAnyOrder(trackEntityTwo);

        etherRepository.delete(etherEntityTwo);

        assertThat(stampRepository.findAll())
            .containsExactlyInAnyOrder(stampEntityOne, stampEntityTwo);
        assertThat(boundRepository.findAll())
            .containsExactlyInAnyOrder(boundEntityOne, boundEntityTwo);
        assertThat(etherRepository.findAll())
            .containsExactlyInAnyOrder(etherEntityOne);
        assertThat(trackRepository.findAll())
            .isEmpty();
    }

    @Test
    void trackStamp() {
        final var stampEntityOne = stampRepository.save(StampEntity.with());
        final var stampEntityTwo = stampRepository.save(StampEntity.with());
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, null));
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var trackEntityOne = trackRepository.save(TrackEntity.with(
            stampEntityOne, etherEntity, boundEntity, State.ACTIVE
        ));
        final var trackEntityTwo = trackRepository.save(TrackEntity.with(
            stampEntityTwo, etherEntity, boundEntity, State.ACTIVE
        ));

        assertThat(stampRepository.findAll())
            .containsExactlyInAnyOrder(stampEntityOne, stampEntityTwo);
        assertThat(boundRepository.findAll())
            .containsExactlyInAnyOrder(boundEntity);
        assertThat(etherRepository.findAll())
            .containsExactlyInAnyOrder(etherEntity);
        assertThat(trackRepository.findAll())
            .containsExactlyInAnyOrder(trackEntityOne, trackEntityTwo);

        trackRepository.delete(trackEntityOne);

        assertThat(stampRepository.findAll())
            .containsExactlyInAnyOrder(stampEntityOne, stampEntityTwo);
        assertThat(boundRepository.findAll())
            .containsExactlyInAnyOrder(boundEntity);
        assertThat(etherRepository.findAll())
            .containsExactlyInAnyOrder(etherEntity);
        assertThat(trackRepository.findAll())
            .containsExactlyInAnyOrder(trackEntityTwo);

        stampRepository.delete(stampEntityTwo);

        assertThat(stampRepository.findAll())
            .containsExactlyInAnyOrder(stampEntityOne);
        assertThat(boundRepository.findAll())
            .containsExactlyInAnyOrder(boundEntity);
        assertThat(etherRepository.findAll())
            .containsExactlyInAnyOrder(etherEntity);
        assertThat(trackRepository.findAll())
            .isEmpty();
    }
}
