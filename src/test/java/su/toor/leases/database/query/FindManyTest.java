package su.toor.leases.database.query;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;

import java.time.OffsetDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Pageable;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.shared.State;

class FindManyTest extends ReposBase {

    @Test
    void etherFindMany() {
        final var etherEntityOne = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, "ZZZ"));
        final var etherEntityTwo = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, "AAA"));

        assertThat(etherRepository.findAllByOrderByHostnameAscMacValueAsc(Pageable.unpaged()))
            .containsExactly(etherEntityTwo, etherEntityOne);
    }

    @Test
    void boundFindMany() {
        final var boundEntityOne = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var boundEntityTwo = boundRepository.save(BoundEntity.with(IP_ADDR_TWO));

        assertThat(boundRepository.findAllByOrderByIpValueAsc(Pageable.unpaged()))
            .containsExactly(boundEntityTwo, boundEntityOne);
    }

    @Test
    void stampFindMany() {
        final var stampEntityNil = stampRepository.save(StampEntity.with());
        final var stampEntityOne = stampRepository.save(StampEntity.with());
        final var mark = OffsetDateTime.now();
        final var stampEntityTwo = stampRepository.save(StampEntity.with());

        assertThat(stampRepository.findFirstByOrderByValueDesc())
            .hasValue(stampEntityTwo);

        assertThat(stampRepository.findAllByValueBefore(mark))
            .containsExactlyInAnyOrder(stampEntityNil, stampEntityOne);
        assertThat(stampRepository.findAllByValueBefore(OffsetDateTime.now()))
            .containsExactlyInAnyOrder(stampEntityNil, stampEntityOne, stampEntityTwo);

        assertThat(stampRepository.findAllByOrderByValueAsc(Pageable.unpaged()))
            .containsExactly(stampEntityNil, stampEntityOne, stampEntityTwo);
    }

    @Test
    void trackFindMany() {
        final var stampEntityNil = stampRepository.save(StampEntity.with());
        final var stampEntityOne = stampRepository.save(StampEntity.with());
        final var stampEntityTwo = stampRepository.save(StampEntity.with());
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_TWO));
        final var stateActive = State.ACTIVE;
        final var stateFree = State.FREE;

        final var trackEntityStampOneFreeOne = trackRepository.save(TrackEntity.with(
            stampEntityOne, etherEntity, boundEntity, stateFree
        ));
        final var trackEntityStampOneFreeTwo = trackRepository.save(TrackEntity.with(
            stampEntityOne, etherEntity, boundEntity, stateFree
        ));
        final var trackEntityStampTwoFree = trackRepository.save(TrackEntity.with(
            stampEntityTwo, etherEntity, boundEntity, stateFree
        ));
        final var trackEntityStampOneActiveOne = trackRepository.save(TrackEntity.with(
            stampEntityOne, etherEntity, boundEntity, stateActive
        ));
        final var trackEntityStampOneActiveTwo = trackRepository.save(TrackEntity.with(
            stampEntityOne, etherEntity, boundEntity, stateActive
        ));
        final var trackEntityStampTwoActive = trackRepository.save(TrackEntity.with(
            stampEntityTwo, etherEntity, boundEntity, stateActive
        ));

        assertThat(trackRepository.findAllBy())
            .containsExactlyInAnyOrder(
                trackEntityStampOneActiveOne,
                trackEntityStampOneActiveTwo,
                trackEntityStampTwoActive,
                trackEntityStampOneFreeOne,
                trackEntityStampOneFreeTwo,
                trackEntityStampTwoFree
            );

        assertThat(trackRepository.findAllByOrderByStateAsc(Pageable.unpaged()))
            .containsExactly(
                trackEntityStampOneActiveOne,
                trackEntityStampOneActiveTwo,
                trackEntityStampTwoActive,
                trackEntityStampOneFreeOne,
                trackEntityStampOneFreeTwo,
                trackEntityStampTwoFree
            );

        assertThat(trackRepository.findAllByStampEntityOrderByStateAsc(stampEntityNil, Pageable.unpaged()))
            .isEmpty();
        assertThat(trackRepository.findAllByStampEntityOrderByStateAsc(stampEntityOne, Pageable.unpaged()))
            .containsExactly(
                trackEntityStampOneActiveOne,
                trackEntityStampOneActiveTwo,
                trackEntityStampOneFreeOne,
                trackEntityStampOneFreeTwo
            );
        assertThat(trackRepository.findAllByStampEntityOrderByStateAsc(stampEntityTwo, Pageable.unpaged()))
            .containsExactly(trackEntityStampTwoActive, trackEntityStampTwoFree);
    }

    @Test
    void guideFindMany() {
        final var guideEntityNil = guideRepository.save(GuideEntity.with(
            IP_ADDR_ONE, 23, "AAA", null
        ));
        final var guideEntityOne = guideRepository.save(GuideEntity.with(
            IP_ADDR_ONE, null, "BBB", null
        ));
        final var guideEntityTwo = guideRepository.save(GuideEntity.with(
            IP_ADDR_TWO, 23, "ZZZ", null
        ));

        assertThat(guideRepository.findAllByOrderByWeightAscTitleAsc())
            .containsExactly(guideEntityNil, guideEntityTwo, guideEntityOne);

        assertThat(guideRepository.findAllByOrderByWeightAscTitleAsc(Pageable.unpaged()))
            .containsExactly(guideEntityNil, guideEntityTwo, guideEntityOne);
    }
}
