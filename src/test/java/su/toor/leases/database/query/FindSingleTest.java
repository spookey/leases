package su.toor.leases.database.query;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.IP_ONE;
import static su.toor.leases.Net.IP_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.Net.MAC_ONE;
import static su.toor.leases.Net.MAC_TWO;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.shared.State;

class FindSingleTest extends ReposBase {

    @Test
    void stampFindSingle() {
        final var stampEntity = stampRepository.save(StampEntity.with());

        assertThat(stampRepository.findFirstByUuid(UUID.randomUUID()))
            .isEmpty();
        assertThat(stampRepository.findFirstByUuid(stampEntity.getUuid()))
            .hasValue(stampEntity);
    }

    @Test
    void etherFindSingle() {
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, HOSTNAME));

        assertThat(etherRepository.findFirstByUuid(UUID.randomUUID()))
            .isEmpty();
        assertThat(etherRepository.findFirstByUuid(etherEntity.getUuid()))
            .hasValue(etherEntity);

        assertThat(etherRepository.findFirstByMac(null))
            .isEmpty();
        assertThat(etherRepository.findFirstByMac(MAC_ADDR_ONE))
            .hasValue(etherEntity);
        assertThat(etherRepository.findFirstByMac(MAC_ADDR_TWO))
            .isEmpty();
        assertThat(etherRepository.findFirstByMacValue(MAC_ONE))
            .hasValue(etherEntity);
        assertThat(etherRepository.findFirstByMacValue(MAC_TWO))
            .isEmpty();
    }

    @Test
    void boundFindSingle() {
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));

        assertThat(boundRepository.findFirstByUuid(UUID.randomUUID()))
            .isEmpty();
        assertThat(boundRepository.findFirstByUuid(boundEntity.getUuid()))
            .hasValue(boundEntity);

        assertThat(boundRepository.findFirstByIp(null))
            .isEmpty();
        assertThat(boundRepository.findFirstByIp(IP_ADDR_ONE))
            .hasValue(boundEntity);
        assertThat(boundRepository.findFirstByIp(IP_ADDR_TWO))
            .isEmpty();
        assertThat(boundRepository.findFirstByIpValue(IP_ONE))
            .hasValue(boundEntity);
        assertThat(boundRepository.findFirstByIpValue(IP_TWO))
            .isEmpty();
    }

    @Test
    void trackFindSingle() {
        final var stampEntity = stampRepository.save(StampEntity.with());
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, null));
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var trackEntity = trackRepository.save(TrackEntity.with(
            stampEntity, etherEntity, boundEntity, State.ACTIVE
        ));

        assertThat(trackRepository.findFirstByUuid(UUID.randomUUID()))
            .isEmpty();
        assertThat(trackRepository.findFirstByUuid(trackEntity.getUuid()))
            .hasValue(trackEntity);

        assertThat(trackRepository.findFirstByStampEntityAndEtherEntityAndStateIsNot(stampEntity, etherEntity, State.FREE))
            .hasValue(trackEntity);
        assertThat(trackRepository.findFirstByStampEntityAndEtherEntityAndStateIsNot(stampEntity, etherEntity, State.ACTIVE))
            .isEmpty();

        assertThat(trackRepository.findFirstByStampEntityAndBoundEntityAndStateIsNot(stampEntity, boundEntity, State.FREE))
            .hasValue(trackEntity);
        assertThat(trackRepository.findFirstByStampEntityAndBoundEntityAndStateIsNot(stampEntity, boundEntity, State.ACTIVE))
            .isEmpty();
    }

    @Test
    void guideFindSingle() {
        final var guideEntity = guideRepository.save(GuideEntity.with(
            IP_ADDR_ONE, null, "test", null
        ));

        assertThat(guideRepository.findFirstByUuid(UUID.randomUUID()))
            .isEmpty();
        assertThat(guideRepository.findFirstByUuid(guideEntity.getUuid()))
            .hasValue(guideEntity);
    }
}
