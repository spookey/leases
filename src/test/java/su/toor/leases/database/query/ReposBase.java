package su.toor.leases.database.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.leases.database.BoundRepository;
import su.toor.leases.database.EtherRepository;
import su.toor.leases.database.GuideRepository;
import su.toor.leases.database.StampRepository;
import su.toor.leases.database.TrackRepository;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public abstract class ReposBase {

    @Autowired
    protected EtherRepository etherRepository;
    @Autowired
    protected BoundRepository boundRepository;
    @Autowired
    protected StampRepository stampRepository;
    @Autowired
    protected TrackRepository trackRepository;
    @Autowired
    protected GuideRepository guideRepository;

    @BeforeEach
    public void setup() {
        etherRepository.deleteAllInBatch();
        boundRepository.deleteAllInBatch();
        stampRepository.deleteAllInBatch();
        trackRepository.deleteAllInBatch();
        guideRepository.deleteAllInBatch();
    }
}
