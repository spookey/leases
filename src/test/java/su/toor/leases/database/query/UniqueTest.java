package su.toor.leases.database.query;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.dao.DataIntegrityViolationException;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.shared.State;

class UniqueTest extends ReposBase {

    @Test
    void etherUniqueUuid() {
        final var uuid = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuid);

            etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, HOSTNAME));

            final var etherEntity = EtherEntity.with(MAC_ADDR_TWO, HOSTNAME);
            assertThatExceptionOfType(DataIntegrityViolationException.class)
                .isThrownBy(() -> etherRepository.save(etherEntity));
        }
    }

    @Test
    void etherUniqueMac() {
        etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, HOSTNAME));

        final var etherEntity = EtherEntity.with(MAC_ADDR_TWO, HOSTNAME);
        assertThatExceptionOfType(DataIntegrityViolationException.class)
            .isThrownBy(() -> etherRepository.save(etherEntity));
    }

    @Test
    void boundUniqueUuid() {
        final var uuid = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuid);

            boundRepository.save(BoundEntity.with(IP_ADDR_ONE));

            final var boundEntity = BoundEntity.with(IP_ADDR_TWO);
            assertThatExceptionOfType(DataIntegrityViolationException.class)
                .isThrownBy(() -> boundRepository.save(boundEntity));
        }
    }

    @Test
    void boundUniqueIp() {
        boundRepository.save(BoundEntity.with(IP_ADDR_TWO));

        final var boundEntity = BoundEntity.with(IP_ADDR_TWO);
        assertThatExceptionOfType(DataIntegrityViolationException.class)
            .isThrownBy(() -> boundRepository.save(boundEntity));
    }

    @Test
    void stampUniqueUuid() {
        final var uuid = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuid);

            stampRepository.save(StampEntity.with());

            final var stampEntity = StampEntity.with();
            assertThatExceptionOfType(DataIntegrityViolationException.class)
                .isThrownBy(() -> stampRepository.save(stampEntity));
        }
    }

    @Test
    void stampUniqueValue() {
        final var stamp = OffsetDateTime.now();
        try (final var offsetDateTimeMock = mockStatic(OffsetDateTime.class)) {
            offsetDateTimeMock.when(() -> OffsetDateTime.now(any(ZoneId.class)))
                .thenReturn(stamp);

            stampRepository.save(StampEntity.with());

            final var stampEntity = StampEntity.with();
            assertThatExceptionOfType(DataIntegrityViolationException.class)
                .isThrownBy(() -> stampRepository.save(stampEntity));
        }
    }

    @Test
    void trackUniqueUuid() {
        final var stampEntity = stampRepository.save(StampEntity.with());
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, null));
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));

        final var uuid = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuid);

            trackRepository.save(TrackEntity.with(
                stampEntity, etherEntity, boundEntity, State.ACTIVE
            ));

            final var trackEntity = TrackEntity.with(
                stampEntity, etherEntity, boundEntity, State.FREE
            );
            assertThatExceptionOfType(DataIntegrityViolationException.class)
                .isThrownBy(() -> trackRepository.save(trackEntity));
        }
    }

    @Test
    void guideUniqueUuid() {
        final var uuid = UUID.randomUUID();
        try (final var uuidMock = mockStatic(UUID.class)) {
            uuidMock.when(UUID::randomUUID)
                .thenReturn(uuid);

            guideRepository.save(GuideEntity.with(IP_ADDR_ONE, null, "A", "Aaa"));

            final var guideEntity = GuideEntity.with(IP_ADDR_TWO, null, "B", "Bbb");
            assertThatExceptionOfType(DataIntegrityViolationException.class)
                .isThrownBy(() -> guideRepository.save(guideEntity));
        }
    }
}
