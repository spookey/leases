package su.toor.leases.parse;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.IP_ONE;
import static su.toor.leases.Net.IP_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.Net.MAC_ONE;
import static su.toor.leases.Net.MAC_TWO;

import inet.ipaddr.IPAddress;
import inet.ipaddr.mac.MACAddress;
import org.junit.jupiter.api.Test;
import su.toor.leases.shared.RequireException;
import su.toor.leases.shared.State;

class LeaseConverterTest {

    private static final State STATE = State.FREE;

    @Test
    void conversion() {
        final var ipAddress = LeaseConverter.formatAddress(IP_ADDR_ONE);
        assertThat(ipAddress).isEqualTo(IP_ONE);
        assertThat(LeaseConverter.convertIpAddress(ipAddress)).isEqualTo(IP_ADDR_ONE);

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> LeaseConverter.formatAddress((IPAddress) null));

        final var macAddress = LeaseConverter.formatAddress(MAC_ADDR_ONE);
        assertThat(macAddress).isEqualTo(MAC_ONE);
        assertThat((Iterable<? extends MACAddress>) LeaseConverter.convertMacAddress(macAddress)).isEqualTo(MAC_ADDR_ONE);

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> LeaseConverter.formatAddress((MACAddress) null));
    }

    @Test
    void empty() {
        final var converter = LeaseConverter.get();
        assertThat(converter.hasIpAddress()).isFalse();
        assertThat(converter.hasMacAddress()).isFalse();
        assertThat(converter.hasState()).isFalse();
        assertThat(converter.hasHostname()).isFalse();
        assertThat(converter.isComplete()).isFalse();
    }

    @Test
    void missing() {
        final var converter = LeaseConverter.get();
        converter.consumeHostname(HOSTNAME);

        assertThat(converter.build()).isEmpty();
        assertThat(converter.isComplete()).isFalse();
    }

    @Test
    void strings() {
        final var converter = LeaseConverter.get()
            .consumeIpAddress(String.format(" %s ", IP_TWO))
            .consumeMacAddress(String.format(" %s ", MAC_TWO))
            .consumeState(String.format(" %s ", STATE.name()))
            .consumeHostname(String.format(" \" %s \" ", HOSTNAME));

        assertThat(converter.isComplete()).isTrue();

        assertThat(converter.build())
            .hasValueSatisfying(lease -> {
                assertThat(lease.getIpAddress()).isEqualTo(IP_ADDR_TWO);
                assertThat((Iterable<? extends MACAddress>) lease.getMacAddress()).isEqualTo(MAC_ADDR_TWO);
                assertThat(lease.getState()).isEqualTo(STATE);
                assertThat(lease.getHostname()).isEqualTo(HOSTNAME);
            });
    }

    @Test
    void complete() {
        final var converter = LeaseConverter.get()
            .consumeIpAddress(IP_ONE)
            .consumeMacAddress(MAC_ONE)
            .consumeState(STATE.name())
            .consumeHostname(HOSTNAME);

        assertThat(converter.hasIpAddress()).isTrue();
        assertThat(converter.hasMacAddress()).isTrue();
        assertThat(converter.hasState()).isTrue();
        assertThat(converter.hasHostname()).isTrue();
        assertThat(converter.isComplete()).isTrue();

        assertThat(converter.build())
            .hasValueSatisfying(lease -> {
                assertThat(lease.getIpAddress()).isEqualTo(IP_ADDR_ONE);
                assertThat((Iterable<? extends MACAddress>) lease.getMacAddress()).isEqualTo(MAC_ADDR_ONE);
                assertThat(lease.getState()).isEqualTo(STATE);
                assertThat(lease.getHostname()).isEqualTo(HOSTNAME);
            });

        converter.reset();

        assertThat(converter.hasIpAddress()).isFalse();
        assertThat(converter.hasMacAddress()).isFalse();
        assertThat(converter.hasState()).isFalse();
        assertThat(converter.hasHostname()).isFalse();
        assertThat(converter.isComplete()).isFalse();
    }
}
