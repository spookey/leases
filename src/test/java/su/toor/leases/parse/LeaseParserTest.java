package su.toor.leases.parse;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.IP_ONE;
import static su.toor.leases.Net.IP_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.Net.MAC_ONE;
import static su.toor.leases.Net.MAC_TWO;

import java.util.List;
import java.util.stream.Stream;

import inet.ipaddr.mac.MACAddress;
import org.junit.jupiter.api.Test;
import su.toor.leases.shared.State;

class LeaseParserTest {

    @Test
    void lineInclude() {
        Stream.of(
            "", "\t", "  ", "#", "# comment", " # comment "
        ).forEach(line -> assertThat(LeaseParser.lineInclude(line)).isFalse());

        assertThat(LeaseParser.lineInclude(null)).isFalse();
        assertThat(LeaseParser.lineInclude("no comment")).isTrue();
    }

    @Test
    void extract() {
        assertThat(LeaseParser.extract(null,"", ""))
            .isNull();
        assertThat(LeaseParser.extract("text",null, "t"))
            .isNull();
        assertThat(LeaseParser.extract("text","t", null))
            .isNull();

        assertThat(LeaseParser.extract("public static void main", "public", "main"))
            .isEqualTo("static void");
        assertThat(LeaseParser.extract("return 0;", "return"))
            .isEqualTo("0");
    }

    @Test
    void parseEmpty() {
        final var result = LeaseParser.parse(List.of());
        assertThat(result).isEmpty();
    }

    @Test
    void parseIrrelevant() {
        final var content = """
# this is a comment
# another comment

top-level-setting: present;
            """.strip();

        final var result = LeaseParser.parse(content.lines().toList());
        assertThat(result).isEmpty();
    }

    @Test
    void parseInvalidData() {
        final var content = """
lease BANANA {
  hardware ethernet fruity;
  binding state yellow;
}
            """.strip();

        final var result = LeaseParser.parse(content.lines().toList());
        assertThat(result).isEmpty();
    }

    @Test
    void parseLastOneIsActiveOne() {
        final var stateOne = State.FREE;
        final var stateTwo = State.ACTIVE;

        final var content = """
lease %ADDR% {
    hardware ethernet %ETH1%;
    binding state %STA1%;
}
lease %ADDR% {
    hardware ethernet %ETH2%;
    binding state %STA2%;
}
            """
            .replace("%ADDR%", IP_TWO)
            .replace("%ETH1%", MAC_ONE)
            .replace("%STA1%", stateOne.name().toLowerCase())
            .replace("%ETH2%", MAC_TWO)
            .replace("%STA2%", stateTwo.name().toLowerCase())
            .strip();

        final var result = LeaseParser.parse(content.lines().toList());
        assertThat(result).hasSize(1);

        final var lease = result.get(0);
        assertThat(lease.getIpAddress()).isEqualTo(IP_ADDR_TWO);
        assertThat((Iterable<? extends MACAddress>) lease.getMacAddress()).isEqualTo(MAC_ADDR_TWO);
        assertThat(lease.getState()).isEqualTo(stateTwo);
        assertThat(lease.getHostname()).isNull();
    }

    @Test
    void parseFull() {
        final var stateOne = State.FREE;
        final var hostnameOne = "computer";
        final var stateTwo = State.ACTIVE;
        final var hostnameTwo = "machine";

        final var content = """
lease %ADR1% {
  hardware ethernet %ETH1%;
  binding state %STA1%;
  client-hostname "%HSN1%";
}
lease %ADR2% {
  client-hostname "%HSN2%";
  binding state %STA2%;
  hardware ethernet %ETH2%;
}
            """
            .replace("%ADR1%", IP_ONE)
            .replace("%ETH1%", MAC_ONE)
            .replace("%STA1%", stateOne.name().toLowerCase())
            .replace("%HSN1%", hostnameOne)
            .replace("%ADR2%", IP_TWO)
            .replace("%ETH2%", MAC_TWO)
            .replace("%STA2%", stateTwo.name().toLowerCase())
            .replace("%HSN2%", hostnameTwo)
            .strip();

        final var result = LeaseParser.parse(content.lines().toList());
        assertThat(result).hasSize(2);

        final var leaseOne = result.get(0);
        assertThat(leaseOne.getIpAddress()).isEqualTo(IP_ADDR_ONE);
        assertThat((Iterable<? extends MACAddress>) leaseOne.getMacAddress()).isEqualTo(MAC_ADDR_ONE);
        assertThat(leaseOne.getState()).isEqualTo(stateOne);
        assertThat(leaseOne.getHostname()).isEqualTo(hostnameOne);

        final var leaseTwo = result.get(1);
        assertThat(leaseTwo.getIpAddress()).isEqualTo(IP_ADDR_TWO);
        assertThat((Iterable<? extends MACAddress>) leaseTwo.getMacAddress()).isEqualTo(MAC_ADDR_TWO);
        assertThat(leaseTwo.getState()).isEqualTo(stateTwo);
        assertThat(leaseTwo.getHostname()).isEqualTo(hostnameTwo);
    }
}
