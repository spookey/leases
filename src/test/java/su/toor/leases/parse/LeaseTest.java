package su.toor.leases.parse;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.IP_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.Net.MAC_TWO;

import inet.ipaddr.mac.MACAddress;
import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.leases.shared.RequireException;
import su.toor.leases.shared.State;

class LeaseTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final State STATE = State.ACTIVE;

    @Test
    void createInvalid() {
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new Lease(null, MAC_ADDR_ONE, STATE, HOSTNAME));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new Lease(IP_ADDR_ONE, null, STATE, HOSTNAME));
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> new Lease(IP_ADDR_ONE, MAC_ADDR_ONE, null, HOSTNAME));
        assertThatNoException()
            .isThrownBy(() -> new Lease(IP_ADDR_ONE, MAC_ADDR_ONE, STATE, null));
    }

    @Test
    void fields() {
        final var lease = new Lease(IP_ADDR_TWO, MAC_ADDR_TWO, STATE, HOSTNAME);

        assertThat(lease.getIpAddress()).isEqualTo(IP_ADDR_TWO);
        assertThat((Iterable<? extends MACAddress>) lease.getMacAddress()).isEqualTo(MAC_ADDR_TWO);
        assertThat(lease.getState()).isEqualTo(STATE);
        assertThat(lease.getHostname()).isEqualTo(HOSTNAME);

        assertThat(lease.toString())
            .isNotBlank()
            .contains(IP_TWO)
            .contains(MAC_TWO)
            .contains(STYLER.style(STATE))
            .contains(HOSTNAME);
    }

    @Test
    void equals() {
        final var lease = new Lease(IP_ADDR_ONE, MAC_ADDR_ONE, STATE, HOSTNAME);

        final var recreated = new Lease(IP_ADDR_ONE, MAC_ADDR_ONE, STATE, HOSTNAME);
        final var different = new Lease(IP_ADDR_TWO, MAC_ADDR_ONE, State.BACKUP, null);
        assertThat(lease)
            .hasSameHashCodeAs(recreated)
            .isEqualTo(lease)
            .isEqualTo(recreated)
            .isNotEqualTo(new Object())
            .isNotEqualTo(different);
    }
}
