package su.toor.leases.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.file.Files;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.leases.TempDirBase;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.configs.spring.JsonMapperConfiguration;
import su.toor.leases.services.query.EntityService;

@ExtendWith(SpringExtension.class)
@Import(JsonMapperConfiguration.class)
class ChartServiceDumpTest extends TempDirBase {

    @MockBean
    private EntityService entityService;
    @Autowired
    private JsonMapper jsonMapper;

    private ChartService create(final JsonMapper jsonMapper) {
        return new ChartService(
            new BasicConfig()
                .setDumpLocation(base),
            entityService,
            jsonMapper
        );
    }

    private ChartService create() {
        return create(jsonMapper);
    }

    private static final class AccessibleJsonProcessingException extends JsonProcessingException {
        public AccessibleJsonProcessingException(final String message) {
            super(message);
        }
    }

    @Test
    void dumpToDiskJsonIssue() throws JsonProcessingException {
        final var target = base.resolve("none");
        final var content = "text";

        final var jsonMapper = mock(JsonMapper.class);
        when(jsonMapper.writeValueAsString(content))
            .thenThrow(new AccessibleJsonProcessingException("test"));

        final var service = create(jsonMapper);
        assertThat(service.dumpToDisk(target, content))
            .isFalse();

        verify(jsonMapper, times(1))
            .writeValueAsString(content);
        verifyNoMoreInteractions(jsonMapper);
    }

    @Test
    void dumpToDiskIOIssue() {
        final var target = base.resolve("none");
        final var content = "text";

        try (final var mockedFiles = mockStatic(Files.class)) {
            mockedFiles.when(() -> Files.writeString(eq(target), anyString(), any()))
                .thenThrow(new IOException("test"));

            final var service = create();
            assertThat(service.dumpToDisk(target, content))
                .isFalse();

            mockedFiles.verify(() -> Files.writeString(eq(target), anyString(), any()), times(1));
        }
    }

    @Test
    void dumpToDisk() throws JsonProcessingException {
        final var target = base.resolve("some");
        final var content = "text";

        assertThat(target)
            .doesNotExist();

        final var service = create();
        assertThat(service.dumpToDisk(target, content))
            .isTrue();

        assertThat(target)
            .exists()
            .content()
            .isEqualTo(jsonMapper.writeValueAsString(content));
    }
}
