package su.toor.leases.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.IP_ONE;
import static su.toor.leases.Net.IP_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MASK_ADDR_ONE;
import static su.toor.leases.Net.MASK_ADDR_TWO;

import java.nio.file.Path;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;

import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.configs.spring.JsonMapperConfiguration;
import su.toor.leases.controllers.dtos.side.BaseGuideDto;
import su.toor.leases.controllers.dtos.side.ChartSeriesDto;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.services.query.EntityService;
import su.toor.leases.shared.State;

@ExtendWith(SpringExtension.class)
@Import(JsonMapperConfiguration.class)
class ChartServiceTest {

    @MockBean
    private EntityService entityService;

    private ChartService create() {
        return new ChartService(
            new BasicConfig()
                .setHiddenState(true),
            entityService,
            mock(JsonMapper.class)
        );
    }

    @Test
    void constructBaseGuides() {
        final var guideEntityOne = GuideEntity.with(IP_ADDR_ONE, null, "One", "First");
        final var guideEntityTwo = GuideEntity.with(IP_ADDR_TWO, null, "Two", "Second");
        when(entityService.findGuides())
            .thenReturn(List.of(guideEntityOne, guideEntityTwo));

        final var service = create();
        final var result = service.constructBaseGuides();
        assertThat(result)
            .hasSize(2);

        final var one = result.get(0);
        assertThat(one.getUuid()).isEqualTo(guideEntityOne.getUuid());
        assertThat(one.getMask()).isEqualTo(IP_ONE);
        assertThat(one.getTitle()).isEqualTo(guideEntityOne.getTitle());
        assertThat(one.getDescription()).isEqualTo(guideEntityOne.getDescription());

        final var two = result.get(1);
        assertThat(two.getUuid()).isEqualTo(guideEntityTwo.getUuid());
        assertThat(two.getMask()).isEqualTo(IP_TWO);
        assertThat(two.getTitle()).isEqualTo(guideEntityTwo.getTitle());
        assertThat(two.getDescription()).isEqualTo(guideEntityTwo.getDescription());
    }

    @Test
    void constructChartSeriesEmpty() {
        final var wrongUuid = UUID.randomUUID();
        final var guideEntity = GuideEntity.with(IP_ADDR_TWO, null, "fake", null);

        when(entityService.findGuide(wrongUuid))
            .thenReturn(Optional.empty());
        when(entityService.findGuide(guideEntity.getUuid()))
            .thenReturn(Optional.of(guideEntity));

        when(entityService.findTracks())
            .thenReturn(Set.of());

        final var service = create();
        assertThat(service.constructChartSeries(wrongUuid))
            .isEmpty();

        assertThat(service.constructChartSeries(guideEntity.getUuid()))
            .hasValue(List.of(
                ChartSeriesDto.from(State.ACTIVE, Map.of()),
                ChartSeriesDto.from(State.BACKUP, Map.of()),
                ChartSeriesDto.from(State.FREE, Map.of())
            ));

        verify(entityService, times(1))
            .findGuide(wrongUuid);
        verify(entityService, times(1))
            .findGuide(guideEntity.getUuid());
        verify(entityService, times(1))
            .findTracks();
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void constructChartSeries() {
        final Function<Long, StampEntity> make = (seconds) -> {
            final var stampEntity = mock(StampEntity.class);
            when(stampEntity.getValue())
                .thenReturn(OffsetDateTime.ofInstant(
                    Instant.ofEpochSecond(seconds), ZoneOffset.UTC
                ));
            return stampEntity;
        };

        final var stampEntityNil = make.apply(23L);
        final var stampEntityOne = make.apply(42L);
        final var stampEntityTwo = make.apply(1337L);

        final var etherEntity = EtherEntity.with(MAC_ADDR_ONE, null);
        final var boundEntity = BoundEntity.with(IP_ADDR_ONE);

        final var trackEntities = Set.of(
            TrackEntity.with(stampEntityNil, etherEntity, boundEntity, State.FREE),
            TrackEntity.with(stampEntityNil, etherEntity, boundEntity, State.FREE),
            TrackEntity.with(stampEntityNil, etherEntity, boundEntity, State.FREE),
            TrackEntity.with(stampEntityNil, etherEntity, boundEntity, State.BACKUP),
            TrackEntity.with(stampEntityNil, etherEntity, boundEntity, State.BACKUP),
            TrackEntity.with(stampEntityNil, etherEntity, boundEntity, State.ACTIVE),
            TrackEntity.with(stampEntityOne, etherEntity, boundEntity, State.FREE),
            TrackEntity.with(stampEntityOne, etherEntity, boundEntity, State.FREE),
            TrackEntity.with(stampEntityOne, etherEntity, boundEntity, State.BACKUP),
            TrackEntity.with(stampEntityOne, etherEntity, boundEntity, State.ACTIVE),
            TrackEntity.with(stampEntityOne, etherEntity, boundEntity, State.ACTIVE),
            TrackEntity.with(stampEntityTwo, etherEntity, boundEntity, State.FREE),
            TrackEntity.with(stampEntityTwo, etherEntity, boundEntity, State.ACTIVE)
        );

        final var guideEntity = GuideEntity.with(MASK_ADDR_ONE, null, "some", null);

        when(entityService.findGuide(guideEntity.getUuid()))
            .thenReturn(Optional.of(guideEntity));
        when(entityService.findTracks())
            .thenReturn(trackEntities);

        final var service = create();
        assertThat(service.constructChartSeries(guideEntity.getUuid()))
            .hasValue(List.of(
                ChartSeriesDto.from(State.ACTIVE, Map.of(
                    stampEntityNil, 1L,
                    stampEntityOne, 2L,
                    stampEntityTwo, 1L
                )),
                ChartSeriesDto.from(State.BACKUP, Map.of(
                    stampEntityNil, 2L,
                    stampEntityOne, 1L
                )),
                ChartSeriesDto.from(State.FREE, Map.of(
                    stampEntityNil, 3L,
                    stampEntityOne, 2L,
                    stampEntityTwo, 1L
                ))
            ));

        verify(entityService, times(1))
            .findGuide(guideEntity.getUuid());
        verify(entityService, times(1))
            .findTracks();
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void performChartDumpEmptyGuides() {
        final var service = spy(create());
        doReturn(List.of())
            .when(service)
            .constructBaseGuides();

        assertThat(service.performChartDump())
            .isFalse();

        verify(service, times(1))
            .performChartDump();
        verify(service, times(1))
            .constructBaseGuides();
        verify(service, times(0))
            .constructChartSeries(any(UUID.class));
        verify(service, times(0))
            .dumpToDisk(any(Path.class), any());
        verifyNoMoreInteractions(service);
    }


    @Test
    void performChartDumpEmptySeries() {
        final var guideEntity = GuideEntity.with(MASK_ADDR_ONE, null, ".", null);
        final var baseGuides = List.of(BaseGuideDto.from(guideEntity));

        final var service = spy(create());
        doReturn(baseGuides)
            .when(service)
            .constructBaseGuides();

        doReturn(Optional.empty())
            .when(service)
            .constructChartSeries(guideEntity.getUuid());

        assertThat(service.performChartDump())
            .isFalse();

        verify(service, times(1))
            .performChartDump();
        verify(service, times(1))
            .constructBaseGuides();
        verify(service, times(1))
            .constructChartSeries(guideEntity.getUuid());
        verify(service, times(0))
            .dumpToDisk(any(Path.class), any());
        verifyNoMoreInteractions(service);
    }

    @Test
    void performChartDump() {
        final var guideEntity = GuideEntity.with(MASK_ADDR_TWO, null, ".", null);
        final var baseGuides = List.of(BaseGuideDto.from(guideEntity));

        final var chartSeries = List.of(ChartSeriesDto.from(
            State.FREE,
            Map.of(StampEntity.with(), 23L)
        ));

        final var service = spy(create());
        doReturn(baseGuides)
            .when(service)
            .constructBaseGuides();

        doReturn(Optional.of(chartSeries))
            .when(service)
            .constructChartSeries(guideEntity.getUuid());

        doAnswer(invocation -> {
            final var target = invocation.getArgument(0, Path.class);
            assertThat(target).hasFileName("chart-networks.json");
            final var payload = invocation.getArgument(1, List.class);
            assertThat(payload).isEqualTo(baseGuides);

            return true;
        })
            .when(service)
            .dumpToDisk(any(Path.class), eq(baseGuides));

        doAnswer(invocation -> {
            final var target = invocation.getArgument(0, Path.class);
            assertThat(target).hasFileName(String.format("chart-%s.json", guideEntity.getUuid()));
            final var payload = invocation.getArgument(1, List.class);
            assertThat(payload).isEqualTo(chartSeries);

            return true;
        })
            .when(service)
            .dumpToDisk(any(Path.class), eq(chartSeries));

        assertThat(service.performChartDump())
            .isTrue();

        verify(service, times(1))
            .performChartDump();
        verify(service, times(1))
            .constructBaseGuides();
        verify(service, times(1))
            .constructChartSeries(guideEntity.getUuid());
        verify(service, times(1))
            .dumpToDisk(any(Path.class), eq(baseGuides));
        verify(service, times(1))
            .dumpToDisk(any(Path.class), eq(chartSeries));
        verifyNoMoreInteractions(service);
    }
}
