package su.toor.leases.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.Net.MASK_ADDR_ONE;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.leases.components.mqtt.Client;
import su.toor.leases.configs.sinks.AlertBoundConfig;
import su.toor.leases.configs.sinks.AlertEtherConfig;
import su.toor.leases.configs.sinks.AlertGuideConfig;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.database.alert.AlertGuideEntity;
import su.toor.leases.parse.LeaseConverter;
import su.toor.leases.shared.State;

@ExtendWith(SpringExtension.class)
class MessageServiceTest {

    @MockBean
    private Client client;

    private static final String TOPIC = "some/topic";
    private static final String ETHER_TEMPLATE = "{{uuid}} {{hostname|no-hostname}} {{state}}";
    private static final String BOUND_TEMPLATE = "{{uuid}} {{ip}} {{state}}";
    private static final String GUIDE_TEMPLATE = "{{uuid}} {{title}} {{mask}} {{high}} {{down}}";

    private MessageService create(
        final AlertEtherConfig alertEtherConfig,
        final AlertBoundConfig alertBoundConfig,
        final AlertGuideConfig alertGuideConfig
    ) {
        return new MessageService(
            alertEtherConfig,
            alertBoundConfig,
            alertGuideConfig,
            client
        );
    }

    private MessageService create(final Boolean enabled) {
        return create(
            new AlertEtherConfig()
                .setFallbackTopic(TOPIC)
                .setFallbackTemplate(ETHER_TEMPLATE)
                .setEnabled(enabled),
            new AlertBoundConfig()
                .setFallbackTopic(TOPIC)
                .setFallbackTemplate(BOUND_TEMPLATE)
                .setEnabled(enabled),
            new AlertGuideConfig()
                .setFallbackTopic(TOPIC)
                .setFallbackTemplate(GUIDE_TEMPLATE)
                .setEnabled(enabled)
        );
    }
    private MessageService create() {
        return create(true);
    }

    private static String decode(final byte[] payload) {
        final var decoder = StandardCharsets.UTF_8
            .newDecoder()
            .onMalformedInput(CodingErrorAction.REPORT)
            .onUnmappableCharacter(CodingErrorAction.REPORT);
        try {
            return decoder
                .decode(ByteBuffer.wrap(payload))
                .toString();
        } catch (final CharacterCodingException ex) {
            return null;
        }
    }

    private void setupSend(final String... values) {
        when(client.send(eq(TOPIC), any(MqttMessage.class)))
            .thenAnswer(invocation -> {
                final var message = invocation.getArgument(1, MqttMessage.class);
                assertThat(decode(message.getPayload()))
                    .isNotBlank()
                    .contains(values);

                return CompletableFuture.completedFuture(true);
            });
    }

    @Test
    void valuesEther() {
        final var etherEntityNil = EtherEntity.with(MAC_ADDR_ONE, null);
        final var etherEntityOne = EtherEntity.with(MAC_ADDR_ONE, HOSTNAME);
        final var state = State.ACTIVE;

        assertThat(MessageService.values(etherEntityNil, state))
            .containsExactlyInAnyOrderEntriesOf(Map.of(
                "uuid", etherEntityNil.getUuid().toString(),
                "state", state.getText()
            ));
        assertThat(MessageService.values(etherEntityOne, state))
            .containsExactlyInAnyOrderEntriesOf(Map.of(
                "uuid", etherEntityOne.getUuid().toString(),
                "hostname", HOSTNAME,
                "state", state.getText()
            ));
    }

    @Test
    void valuesBound() {
        final var boundEntity = BoundEntity.with(IP_ADDR_ONE);
        final var state = State.FREE;

        assertThat(MessageService.values(boundEntity, state))
            .containsExactlyInAnyOrderEntriesOf(Map.of(
                "uuid", boundEntity.getUuid().toString(),
                "ip", LeaseConverter.formatAddress(boundEntity.getIp()),
                "state", state.getText()
            ));
    }

    @Test
    void valuesGuide() {
        final var guideEntity = GuideEntity.with(MASK_ADDR_ONE, null, "Title", null);
        final var high = 13L;
        final var down = 12L;

        assertThat(MessageService.values(guideEntity, high, down))
            .containsExactlyInAnyOrderEntriesOf(Map.of(
                "uuid", guideEntity.getUuid().toString(),
                "title", guideEntity.getTitle(),
                "mask", LeaseConverter.formatAddress(guideEntity.getMask()),
                "high", String.valueOf(high),
                "down", String.valueOf(down)
            ));
    }

    @Test
    void notifyEtherDisabled() {
        final var etherEntity = EtherEntity.with(MAC_ADDR_ONE, null);
        final var alertEtherEntity = AlertEtherEntity.with(etherEntity, false, null, null);

        final var serviceNil = create(false);
        final var serviceOne = create();

        assertThatNoException()
            .isThrownBy(() -> serviceNil.notify(alertEtherEntity, State.FREE));
        assertThatNoException()
            .isThrownBy(() -> serviceOne.notify(alertEtherEntity, State.FREE));

        verifyNoInteractions(client);
    }

    @Test
    void notifyEther() {
        final var etherEntity = EtherEntity.with(MAC_ADDR_TWO, HOSTNAME);
        final var alertEtherEntity = AlertEtherEntity.with(etherEntity, true, null, null);
        final var state = State.ACTIVE;

        setupSend(
            etherEntity.getUuid().toString(),
            HOSTNAME,
            state.getText()
        );

        final var service = create();

        assertThatNoException()
            .isThrownBy(() -> service.notify(alertEtherEntity, state));

        verify(client, times(1))
            .send(eq(TOPIC), any(MqttMessage.class));
        verifyNoMoreInteractions(client);
    }

    @Test
    void notifyBoundDisabled() {
        final var boundEntity = BoundEntity.with(IP_ADDR_ONE);
        final var alertBoundEntity = AlertBoundEntity.with(boundEntity, false, null, null);

        final var serviceNil = create(false);
        final var serviceOne = create();

        assertThatNoException()
            .isThrownBy(() -> serviceNil.notify(alertBoundEntity, State.FREE));
        assertThatNoException()
            .isThrownBy(() -> serviceOne.notify(alertBoundEntity, State.FREE));

        verifyNoInteractions(client);
    }

    @Test
    void notifyBound() {
        final var boundEntity = BoundEntity.with(IP_ADDR_ONE);
        final var alertBoundEntity = AlertBoundEntity.with(boundEntity, true, null, null);
        final var state = State.ACTIVE;

        setupSend(
            boundEntity.getUuid().toString(),
            LeaseConverter.formatAddress(boundEntity.getIp()),
            state.getText()
        );

        final var service = create();

        assertThatNoException()
            .isThrownBy(() -> service.notify(alertBoundEntity, state));

        verify(client, times(1))
            .send(eq(TOPIC), any(MqttMessage.class));
        verifyNoMoreInteractions(client);
    }

    @Test
    void notifyGuideDisabled() {
        final var guideEntity = GuideEntity.with(MASK_ADDR_ONE, null, "title", null);
        final var alertGuideEntity = AlertGuideEntity.with(guideEntity, false, null, null);

        final var serviceNil = create(false);
        final var serviceOne = create();

        assertThatNoException()
            .isThrownBy(() -> serviceNil.notify(alertGuideEntity, 23L, 42L));
        assertThatNoException()
            .isThrownBy(() -> serviceOne.notify(alertGuideEntity, 13L, 37L));

        verifyNoInteractions(client);
    }

    @Test
    void notifyGuide() {
        final var guideEntity = GuideEntity.with(MASK_ADDR_ONE, null, "Title", null);
        final var alertGuideEntity = AlertGuideEntity.with(guideEntity, true, null, null);
        final var high = 13L;
        final var down = 12L;

        setupSend(
            guideEntity.getUuid().toString(),
            guideEntity.getTitle(),
            LeaseConverter.formatAddress(guideEntity.getMask()),
            String.valueOf(high),
            String.valueOf(down)
        );

        final var service = create();

        assertThatNoException()
            .isThrownBy(() -> service.notify(alertGuideEntity, high, down));

        verify(client, times(1))
            .send(eq(TOPIC), any(MqttMessage.class));
        verifyNoMoreInteractions(client);
    }
}
