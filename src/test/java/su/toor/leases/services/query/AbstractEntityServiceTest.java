package su.toor.leases.services.query;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MASK_ADDR_ONE;

import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.BoundRepository;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.EtherRepository;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.GuideRepository;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.StampRepository;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.database.TrackRepository;
import su.toor.leases.shared.State;

class AbstractEntityServiceTest extends EntityBase<AbstractEntityService> {

    static class EntityPhonyService extends AbstractEntityService {
        EntityPhonyService(
            final StampRepository stampRepository,
            final EtherRepository etherRepository,
            final BoundRepository boundRepository,
            final TrackRepository trackRepository,
            final GuideRepository guideRepository
        ) {
            super(
                BASIC_CONFIG,
                stampRepository,
                etherRepository,
                boundRepository,
                trackRepository,
                guideRepository
            );
        }
    }

    @Override
    protected EntityPhonyService createService() {
        return new EntityPhonyService(
            stampRepository,
            etherRepository,
            boundRepository,
            trackRepository,
            guideRepository
        );
    }

    @Test
    void pagination() {
        assertThat(service.pagination(-1, PAGINATION_SIZE).getPageNumber()).isZero();
        assertThat(service.pagination(0, PAGINATION_SIZE).getPageNumber()).isZero();
        assertThat(service.pagination(null, PAGINATION_SIZE).getPageNumber()).isZero();
        assertThat(service.pagination(1, PAGINATION_SIZE).getPageNumber()).isOne();
        assertThat(service.pagination(1337, PAGINATION_SIZE).getPageNumber()).isEqualTo(1337);

        assertThat(service.pagination(0, -1).getPageSize()).isEqualTo(PAGINATION_SIZE);
        assertThat(service.pagination(0, 0).getPageSize()).isEqualTo(PAGINATION_SIZE);
        assertThat(service.pagination(0, null).getPageSize()).isEqualTo(PAGINATION_SIZE);
        assertThat(service.pagination(0, 1).getPageSize()).isOne();
        assertThat(service.pagination(0, 2 * AbstractEntityService.MAX_PAGINATION_SIZE).getPageSize())
            .isEqualTo(AbstractEntityService.MAX_PAGINATION_SIZE);
    }

    @Test
    void counters() {
        final List<Supplier<Long>> counters = List.of(
            service::countStamps,
            service::countEthers,
            service::countBounds,
            service::countTracks,
            service::countGuides
        );

        counters.forEach(counter -> assertThat(counter.get()).isZero());

        final var stampEntity = stampRepository.save(StampEntity.with());
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        trackRepository.save(TrackEntity.with(
            stampEntity, etherEntity, boundEntity, State.ACTIVE
        ));
        guideRepository.save(GuideEntity.with(
            MASK_ADDR_ONE, null, "some", null
        ));

        counters.forEach(counter -> assertThat(counter.get()).isOne());
    }

    @Test
    void findStamp() {
        final var stampEntity = stampRepository.save(StampEntity.with());

        assertThat(service.findStamp(UUID.randomUUID()))
            .isEmpty();
        assertThat(service.findStamp(stampEntity.getUuid()))
            .hasValue(stampEntity);
    }

    @Test
    void findEther() {
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, HOSTNAME));

        assertThat(service.findEther(UUID.randomUUID()))
            .isEmpty();
        assertThat(service.findEther(etherEntity.getUuid()))
            .hasValue(etherEntity);
    }

    @Test
    void findBound() {
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));

        assertThat(service.findBound(UUID.randomUUID()))
            .isEmpty();
        assertThat(service.findBound(boundEntity.getUuid()))
            .hasValue(boundEntity);
    }

    @Test
    void findTrack() {
        final var stampEntity = stampRepository.save(StampEntity.with());
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_TWO));
        final var trackEntity = trackRepository.save(TrackEntity.with(
            stampEntity, etherEntity, boundEntity, State.FREE
        ));

        assertThat(service.findTrack(UUID.randomUUID()))
            .isEmpty();
        assertThat(service.findTrack(trackEntity.getUuid()))
            .hasValue(trackEntity);
    }

    @Test
    void findGuide() {
        final var guideEntity = guideRepository.save(GuideEntity.with(
            MASK_ADDR_ONE, null, "one", null
        ));

        assertThat(service.findGuide(UUID.randomUUID()))
            .isEmpty();
        assertThat(service.findGuide(guideEntity.getUuid()))
            .hasValue(guideEntity);
    }
}
