package su.toor.leases.services.query;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.Net.MASK_ADDR_ONE;
import static su.toor.leases.Net.MASK_ADDR_TWO;

import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.database.alert.AlertBoundEntity;
import su.toor.leases.database.alert.AlertBoundRepository;
import su.toor.leases.database.alert.AlertEtherEntity;
import su.toor.leases.database.alert.AlertEtherRepository;
import su.toor.leases.database.alert.AlertGuideEntity;
import su.toor.leases.database.alert.AlertGuideRepository;
import su.toor.leases.shared.State;

class AlertEntityServiceTest extends EntityBase<AlertEntityService> {

    @Autowired
    private AlertEtherRepository alertEtherRepository;
    @Autowired
    private AlertBoundRepository alertBoundRepository;
    @Autowired
    private AlertGuideRepository alertGuideRepository;

    @BeforeEach
    public void setup() {
        alertEtherRepository.deleteAllInBatch();
        alertBoundRepository.deleteAllInBatch();
        alertGuideRepository.deleteAllInBatch();

        super.setup();
    }

    @Override
    protected AlertEntityService createService() {
        return new AlertEntityService(
            BASIC_CONFIG,
            stampRepository,
            etherRepository,
            boundRepository,
            trackRepository,
            guideRepository,
            alertEtherRepository,
            alertBoundRepository,
            alertGuideRepository
        );
    }

    @Test
    void counters() {
        final List<Supplier<Long>> counters = List.of(
            service::countAlertEthers,
            service::countAlertBounds,
            service::countAlertGuides
        );

        counters.forEach(counter -> assertThat(counter.get()).isZero());

        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        alertEtherRepository.save(AlertEtherEntity.with(
            etherEntity, true, null, null
        ));
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        alertBoundRepository.save(AlertBoundEntity.with(
            boundEntity, true, null, null
        ));
        final var guideEntity = guideRepository.save(GuideEntity.with(MASK_ADDR_ONE, null, ".", null));
        alertGuideRepository.save(AlertGuideEntity.with(
            guideEntity, true, null, null
        ));

        counters.forEach(counter -> assertThat(counter.get()).isOne());
    }

    @Test
    void findTrackWithoutState() {
        final var stampEntity = stampRepository.save(StampEntity.with());
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var trackEntity = trackRepository.save(TrackEntity.with(
            stampEntity, etherEntity, boundEntity, State.FREE
        ));

        assertThat(service.findTrackWithoutState(stampEntity, etherEntity, State.ACTIVE))
            .hasValue(trackEntity);
        assertThat(service.findTrackWithoutState(stampEntity, etherEntity, State.FREE))
            .isEmpty();

        assertThat(service.findTrackWithoutState(stampEntity, boundEntity, State.ACTIVE))
            .hasValue(trackEntity);
        assertThat(service.findTrackWithoutState(stampEntity, boundEntity, State.FREE))
            .isEmpty();
    }

    @Test
    void findAlertEthers() {
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, null));
        final var alertEtherEntityOne = alertEtherRepository.save(AlertEtherEntity.with(etherEntity, true, "one", null));
        final var alertEtherEntityTwo = alertEtherRepository.save(AlertEtherEntity.with(etherEntity, true, "two", null));

        assertThat(service.findAlertEthers(0, PAGINATION_SIZE))
            .containsExactly(alertEtherEntityOne, alertEtherEntityTwo);
        assertThat(service.findAlertEthers(99, null))
            .isEmpty();
    }

    @Test
    void findAlertEthersEnabled() {
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var alertEtherEntityOne = alertEtherRepository.save(AlertEtherEntity.with(etherEntity, true, "enabled", null));
        alertEtherRepository.save(AlertEtherEntity.with(etherEntity, false, "disabled", null));

        assertThat(service.findAlertEthersEnabled())
            .containsExactly(alertEtherEntityOne);
    }

    @Test
    void findAlertEther() {
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var alertEtherEntity = alertEtherRepository.save(AlertEtherEntity.with(etherEntity, false, null, null));

        assertThat(service.findAlertEther(UUID.randomUUID()))
            .isEmpty();
        assertThat(service.findAlertEther(alertEtherEntity.getUuid()))
            .hasValue(alertEtherEntity);
    }

    @Test
    void modifyAlertEther() {
        final var enabled = true;
        final var topic = "some/topic";
        final var template = "Template";

        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var alertEtherEntity = alertEtherRepository.save(AlertEtherEntity.with(etherEntity, !enabled, null, null));

        assertThat(alertEtherRepository.findAll())
            .containsExactly(alertEtherEntity);

        assertThat(service.modifyAlertEther(UUID.randomUUID(), true, null, null))
            .isEmpty();

        assertThat(service.modifyAlertEther(alertEtherEntity.getUuid(), enabled, topic, template))
            .hasValue(alertEtherEntity);
        assertThat(alertEtherRepository.findAll())
            .containsExactly(alertEtherEntity);

        assertThat(alertEtherEntity.getEtherEntity()).isEqualTo(etherEntity);
        assertThat(alertEtherEntity.isEnabled()).isEqualTo(enabled);
        assertThat(alertEtherEntity.getTopic()).isEqualTo(topic);
        assertThat(alertEtherEntity.getTemplate()).isEqualTo(template);
    }

    @Test
    void createAlertEther() {
        assertThat(alertEtherRepository.findAll())
            .isEmpty();

        final var enabled = false;
        final var topic = "some/topic";
        final var template = "text";
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO,  null));

        assertThat(service.createAlertEther(UUID.randomUUID(), false, null, null))
            .isEmpty();
        assertThat(alertEtherRepository.findAll())
            .isEmpty();

        final var optionalAlertEtherEntity = service.createAlertEther(etherEntity.getUuid(), enabled, topic, template);
        assertThat(optionalAlertEtherEntity).isPresent();

        final var alertEtherEntity = optionalAlertEtherEntity.get();

        assertThat(alertEtherRepository.findAll())
            .containsExactly(alertEtherEntity);

        assertThat(alertEtherEntity.getEtherEntity()).isEqualTo(etherEntity);
        assertThat(alertEtherEntity.isEnabled()).isEqualTo(enabled);
        assertThat(alertEtherEntity.getTemplate()).isEqualTo(template);
        assertThat(alertEtherEntity.getTopic()).isEqualTo(topic);
    }

    @Test
    void deleteAlertEther() {
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var alertEtherEntity = alertEtherRepository.save(AlertEtherEntity.with(etherEntity, true, null, null));

        assertThat(alertEtherRepository.findAll())
            .containsExactly(alertEtherEntity);

        assertThat(service.deleteAlertEther(UUID.randomUUID()))
            .isFalse();
        assertThat(alertEtherRepository.findAll())
            .containsExactly(alertEtherEntity);

        assertThat(service.deleteAlertEther(alertEtherEntity.getUuid()))
            .isTrue();
        assertThat(alertEtherRepository.findAll())
            .isEmpty();
    }


    @Test
    void findAlertBounds() {
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_TWO));
        final var alertBoundEntityOne = alertBoundRepository.save(AlertBoundEntity.with(boundEntity, true, "one", null));
        final var alertBoundEntityTwo = alertBoundRepository.save(AlertBoundEntity.with(boundEntity, true, "two", null));

        assertThat(service.findAlertBounds(0, PAGINATION_SIZE))
            .containsExactly(alertBoundEntityOne, alertBoundEntityTwo);
        assertThat(service.findAlertBounds(99, null))
            .isEmpty();
    }

    @Test
    void findAlertBoundsEnabled() {
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var alertBoundEntityOne = alertBoundRepository.save(AlertBoundEntity.with(boundEntity, true, "enabled", null));
        alertBoundRepository.save(AlertBoundEntity.with(boundEntity, false, "disabled", null));

        assertThat(service.findAlertBoundsEnabled())
            .containsExactly(alertBoundEntityOne);
    }

    @Test
    void findAlertBound() {
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var alertBoundEntity = alertBoundRepository.save(AlertBoundEntity.with(boundEntity, false, null, null));

        assertThat(service.findAlertBound(UUID.randomUUID()))
            .isEmpty();
        assertThat(service.findAlertBound(alertBoundEntity.getUuid()))
            .hasValue(alertBoundEntity);
    }

    @Test
    void modifyAlertBound() {
        final var enabled = true;
        final var topic = "some/topic";
        final var template = "Template";

        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var alertBoundEntity = alertBoundRepository.save(AlertBoundEntity.with(boundEntity, !enabled, null, null));

        assertThat(alertBoundRepository.findAll())
            .containsExactly(alertBoundEntity);

        assertThat(service.modifyAlertBound(UUID.randomUUID(), true, null, null))
            .isEmpty();

        assertThat(service.modifyAlertBound(alertBoundEntity.getUuid(), enabled, topic, template))
            .hasValue(alertBoundEntity);
        assertThat(alertBoundRepository.findAll())
            .containsExactly(alertBoundEntity);

        assertThat(alertBoundEntity.getBoundEntity()).isEqualTo(boundEntity);
        assertThat(alertBoundEntity.isEnabled()).isEqualTo(enabled);
        assertThat(alertBoundEntity.getTopic()).isEqualTo(topic);
        assertThat(alertBoundEntity.getTemplate()).isEqualTo(template);
    }

    @Test
    void createAlertBound() {
        assertThat(alertBoundRepository.findAll())
            .isEmpty();

        final var enabled = false;
        final var topic = "some/topic";
        final var template = "text";
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_TWO));

        assertThat(service.createAlertBound(UUID.randomUUID(), false, null, null))
            .isEmpty();
        assertThat(alertBoundRepository.findAll())
            .isEmpty();

        final var optionalAlertBoundEntity = service.createAlertBound(boundEntity.getUuid(), enabled, topic, template);
        assertThat(optionalAlertBoundEntity).isPresent();

        final var alertBoundEntity = optionalAlertBoundEntity.get();

        assertThat(alertBoundRepository.findAll())
            .containsExactly(alertBoundEntity);

        assertThat(alertBoundEntity.getBoundEntity()).isEqualTo(boundEntity);
        assertThat(alertBoundEntity.isEnabled()).isEqualTo(enabled);
        assertThat(alertBoundEntity.getTemplate()).isEqualTo(template);
        assertThat(alertBoundEntity.getTopic()).isEqualTo(topic);
    }

    @Test
    void deleteAlertBound() {
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var alertBoundEntity = alertBoundRepository.save(AlertBoundEntity.with(boundEntity, true, null, null));

        assertThat(alertBoundRepository.findAll())
            .containsExactly(alertBoundEntity);

        assertThat(service.deleteAlertBound(UUID.randomUUID()))
            .isFalse();
        assertThat(alertBoundRepository.findAll())
            .containsExactly(alertBoundEntity);

        assertThat(service.deleteAlertBound(alertBoundEntity.getUuid()))
            .isTrue();
        assertThat(alertBoundRepository.findAll())
            .isEmpty();
    }


    @Test
    void findAlertGuides() {
        final var guideEntity = guideRepository.save(GuideEntity.with(MASK_ADDR_TWO, null, ".", null));
        final var alertGuideEntityOne = alertGuideRepository.save(AlertGuideEntity.with(guideEntity, true, "one", null));
        final var alertGuideEntityTwo = alertGuideRepository.save(AlertGuideEntity.with(guideEntity, true, "two", null));

        assertThat(service.findAlertGuides(0, PAGINATION_SIZE))
            .containsExactly(alertGuideEntityOne, alertGuideEntityTwo);
        assertThat(service.findAlertGuides(99, null))
            .isEmpty();
    }

    @Test
    void findAlertGuidesEnabled() {
        final var guideEntity = guideRepository.save(GuideEntity.with(MASK_ADDR_ONE, null, ".", null));
        final var alertGuideEntityOne = alertGuideRepository.save(AlertGuideEntity.with(guideEntity, true, "enabled", null));
        alertGuideRepository.save(AlertGuideEntity.with(guideEntity, false, "disabled", null));

        assertThat(service.findAlertGuidesEnabled())
            .containsExactly(alertGuideEntityOne);
    }

    @Test
    void findAlertGuide() {
        final var guideEntity = guideRepository.save(GuideEntity.with(MASK_ADDR_ONE, null, ".", null));
        final var alertGuideEntity = alertGuideRepository.save(AlertGuideEntity.with(guideEntity, false, null, null));

        assertThat(service.findAlertGuide(UUID.randomUUID()))
            .isEmpty();
        assertThat(service.findAlertGuide(alertGuideEntity.getUuid()))
            .hasValue(alertGuideEntity);
    }

    @Test
    void modifyAlertGuide() {
        final var enabled = true;
        final var topic = "some/topic";
        final var template = "Template";

        final var guideEntity = guideRepository.save(GuideEntity.with(MASK_ADDR_ONE, null, ".", null));
        final var alertGuideEntity = alertGuideRepository.save(AlertGuideEntity.with(guideEntity, !enabled, null, null));

        assertThat(alertGuideRepository.findAll())
            .containsExactly(alertGuideEntity);

        assertThat(service.modifyAlertGuide(UUID.randomUUID(), true, null, null))
            .isEmpty();

        assertThat(service.modifyAlertGuide(alertGuideEntity.getUuid(), enabled, topic, template))
            .hasValue(alertGuideEntity);
        assertThat(alertGuideRepository.findAll())
            .containsExactly(alertGuideEntity);

        assertThat(alertGuideEntity.getGuideEntity()).isEqualTo(guideEntity);
        assertThat(alertGuideEntity.isEnabled()).isEqualTo(enabled);
        assertThat(alertGuideEntity.getTopic()).isEqualTo(topic);
        assertThat(alertGuideEntity.getTemplate()).isEqualTo(template);
    }

    @Test
    void createAlertGuide() {
        assertThat(alertGuideRepository.findAll())
            .isEmpty();

        final var enabled = false;
        final var topic = "some/topic";
        final var template = "text";
        final var guideEntity = guideRepository.save(GuideEntity.with(MASK_ADDR_TWO,  null, ".", null));

        assertThat(service.createAlertGuide(UUID.randomUUID(), false, null, null))
            .isEmpty();
        assertThat(alertGuideRepository.findAll())
            .isEmpty();

        final var optionalAlertGuideEntity = service.createAlertGuide(guideEntity.getUuid(), enabled, topic, template);
        assertThat(optionalAlertGuideEntity).isPresent();

        final var alertGuideEntity = optionalAlertGuideEntity.get();

        assertThat(alertGuideRepository.findAll())
            .containsExactly(alertGuideEntity);

        assertThat(alertGuideEntity.getGuideEntity()).isEqualTo(guideEntity);
        assertThat(alertGuideEntity.isEnabled()).isEqualTo(enabled);
        assertThat(alertGuideEntity.getTemplate()).isEqualTo(template);
        assertThat(alertGuideEntity.getTopic()).isEqualTo(topic);
    }

    @Test
    void deleteAlertGuide() {
        final var guideEntity = guideRepository.save(GuideEntity.with(MASK_ADDR_ONE, null, ".", null));
        final var alertGuideEntity = alertGuideRepository.save(AlertGuideEntity.with(guideEntity, true, null, null));

        assertThat(alertGuideRepository.findAll())
            .containsExactly(alertGuideEntity);

        assertThat(service.deleteAlertGuide(UUID.randomUUID()))
            .isFalse();
        assertThat(alertGuideRepository.findAll())
            .containsExactly(alertGuideEntity);

        assertThat(service.deleteAlertGuide(alertGuideEntity.getUuid()))
            .isTrue();
        assertThat(alertGuideRepository.findAll())
            .isEmpty();
    }
}
