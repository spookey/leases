package su.toor.leases.services.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.leases.configs.BasicConfig;
import su.toor.leases.database.BoundRepository;
import su.toor.leases.database.EtherRepository;
import su.toor.leases.database.GuideRepository;
import su.toor.leases.database.StampRepository;
import su.toor.leases.database.TrackRepository;

@ExtendWith(SpringExtension.class)
@DataJpaTest
abstract class EntityBase<T> {

    @Autowired
    protected StampRepository stampRepository;
    @Autowired
    protected EtherRepository etherRepository;
    @Autowired
    protected BoundRepository boundRepository;
    @Autowired
    protected TrackRepository trackRepository;
    @Autowired
    protected GuideRepository guideRepository;

    static final int PAGINATION_SIZE = 23;

    static final BasicConfig BASIC_CONFIG = new BasicConfig()
        .setPaginationSize(PAGINATION_SIZE);

    protected T service;

    abstract protected T createService();

    @BeforeEach
    public void setup() {
        stampRepository.deleteAllInBatch();
        etherRepository.deleteAllInBatch();
        boundRepository.deleteAllInBatch();
        trackRepository.deleteAllInBatch();
        guideRepository.deleteAllInBatch();

        service = createService();
    }
}
