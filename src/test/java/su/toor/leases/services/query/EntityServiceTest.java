package su.toor.leases.services.query;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.leases.Net.HOSTNAME;
import static su.toor.leases.Net.IP_ADDR_ONE;
import static su.toor.leases.Net.IP_ADDR_TWO;
import static su.toor.leases.Net.MAC_ADDR_ONE;
import static su.toor.leases.Net.MAC_ADDR_TWO;
import static su.toor.leases.Net.MASK_ADDR_ONE;
import static su.toor.leases.Net.MASK_ADDR_TWO;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import su.toor.leases.database.BoundEntity;
import su.toor.leases.database.EtherEntity;
import su.toor.leases.database.GuideEntity;
import su.toor.leases.database.StampEntity;
import su.toor.leases.database.StampRepository;
import su.toor.leases.database.TrackEntity;
import su.toor.leases.database.TrackRepository;
import su.toor.leases.shared.State;

class EntityServiceTest extends EntityBase<EntityService> {

    @Autowired
    private StampRepository stampRepository;
    @Autowired
    private TrackRepository trackRepository;

    @Override
    protected EntityService createService() {
        return new EntityService(
            BASIC_CONFIG,
            stampRepository,
            etherRepository,
            boundRepository,
            trackRepository,
            guideRepository
        );
    }

    @Test
    void findStamps() {
        final var stampEntityOne = stampRepository.save(StampEntity.with());
        final var stampEntityTwo = stampRepository.save(StampEntity.with());

        assertThat(service.findStamps(0, PAGINATION_SIZE))
            .containsExactly(stampEntityOne, stampEntityTwo);
        assertThat(service.findStamps(null, null))
            .containsExactly(stampEntityOne, stampEntityTwo);

        assertThat(service.findStamps(99, null))
            .isEmpty();
    }

    @Test
    void findStampBefore() {
        final var stampEntityNil = stampRepository.save(StampEntity.with());
        final var stampEntityOne = stampRepository.save(StampEntity.with());
        final var stamp = OffsetDateTime.now();
        final var stampEntityTwo = stampRepository.save(StampEntity.with());

        assertThat(stampRepository.findAll())
            .containsExactlyInAnyOrder(stampEntityNil, stampEntityOne, stampEntityTwo);

        assertThat(service.findStampBefore(stamp))
            .containsExactlyInAnyOrder(stampEntityNil, stampEntityOne);
    }

    @Test
    void latestStamp() {
        assertThat(service.latestStamp())
            .isEmpty();

        final var stampEntity = stampRepository.save(StampEntity.with());
        assertThat(service.latestStamp())
            .hasValue(stampEntity);
    }

    @Test
    void createStamp() {
        assertThat(stampRepository.findAll()).isEmpty();

        final var stampEntity = service.createStamp();
        assertThat(stampRepository.findAll())
            .containsExactly(stampEntity);
    }

    @Test
    void deleteStamp() {
        final var stampEntity = stampRepository.save(StampEntity.with());
        assertThat(stampRepository.findAll())
            .containsExactly(stampEntity);

        assertThat(service.deleteStamp(UUID.randomUUID())).isFalse();
        assertThat(stampRepository.findAll())
            .containsExactly(stampEntity);

        assertThat(service.deleteStamp(stampEntity.getUuid())).isTrue();
        assertThat(stampRepository.findAll()).isEmpty();
    }

    @Test
    void findEthers() {
        final var etherEntityOne = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, null));
        final var etherEntityTwo = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, HOSTNAME));

        assertThat(service.findEthers(0, PAGINATION_SIZE))
            .containsExactly(etherEntityOne, etherEntityTwo);
        assertThat(service.findEthers(99, null))
            .isEmpty();
    }

    @Test
    void findEther() {
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, HOSTNAME));

        assertThat(service.findEther(MAC_ADDR_TWO))
            .isEmpty();
        assertThat(service.findEther(etherEntity.getMac()))
            .hasValue(etherEntity);
    }

    @Test
    void ensureEther() {
        assertThat(etherRepository.findAll()).isEmpty();

        final var etherEntity = service.ensureEther(MAC_ADDR_ONE, null);
        assertThat(etherRepository.findAll())
            .containsExactly(etherEntity);

        assertThat(service.ensureEther(MAC_ADDR_ONE, HOSTNAME))
            .isSameAs(etherEntity);

        assertThat(etherRepository.findAll())
            .containsExactly(etherEntity);
    }

    @Test
    void findBounds() {
        final var boundEntityOne = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));
        final var boundEntityTwo = boundRepository.save(BoundEntity.with(IP_ADDR_TWO));

        assertThat(service.findBounds(0, PAGINATION_SIZE))
            .containsExactly(boundEntityTwo, boundEntityOne);
        assertThat(service.findBounds(99, null))
            .isEmpty();
    }

    @Test
    void findBound() {
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));

        assertThat(service.findBound(IP_ADDR_TWO))
            .isEmpty();
        assertThat(service.findBound(boundEntity.getIp()))
            .hasValue(boundEntity);
    }

    @Test
    void ensureBound() {
        assertThat(boundRepository.findAll()).isEmpty();

        final var boundEntity = service.ensureBound(IP_ADDR_ONE);
        assertThat(boundRepository.findAll())
            .containsExactly(boundEntity);

        assertThat(service.ensureBound(IP_ADDR_ONE))
            .isSameAs(boundEntity);

        assertThat(boundRepository.findAll())
            .containsExactly(boundEntity);
    }

    @Test
    void findTracks() {
        final var stampEntity = stampRepository.save(StampEntity.with());
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_ONE, HOSTNAME));
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_ONE));

        final var trackEntityOne = trackRepository.save(TrackEntity.with(
            stampEntity, etherEntity, boundEntity, State.FREE
        ));
        final var trackEntityTwo = trackRepository.save(TrackEntity.with(
            stampEntity, etherEntity, boundEntity, State.ACTIVE
        ));

        assertThat(service.findTracks())
            .containsExactlyInAnyOrder(trackEntityTwo, trackEntityOne);

        assertThat(service.findTracks(0, PAGINATION_SIZE))
            .containsExactly(trackEntityTwo, trackEntityOne);
        assertThat(service.findTracks(99, null))
            .isEmpty();

        assertThat(service.findTracks(UUID.randomUUID(), 0, PAGINATION_SIZE))
            .isEmpty();
        assertThat(service.findTracks(stampEntity.getUuid(), 0, PAGINATION_SIZE))
            .containsExactly(trackEntityTwo, trackEntityOne);
        assertThat(service.findTracks(stampEntity.getUuid(), 99, null))
            .isEmpty();
    }

    @Test
    void latestTracks() {
        final var stampEntity = stampRepository.save(StampEntity.with());
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, HOSTNAME));
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_TWO));

        final var trackEntityOne = trackRepository.save(TrackEntity.with(
            stampEntity, etherEntity, boundEntity, State.FREE
        ));
        final var trackEntityTwo = trackRepository.save(TrackEntity.with(
            stampEntity, etherEntity, boundEntity, State.ACTIVE
        ));

        assertThat(service.latestTracks(0, PAGINATION_SIZE))
            .containsExactly(trackEntityTwo, trackEntityOne);
        assertThat(service.latestTracks(99, null))
            .isEmpty();
    }

    @Test
    void createTrack() {
        assertThat(trackRepository.findAll()).isEmpty();

        final var stampEntity = stampRepository.save(StampEntity.with());
        final var etherEntity = etherRepository.save(EtherEntity.with(MAC_ADDR_TWO, null));
        final var boundEntity = boundRepository.save(BoundEntity.with(IP_ADDR_TWO));
        final var state = State.FREE;

        final var trackEntity = service.createTrack(stampEntity, etherEntity, boundEntity, state);

        assertThat(trackRepository.findAll())
            .containsExactly(trackEntity);
    }

    @Test
    void findGuides() {
        final var guideEntityOne = guideRepository.save(GuideEntity.with(
            MASK_ADDR_ONE, 42, "one", null
        ));
        final var guideEntityTwo = guideRepository.save(GuideEntity.with(
            MASK_ADDR_TWO, 23, "two", null
        ));

        assertThat(service.findGuides())
            .containsExactly(guideEntityTwo, guideEntityOne);

        assertThat(service.findGuides(0, PAGINATION_SIZE))
            .containsExactly(guideEntityTwo, guideEntityOne);
        assertThat(service.findGuides(99, null))
            .isEmpty();
    }

    @Test
    void modifyGuide() {
        final var weight = 23;
        final var title = "Title";
        final var description = "Some description";

        final var guideEntity = guideRepository.save(GuideEntity.with(MASK_ADDR_ONE, null, "old", null));

        assertThat(guideRepository.findAll())
            .containsExactly(guideEntity);

        assertThat(service.modifyGuide(UUID.randomUUID(), null, null, null, null))
            .isEmpty();

        assertThat(service.modifyGuide(guideEntity.getUuid(), MASK_ADDR_TWO, weight, title, description))
            .hasValue(guideEntity);
        assertThat(guideRepository.findAll())
            .containsExactly(guideEntity);

        assertThat(guideEntity.getMask()).isEqualTo(MASK_ADDR_TWO);
        assertThat(guideEntity.getWeight()).isEqualTo(weight);
        assertThat(guideEntity.getTitle()).isEqualTo(title);
        assertThat(guideEntity.getDescription()).isEqualTo(description);
    }

    @Test
    void createGuide() {
        assertThat(guideRepository.findAll())
            .isEmpty();

        final var weight = 23;
        final var title = "Title";
        final var description = "Description";

        final var guideEntity = service.createGuide(MASK_ADDR_ONE, weight, title, description);

        assertThat(guideRepository.findAll())
            .containsExactly(guideEntity);

        assertThat(guideEntity.getMask()).isEqualTo(MASK_ADDR_ONE);
        assertThat(guideEntity.getWeight()).isEqualTo(weight);
        assertThat(guideEntity.getTitle()).isEqualTo(title);
        assertThat(guideEntity.getDescription()).isEqualTo(description);
    }

    @Test
    void deleteGuide() {
        final var guideEntity = guideRepository.save(GuideEntity.with(MASK_ADDR_ONE, null, "some", null));
        assertThat(guideRepository.findAll())
            .containsExactly(guideEntity);

        assertThat(service.deleteGuide(UUID.randomUUID())).isFalse();
        assertThat(guideRepository.findAll())
            .containsExactly(guideEntity);

        assertThat(service.deleteGuide(guideEntity.getUuid())).isTrue();
        assertThat(guideRepository.findAll()).isEmpty();
    }
}
