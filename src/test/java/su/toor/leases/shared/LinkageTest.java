package su.toor.leases.shared;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import su.toor.leases.TempDirBase;

class LinkageTest extends TempDirBase {

    @Test
    void path() {
        assertThat(Linkage.path(null)).isNull();

        final var path = base.resolve("some");
        assertThat(Linkage.path(path))
            .isAbsolute()
            .doesNotExist()
            .isEqualByComparingTo(path);
    }

    @Test
    void safePath() {
        final var fieldName = "someName";
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Linkage.safePath(null, null))
            .withMessageContaining(Require.IS_NULL);

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Linkage.safePath(null, fieldName))
            .withMessageContaining(fieldName)
            .withMessageContaining(Require.IS_NULL);

        final var path = Path.of("what", "ever");
        assertThat(Linkage.safePath(path, fieldName))
            .isAbsolute()
            .doesNotExist();
    }

    @Test
    void verified() throws IOException {
        final var empty = base.resolve("empty");
        final var file = Files.createFile(base.resolve("file"));
        final var folder = Files.createDirectory(base.resolve("folder"));

        Stream.of(true, false).forEach(
            mustBeFolder -> assertThatExceptionOfType(RequireException.class)
                .isThrownBy(() -> Linkage.verified(empty, mustBeFolder, "empty"))
                .withMessageContaining("does not exist")
        );

        Map.of(
            file, true,
            folder, false
        ).forEach(
            (path, mustBeFolder) -> assertThatExceptionOfType(RequireException.class)
                .isThrownBy(() -> Linkage.verified(path, mustBeFolder, "path"))
                .withMessageContaining("is not a")
        );

        Map.of(
            file, false,
            folder, true
        ).forEach(
            (path, mustBeFolder) -> assertThat(Linkage.verified(path, mustBeFolder, "path"))
                .isEqualTo(path)
        );
    }
}
