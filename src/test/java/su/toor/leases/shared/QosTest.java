package su.toor.leases.shared;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.EnumSet;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

class QosTest {

    @Test
    void names() {
        final var names = EnumSet.allOf(Qos.class)
            .stream()
            .map(Qos::name)
            .collect(Collectors.toSet());

        assertThat(names)
            .containsExactlyInAnyOrder("ZERO", "ONE", "TWO");
    }

    @Test
    void ordinals() {
        Map.of(
            Qos.ZERO, 0,
            Qos.ONE, 1,
            Qos.TWO, 2
        ).forEach((qos, ord) -> assertThat(qos.ordinal()).isEqualTo(ord));
    }
}
