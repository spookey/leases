package su.toor.leases.shared;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

class RequireTest {

    private static <T> Predicate<T> makeNullPredicate() {
        return null;
    }
    private static String makeNullMessage() {
        return null;
    }

    @Test
    void condition() {
        final var nullPredicate = makeNullPredicate();
        final var nullMessage = makeNullMessage();
        final var message = "custom error message";

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Require.condition(42, nullPredicate, message))
            .withMessage("check is null");
        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Require.condition(23, Objects::isNull, nullMessage))
            .withMessage("message is null or is empty");

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Require.condition(null, Objects::nonNull, message))
            .withMessage(message);

        assertThat(Require.condition((String) null, Objects::isNull, message))
            .isNull();
    }

    @Test
    void notNull() {
        final var text = "text";

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Require.notNull(null, text))
            .withMessage(text + " is null");

        assertThat(Require.notNull(text, "message")).isEqualTo(text);
    }

    @Test
    void notEmpty() {
        final var text = "text";
        final var message = "something";

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Require.notEmpty(null, message))
            .withMessage(message + " is null");

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Require.notEmpty("", message))
            .withMessage(message + " is empty");

        assertThat(Require.notEmpty(text, message)).isEqualTo(text);
    }

    @Test
    void notBlank() {
        final var text = "text";
        final var message = "something";

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Require.notBlank(null, message))
            .withMessage(message + " is null");

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Require.notBlank("", message))
            .withMessage(message + " is empty");

        assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Require.notBlank("\t", message))
            .withMessage(message + " is blank");

        assertThat(Require.notEmpty(text, message)).isEqualTo(text);
    }

    @Test
    void validTopic() {
        assertThat(Require.validTopic("topic", true)).isTrue();
        assertThat(Require.validTopic("topic", false)).isTrue();

        assertThat(Require.validTopic("$share/topic", true)).isTrue();
        assertThat(Require.validTopic("$share/topic", false)).isFalse();

        assertThat(Require.validTopic("some/+/topic", true)).isTrue();
        assertThat(Require.validTopic("some/+/topic", false)).isFalse();

        assertThat(Require.validTopic("", true)).isFalse();
        assertThat(Require.validTopic("", false)).isFalse();
    }

    @Test
    void validMessageTopic() {
        Stream.of(
            "topic", "some/topic"
        ).forEach(topic -> assertThat(Require.validMessageTopic(topic))
            .isEqualTo(topic)
        );

        Stream.of(
            "", "\n", "c++", "c#"
        ).forEach(topic -> assertThatExceptionOfType(RequireException.class)
            .isThrownBy(() -> Require.validMessageTopic(topic))
        );
    }
}
