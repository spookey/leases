package su.toor.leases.shared;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.EnumSet;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

class StateDirectionTest {

    @Test
    void names() {
        final var names = EnumSet.allOf(State.Direction.class)
            .stream()
            .map(State.Direction::name)
            .collect(Collectors.toSet());

        assertThat(names)
            .containsExactlyInAnyOrder("HIGH", "DOWN", "NONE");
    }

    @Test
    void text() {
        Map.of(
            State.Direction.HIGH, "up",
            State.Direction.DOWN, "down",
            State.Direction.NONE, "unknown"
        ).forEach((state, text) -> assertThat(state.getText()).isEqualTo(text));
    }

}
