package su.toor.leases.shared;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

class StateTest {

    @Test
    void names() {
        final var names = EnumSet.allOf(State.class)
            .stream()
            .map(State::name)
            .collect(Collectors.toSet());

        assertThat(names)
            .containsExactlyInAnyOrder("ACTIVE", "BACKUP", "FREE");
    }

    @Test
    void parse() {
        Map.of(
            "Active", State.ACTIVE,
            "active", State.ACTIVE,
            "ACTIVE", State.ACTIVE,
            "BACKUP", State.BACKUP,
            "Backup", State.BACKUP,
            "backup", State.BACKUP,
            "FREE", State.FREE,
            "Free", State.FREE,
            "free", State.FREE
        ).forEach((value, state) -> assertThat(State.parse(value)).isEqualTo(state));

        Stream.of(
            "UNKNOWN", "Banana", "🌭", " ", ""
        ).forEach(value -> assertThat(State.parse(value)).isNull());
        assertThat(State.parse(null)).isNull();
    }

    @Test
    void items() {
        final var total = List.of(State.ACTIVE, State.BACKUP, State.FREE);
        final var normal = List.of(State.ACTIVE, State.FREE);

        assertThat(State.items(true)).containsExactlyElementsOf(total);
        assertThat(State.items(Boolean.TRUE)).containsExactlyElementsOf(total);

        assertThat(State.items(false)).containsExactlyElementsOf(normal);
        assertThat(State.items(Boolean.FALSE)).containsExactlyElementsOf(normal);

        assertThat(State.items(null)).containsExactlyElementsOf(normal);
    }

    @Test
    void changeNull() {
        assertThat(State.ACTIVE.change(null, null)).isEqualTo(State.Direction.NONE);
        assertThat(State.BACKUP.change(null, null)).isEqualTo(State.Direction.NONE);
        assertThat(State.FREE.change(null, null)).isEqualTo(State.Direction.NONE);
    }

    @Test
    void changeActive() {
        assertThat(State.ACTIVE.change(State.ACTIVE, true)).isEqualTo(State.Direction.NONE);
        assertThat(State.ACTIVE.change(State.ACTIVE, false)).isEqualTo(State.Direction.NONE);
        assertThat(State.ACTIVE.change(State.ACTIVE, null)).isEqualTo(State.Direction.NONE);

        assertThat(State.ACTIVE.change(State.BACKUP, true)).isEqualTo(State.Direction.DOWN);
        assertThat(State.ACTIVE.change(State.BACKUP, false)).isEqualTo(State.Direction.DOWN);
        assertThat(State.ACTIVE.change(State.BACKUP, null)).isEqualTo(State.Direction.DOWN);

        assertThat(State.ACTIVE.change(State.FREE, true)).isEqualTo(State.Direction.DOWN);
        assertThat(State.ACTIVE.change(State.FREE, false)).isEqualTo(State.Direction.DOWN);
        assertThat(State.ACTIVE.change(State.FREE, null)).isEqualTo(State.Direction.DOWN);
    }

    @Test
    void changeBackup() {
        assertThat(State.BACKUP.change(State.BACKUP, true)).isEqualTo(State.Direction.NONE);
        assertThat(State.BACKUP.change(State.BACKUP, false)).isEqualTo(State.Direction.NONE);
        assertThat(State.BACKUP.change(State.BACKUP, null)).isEqualTo(State.Direction.NONE);

        assertThat(State.BACKUP.change(State.ACTIVE, true)).isEqualTo(State.Direction.HIGH);
        assertThat(State.BACKUP.change(State.ACTIVE, false)).isEqualTo(State.Direction.HIGH);
        assertThat(State.BACKUP.change(State.ACTIVE, null)).isEqualTo(State.Direction.HIGH);

        assertThat(State.BACKUP.change(State.FREE, true)).isEqualTo(State.Direction.DOWN);
        assertThat(State.BACKUP.change(State.FREE, false)).isEqualTo(State.Direction.NONE);
        assertThat(State.BACKUP.change(State.FREE, null)).isEqualTo(State.Direction.NONE);
    }

    @Test
    void change() {
        assertThat(State.FREE.change(State.FREE, true)).isEqualTo(State.Direction.NONE);
        assertThat(State.FREE.change(State.FREE, false)).isEqualTo(State.Direction.NONE);
        assertThat(State.FREE.change(State.FREE, null)).isEqualTo(State.Direction.NONE);

        assertThat(State.FREE.change(State.ACTIVE, true)).isEqualTo(State.Direction.HIGH);
        assertThat(State.FREE.change(State.ACTIVE, false)).isEqualTo(State.Direction.HIGH);
        assertThat(State.FREE.change(State.ACTIVE, null)).isEqualTo(State.Direction.HIGH);

        assertThat(State.FREE.change(State.BACKUP, true)).isEqualTo(State.Direction.HIGH);
        assertThat(State.FREE.change(State.BACKUP, false)).isEqualTo(State.Direction.NONE);
        assertThat(State.FREE.change(State.BACKUP, null)).isEqualTo(State.Direction.NONE);
    }

    @Test
    void text() {
        Map.of(
            State.ACTIVE, "Active",
            State.BACKUP, "Backup",
            State.FREE, "Free"
        ).forEach((state, text) -> assertThat(state.getText()).isEqualTo(text));
    }

    @Test
    void hide() {
        Map.of(
            State.ACTIVE, false,
            State.BACKUP, true,
            State.FREE, false
        ).forEach((state, hide) -> assertThat(state.isHide()).isEqualTo(hide));
    }
}
