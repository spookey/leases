import axios from 'axios';

const client = axios.create({
  baseURL: '/api',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
  },
  responseEncoding: 'utf8',
  responseType: 'json',
  withCredentials: false,
});

export default {
  getGuides() {
    return client({
      method: 'get',
      url: '/chart-networks.json',
    });
  },

  getChart(uuid) {
    return client({
      method: 'get',
      url: `/chart-${uuid}.json`,
    });
  },
};
