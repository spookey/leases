export default {
  main: {
    title: 'Leases',
  },

  error: {
    notFound: 'Not found!',
    noData: '¯\\_(ツ)_/¯',
  },

  chart: {
    color: 'rgb(62, 142, 208);',
    dtFormat: 'yyyy-MM-dd HH:mm:ss',
    fontFamily: [
      'BlinkMacSystemFont',
      '-apple-system',
      '"Segoe UI"',
      '"Roboto"',
      '"Oxygen"',
      '"Ubuntu"',
      '"Cantarell"',
      '"Fira Sans"',
      '"Droid Sans"',
      '"Helvetica Neue"',
      '"Helvetica"',
      '"Arial"',
      'sans-serif;',
    ].join(', '),
  },
};
