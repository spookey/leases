import { createRouter, createWebHistory } from 'vue-router';
import store from '@/store';

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import(
      /* webpackChunkName: "home" */
      '@/views/HomeView.vue'
    ),
  },
  {
    path: '/:uuid',
    name: 'chart',
    props: true,
    component: () => import(
      /* webpackChunkName: "chart" */
      '@/views/ChartView.vue'
    ),
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'notfound',
    component: () => import(
      /* webpackChunkName: "notfound" */
      '@/views/NotFoundView.vue'
    ),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to) => {
  const { uuid } = to.params;

  const check = () => {
    if (uuid) {
      if (!store.getters.isKnownGuide(uuid)) {
        return { name: 'notfound' };
      }

      if (!store.getters.isKnownSeries(uuid)) {
        return store.dispatch('fetchChart', { uuid })
          .then(() => true);
      }
    }

    return true;
  };

  if (!store.getters.available) {
    return store.dispatch('initialize')
      .then(() => check());
  }
  return check();
});

export default router;
