import { createStore } from 'vuex';
import client from '@/client';

const raiseError = (context, err, hide404) => {
  if (hide404 && err.response && err.response.status === 404) { return; }
  context.commit('setConnectionError', err);
  throw err;
};

export default createStore({
  strict: true,

  state: {
    connectionError: null,
    available: false,

    guides: [],
    charts: {},
  },
  mutations: {
    resetConnectionError(state) { state.connectionError = null; },
    setConnectionError(state, payload) { state.connectionError = payload; },

    resetAvailable(state) { state.available = false; },
    setAvailable(state) { state.available = true; },

    resetGuides(state) { state.guides = []; },
    setGuides(state, payload) { state.guides = payload; },

    resetCharts(state) { state.charts = {}; },
    setChart(state, { uuid, payload }) { state.charts[uuid] = payload; },
  },
  getters: {
    connectionError(state) { return state.connectionError; },
    available(state) { return state.available; },

    allGuides(state) { return state.guides || []; },
    getGuide(_, getters) {
      return (uuid) => getters.allGuides
        .filter((guide) => guide.uuid === uuid)
        .find((guide) => guide !== undefined)
      || [];
    },
    isKnownGuide(_, getters) {
      return (uuid) => getters.allGuides
        .map((guide) => guide.uuid)
        .includes(uuid);
    },

    getSeries(state) {
      return (uuid) => (state.charts || {})[uuid];
    },
    isKnownSeries(_, getters) {
      return (uuid) => getters.getSeries(uuid) !== undefined;
    },
  },
  actions: {
    fetchGuides(context) {
      context.commit('resetConnectionError');
      context.commit('resetAvailable');

      return client.getGuides()
        .then((resp) => context.commit('setGuides', resp.data))
        .then(() => context.commit('setAvailable'))
        .catch((err) => raiseError(context, err));
    },

    fetchChart(context, { uuid }) {
      context.commit('resetConnectionError');
      context.commit('resetAvailable');

      return client.getChart(uuid)
        .then((resp) => context.commit('setChart', { uuid, payload: resp.data }))
        .then(() => context.commit('setAvailable'))
        .catch((err) => raiseError(context, err));
    },

    initialize(context) {
      context.commit('resetConnectionError');
      context.commit('resetAvailable');

      context.commit('resetGuides');
      context.commit('resetCharts');

      return Promise.all([
        context.dispatch('fetchGuides'),
      ])
        .then(() => context.commit('setAvailable'));
    },
  },
});
