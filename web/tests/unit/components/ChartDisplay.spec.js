import { shallowMount } from '@vue/test-utils';

import conf from '@/conf';
import ChartDisplay from '@/components/ChartDisplay.vue';

describe('components/ChartDisplay.vue', () => {
  it('has default series', () => {
    const wrapper = shallowMount(
      ChartDisplay,
    );

    expect(wrapper.vm.series).toStrictEqual([]);
  });

  it('uses passed series', () => {
    const series = [
      { name: 'test-one', data: [] },
      { name: 'test-two', data: [] },
    ];

    const wrapper = shallowMount(
      ChartDisplay,
      { propsData: { series } },
    );

    expect(wrapper.vm.series).toStrictEqual(series);
  });

  it('sets options', () => {
    const { color, dtFormat, fontFamily } = conf.chart;

    const wrapper = shallowMount(
      ChartDisplay,
    );

    const opt = wrapper.vm.options;

    expect(opt).toBeInstanceOf(Object);

    expect(opt.chart.fontFamily).toEqual(fontFamily);
    expect(opt.chart.id).toMatch(/.+/);

    expect(opt.noData.text).toEqual(conf.error.noData);

    expect(opt.theme.monochrome.enabled).toBe(true);
    expect(opt.theme.monochrome.color).toEqual(color);

    expect(opt.tooltip.shared).toBe(false);
    expect(opt.tooltip.x.format).toEqual(dtFormat);

    expect(opt.xaxis.labels.datetimeFormatter.year).toEqual(dtFormat);
    expect(opt.xaxis.labels.datetimeFormatter.month).toEqual(dtFormat);
    expect(opt.xaxis.labels.datetimeFormatter.day).toEqual(dtFormat);
    expect(opt.xaxis.labels.datetimeFormatter.hour).toEqual(dtFormat);

    expect(opt.xaxis.tooltip.enabled).toBe(false);
    expect(opt.xaxis.type).toEqual('datetime');
    expect(opt.yaxis.min).toBe(0);
  });
});
