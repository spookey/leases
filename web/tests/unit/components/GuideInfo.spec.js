import { shallowMount, RouterLinkStub } from '@vue/test-utils';

import GuideInfo from '@/components/GuideInfo.vue';
import conf from '@/conf';
import PageMessage from '@/components/PageMessage.vue';

describe('components/GuideInfo.vue', () => {
  it('handles no content', () => {
    const wrapper = shallowMount(GuideInfo, {
      global: {
        stubs: { PageMessage, RouterLink: RouterLinkStub },
      },
    });

    expect(wrapper.text()).toMatch(conf.error.noData);
  });

  it('displays guide data', () => {
    const uuid = 'u-u-i-d';
    const title = 'Test';
    const description = 'Description';
    const mask = '::1/128';

    const wrapper = shallowMount(GuideInfo, {
      propsData: {
        guide: {
          uuid, title, description, mask,
        },
      },
      global: {
        stubs: { RouterLink: RouterLinkStub },
      },
    });

    const text = wrapper.text();

    expect(text).toMatch(title);
    expect(text).toMatch(description);
    expect(text).toMatch(mask);

    wrapper.findAllComponents(RouterLinkStub)
      .map((stub) => stub.props())
      .map((props) => props.to)
      .filter((to) => {
        expect(to).toEqual({ name: 'chart', params: { uuid } });

        return true;
      });
  });
});
