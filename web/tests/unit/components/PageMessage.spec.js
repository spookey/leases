import { shallowMount } from '@vue/test-utils';

import PageMessage from '@/components/PageMessage.vue';

describe('components/PageMessage.vue', () => {
  it('displays message text', () => {
    const message = 'This is a very serious error message!!1!';

    const wrapper = shallowMount(PageMessage, {
      propsData: { text: message },
    });

    const text = wrapper.text();

    expect(text).toBe(message);
  });
});
