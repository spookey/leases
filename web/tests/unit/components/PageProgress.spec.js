import { shallowMount } from '@vue/test-utils';

import PageProgress from '@/components/PageProgress.vue';

describe('components/PageProgress.vue', () => {
  it('displays progress bar', () => {
    const wrapper = shallowMount(PageProgress, {});

    const progress = wrapper.find('progress');

    expect(progress.exists()).toBe(true);
    expect(progress.text()).toBe('');
  });
});
