import conf from '@/conf';

describe('conf.js', () => {
  it('has main settings', () => {
    expect(conf.main).toBeInstanceOf(Object);

    expect(conf.main.title).toEqual('Leases');
  });

  it('has error settings', () => {
    expect(conf.error).toBeInstanceOf(Object);

    expect(conf.error.notFound).toEqual('Not found!');
    expect(conf.error.noData).toEqual('¯\\_(ツ)_/¯');
  });

  it('has chart settings', () => {
    expect(conf.chart).toBeInstanceOf(Object);

    expect(conf.chart.color).toEqual('rgb(62, 142, 208);');
    expect(conf.chart.dtFormat).toEqual('yyyy-MM-dd HH:mm:ss');
    expect(conf.chart.fontFamily).toMatch(/^.+;$/);
  });
});
