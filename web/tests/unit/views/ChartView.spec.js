import { shallowMount } from '@vue/test-utils';
import { createStore } from 'vuex';

import ChartView from '@/views/ChartView.vue';

describe('views/ChartView.vue', () => {
  const storage = (guide, series) => createStore({
    getters: {
      getGuide: () => () => guide,
      getSeries: () => () => series,
    },
  });

  it('passes stored data', () => {
    const uuid = 'u-u-i-d';
    const guide = { uuid };
    const series = [{ name: uuid, data: [] }];

    const wrapper = shallowMount(ChartView, {
      propsData: { uuid },
      global: {
        plugins: [storage(guide, series)],
      },
    });

    expect(wrapper.text()).toBeFalsy();
    expect(wrapper.vm.guide).toEqual(guide);
    expect(wrapper.vm.series).toEqual(series);
  });
});
