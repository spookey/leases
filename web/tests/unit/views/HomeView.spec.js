import { shallowMount } from '@vue/test-utils';
import { createStore } from 'vuex';

import conf from '@/conf';
import PageMessage from '@/components/PageMessage.vue';
import HomeView from '@/views/HomeView.vue';

describe('views/HomeView.vue', () => {
  const storage = (response) => createStore({
    getters: {
      allGuides: () => response,
    },
  });

  it('handles no content', () => {
    const wrapper = shallowMount(HomeView, {
      global: {
        stubs: { PageMessage },
        plugins: [storage([])],
      },
    });

    expect(wrapper.text()).toMatch(conf.error.noData);
  });

  it('passes stored data', () => {
    const guides = [{ uuid: 'u-u-i-d' }];

    const wrapper = shallowMount(HomeView, {
      global: {
        plugins: [storage(guides)],
      },
    });

    expect(wrapper.text()).toBeFalsy();
    expect(wrapper.vm.guides).toEqual(guides);
  });
});
