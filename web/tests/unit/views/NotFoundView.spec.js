import { shallowMount } from '@vue/test-utils';

import conf from '@/conf';
import NotFoundView from '@/views/NotFoundView.vue';
import PageMessage from '@/components/PageMessage.vue';

describe('views/NotFoundView.vue', () => {
  it('shows error message', () => {
    const wrapper = shallowMount(NotFoundView, {
      global: {
        stubs: { PageMessage },
      },
    });

    expect(wrapper.text()).toMatch(conf.error.notFound);
  });
});
