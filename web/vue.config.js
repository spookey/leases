const { defineConfig } = require('@vue/cli-service');
const glob = require('glob-all');
const path = require('path');
const { PurgeCSSPlugin } = require('purgecss-webpack-plugin');

module.exports = defineConfig({
  transpileDependencies: true,
  productionSourceMap: false,
  publicPath: '/',

  configureWebpack: {
    plugins: [
      new PurgeCSSPlugin({
        paths: glob.sync([
          path.join(__dirname, './public/index.html'),
          path.join(__dirname, './src/**/*.vue'),
          path.join(__dirname, './src/**/*.ts'),
        ]),
      }),
    ],
  },

  devServer: {
    host: 'localhost',
    proxy: {
      '^/api': {
        target: 'http://localhost:9999',
        changeOrigin: true,
      },
    },
  },
});
